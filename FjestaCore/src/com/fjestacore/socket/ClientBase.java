/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.socket;

import com.fjestacore.core.crypt.INetCrypt;
import com.fjestacore.core.net.SocketType;
import com.fjestacore.core.stream.SocketInputStream;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDump;
import com.fjestacore.packet.PacketReader;
import com.fjestacore.packet.PacketUnReady;
import com.fjestacore.packet.PacketWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ClientBase
 * @author FantaBlueMystery
 */
abstract public class ClientBase implements Runnable {

	/**
	 * _log
	 */
	protected Logger _log = null;

	/**
	 * Socket
	 */
	protected Socket _socket = null;

	/**
	 * running thread
	 */
	protected Thread _runningThread = null;

	/**
	 * crypt
	 */
	protected INetCrypt _crypt = null;

	/**
	 * debug modus
	 */
	protected boolean _debug = false;

	/**
	 * getLogger
	 * @return
	 */
	public Logger getLogger() {
		return this._log;
	}

	/**
	 * setDebugging
	 * @param enable
	 */
	public void setDebugging(boolean enable) {
		this._debug = enable;
	}

	/**
	 * isConnected
	 * @return
	 */
	public Boolean isConnected() {
		if( this._socket != null ) {
			return this._socket.isConnected();
		}

		return false;
	}

	/**
	 * close
	 */
	public void close() {
		if( this._socket != null ) {
			try {
				this._socket.close();
			}
			catch( IOException ex ) {
				this._log.log(Level.SEVERE, "close", ex);
			}
		}
	}

	/**
	 * getInetAddress
	 * @return
	 */
	public InetAddress getInetAddress() {
		return this._socket.getInetAddress();
	}

	/**
	 * _onPacket
	 * overwrite
	 * @param pac
	 */
	abstract protected void _onPacket(Packet pac);

	/**
	 * _onPacketThread
	 * @param pac
	 */
	protected void _onPacketThread(Packet pac) {
		ClientBase csb = this;

		String threadName = "FjestaCore - PacketRead-" + Integer.toString(pac.getCommand());

		Thread thread = new Thread(threadName) {

			@Override
			public void run(){
				if( !pac.isBufferRead() ) {
					pac.onReadBuffer();
				}

				csb._onPacket(pac);
			}
		};

		thread.start();
	}

	/**
	 * _onClose
	 */
	protected void _onClose() {}

	/**
	 * _readPacket
	 * @param is
	 * @return
	 * @throws java.lang.InterruptedException
	 */
	protected Packet _readPacket(InputStream is) throws InterruptedException {
		Packet pac = PacketReader.readPacketN(is, SocketType.SERVER);

		if( pac != null ) {
			pac.setType(SocketType.SERVER);
		}

		return pac;
	}

	/**
	 * _readUnreadyPacket
	 * @param upac
	 * @param is
	 * @return
	 */
	protected Packet _readUnreadyPacket(PacketUnReady upac, InputStream is) {
		Packet pac = PacketReader.readUnreadyPacket(upac, is, null, SocketType.SERVER);

		return pac;
	}

	/**
	 * _getClassName
	 * @return
	 */
	protected String _getClassName() {
		return this.getClass().getName().replace("com.fjestacore.socket.ClientBase", "");
	}

	/**
	 * _onSocketLoop
	 */
	protected void _onSocketLoop() {}

	/**
	 * run
	 */
	@Override
	public void run() {
		synchronized(this){
            this._runningThread = Thread.currentThread();
        }

		String className = this._getClassName();

		try( SocketInputStream is = new SocketInputStream(this._socket) ) {
			while( !this._socket.isClosed() ) {
				Thread.sleep(0, 100);

				this._onSocketLoop();

				while( is.available() > 0 ) {
					Packet pac = this._readPacket(is);

					if( pac != null ) {
						while( pac instanceof PacketUnReady ) {
							pac = this._readUnreadyPacket((PacketUnReady) pac, is);
						}

						if( this._debug ) {
							switch( pac.getCommand() ) {
								default:
									PacketDump pd = (PacketDump)pac.cast(PacketDump.class);

									this._log.log(Level.INFO, pd.dumpStr(className + "-Read"));
							}
						}

						this._onPacketThread(pac);
					}
					else {
						this._log.log(Level.INFO, "{0}: Packet = NULL", className);

						if( this._socket.isClosed() ) {
							break;
						}
					}
				}
			}
		}
		catch( IOException | InterruptedException ex ) {
			this._log.log(Level.SEVERE, "run", ex);
		}

		this._log.info("Socket closed or terminate");

		this._onClose();
	}

	/**
	 * writeRaw
	 * @param b
	 * @return
	 */
	protected boolean _writeRaw(byte[] b) {
		try {
			OutputStream out = this._socket.getOutputStream();

			if( this._debug ) {
				this._log.log(Level.INFO, "Write-Size: {0}", Integer.toString(b.length));
			}

			out.write(b);
			out.flush();

			return true;
		}
		catch( SocketException ex ) {
			if( this._debug ) {
				this._log.log(Level.SEVERE, "_writeRaw", ex);
			}

			try {
				this._socket.close();
			}
			catch( IOException ex1 ) {
				this._log.log(Level.SEVERE, null, ex1);
			}
		}
		catch ( IOException ex) {
			Logger.getLogger(ClientBase.class.getName()).log(Level.SEVERE, null, ex);
		}

		this._socket = null;

		return false;
	}

	/**
	 * _writePacket
	 * @param packet
	 * @param crypt
	 * @return
	 */
	protected boolean _writePacket(Packet packet, INetCrypt crypt) {
		try {
			packet.setType(SocketType.CLIENT);

			byte[] buffer = PacketWriter.writePacket(packet, crypt);

			if( this._debug ) {
				switch( packet.getCommand() ) {
					default:
						this._log.info( ((PacketDump)packet).dumpStr(this._getClassName() + "-Write"));
				}
			}

			if( buffer != null ) {
				return this._writeRaw(buffer);
			}
			else {
				return false;
			}
		}
		catch( Exception ex ) {
			this._log.log(Level.SEVERE, "_writePacket", ex);
		}

		return false;
	}

	/**
	 * _writePacket
	 * @param pac
	 * @return
	 */
	protected boolean _writePacket(Packet pac) {
		return this._writePacket(pac, this._crypt);
	}
}