/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.socket.config;

import java.util.Properties;

/**
 * SocketConfig
 * @author FantaBlueMystery
 */
public class SocketConfig extends Properties {

	/**
	 * Properties key
	 */
	static public String DEBUG				= "debug";
	static public String PORT				= "port";
	static public String BINDING_ADDRESS		= "binding_address";

	/**
	 * getPort
	 * @return
	 */
	public int getPort() {
		return Integer.valueOf(this.getProperty(SocketConfig.PORT, "0"));
	}

	/**
	 * getBindingAddress
	 * @return
	 */
	public String getBindingAddress() {
		return this.getProperty(SocketConfig.BINDING_ADDRESS, "");
	}

	/**
	 * isDebug
	 * @return
	 */
	public boolean isDebug() {
		String isDebug = this.getProperty(SocketConfig.DEBUG, "0");

		return (isDebug.compareTo("1") == 0);
	}
}