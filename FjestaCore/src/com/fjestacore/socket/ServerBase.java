/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.socket;

import com.fjestacore.core.net.ServerType;
import com.fjestacore.socket.config.SocketConfig;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ServerBase
 * @author FantaBlueMystery
 */
abstract public class ServerBase implements Runnable {

	/**
	 * server properties
	 */
	protected SocketConfig _properties = null;

	/**
	 * socket
	 */
	protected ServerSocket _server;

	/**
	 * debug modus
	 */
	protected boolean _debug = false;

	/**
	 * server type
	 */
	protected ServerType _serverType = ServerType.NONE;

	/**
	 * clients
	 */
	protected List<ClientBase> _clients = new ArrayList<>();

	/**
	 * ServerBase
	 */
	public ServerBase() {
		this._properties = new SocketConfig();
	}

	/**
	 * ServerBase
	 * @param prop
	 */
	public ServerBase(SocketConfig prop) {
		this._properties = prop;
	}

	/**
	 * getProperties
	 * @return
	 */
	public SocketConfig getProperties() {
		return this._properties;
	}

	/**
	 * getServerType
	 * @return
	 */
	public ServerType getServerType() {
		return this._serverType;
	}

	/**
	 * setDebugging
	 * @param enable
	 */
	public void setDebugging(Boolean enable) {
		this._debug = enable;
	}

	/**
	 * start
	 * @throws IOException
	 */
	public void start() throws IOException {
		if( this._server == null ) {
			this._debug				= this._properties.isDebug();
			int port				= this._properties.getPort();
			String bindingAddress	= this._properties.getBindingAddress();

			if( !bindingAddress.isEmpty() ) {
				this._server = new ServerSocket();

				this._server.bind(new InetSocketAddress(
					InetAddress.getByName(bindingAddress),
					port
					));
			}
			else {
				this._server = new ServerSocket(port);
			}

			new Thread(this, "FjestaCore - ServerBase" +
				this._serverType.name()).start();
		}
	}

		/**
	 * stop
	 * @throws IOException
	 */
	public void stop() throws IOException {
		this._server.close();
		this._server = null;
	}

	/**
	 * castSocket
	 * @param cs
	 * @return
	 */
	abstract protected ClientBase _castSocket(Socket cs);

	/**
	 * run
	 */
	@Override
	public void run() {
		try {
			if( this._server != null ) {
				Logger.getLogger(ServerBase.class.getName()).log(Level.INFO,
					"Server {0} listen IP: {1} Port: {2}",
					new Object[]{
						this.getClass().getName(),
						this._server.getInetAddress().toString(),
						Integer.toString(this._properties.getPort())
					});
			}

			while( this._server != null ) {
				Thread.sleep(0, 100);

				try {
					Socket cs = this._server.accept();

					if( cs != null ) {
						ClientBase scs = this._castSocket(cs);
						scs.setDebugging(this._debug);

						this._clients.add(scs);

						new Thread(scs, "FjestaCore - ClientBase " +
							this._serverType.name() + "-" +
							scs.getInetAddress().toString()).start();
					}
				}
				catch( Exception ex ) {
					Logger.getLogger(ServerBase.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		catch( InterruptedException ex ) {
			Logger.getLogger(ServerBase.class.getName()).log(Level.SEVERE, null, ex);
		}
		finally {
			for( ClientBase tclient: this._clients ) {
				tclient.close();
			}

			this._clients.clear();
		}
	}

	/**
	 * removeClient
	 * @param client
	 * @return
	 */
	public boolean removeClient(ClientBase client) {
		if( this._clients.contains(client) ) {
			client.close();

			return this._clients.remove(client);
		}

		return false;
	}
}