/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.server;

/**
 * ServerError
 * @author FantaBlueMystery
 */
public enum ServerError {
	NONE(0),						// NONE
	UNKNOWN_ERROR(66),				// An unknown error has occurred
    DB_ERROR_1(67),					// DB error
    AUTHENTICATION_FAILED(68),		// Authentication failed.
	CHECK_ID_OR_PASS(69),			// Please check ID or Password.
	DB_ERROR_2(70),					// DB error
	ID_BLOCKED(71),					// The ID has been blocked.
	WORLD_IS_MAINTENANCE(72),		// The World Servers are down for maintenance.
	AUTH_TIMED_OUT(73),				// Authentication timed out. Please try to log in again.
	LOGIN_FAILED(74),				// Login failed
	AGREEMENT_MISSING(75),			// Please accept the agreement to continue.
	WRONG_REGION(81),				// You are not in our service area.

	FAILED_TO_CREATE(130),
	WRONG_CLASS(131),
	NAME_TAKEN(132),
	ERROR_IN_MAX_SLOT(133),
	NAME_IN_USE(385),

	FAILED_WORLDSERVER(321),
	FAILED_ZONESERVER(322),
	NOCHAR_INSLOT(324),
	CLIENT_MANIPULATED(327),
	ERROR_CLIENTDATA(328),
	ERROR_CHAR_INFO(1410),
	ERROR_APPEARANCE(1411),
	ERROR_OPTIONS(1412),
	ERROR_STATUS(1413),
	ERROR_SKILL(1414),
	ERROR_QUEST(1415),
	ERROR_HOUSE(1416),
	ERROR_FRIEND_INFO(1417),
	ERROR_MONARCHY(1418),
	ERROR_GUILD(1419),
	ERROR_EMBLEM(1420),
	ERROR_MOVER(1421),
	ERROR_ARENA(1422),
	ERROR_MARINE(1423),
	ID_NOMATCH(1424),
	ERROR_ITEM(1425),
	ERROR_TREASURE(1426),
	ERROR_TITLE(1427),
	ERROR_KQ(1428),
	ERROR_PREM(1429),
	MAP_UNDER_MAINT(1430)
	;

	private int _value;

	/**
	 * ServerError
	 * @param value
	 */
	private ServerError(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getMsg
	 * @param value
	 * @return
	 */
	static public String getMsg(ServerError value) {
		switch( value ) {
			case UNKNOWN_ERROR:
				return "unknown error";

			case DB_ERROR_1:
				return "database error";

			case AUTHENTICATION_FAILED:
				return "authentication failed";

			case CHECK_ID_OR_PASS:
				return "please check id or password";

			case DB_ERROR_2:
				return "database error";

			case ID_BLOCKED:
				return "The ID has been blocked";

			case WORLD_IS_MAINTENANCE:
				return "The World Servers are down for maintenance";

			case AUTH_TIMED_OUT:
				return "Authentication timed out. Please try to log in again";

			case LOGIN_FAILED:
				return "Login failed";

			case AGREEMENT_MISSING:
				return "Please accept the agreement to continue";

			case WRONG_REGION:
				return "You are not in our service area";

			case FAILED_WORLDSERVER:
				return "Connection to the game server failed";

			case FAILED_ZONESERVER:
				return "Connection to the map server failed.";

			case NOCHAR_INSLOT:
				return "No character in the slot.";
		}

		return "errorcode: " + value.toString() + " (" + Integer.toString(value.getValue()) + ")";
	}

	/**
	 * intToError
	 * @param value
	 * @return
	 */
	static public ServerError intToError(int value) {
		ServerError[] errors = ServerError.values();

		for( ServerError error : errors ) {
			if (error.getValue() == value) {
				return error;
			}
		}

		return ServerError.UNKNOWN_ERROR;
	}
}