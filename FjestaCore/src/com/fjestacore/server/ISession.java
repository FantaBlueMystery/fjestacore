/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.server;

import com.fjestacore.core.handle.IWorldHandle;

/**
 * ISession
 * @author FantaBlueMystery
 */
public interface ISession extends IWorldHandle {

	/**
	 * getIdBytes
	 * @return
	 */
	public byte[] getIdBytes();
}