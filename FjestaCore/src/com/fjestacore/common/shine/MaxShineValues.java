/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.shine;

/**
 * MaxShineValues
 * @author FantaBlueMystery
 */
public enum MaxShineValues {
	MAX_SHINE_MAP_NAME(0xC),
	MAX_SHINE_PASSWORD(0x10),
	MAX_SHINE_WORLD_NAME(0x10),
	MAX_SHINE_IP_ADDRESS(0x10),
	MAX_SHINE_DATA_INDEX_STRING(0x20),
	MAX_SHINE_USER_ID(0x12),
	MAX_SHINE_USER_PW(0x10),
	MAX_SHINE_CHAR_ID(0x14),
	MAX_SHINE_PET_NAME(0x10)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * MaxShineValues
	 * @param value
	 */
	private MaxShineValues(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}