/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.shine;

/**
 * ShinePutOnBelongedItem
 * @author FantaBlueMystery
 */
public enum ShinePutOnBelongedItem {
	SPOBI_NOT_BELONGED(0x0),
	SPOBI_BELONGED(0x1)
	;

	private int _value;

	/**
	 * ShinePutOnBelongedItem
	 * @param value
	 */
	private ShinePutOnBelongedItem(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToSPOBI
	 * @param value
	 * @return
	 */
	static public ShinePutOnBelongedItem intToSPOBI(int value) {
		ShinePutOnBelongedItem[] list = ShinePutOnBelongedItem.values();

		for( ShinePutOnBelongedItem spobi: list ) {
			if( spobi.getValue() == value ) {
				return spobi;
			}
		}

		return ShinePutOnBelongedItem.SPOBI_NOT_BELONGED;
	}
}