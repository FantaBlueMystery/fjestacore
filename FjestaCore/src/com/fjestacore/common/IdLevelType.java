/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common;

/**
 * IdLevelType
 * @author FantaBlueMystery
 */
public enum IdLevelType {
	ILT_EASY(0),
	ILT_NORMAL(1),
	ILT_HARD(2),
	ILT_MAX(3);

	private int _value;

	/**
	 * IdLevelType
	 * @param value
	 */
	private IdLevelType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToType
	 * @param value
	 * @return
	 */
	static public IdLevelType intToType(int value) {
		IdLevelType[] ilts = IdLevelType.values();

		for( IdLevelType ilt: ilts ) {
			if( ilt.getValue() == value ) {
				return ilt;
			}
		}

		return IdLevelType.ILT_EASY;
	}
}