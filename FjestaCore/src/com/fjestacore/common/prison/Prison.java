/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.prison;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Prison
 * @author FantaBlueMystery
 */
public class Prison extends AbstractStruct {

	/**
	 * error code
	 */
	protected int _errorCode = 3505; // ? TODO

	/**
	 * minute
	 */
	protected int _minute = 0;

	/**
	 * reason
	 */
	protected String _reason = "";

	/**
	 * remark
	 */
	protected String _remark = "";

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._errorCode = reader.readShort();
		this._minute = reader.readShort();

		if( this._minute > 0 ) {
			this._reason = reader.readString(16);
			this._remark = reader.readString(64);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((short) this._errorCode);
		writer.writeShort((short) this._minute);

		if( this._minute > 0 ) {
			writer.writeString(this._reason, 16);
			writer.writeString(this._remark, 64);
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 4;

		if( this._minute > 0 ) {
			size += 16 + 64;
		}

		return size;
	}
}