/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.misc;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import com.fjestacore.server.ServerError;
import java.io.IOException;

/**
 * ItemshopUrlAck
 * @author FantaBlueMystery
 */
public class ItemshopUrlAck extends AbstractStruct implements IAbstractStructDump {

	/**
	 * error code
	 */
	protected int _nError = 0;

	/**
	 * shop url
	 */
	protected String _sURL = "";

	/**
	 * ItemshopUrlAck
	 */
	public ItemshopUrlAck() {}

	/**
	 * ItemshopUrlAck
	 * @param nError
	 */
	public ItemshopUrlAck(ServerError nError) {
		this._nError = nError.getValue();
	}

	/**
	 * ItemshopUrlAck
	 * @param nError
	 * @param sUrl
	 */
	public ItemshopUrlAck(int nError, String sUrl) {
		this._nError = nError;
		this._sURL = sUrl;
	}

	/**
	 * ItemshopUrlAck
	 * @param nError
	 * @param sUrl
	 */
	public ItemshopUrlAck(ServerError nError, String sUrl) {
		this._nError = nError.getValue();
		this._sURL = sUrl;
	}



	/**
	 * getError
	 * @return
	 */
	public int getError() {
		return this._nError;
	}

	/**
	 * setError
	 * @param error
	 */
	public void setError(int error) {
		this._nError = error;
	}

	/**
	 * getSUrl
	 * @return
	 */
	public String getSUrl() {
		return this._sURL;
	}

	/**
	 * setSUrl
	 * @param surl
	 */
	public void setSUrl(String surl) {
		this._sURL = surl;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + this._sURL.length();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nError = reader.readShort();

		int size = reader.readShort();

		if( size > 0 ) {
			this._sURL = reader.readString(size).trim();
		}
	}

	/**
	 *
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nError);
		writer.writeShort(this._sURL.length());

		if( this._sURL.length() > 0 ) {
			writer.writeString(this._sURL, this._sURL.length());
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ItemshopUrlAck: [" +
			"nError: " + Integer.toString(this._nError) +
			" sUrl: " + this._sURL +
			"]"
			);

		return dump;
	}
}