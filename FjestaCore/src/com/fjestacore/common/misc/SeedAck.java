/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.misc;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * SeedAck
 * @author FantaBlueMystery
 */
public class SeedAck extends AbstractStruct implements IAbstractStructDump {

	/**
	 * offset
	 */
	protected int _offset = 0;

	/**
	 * SeedAck
	 */
	public SeedAck() {}

	/**
	 * SeedAck
	 * @param offset
	 */
	public SeedAck(int offset) {
		this._offset = offset;
	}

	/**
	 * getOffset
	 * @return
	 */
	public int getOffset() {
		return this._offset;
	}

	/**
	 * setOffset
	 * @param offset
	 */
	public void setOffset(short offset) {
		this._offset = offset;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._offset = (short) reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._offset);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"SeedAck: [" +
			"Offset: " + Integer.toString(this._offset) +
			"]"
			);

		return dump;
	}
}