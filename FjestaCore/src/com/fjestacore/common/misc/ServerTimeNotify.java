/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.misc;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.TmTime;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * ServerTimeNotify
 * @author FantaBlueMystery
 */
public class ServerTimeNotify extends AbstractStruct {

	/**
	 * tm time
	 */
	protected TmTime _time = new TmTime();

	/**
	 * time zone
	 */
	protected int _timeZone = 0;

	/**
	 * getTime
	 * @return
	 */
	public TmTime getTime() {
		return this._time;
	}

	/**
	 * getTimeZone
	 * @return
	 */
	public int getTimeZone() {
		return this._timeZone;
	}

	/**
	 * getDate
	 * @return
	 */
	public Date getDate() {
		Calendar cal = this._time.getCalendar();
		return cal.getTime();
	}

	/**
	 * getTimestamp
	 * @return
	 */
	public Timestamp getTimestamp() {
		return new Timestamp(this.getDate().getTime());
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 37;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._time.read(reader);
		this._timeZone = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._time.write(writer);
		writer.writeByteInt(this._timeZone);
	}
}