/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.misc;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * GameTime
 * @author FantaBlueMystery
 */
public class GameTime extends AbstractStruct implements IAbstractStructDump {

	/**
	 * hour
	 */
	protected int _hour = 0;

	/**
	 * minute
	 */
	protected int _minute = 0;

	/**
	 * second
	 */
	protected int _second = 0;

	/**
	 * GameTime
	 */
	public GameTime() {
		LocalDateTime now = LocalDateTime.now();

		this._hour		= now.getHour();
		this._minute	= now.getMinute();
		this._second	= now.getSecond();
	}

	/**
	 * getHour
	 * @return
	 */
	public int getHour() {
		return this._hour;
	}

	/**
	 * getMinute
	 * @return
	 */
	public int getMinute() {
		return this._minute;
	}

	/**
	 * getSecound
	 * @return
	 */
	public int getSecound() {
		return this._second;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._hour		= reader.readByteInt();
		this._minute	= reader.readByteInt();
		this._second	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._hour);
		writer.writeByteInt(this._minute);
		writer.writeByteInt(this._second);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"GameTime: [" +
			"Hour: " + Integer.toString(this._hour) +
			" Minute: " + Integer.toString(this._minute) +
			" Second: " + Integer.toString(this._second) +
			"]"
			);

		return dump;
	}
}