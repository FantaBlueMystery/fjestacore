/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.card;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CardInform
 * @author FantaBlueMystery
 */
public class CardInform extends AbstractStruct {

	/**
	 * SIZE
	 */
	static public int SIZE = 11;

	/**
	 * regist time
	 */
	protected int _registTime = 0;

	/**
	 * serial number
	 */
	protected long _serialNumber = 0;

	/**
	 * card id
	 */
	protected int _cardID = 0;

	/**
	 * start number
	 */
	protected int _starNumber = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CardInform.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._registTime	= reader.readInt();
		this._serialNumber	= reader.readUInt();
		this._cardID		= reader.readShort();
		this._starNumber	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeInt(this._registTime);
		writer.writeUInt(this._serialNumber);
		writer.writeShort(this._cardID);
		writer.writeByteInt(this._starNumber);
	}
}