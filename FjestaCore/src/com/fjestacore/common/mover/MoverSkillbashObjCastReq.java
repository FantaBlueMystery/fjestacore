/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.mover;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * MoverSkillbashObjCastReq
 * @author FantaBlueMystery
 */
public class MoverSkillbashObjCastReq extends AbstractStruct {

	/**
	 * n skill id
	 */
	protected int _nSkillID = 0;

	/**
	 * n target
	 */
	protected int _nTarget = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nSkillID = reader.readShort();
		this._nTarget = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nSkillID);
		writer.writeShort(this._nTarget);
	}
}