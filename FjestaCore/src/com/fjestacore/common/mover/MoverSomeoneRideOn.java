/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.mover;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * MoverSomeoneRideOn
 * @author FantaBlueMystery
 */
public class MoverSomeoneRideOn extends AbstractStruct {

	/**
	 * handle
	 */
	protected int _nHandle = 0;

	/**
	 * mover handle
	 */
	protected int _nMoverHandle = 0;

	/**
	 * slot
	 */
	protected int _nSlot = 0;

	/**
	 * grade
	 */
	protected int _nGrade = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nHandle		= reader.readShort();
		this._nMoverHandle	= reader.readShort();
		this._nSlot			= reader.readByteInt();
		this._nGrade		= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nHandle);
		writer.writeShort(this._nMoverHandle);
		writer.writeByteInt(this._nSlot);
		writer.writeByteInt(this._nGrade);
	}
}