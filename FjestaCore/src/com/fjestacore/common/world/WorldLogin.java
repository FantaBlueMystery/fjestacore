/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

import com.fjestacore.common.name.Name18Byte;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.Arrays;

/**
 * WorldLogin
 * @author FantaBlueMystery
 */
public class WorldLogin extends AbstractStruct implements IAbstractStructDump {

	/**
	 * username
	 */
	protected Name18Byte _username = new Name18Byte();

	/**
	 * session
	 */
	protected byte[] _session = null;

	/**
	 * WorldLogin
	 */
	public WorldLogin() {
		this._session = new byte[64];
	}

	/**
	 * WorldLogin
	 * @param username
	 * @param session
	 */
	public WorldLogin(String username, byte[] session) {
		this._username.setContent(username);
		this._session = Arrays.copyOf(session, 64);
	}

	/**
	 * getUsername
	 * @return
	 */
	public String getUsername() {
		return this._username.getContent();
	}

	/**
	 * setUsername
	 * @param username
	 */
	public void setUsername(String username) {
		this._username.setContent(username);
	}

	/**
	 * getSession
	 * @return
	 */
	public byte[] getSession() {
		return this._session;
	}

	/**
	 * setSession
	 * @param session
	 */
	public void setSession(byte[] session) {
		this._session = Arrays.copyOf(session, 64);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 82;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._username.read(reader);
		this._session = reader.readBuffer(64);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._username.write(writer);
		writer.writeBuffer(this._session);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"WorldLogin: [" +
			" Username: " + this._username.getContent() +
			" Session: " + HexString.byteArrayToHexString(this._session) +
			"]"
			);

		return dump;
	}
}
