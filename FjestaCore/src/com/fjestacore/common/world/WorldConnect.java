/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import com.fjestacore.server.ISession;
import java.io.IOException;
import java.util.Arrays;

/**
 * FantaBlueMystery
 * @author WorldConnect
 */
public class WorldConnect extends AbstractStruct implements IAbstractStructDump {

	/**
	 * world
	 */
	protected IWorld _world = null;

	/**
	 * session
	 */
	protected byte[] _session = null;

	/**
	 * WorldConnect
	 */
	public WorldConnect() {
		this._world = new World();
		this._session = new byte[64];
	}

	/**
	 * WorldConnect
	 * @param world
	 * @param session
	 */
	public WorldConnect(IWorld world, byte[] session) {
		this._world = world;
		this._session = Arrays.copyOf(session, 64);
	}

	/**
	 * WorldConnect
	 * @param world
	 * @param session
	 */
	public WorldConnect(IWorld world, ISession session) {
		this._world = world;
		this._session = Arrays.copyOf(session.getIdBytes(), 64);
	}

	/**
	 * getWorld
	 * @return
	 */
	public IWorld getWorld() {
		return this._world;
	}

	/**
	 * setWorld
	 * @param world
	 */
	public void setWorld(IWorld world) {
		this._world = world;
	}

	/**
	 * getSession
	 * @return
	 */
	public byte[] getSession() {
		return this._session;
	}

	/**
	 * setSession
	 * @param session
	 */
	public void setSession(byte[] session) {
		this._session = Arrays.copyOf(session, 64);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 83;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._world.setStatus(reader.readByteInt());
		this._world.setIp(reader.readString(16).trim());
		this._world.setPort(reader.readShort());

		this._session = reader.readBuffer(64);
	}

	/**
	 *
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._world.getStatus().getValue());
		writer.writeString(this._world.getIp(), 16);
		writer.writeShort(this._world.getPort());
		writer.writeBuffer(this._session);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"WorldConnect: [" +
			" Status: " + WorldStatus.getStr(this._world.getStatus()) +
			" IP: " + this._world.getIp() +
			" Port: " + Integer.toString(this._world.getPort()) +
			" Session: '" + HexString.byteArrayToHexString(this._session) + "'" +
			"]"
			);

		return dump;
	}
}