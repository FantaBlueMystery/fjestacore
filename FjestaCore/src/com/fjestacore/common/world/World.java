/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

import com.fjestacore.common.name.Name4;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * World
 * Base object for login world
 * @author FantaBlueMystery
 */
public class World extends AbstractStruct implements IAbstractStructDump, IWorld {

	/**
	 * size
	 */
	static public int SIZE = 18;

	/**
	 * id
	 */
	protected int _id = 0;

	/**
	 * name
	 */
	protected Name4 _name = new Name4();

	/**
	 * status
	 */
	protected int _status = 0;

	/**
	 * ip
	 */
	protected String _ip = "";

	/**
	 * port
	 */
	protected int _port = 0;

	/**
	 * World
	 */
	public World() {}

	/**
	 * World
	 * @param id
	 * @param name
	 * @param status
	 */
	public World(int id, String name, int status) {
		this._id = id;
		this._name.setContent(name);
		this._status = status;
	}

	/**
	 * World
	 * @param id
	 * @param name
	 * @param status
	 * @param ip
	 * @param port
	 */
	public World(int id, String name, int status, String ip, int port) {
		this._id		= id;
		this._name.setContent(name);
		this._status	= status;
		this._ip		= ip;
		this._port		= port;
	}

	/**
	 * World
	 * @param id
	 * @param name
	 * @param status
	 * @param ip
	 * @param port
	 */
	public World(int id, WorldName name, WorldStatus status, String ip, int port) {
		this._id		= id;
		this._name.setContent(name.getValue());
		this._status	= status.getValue();
		this._ip		= ip;
		this._port		= port;
	}

	/**
	 * getId
	 * @return
	 */
	@Override
	public int getId() {
		return this._id;
	}

	/**
	 * getName
	 * @return
	 */
	@Override
	public String getName() {
		return this._name.getContent();
	}

	/**
	 * getStatus
	 * @return
	 */
	@Override
	public WorldStatus getStatus() {
		return WorldStatus.getWorldStatus(this._status);
	}

	/**
	 * setStatus
	 * @param status
	 */
	@Override
	public void setStatus(WorldStatus status) {
		this._status = status.getValue();
	}

	/**
	 * setStatus
	 * @param status
	 */
	@Override
	public void setStatus(int status) {
		this._status = status;
	}

	/**
	 * getIp
	 * @return
	 */
	@Override
	public String getIp() {
		return this._ip;
	}

	/**
	 * setIp
	 * @param ip
	 */
	@Override
	public void setIp(String ip) {
		this._ip = ip;
	}

	/**
	 * getPort
	 * @return
	 */
	@Override
	public int getPort() {
		return this._port;
	}

	/**
	 * setPort
	 * @param port
	 */
	@Override
	public void setPort(int port) {
		this._port = port;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return World.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._id = reader.readByteInt();
		this._name.read(reader);
		this._status = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._id);
		this._name.write(writer);
		writer.writeByteInt(this._status);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"World: [" +
			"id: " + Integer.toString(this._id) +
			" Name: " + this._name.getContent() +
			" Status: " + WorldStatus.getStr(WorldStatus.getWorldStatus(this._status)) +
			"]"
			);

		return dump;
	}
}