/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.ArrayList;

/**
 * WorldList
 * @author FantaBlueMystery
 */
public class WorldList extends AbstractStruct implements IAbstractStructDump, IWorldList {

	/**
	 * list
	 */
	protected ArrayList<IWorld> _list = new ArrayList<>();

	/**
	 * add
	 * @param aworld
	 */
	public void add(IWorld aworld) {
		this._list.add(aworld);
	}

	/**
	 * getList
	 * @return
	 */
	@Override
	public ArrayList<IWorld> getList() {
		return this._list;
	}

	/**
	 * getByName
	 * @param name
	 * @return
	 */
	@Override
	public IWorld getByName(WorldName name) {
		for( IWorld aWorld: this._list ) {
			if( aWorld.getName().compareTo(name.getValue()) == 0 ) {
				return aWorld;
			}
		}

		return null;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int count = reader.readByteInt();

		for( int i=0; i<count; i++ ) {
			World tworld = new World();
			tworld.read(reader);

			this._list.add(tworld);
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1 + (this._list.size() * World.SIZE);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._list.size());

		for( IWorld tworld: this._list ) {
			((World)tworld).write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"WorldList: [count: " + Integer.toString(this._list.size()) + "]"
			);

		for( IWorld aWorld: this._list ) {
			World tworld = (World) aWorld;

			dump += tworld.dump(deep+1);
		}

		return dump;
	}

	/**
	 * count
	 * @return
	 */
	@Override
	public int count() {
		return this._list.size();
	}
}