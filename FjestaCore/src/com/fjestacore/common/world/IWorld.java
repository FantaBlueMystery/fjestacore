/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

/**
 * IWorld
 * @author FantaBlueMystery
 */
public interface IWorld {

	/**
	 * getId
	 * @return
	 */
	public int getId();

	/**
	 * getName
	 * @return
	 */
	public String getName();

	/**
	 * getStatus
	 * @return
	 */
	public WorldStatus getStatus();

	/**
	 * setStatus
	 * @param status
	 */
	public void setStatus(WorldStatus status);

	/**
	 * setStatus
	 * @param status
	 */
	public void setStatus(int status);

	/**
	 * getIp
	 * @return
	 */
	public String getIp();

	/**
	 * setIp
	 * @param ip
	 */
	public void setIp(String ip);

	/**
	 * getPort
	 * @return
	 */
	public int getPort();

	/**
	 * setPort
	 * @param port
	 */
	public void setPort(int port);
}