/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

import java.util.ArrayList;

/**
 * IWorldList
 * @author FantaBlueMystery
 */
public interface IWorldList {

	/**
	 * getList
	 * @return
	 */
	public ArrayList<IWorld> getList();

	/**
	 * count
	 * @return return count of worlds
	 */
	public int count();

	/**
	 * getByName
	 * @param name
	 * @return
	 */
	public IWorld getByName(WorldName name);
}