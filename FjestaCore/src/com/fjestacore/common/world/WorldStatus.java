/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

/**
 * WorldStatus
 * @author FantaBlueMystery
 */
public enum WorldStatus {
	OFFLINE(0),
	MAINTENANCE(1),
	EMPTYSERVER(2),
	RESERVED(3),
	OFFLINEUNKERROR(4),
	FULL(5),
	LOW(6),
	MEDIUM(9),
	HIGH(10);

	private int _value;

	/**
	 * WorldStatus
	 * @param value
	 */
	private WorldStatus(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getStr
	 * @param status
	 * @return
	 */
	static public String getStr(WorldStatus status) {
		switch( status ) {
			case OFFLINE:
				return "Offline";

			case MAINTENANCE:
				return "Maintenace";

			case EMPTYSERVER:
				return "Empty server";

			case RESERVED:
				return "reserved";

			case OFFLINEUNKERROR:
				return "Offline unknow error";

			case FULL:
				return "Full";

			case LOW:
				return "Low";

			case MEDIUM:
				return "Medium";

			case HIGH:
				return "High";
		}

		return null;
	}

	/**
	 * getWorldStatus
	 * @param status
	 * @return
	 */
	static public WorldStatus getWorldStatus(int status) {
		WorldStatus[] states = WorldStatus.values();

		for( WorldStatus wstatus: states ) {
			if( wstatus.getValue() == status ) {
				return wstatus;
			}
		}

		return WorldStatus.OFFLINE;
	}
}