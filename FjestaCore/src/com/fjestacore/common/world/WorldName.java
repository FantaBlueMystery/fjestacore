/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

/**
 * WorldName
 * @author FantaBlueMystery
 */
public enum WorldName {
	// DE
	TEVA_DE("GFT-GRM0"),
	EPITH_DE("GFT-GRM1"),
	APOLINE_DE("GFT-GRM2"),
	NATURE_DE("GFT-GRM3"),
	ELGA_DE("GFT-GRM4"),
	JENIRA_DE("GFT-GRM5"),
	MARKIS_DE("GFT-GRM6"),
	IYZEL_DE("GFT-GRM7"),

	// EN
	TEVA_EN("GFT-ENG0"),

	// FR
	TEVA_FR("GFT-FRA0"),
	EPITH_FR("GFT-FRA1"),

	// SP
	TEVA_SP("GFT-SPA0")
	;

	private String _value;

	/**
	 * WorldStatus
	 * @param value
	 */
	private WorldName(String value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public String getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(String value) {
		this._value = value;
	}

	/**
	 * getName
	 * @param name
	 * @return
	 */
	static public String getName(WorldName name) {
		switch( name ) {
			case TEVA_DE:
			case TEVA_EN:
			case TEVA_FR:
			case TEVA_SP:
				return "Teva";

			case EPITH_DE:
			case EPITH_FR:
				return "Epith";

			case APOLINE_DE:
				return "Apoline";

			case NATURE_DE:
				return "Nature";

			case ELGA_DE:
				return "Elga";

			case JENIRA_DE:
				return "Jenira";

			case MARKIS_DE:
				return "Markis";

			case IYZEL_DE:
				return "Iyzel";
		}

		return "Unknow";
	}

	/**
	 * getWorldByName
	 * @param name
	 * @return
	 */
	static public WorldName getWorldByName(String name) {
		WorldName[] wns = WorldName.values();

		for( WorldName aname: wns ) {
			if( aname.getValue().compareTo(name) == 0 ) {
				return aname;
			}
		}

		return null;
	}
}