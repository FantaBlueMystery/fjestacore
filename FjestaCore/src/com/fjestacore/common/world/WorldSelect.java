/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.world;

import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * WorldSelect
 * @author FantaBlueMystery
 */
public class WorldSelect extends AbstractStruct implements IAbstractStructDump {

	/**
	 * world id
	 */
	protected int _worldId = 0;

	/**
	 * WorldSelect
	 */
	public WorldSelect() {}

	/**
	 * WorldSelect
	 * @param worldId
	 */
	public WorldSelect(int worldId) {
		this._worldId = worldId;
	}

	/**
	 * getWorldId
	 * @return
	 */
	public int getWorldId() {
		return this._worldId;
	}

	/**
	 * setWorldId
	 * @param id
	 */
	public void setWorldId(int id) {
		this._worldId = id;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._worldId = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._worldId);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"WorldSelect: [" +
			" WorldId: " + Integer.toString(this._worldId) +
			"]"
			);

		return dump;
	}
}