/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.logout;

/**
 * LogoutType
 * @author FantaBlueMystery
 */
public enum LogoutType {
	SERVERSELECT(0),
	CHARSELECT(1),
	EXIT(2);


	/**
	 * value
	 */
	private int _value;

	/**
	 * LogoutType
	 * @param value
	 */
	private LogoutType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getType
	 * @param value
	 * @return
	 */
	static public LogoutType getType(int value) {
		LogoutType[] types = LogoutType.values();

		for( LogoutType type: types ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return LogoutType.EXIT;
	}
}