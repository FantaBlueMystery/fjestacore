/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.logout;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * NormalLogout
 * @author FantaBlueMystery
 */
public class NormalLogout extends AbstractStruct {

	/**
	 * type
	 */
	protected LogoutType _type = LogoutType.CHARSELECT;

	/**
	 * getType
	 * @return
	 */
	public LogoutType getType() {
		return this._type;
	}

	/**
	 * setType
	 * @param type
	 */
	public void setType(int type) {
		this._type = LogoutType.getType(type);
	}

	/**
	 * setType
	 * @param type
	 */
	public void setType(LogoutType type) {
		this._type = type;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._type = LogoutType.getType(reader.readByteInt());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._type.getValue());
	}
}