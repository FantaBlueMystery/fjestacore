/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.inventory;

import com.fjestacore.common.item.Item;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * FantaBlueMystery
 * @author FantaBlueMystery
 */
public class Inventory extends AbstractStruct implements IAbstractStructDump {

	/**
	 * type
	 */
	protected InventoryType _type = null;

	/**
	 * flag
	 */
	protected int _flag = 0;

	/**
	 * items
	 */
	protected Map<Integer, Item> _items = new HashMap<>();

	/**
	 * Inventory
	 */
	public Inventory() {
		this._type = InventoryType.CHAR_INVENTORY;
	}

	/**
	 * Inventory
	 * @param type
	 */
	public Inventory(InventoryType type) {
		this._type = type;
	}

	/**
	 * getType
	 * @return
	 */
	public InventoryType getType() {
		return this._type;
	}

	/**
	 * getFlagInt
	 * @return
	 */
	public int getFlagInt() {
		return this._flag;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * getItems
	 * @return
	 */
	public Map<Integer, Item> getItems() {
		return this._items;
	}

	/**
	 * addItem
	 * @param pos
	 * @param aitem
	 */
	public void addItem(int pos, Item aitem) {
		this._items.put(pos, aitem);
	}

	/**
	 * getFlag
	 * @return
	 */
	public int getFlag() {
		if( this._flag > 0 ) {
			return this._flag;
		}

		switch( this._type ) {
			// 08
			case EQUIPPED:
				//return 115;	// 0x73;
				return 0x5B;

			// 09
			case CHAR_INVENTORY:
				//return 91;	// 0x5B;, (0x3d by offi);
				return 0x3D;

			// 12 (0c)
			case MINIHOUSE_SKIN:
				//return 63;
				return 0x01;

			// 15 (0f)
			case ACTION_BOX:
				//return 61;
				return 0x17;

			default:
				return 0;
		}
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int itemCount = reader.readByteInt();

		this._type = InventoryType.getInventoryType(reader.readByteInt());
		this._flag = reader.readByteInt();

		for( int i=0; i<itemCount; i++ ) {
			int dataSize = reader.readByteInt()-2;
			InventoryLocation il = new InventoryLocation(reader.readShort());

			Item aitem = new Item();
			aitem.read(reader.readBuffer(dataSize));

			this._items.put(il.getIndex(), aitem);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._items.size());
		writer.writeByteInt(this._type.getValue());
		writer.writeByteInt(this.getFlag());	// ? flag

		for( Map.Entry<Integer, Item> entry : this._items.entrySet() ) {
			InventoryLocation il = new InventoryLocation(this._type.getValue(), entry.getKey());

			Item aitem = entry.getValue();

			WriterStream twriter = new WriterStream(3 + aitem.getSize());

			twriter.writeByteInt(aitem.getSize()+2);
			twriter.writeShort(il.getLocation());
			aitem.write(twriter);

			writer.appendBuffer(twriter.getBuffer());
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"Inventory: [" +
			" Type: " + this._type.name() +
			" Flag: " + Integer.toString(this._flag) +
			" ItemCount: " + Integer.toString(this._items.size()) +
			"]"
			);

		for( Map.Entry<Integer, Item> entry : this._items.entrySet() ) {
			dump += "\r\nSlot: " + Integer.toString(entry.getKey());
			dump += entry.getValue().dump(deep+1);
		}

		return dump;
	}
}