/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.inventory;

/**
 * InventoryType
 * @author FantaBlueMystery
 */
public enum  InventoryType {
	SEA_WAR(0),
	EMBLEM(1),
	REWARD_STORAGE(2),
	MH_FURNITURE(3),
	GUILD(4),
	PAY_STORAGE(5),
	STORAGE(6),
	QUEST_INVENTORY(7),
	EQUIPPED(8),
	CHAR_INVENTORY(9),
	ITEM_CHEST(10),
	DB_EXCHANGE(11),
	MINIHOUSE_SKIN(12),
	MH_ACCESSORY(13),
	MH_TILEALL(14),
	ACTION_BOX(15),
	EMOTION(20),
	VENDOR(21),
	TRADE(22),
	SELL(23),
	ALL(50);

	/**
	 * value
	 */
	private int _value;

	/**
	 * InventoryType
	 * @param value
	 */
	private InventoryType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getInventoryType
	 * @param value
	 * @return
	 */
	static public InventoryType getInventoryType(int value) {
		InventoryType[] types = InventoryType.values();

		for( InventoryType type: types ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return InventoryType.ALL;
	}
}