/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.inventory;

/**
 * InventoryLocation
 * @author FantaBlueMystery
 */
public class InventoryLocation {

	/**
	 * box
	 */
	protected int _box = 0;

	/**
	 * index
	 */
	protected int _index = 0;

	/**
	 * InventoryLocation
	 * @param location
	 */
	public InventoryLocation(int location) {
		this._box = location >> 10;
		this._index = location & 0x3FF;
	}

	/**
	 * InventoryLocation
	 * @param box
	 * @param index
	 */
	public InventoryLocation(int box, int index) {
		this._box = box;
		this._index = index;
	}

	/**
	 * getBox
	 * @return
	 */
	public int getBox() {
		return this._box;
	}

	/**
	 * getIndex
	 * @return
	 */
	public int getIndex() {
		return this._index;
	}

	/**
	 * getLocation
	 * @return
	 */
	public int getLocation() {
		return (this._box << 10) | (this._index & 0x3FF);
	}
}