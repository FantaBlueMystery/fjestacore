/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.sound;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * PlaySound
 * @author FantaBlueMystery
 */
public class PlaySound extends AbstractStruct {

	/**
	 * filename
	 */
	protected String _filename = "";

	/**
	 * PlaySound
	 */
	public PlaySound() {}

	/**
	 * PlaySound
	 * @param filename
	 */
	public PlaySound(String filename) {
		this._filename = filename;
	}

	/**
	 * setFilename
	 * @param filename
	 */
	public void setFilename(String filename) {
		this._filename = filename;
	}

	/**
	 * getFilename
	 * @return
	 */
	public String getFilename() {
		return this._filename;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + this._filename.length();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int filenameLen = reader.readShort();

		this._filename = reader.readString(filenameLen);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._filename.length());
		writer.writeString(this._filename, this._filename.length());
	}
}