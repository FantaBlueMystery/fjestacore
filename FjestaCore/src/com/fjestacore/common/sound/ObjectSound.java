/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.sound;

import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ObjectSound
 * @author FantaBlueMystery
 */
public class ObjectSound extends AbstractStruct implements IObjectHandle {

	/**
	 * n handle
	 */
	protected int _nHandle = 0;

	/**
	 * filename
	 */
	protected String _filename = "";

	/**
	 * ObjectSound
	 */
	public ObjectSound() {}

	/**
	 * ObjectSound
	 * @param handle
	 * @param filename
	 */
	public ObjectSound(int handle, String filename) {
		this._nHandle = handle;
		this._filename = filename;
	}

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._nHandle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._nHandle = handle;
	}

	/**
	 * getFilename
	 * @return
	 */
	public String getFilename() {
		return this._filename;
	}

	/**
	 * setFilename
	 * @param filename
	 */
	public void setFilename(String filename) {
		this._filename = filename;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 34;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nHandle = reader.readShort();
		this._filename = reader.readString(32);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nHandle);
		writer.writeString(this._filename, 32);
	}
}