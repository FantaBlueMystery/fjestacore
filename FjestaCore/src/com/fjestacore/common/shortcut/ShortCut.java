/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.shortcut;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ShortCut
 * @author FantaBlueMystery
 */
public class ShortCut extends AbstractStruct {

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * code
	 */
	protected int _code = 0;

	/**
	 * value
	 */
	protected int _value = 0;

	/**
	 * ShortCut
	 */
	public ShortCut() {}

	/**
	 * ShortCut
	 * @param slot
	 * @param code
	 * @param value
	 */
	public ShortCut(int slot, int code, int value) {
		this._slot = slot;
		this._code = code;
		this._value = value;
	}

	/**
	 * getSlot
	 * @return
	 */
	public int getSlot() {
		return this._slot;
	}

	/**
	 * setSlot
	 * @param slot
	 */
	public void setSlot(int slot) {
		this._slot = slot;
	}

	/**
	 * getCode
	 * @return
	 */
	public int getCode() {
		return this._code;
	}

	/**
	 * setCode
	 * @param code
	 */
	public void setCode(int code) {
		this._code = code;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	public void setValue(int value) {
		this._value = value;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._slot	= reader.readByteInt();
		this._code	= reader.readShort();
		this._value = reader.readShort();
		int tmp = reader.readShort(); // ?
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._slot);
		writer.writeShort((short) this._code);
		writer.writeShort((short) this._value);
		writer.writeShort((short) 0);	// ?
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 7;
	}
}