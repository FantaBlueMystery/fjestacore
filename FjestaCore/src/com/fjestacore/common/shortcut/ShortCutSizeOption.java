/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.shortcut;

import com.fjestacore.common.Version;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ShortCutSizeOption
 * @author FantaBlueMystery
 */
public class ShortCutSizeOption extends AbstractStruct {

	/**
	 * mk version
	 */
	protected Version _mkVersion = new Version();

	/**
	 * by total num
	 */
	protected int _byTotalNum = 0;

	/**
	 * by sub win index
	 */
	protected int _bySubWinIndex = 0;

	/**
	 * st
	 */
	protected ShortCutSizeOptionSt[] _st = new ShortCutSizeOptionSt[5];

	/**
	 * ShortCutSizeOption
	 */
	public ShortCutSizeOption() {
		for( int i=0; i<this._st.length; i++ ) {
			this._st[i] = new ShortCutSizeOptionSt();
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 24;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._mkVersion.read(reader);
		this._byTotalNum = reader.readByteInt();
		this._bySubWinIndex = reader.readByteInt();

		for( int i=0; i<this._st.length; i++ ) {
			this._st[i].read(reader);
		}

		// padding
		reader.readBuffer(2);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._mkVersion.write(writer);
		writer.writeByteInt(this._byTotalNum);
		writer.writeByteInt(this._bySubWinIndex);

		for( int i=0; i<this._st.length; i++ ) {
			this._st[i].write(writer);
		}

		writer.writeString("", 2);
	}
}