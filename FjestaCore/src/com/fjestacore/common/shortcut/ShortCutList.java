/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.shortcut;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ShortCutList
 * @author FantaBlueMystery
 */
public class ShortCutList extends AbstractStruct {

	/**
	 * shortcuts
	 */
	protected ArrayList<ShortCut> _list = new ArrayList<>();

	/**
	 * ShortCutList
	 */
	public ShortCutList() {}

	/**
	 * add
	 * @param sc
	 */
	public void add(ShortCut sc) {
		this._list.add(sc);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int count = reader.readShort();

		for( int i=0; i<count; i++ ) {
			ShortCut sc = new ShortCut();
			sc.read(reader);

			this._list.add(sc);
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (this._list.size()*7);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((short) this._list.size());

		for( ShortCut sc: this._list ) {
			sc.write(writer);
		}
	}
}