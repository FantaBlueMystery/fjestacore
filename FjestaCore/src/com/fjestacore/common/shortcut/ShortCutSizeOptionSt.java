/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.shortcut;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ShortCutSizeOptionSt
 * @author FantaBlueMystery
 */
public class ShortCutSizeOptionSt extends AbstractStruct {

	/**
	 * by display slot
	 */
	protected int _byDisplaySlot = 0;

	/**
	 * by page
	 */
	protected int _byPage = 0;

	/**
	 * b turn mode
	 */
	protected boolean _bTurnMode = false;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._byDisplaySlot = reader.readByteInt();
		this._byPage		= reader.readByteInt();
		this._bTurnMode		= reader.readBoolean();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._byDisplaySlot);
		writer.writeByteInt(this._byPage);
		writer.writeBoolean(this._bTurnMode);
	}
}