/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.shortcut;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ShortCutSize
 * @author FantaBlueMystery
 */
public class ShortCutSize extends AbstractStruct {

	/**
	 * bSuccess
	 */
	protected boolean _bSuccess = false;

	/**
	 * data
	 */
	protected ShortCutSizeOption _data = new ShortCutSizeOption();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 25;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._bSuccess = reader.readBoolean();
		this._data.read(reader);
	}

	/**
	 *
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBoolean(this._bSuccess);
		this._data.write(writer);
	}
}