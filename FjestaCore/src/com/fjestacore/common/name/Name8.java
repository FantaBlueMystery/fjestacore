/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.name;

/**
 * Name8
 * @author FantaBlueMystery
 */
public class Name8 extends NameBase {

	/**
	 * SIZE
	 */
	static public int SIZE = 32;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Name8.SIZE;
	}
}