/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.name;

/**
 * Name4P
 * @author FantaBlueMystery
 */
public class Name4P extends NameBase {

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 17;
	}
}