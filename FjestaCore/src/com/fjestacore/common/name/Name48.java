/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.name;

/**
 * Name48
 * @author FantaBlueMystery
 */
public class Name48 extends NameBase {

	/**
	 * consts
	 */
	static public int SIZE = 48;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Name48.SIZE;
	}
}