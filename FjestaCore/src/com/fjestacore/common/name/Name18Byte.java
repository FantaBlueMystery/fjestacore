/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.name;

/**
 * Name18Byte
 * @author FantaBlueMystery
 */
public class Name18Byte extends NameBase {

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 18;
	}
}
