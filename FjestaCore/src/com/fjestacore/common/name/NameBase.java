/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.name;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * NameBase
 * @author FantaBlueMystery
 */
public abstract class NameBase extends AbstractStruct {

	/**
	 * content
	 */
	protected String _content = "";

	/**
	 * getSize
	 * @return
	 */
	@Override
	public abstract int getSize();

	/**
	 * getContent
	 * @return
	 */
	public String getContent() {
		return this._content;
	}

	/**
	 * setContent
	 * @param content
	 */
	public void setContent(String content) {
		this._content = content;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._content = reader.readString(this.getSize()).trim();
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeString(this._content, this.getSize());
	}
}