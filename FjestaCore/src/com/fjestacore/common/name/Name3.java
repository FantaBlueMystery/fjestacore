/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.name;

/**
 * Name3
 * @author FantaBlueMystery
 */
public class Name3 extends NameBase {

	/**
	 * consts
	 */
	static public int SIZE = 12;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Name3.SIZE;
	}
}