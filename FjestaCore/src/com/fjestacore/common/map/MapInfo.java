/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.map;

import com.fjestacore.files.shn.CDataData;
import java.util.ArrayList;
import java.util.Collections;

/**
 * MapInfo
 * @author FantaBlueMystery
 */
public class MapInfo extends CDataData {

	/**
	 * MapInfo
	 */
	public MapInfo() {}

	/**
	 * MapInfo
	 * @param id
	 * @param mapName
	 * @param regenX
	 * @param regenY
	 * @param inSide
	 * @param sight
	 */
	public MapInfo(int id, String mapName, int regenX, int regenY, boolean inSide, int sight) {
		// TODO
		this._data = new ArrayList<>(Collections.nCopies(10, new Object()));

		this.setId(id);
		this.setMapName(mapName);
		this.setRegenX(regenX);
		this.setRegenY(regenY);

	}

	/**
	 * getId
	 * @return
	 */
	public int getId() {
		return (int) this.getData(0);
	}

	/**
	 * setId
	 * @param id
	 */
	public void setId(int id) {
		this._data.set(0, id);
	}

	/**
	 * getMapName
	 * @return
	 */
	public String getMapName() {
		return (String) this.getData(1);
	}

	/**
	 * setMapName
	 * @param name
	 */
	public void setMapName(String name) {
		this._data.set(1, name);
	}

	/**
	 * getName
	 * @return
	 */
	public String getName() {
		return (String) this.getData(2);
	}

	/**
	 * setName
	 * @param name
	 */
	public void setName(String name) {
		this._data.set(2, name);
	}

	// TODO

	/**
	 * getRegenX
	 * @return
	 */
	public int getRegenX() {
		return (int) this.getData(4);
	}

	/**
	 * setRegenX
	 * @param x
	 */
	public void setRegenX(int x) {
		this._data.set(4, x);
	}

	/**
	 * getRegenY
	 * @return
	 */
	public int getRegenY() {
		return (int) this.getData(5);
	}

	/**
	 * setRegenY
	 * @param y
	 */
	public void setRegenY(int y) {
		this._data.set(5, y);
	}
}