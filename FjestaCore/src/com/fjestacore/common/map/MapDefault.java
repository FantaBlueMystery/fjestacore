/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.map;

import com.fjestacore.common.position.PositionXY;

/**
 * MapDefault
 * @author FantaBlueMystery
 */
public class MapDefault {

	static public int ID			= 9;
	static public String MAPNAME	= "Eld";
	static public int REGEN_X		= 17214;
	static public int REGEN_Y		= 13445;
	static public int SIGHT			= 1600;
	static public boolean INSIDE	= false;

	/**
	 * getMapInfo
	 * @return
	 */
	static public MapInfo getMapInfo() {
		return new MapInfo(
			MapDefault.ID,
			MapDefault.MAPNAME,
			MapDefault.REGEN_X,
			MapDefault.REGEN_Y,
			MapDefault.INSIDE,
			MapDefault.SIGHT
			);
	}

	/**
	 * getPosition
	 * @return
	 */
	static public PositionXY getPosition() {
		return new PositionXY(MapDefault.REGEN_X, MapDefault.REGEN_Y);
	}
}