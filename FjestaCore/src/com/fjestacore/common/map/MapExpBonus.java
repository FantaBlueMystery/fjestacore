/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.map;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * MapExpBonus
 * @author FantaBlueMystery
 */
public class MapExpBonus extends AbstractStruct implements IAbstractStructDump {

	/**
	 * start
	 */
	protected ZoneRingLinkAgeStart _start = new ZoneRingLinkAgeStart();

	/**
	 * bonus
	 */
	protected int _bonus = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 9;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._start.read(reader);

		this._bonus = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._start.write(writer);

		writer.writeShort(this._bonus);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"MapExpBonus: [" +
			"Bonus: " + Integer.toString(this._bonus) +
			"]");

		dump += this._start.dump(deep+1);

		return dump;
	}
}