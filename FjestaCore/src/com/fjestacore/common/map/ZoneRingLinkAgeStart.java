/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.map;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * ZoneRingLinkAgeStart
 * @author FantaBlueMystery
 */
public class ZoneRingLinkAgeStart extends AbstractStruct implements IAbstractStructDump {

	/**
	 * machine
	 */
	protected int _machine = 0;

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * regnum
	 */
	protected long _regnum = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 7;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._machine	= reader.readByteInt();
		this._handle	= reader.readShort();
		this._regnum	= reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._machine);
		writer.writeShort(this._handle);
		writer.writeUInt(this._regnum);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ZoneRingLinkAgeStart: [" +
			"Machine: " + Integer.toString(this._machine) +
			" Handle: " + Integer.toString(this._handle) +
			" Regnum: " + Long.toString(this._regnum) +
			"]"
			);

		return dump;
	}
}