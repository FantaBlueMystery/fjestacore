/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.map;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * FieldAttribute
 * @author FantaBlueMystery
 */
public class FieldAttribute extends AbstractStruct {

	/**
	 * field map type
	 */
	protected FieldMapType _eFieldMapType = FieldMapType.FMT_NORMAL;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._eFieldMapType = FieldMapType.intToType(reader.readInt());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeInt(this._eFieldMapType.getValue());
	}
}