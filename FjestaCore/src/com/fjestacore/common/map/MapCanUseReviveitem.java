/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.map;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * MapCanUseReviveitem
 * @author FantaBlueMystery
 */
public class MapCanUseReviveitem extends AbstractStruct implements IAbstractStructDump {

	/**
	 * b can use revive item
	 */
	protected boolean _bCanUseReviveItem = false;

	/**
	 * getCanReviveItem
	 * @return
	 */
	public boolean getCanReviveItem() {
		return this._bCanUseReviveItem;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._bCanUseReviveItem = reader.readBoolean();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBoolean(this._bCanUseReviveItem);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 *
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"MapCanUseReviveitem: [" +
			"bCanUseReviveItem: " + (this._bCanUseReviveItem ? "True" : "False") +
			"]"
			);

		return dump;
	}
}