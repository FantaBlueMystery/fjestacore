/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.map;

import com.fjestacore.common.character.CharReviveSame;

/**
 * MapLinkSame
 * @author FantaBlueMystery
 */
public class MapLinkSame extends CharReviveSame {}