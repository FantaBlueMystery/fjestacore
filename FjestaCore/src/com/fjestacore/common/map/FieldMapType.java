/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.map;

/**
 * FieldMapType
 * @author FantaBlueMystery
 */
public enum FieldMapType {
	FMT_NORMAL(0x0),
	FMT_KINGDOMQUEST(0x1),
	FMT_MINIHOUSE(0x2),
	FMT_INSTANCEDUNGEON(0x3),
	FMT_TUTORIAL(0x4),
	FMT_MAX(0x5)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * MobBriefFlag
	 * @param value
	 */
	private FieldMapType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToType
	 * @param value
	 * @return
	 */
	static public FieldMapType intToType(int value) {
		FieldMapType[] types = FieldMapType.values();

		for( FieldMapType type : types ) {
			if (type.getValue() == value) {
				return type;
			}
		}

		return FieldMapType.FMT_MAX;
	}
}