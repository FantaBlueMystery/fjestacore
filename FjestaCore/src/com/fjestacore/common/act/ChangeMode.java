/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.act;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * ChangeMode
 * @author FantaBlueMystery
 */
public class ChangeMode extends AbstractStruct implements IAbstractStructDump {

	/**
	 * mode
	 */
	protected int _mode = 0;

	/**
	 * getMode
	 * @return
	 */
	public int getMode() {
		return this._mode;
	}

	/**
	 * setMode
	 * @param mode
	 */
	public void setMode(int mode) {
		this._mode = mode;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._mode = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._mode);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 *
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ChangeMode: [" +
			"Mode: " + Integer.toString(this._mode) +
			"]"
			);

		return dump;
	}
}