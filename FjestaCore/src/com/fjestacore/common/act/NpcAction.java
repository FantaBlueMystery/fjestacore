/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.act;

import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * NpcAction
 * @author FantaBlueMystery
 */
public class NpcAction extends AbstractStruct implements IObjectHandle {

	/**
	 * type
	 */
	protected int _nType = 0;

	/**
	 * npc handle
	 */
	protected int _nNPCHandle = 0;

	/**
	 * eCode
	 */
	protected long _nECode = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._nNPCHandle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._nNPCHandle = handle;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 7;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nType			= reader.readByteInt();
		this._nNPCHandle	= reader.readShort();
		this._nECode		= reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._nType);
		writer.writeShort(this._nNPCHandle);
		writer.writeUInt(this._nECode);
	}
}