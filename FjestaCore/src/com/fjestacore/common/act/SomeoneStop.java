/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.act;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SomeOneStop
 * @author FantaBlueMystery
 */
public class SomeoneStop extends AbstractStruct implements IObjectHandle {

	/**
	 * object id
	 */
	protected int _objectId = 0;

	/**
	 * position
	 */
	protected PositionXY _pos = new PositionXY();

	/**
	 * SomeoneStop
	 */
	public SomeoneStop() {}

	/**
	 * SomeoneStop
	 * @param objectId
	 * @param pos
	 */
	public SomeoneStop(int objectId, PositionXY pos) {
		this._objectId = objectId;
		this._pos = pos;
	}

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._objectId;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._objectId = handle;
	}

	/**
	 * getPosition
	 * @return
	 */
	public PositionXY getPosition() {
		return this._pos;
	}

	/**
	 * setPosition
	 * @param pos
	 */
	public void setPosition(PositionXY pos) {
		this._pos = pos;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 10;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._objectId = reader.readShort();
		this._pos.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._objectId);
		this._pos.write(writer);
	}
}