/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.act;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ReinforceStop
 * @author FantaBlueMystery
 */
public class ReinforceStop extends AbstractStruct {

	/**
	 * location xy
	 */
	protected PositionXY _loc = new PositionXY();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._loc.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._loc.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._loc.write(writer);
	}
}
