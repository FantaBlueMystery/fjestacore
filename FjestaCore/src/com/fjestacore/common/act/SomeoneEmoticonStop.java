/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.act;

import com.fjestacore.common.stopemoticon.StopEmoticonDescipt;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SomeoneEmoticonStop
 * @author FantaBlueMystery
 */
public class SomeoneEmoticonStop extends AbstractStruct {

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * emoticon
	 */
	protected StopEmoticonDescipt _emoticon = new StopEmoticonDescipt();

	/**
	 * SomeoneEmoticonStop
	 */
	public SomeoneEmoticonStop() {}

	/**
	 * SomeoneEmoticonStop
	 * @param handle
	 * @param se
	 */
	public SomeoneEmoticonStop(int handle, StopEmoticonDescipt se) {
		this._handle = handle;
		this._emoticon = se;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + this._emoticon.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle = reader.readShort();
		this._emoticon.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);
		this._emoticon.write(writer);
	}
}