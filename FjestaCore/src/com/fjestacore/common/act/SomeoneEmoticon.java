/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.act;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SomeoneEmoticon
 * @author FantaBlueMystery
 */
public class SomeoneEmoticon extends AbstractStruct {

	/**
	 * object id
	 */
	protected int _objectId = 0;

	/**
	 * emote id
	 */
	protected int _emoteId = 0;

	/**
	 * SomeoneEmoticon
	 */
	public SomeoneEmoticon() {}

	/**
	 * SomeoneEmoticon
	 * @param objectId
	 * @param emoteId
	 */
	public SomeoneEmoticon(int objectId, int emoteId) {
		this._objectId = objectId;
		this._emoteId = emoteId;
	}

	/**
	 * SomeoneEmoticon
	 * @param objectId
	 * @param emoticon
	 */
	public SomeoneEmoticon(int objectId, Emoticon emoticon) {
        this(objectId, emoticon.getId());
	}

	/**
	 * getObjectId
	 * @return
	 */
	public int getObjectId() {
		return this._objectId;
	}

	/**
	 * getEmoteId
	 * @return
	 */
	public int getEmoteId() {
		return this._emoteId;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._objectId	= reader.readShort();
		this._emoteId	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._objectId);
		writer.writeByteInt(this._emoteId);
	}
}