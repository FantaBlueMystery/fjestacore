/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.act;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Emoticon
 * @author FantaBlueMystery
 */
public class Emoticon extends AbstractStruct {

	/**
	 * emoticon id
	 */
	protected int _emoticonid = 0;

	/**
	 * Emoticon
	 */
	public Emoticon() {}

	/**
	 * Emoticon
	 * @param emoticonId
	 */
	public Emoticon(int emoticonId) {
        this._emoticonid = emoticonId;
	}


	/**
	 * getId
	 * @return
	 */
	public int getId() {
		return this._emoticonid;
	}

	/**
	 * setId
	 * @param id
	 */
	public void setId(int id) {
		this._emoticonid = id;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._emoticonid = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._emoticonid);
	}
}
