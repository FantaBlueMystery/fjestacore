/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.act;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * MoveFail
 * @author FantaBlueMystery
 */
public class MoveFail extends AbstractStruct {

	/**
	 * back
	 */
	protected PositionXY _back = new PositionXY();

	/**
	 * MoveFail
	 */
	public MoveFail() {}

	/**
	 * MoveFail
	 * @param back
	 */
	public MoveFail(PositionXY back) {
		this._back = back;
	}

	/**
	 * getBackPosition
	 * @return
	 */
	public PositionXY getBackPosition() {
		return this._back;
	}

	/**
	 * setBackPosition
	 * @param back
	 */
	public void setBackPosition(PositionXY back) {
		this._back = back;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 8;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._back.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._back.write(writer);
	}
}
