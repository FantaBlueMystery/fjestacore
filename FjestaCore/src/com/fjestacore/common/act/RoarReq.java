/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.act;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * RoarReq
 * @author FantaBlueMystery
 */
public class RoarReq extends AbstractStruct implements IAbstractStructDump {

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * content
	 */
	protected String _content = "";

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + this._content.length();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._slot = reader.readByteInt();

		int size = reader.readByteInt();

		this._content = reader.readString(size);
	}

	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._slot);
		writer.writeByteInt(this._content.length());
		writer.writeString(this._content);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"RoarReq: [" +
			" Slot: " + Integer.toString(this._slot) +
			" Content: " + this._content +
			"]"
			);

		return dump;
	}
}