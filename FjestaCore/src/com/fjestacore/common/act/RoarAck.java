/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.act;

import com.fjestacore.common.error.AbstractErrorCode;

/**
 * RoarAck
 * @author FantaBlueMystery
 */
public class RoarAck extends AbstractErrorCode {}