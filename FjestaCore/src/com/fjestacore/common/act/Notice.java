/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.act;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Notice
 * @author FantaBlueMystery
 */
public class Notice extends AbstractStruct {

	/**
	 * flag
	 */
	protected int _flag = 10;	// 10 = yellow right, 147 = big message, 153 = ??, 117 = ??, 3 = ??, 221 = ???

	/**
	 * content
	 */
	protected String _content = "";

	/**
	 * Notice
	 */
	public Notice() {}

	/**
	 * Notice
	 * @param content
	 */
	public Notice(String content) {
		this._content = content;
	}

	/**
	 * Notice
	 * @param content
	 * @param flag
	 */
	public Notice(String content, int flag) {
		this._flag = flag;
		this._content = content;
	}

	/**
	 * getFlag
	 * @return
	 */
	public int getFlag() {
		return this._flag;
	}

	/**
	 * setFlag
	 * @param flag
	 */
	public void setFlag(int flag) {
		this._flag = flag;
	}

	/**
	 * getContent
	 * @return
	 */
	public String getContent() {
		return this._content;
	}

	/**
	 * setContent
	 * @param content
	 */
	public void setContent(String content) {
		this._content = content;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + this._content.length();
	}

	/**
	 *
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._flag = reader.readByteInt();

		int clen = reader.readByteInt();

		this._content = reader.readString(clen);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._flag);
		writer.writeByteInt(this._content.length());
		writer.writeString(this._content, this._content.length());
	}
}