/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.animation;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SAnimation
 * @author FantaBlueMystery
 */
public class SAnimation extends AbstractStruct {

	/**
	 * data
	 */
	protected byte[] _data = new byte[32];

	/**
	 * SAnimation
	 */
	public SAnimation() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._data.length;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._data = reader.readBuffer(this._data.length);
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBuffer(this._data);
	}
}