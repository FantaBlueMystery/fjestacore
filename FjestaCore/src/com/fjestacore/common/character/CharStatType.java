/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

/**
 * CharStatType
 * @author FantaBlueMystery
 */
public enum CharStatType {
	STRENGTH(1),		// STR
	CONSTITUTE(2),		// CON
	DEXTERITY(3),		// GES
	INTELLIGENCE(4),	// INT
	WIZDOM(5),			//
	MENTALPOWER(6),		//
	WCLOW(7),			//
	WCHIGH(8),			//
	AC(9),				//
	TH(10),				//
	TB(11),				//
	MALOW(12),			//
	MAHIGH(13),			//
	MR(14),				//
	MH(15),				//
	MB(16)				//
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CharStatType
	 * @param value
	 */
	private CharStatType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}