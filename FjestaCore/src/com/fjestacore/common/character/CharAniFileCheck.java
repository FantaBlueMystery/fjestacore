/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.character;

import com.fjestacore.common.name.Name60Byte;
import com.fjestacore.common.name.Name8;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharAniFileCheck
 * @author FantaBlueMystery
 */
public class CharAniFileCheck extends AbstractStruct {

	/**
	 * checksum
	 */
	protected Name8 _checksum = new Name8();

	/**
	 * sub directory
	 */
	protected Name8 _subDirectory = new Name8();

	/**
	 * filename
	 */
	protected Name60Byte _filename = new Name60Byte();

	/**
	 * getHash
	 * @return
	 */
	public String getHash() {
		return this._checksum.getContent();
	}

	/**
	 * setHash
	 * @param hash
	 */
	public void setHash(String hash) {
		this._checksum.setContent(hash);
	}

	/**
	 * getDir
	 * @return
	 */
	public String getDir() {
		return this._subDirectory.getContent();
	}

	/**
	 * setDir
	 * @param dir
	 */
	public void setDir(String dir) {
		this._subDirectory.setContent(dir);
	}

	/**
	 * getFile
	 * @return
	 */
	public String getFile() {
		return this._filename.getContent();
	}

	/**
	 * setFile
	 * @param file
	 */
	public void setFile(String file) {
		this._filename.setContent(file);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._checksum.getSize() + this._subDirectory.getSize() + this._filename.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._checksum.read(reader);
		this._subDirectory.read(reader);
		this._filename.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._checksum.write(writer);
		this._subDirectory.write(writer);
		this._filename.write(writer);
	}
}