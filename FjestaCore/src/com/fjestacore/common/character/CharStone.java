/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharStone
 * @author FantaBlueMystery
 */
public class CharStone extends AbstractStruct {

	/**
	 * consts
	 */
	static public int SIZE = 8;

	/**
	 * pwr
	 */
	protected int _pwr = 0;

	/**
	 * gr
	 */
	protected int _gr = 0;

	/**
	 * hp
	 */
	protected int _hp = 0;

	/**
	 * sp
	 */
	protected int _sp = 0;

	/**
	 * CharacterStone
	 */
	public CharStone() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharStone.SIZE;
	}

	/**
	 * hp
	 * @param hp
	 */
	public void setHp(int hp) {
		this._hp = hp;
	}

	/**
	 * setSp
	 * @param sp
	 */
	public void setSp(int sp) {
		this._sp = sp;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._pwr);
		writer.writeShort(this._gr);
		writer.writeShort(this._hp);
		writer.writeShort(this._sp);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._pwr	= reader.readShort();
		this._gr	= reader.readShort();
		this._hp	= reader.readShort();
		this._sp	= reader.readShort();
	}
}
