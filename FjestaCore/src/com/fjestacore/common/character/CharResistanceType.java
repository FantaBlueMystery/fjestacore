/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

/**
 * CharResistanceType
 * @author FantaBlueMystery
 */
public enum CharResistanceType {
	PAIN(1),
	RESTRAINT(2),
	CURSE(3),
	SHOCK(4),
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CharResistanceType
	 * @param value
	 */
	private CharResistanceType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}