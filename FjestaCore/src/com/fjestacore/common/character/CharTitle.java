/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.common.title.TitleList;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharTitle
 * @author FantaBlueMystery
 */
public class CharTitle extends AbstractStruct {

	/**
	 * current title
	 */
	protected int _currentTitle = 0;

	/**
	 * current title element
	 */
	protected int _currentTitleElement = 0;

	/**
	 * current title mob id
	 */
	protected int _currentTitleMobId = 0;

	/**
	 * title list
	 */
	protected TitleList _titlelist = new TitleList();

	/**
	 * CharTitle
	 */
	public CharTitle() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4+2+(this._titlelist.getSize()*2);
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._currentTitle);
		writer.writeByteInt(this._currentTitleElement);
		writer.writeShort(this._currentTitleMobId);
		writer.writeBuffer(this._titlelist.write());
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._currentTitle			= reader.readByteInt();
		this._currentTitleElement	= reader.readByteInt();
		this._currentTitleMobId		= reader.readShort();
		this._titlelist.read(reader);
	}
}