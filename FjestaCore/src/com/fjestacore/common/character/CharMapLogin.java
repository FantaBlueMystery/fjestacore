/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * CharMapLogin
 * @author FantaBlueMystery
 */
public class CharMapLogin extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * SIZE
	 */
	static public int SIZE = 242;

	/**
	 * object handle
	 */
	protected int _objectHandle = 0;

	/**
	 * char parameter data
	 */
	protected CharParameterData _param = new CharParameterData();

	/**
	 * login coord
	 */
	protected PositionXY _logincoord = new PositionXY();

	/**
	 * CharMapLogin
	 */
	public CharMapLogin() {}

	/**
	 * CharMapLogin
	 * @param objectHandle
	 * @param param
	 * @param coord
	 */
	public CharMapLogin(int objectHandle, CharParameterData param, PositionXY coord) {
		this._objectHandle	= objectHandle;
		this._param			= param;
		this._logincoord	= coord;
	}

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._objectHandle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._objectHandle = handle;
	}

	/**
	 * getLoginCoord
	 * @return
	 */
	public PositionXY getLoginCoord() {
		return this._logincoord;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharMapLogin.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._objectHandle = reader.readShort();
		this._param.read(reader);
		this._logincoord.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._objectHandle);
		this._param.write(writer);
		this._logincoord.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"CharMapLogin: [" +
			"ObjectHandle: " + Integer.toString(this._objectHandle) +
			"]"
			);

		dump += this._param.dump(deep+1);
		dump += this._logincoord.dump(deep+1);

		return dump;
	}
}