/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharIdChangeData
 * @author FantaBlueMystery
 */
public class CharIdChangeData extends AbstractStruct {

	/**
	 * need change id
	 */
	protected int _needChangeId = 0;

	/**
	 * init
	 */
	protected int _init = 0;

	/**
	 * row no
	 */
	protected int _rowNo = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._needChangeId	= reader.readByteInt();
		this._init			= reader.readByteInt();
		this._rowNo			= (int) reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._needChangeId);
		writer.writeByteInt(this._init);
		writer.writeUInt(this._rowNo);
	}
}
