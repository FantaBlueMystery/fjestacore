/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharBarsMax
 * @author FantaBlueMystery
 */
public class CharBarsMax extends AbstractStruct {

	/**
	 * conts
	 */
	static public int SIZE = 16;

	/**
	 * hp
	 */
	protected long _hp = 0;

	/**
	 * sp
	 */
	protected long _sp = 0;

	/**
	 * lightpower
	 */
	protected long _lp = 0;

	/**
	 * ap
	 */
	protected long _ap = 0;

	/**
	 * CharBarsMax
	 */
	public CharBarsMax() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharBars.SIZE;
	}

	/**
	 * getHp
	 * @return
	 */
	public long getHp() {
		return this._hp;
	}

	/**
	 * setHp
	 * @param hp
	 */
	public void setHp(long hp) {
		this._hp = hp;
	}

	/**
	 * getSp
	 * @return
	 */
	public long getSp() {
		return this._sp;
	}

	/**
	 * setSp
	 * @param sp
	 */
	public void setSp(long sp) {
		this._sp = sp;
	}

	/**
	 * getLp
	 * @return
	 */
	public long getLp() {
		return this._lp;
	}

	/**
	 * setLp
	 * @param lp
	 */
	public void setLp(long lp) {
		this._lp = lp;
	}

	/**
	 * getAp
	 * @return
	 */
	public long getAp() {
		return this._ap;
	}

	/**
	 * setAp
	 * @param ap
	 */
	public void setAp(long ap) {
		this._ap = ap;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._hp);
		writer.writeUInt(this._sp);
		writer.writeUInt(this._lp);
		writer.writeUInt(this._ap);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._hp = reader.readUInt();
		this._sp = reader.readUInt();
		this._lp = reader.readUInt();
		this._ap = reader.readUInt();
	}
}