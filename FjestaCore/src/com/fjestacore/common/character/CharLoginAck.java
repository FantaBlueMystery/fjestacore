/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.common.name.Name4;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * CharLoginAck
 * @author FantaBlueMystery
 */
public class CharLoginAck extends AbstractStruct implements IAbstractStructDump, ICharLogin  {

	/**
	 * server ip
	 */
	protected Name4 _serverIp = new Name4();

	/**
	 * server port
	 */
	protected int _serverPort = 0;

	/**
	 * CharLoginAck
	 */
	public CharLoginAck() {}

	/**
	 * CharLoginAck
	 * @param ip
	 * @param port
	 */
	public CharLoginAck(String ip, int port) {
		this._serverIp.setContent(ip);
		this._serverPort = port;
	}

	/**
	 * getServerIp
	 * @return
	 */
	@Override
	public String getServerIp() {
		return this._serverIp.getContent();
	}

	/**
	 * setServerIp
	 * @param ip
	 */
	public void setServerIp(String ip) {
		this._serverIp.setContent(ip);
	}

	/**
	 * getServerPort
	 * @return
	 */
	@Override
	public int getServerPort() {
		return this._serverPort;
	}

	/**
	 * setServerPort
	 * @param port
	 */
	public void setServerPort(int port) {
		this._serverPort = port;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 18;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._serverIp.read(reader);
		this._serverPort	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._serverIp.write(writer);
		writer.writeShort(this._serverPort);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"CharLoginAck: [" +
			"ServerIP: " + this._serverIp.getContent() +
			" ServerPort: " + Integer.toString(this._serverPort) +
			"]"
			);

		return dump;
	}
}