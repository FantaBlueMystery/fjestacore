/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharStoneMax
 * @author FantaBlueMystery
 */
public class CharStoneMax extends AbstractStruct {

	/**
	 * pwr max
	 */
	protected CharParameterDataStone _pwrMax = new CharParameterDataStone();

	/**
	 * gr max
	 */
	protected CharParameterDataStone _grMax = new CharParameterDataStone();

	/**
	 * hp max
	 */
	protected int _hpMax = 0;

	/**
	 * sp max
	 */
	protected int _spMax = 0;

	/**
	 * CharStoneMax
	 */
	public CharStoneMax() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return (2*4)+(2*CharParameterDataStone.SIZE);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._hpMax);
		writer.writeUInt(this._spMax);
		this._pwrMax.write(writer);
		this._grMax.write(writer);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._hpMax		= (int) reader.readUInt();
		this._spMax		= (int) reader.readUInt();
		this._pwrMax.read(reader);
		this._grMax.read(reader);
	}
}