/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.character;

import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ChargeBuffInfo
 * @author FantaBlueMystery
 */
public class ChargeBuffInfo extends AbstractStruct {

	/**
	 * SIZE
	 */
	static public int SIZE = 14;

	/**
	 * charged buff key
	 */
	protected long _chargedBuffKey = 0;

	/**
	 * charged buff id
	 */
	protected int _chargedBuffID = 0;

	/**
	 * use time
	 */
	protected DateTime _useTime = new DateTime();

	/**
	 * end time
	 */
	protected DateTime _endTime = new DateTime();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return ChargeBuffInfo.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chargedBuffKey = reader.readUInt();
		this._chargedBuffID = reader.readShort();
		this._useTime.read(reader);
		this._endTime.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._chargedBuffKey);
		writer.writeShort(this._chargedBuffID);
		this._useTime.write();
		this._endTime.write();
	}
}