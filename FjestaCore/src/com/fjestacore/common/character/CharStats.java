/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.HashMap;

/**
 * CharStats
 * @author FantaBlueMystery
 */
public class CharStats extends AbstractStruct implements IAbstractStructDump {

	/**
	 * stats
	 */
	protected HashMap<CharStatType, CharStatVar> _stats = new HashMap();

	/**
	 * CharStats
	 */
	public CharStats() {
		// init defaults
		for( CharStatType atype: CharStatType.values() ) {
			this._stats.put(atype, new CharStatVar());
		}
	}

	/**
	 * getStat
	 * @param type
	 * @return
	 */
	public CharStatVar getStat(CharStatType type) {
		return this._stats.get(type);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharStatType.values().length*CharStatVar.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		for( CharStatType atype: CharStatType.values() ) {
			this._stats.get(atype).read(reader);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		for( CharStatType atype: CharStatType.values() ) {
			this._stats.get(atype).write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dumpstr = "CharStats: [";

		for( CharStatType atype: CharStatType.values() ) {
			CharStatVar csv = this._stats.get(atype);

			dumpstr += " " + atype.name() + ": " + Long.toString(csv.getBase()) + "/" + Long.toString(csv.getChange());
		}

		dumpstr += "]";

		String dump = MsgDump.getMsgDump(deep, dumpstr);

		return dump;
	}
}