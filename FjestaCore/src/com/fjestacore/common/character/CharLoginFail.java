/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.server.ServerError;
import java.io.IOException;

/**
 * CharLoginFail
 * @author FantaBlueMystery
 */
public class CharLoginFail extends AbstractStruct {

	/**
	 * error code
	 */
	protected ServerError _errorCode = ServerError.NONE;

	/**
	 * CharLoginFail
	 */
	public CharLoginFail() {}

	/**
	 * CharLoginFail
	 * @param ec
	 */
	public CharLoginFail(ServerError ec) {
		this._errorCode = ec;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._errorCode = ServerError.intToError(reader.readShort());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._errorCode.getValue());
	}
}