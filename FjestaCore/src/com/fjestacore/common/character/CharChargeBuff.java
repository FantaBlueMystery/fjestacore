/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ChargeBuff
 * @author FantaBlueMystery
 */
public class CharChargeBuff extends AbstractStruct {

	/**
	 * charged buff
	 */
	protected ArrayList<ChargeBuffInfo> _chargedBuff = new ArrayList<>();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (this._chargedBuff.size() * ChargeBuffInfo.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int numOfChargedBuff = reader.readShort();

		for( int i=0; i<numOfChargedBuff; i++ ) {
			ChargeBuffInfo tbuff = new ChargeBuffInfo();
			tbuff.read(reader);

			this._chargedBuff.add(tbuff);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this.getSize());

		for( ChargeBuffInfo tbuff: this._chargedBuff ) {
			tbuff.write(writer);
		}
	}
}