/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.TmTime;
import java.io.IOException;

/**
 * CharDeleteInfo
 * @author FantaBlueMystery
 */
public class CharDeleteInfo extends AbstractStruct {

	/**
	 * time
	 */
	protected TmTime _time = new TmTime();

	/**
	 *
	 */
	public CharDeleteInfo() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 5;
	}

	/**
	 * getTime
	 * @return
	 */
	public TmTime getTime() {
		return this._time;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._time.setYear(reader.readByteInt());
		this._time.setMon(reader.readByteInt());
		this._time.setMDay(reader.readByteInt());
		this._time.setHour(reader.readByteInt());
		this._time.setMin(reader.readByteInt());
	}

	/**
	 * writeShort
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._time.getYear());
		writer.writeByteInt(this._time.getMon());
		writer.writeByteInt(this._time.getMDay());
		writer.writeByteInt(this._time.getHour());
		writer.writeByteInt(this._time.getMin());
	}
}