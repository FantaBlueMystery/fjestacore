/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * CharParameterData
 * @author FantaBlueMystery
 */
public class CharParameterData extends AbstractStruct implements IAbstractStructDump {

	/**
	 * consts
	 */
	static public int SIZE = 240;

	/**
	 * prev exp
	 */
	protected ULong _prevExp = ULong.MIN;

	/**
	 * next exp
	 */
	protected ULong _nextExp = ULong.MIN;

	/**
	 * stats
	 */
	protected CharStats _stats = new CharStats();

	/**
	 * bars
	 */
	protected CharBarsMax _bars = new CharBarsMax();

	/**
	 * stones
	 */
	protected CharStoneMax _stones = new CharStoneMax();

	/**
	 * resitance
	 */
	protected CharResistance _resistance = new CharResistance();

	/**
	 * getPrevExp
	 * @return
	 */
	public ULong getPrevExp() {
		return this._prevExp;
	}

	/**
	 * getNexExp
	 * @return
	 */
	public ULong getNexExp() {
		return this._nextExp;
	}

	/**
	 * getStonesMax
	 * @return
	 */
	public CharStoneMax getStonesMax() {
		return this._stones;
	}

	/**
	 * getBarsMax
	 * @return
	 */
	public CharBarsMax getBarsMax() {
		return this._bars;
	}

	/**
	 * getResistance
	 * @return
	 */
	public CharResistance getResistance() {
		return this._resistance;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharParameterData.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._prevExp	= reader.readULong();
		this._nextExp	= reader.readULong();

		this._stats.read(reader);
		this._bars.read(reader);
		this._stones.read(reader);
		this._resistance.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeULong(this._prevExp);	// +8
		writer.writeULong(this._nextExp);	// +8

		this._stats.write(writer);			//
		this._bars.write(writer);
		this._stones.write(writer);
		this._resistance.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"CharParameterData: [" +
			"prevExp: " + this._prevExp.toString() +
			" nextExp: " + this._nextExp.toString() +
			"]"
			);

		// TODO

		return dump;
	}
}