/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.HashMap;

/**
 * CharResistance
 * @author FantaBlueMystery
 */
public class CharResistance extends AbstractStruct {

	/**
	 * resistance
	 */
	protected HashMap<CharResistanceType, CharStatVar> _res = new HashMap();

	/**
	 * CharResistance
	 */
	public CharResistance() {
		for( CharResistanceType rest: CharResistanceType.values() ) {
			this._res.put(rest, new CharStatVar());
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharResistanceType.values().length * CharStatVar.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		for( CharResistanceType rest: CharResistanceType.values() ) {
			this._res.get(rest).read(reader);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		for( CharResistanceType rest: CharResistanceType.values() ) {
			this._res.get(rest).write(writer);
		}
	}
}