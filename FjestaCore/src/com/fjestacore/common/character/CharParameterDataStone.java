/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharParameterDataStone
 * @author FantaBlueMystery
 */
public class CharParameterDataStone extends AbstractStruct {

	/**
	 * conts
	 */
	static public int SIZE = 16;

	/**
	 * flag
	 */
	protected int _flag = 0;

	/**
	 * EPPysic
	 */
	protected int _epPysic = 0;

	/**
	 * EPMagic
	 */
	protected int _epMagic = 0;

	/**
	 * max stone
	 */
	protected int _maxStone = 0;

	/**
	 * CharParameterData
	 */
	public CharParameterDataStone() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 16;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._flag);
		writer.writeUInt(this._epPysic);
		writer.writeUInt(this._epMagic);
		writer.writeUInt(this._maxStone);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._flag = (int) reader.readUInt();
		this._epPysic = (int) reader.readUInt();
		this._epMagic = (int) reader.readUInt();
		this._maxStone = (int) reader.readUInt();
	}
}