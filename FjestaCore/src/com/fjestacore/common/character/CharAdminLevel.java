/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharAdminLevel
 * @author FantaBlueMystery
 */
public class CharAdminLevel extends AbstractStruct {

	/**
	 * admin level
	 */
	protected int _nAdminLevel = 0;

	/**
	 * CharAdminLevel
	 */
	public CharAdminLevel() {}

	/**
	 * CharAdminLevel
	 * @param adminLevel
	 */
	public CharAdminLevel(int adminLevel) {
		this._nAdminLevel = adminLevel;
	}

	/**
	 * getAdminLevel
	 * @return
	 */
	public int getAdminLevel() {
		return this._nAdminLevel;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nAdminLevel = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._nAdminLevel);
	}
}