/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery, Greebo
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharacterEquipmentUpgrade
 * @author FantaBlueMystery
 */
public class CharacterEquipmentUpgrade extends AbstractStruct {

	/**
	 * gap
	 */
	protected byte[] _gap = new byte[]{0x0, 0x0};

	/**
	 *
	 */
	protected byte _bf2 = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._gap = reader.readBuffer(2);
		this._bf2 = reader.readByte();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBuffer(this._gap);
		writer.writeByte(this._bf2);
	}
}