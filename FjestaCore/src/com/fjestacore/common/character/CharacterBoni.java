/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharacterBoni
 * @author FantaBlueMystery
 */
public class CharacterBoni extends AbstractStruct {

	/**
	 * strength
	 */
	protected int _strength = 10;

	/**
	 * constitute
	 */
	protected int _constitute = 20;

	/**
	 * dexterity
	 */
	protected int _dexterity = 30;

	/**
	 * intelligence
	 */
	protected int _intelligence = 40;

	/**
	 * mentalpower
	 */
	protected int _mentalpower = 50;

	/**
	 * redistribute point
	 */
	protected int _redistributePoint = 60;

	/**
	 * CharacterBoni
	 */
	public CharacterBoni() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._strength);
		writer.writeByteInt(this._constitute);
		writer.writeByteInt(this._dexterity);
		writer.writeByteInt(this._intelligence);
		writer.writeByteInt(this._mentalpower);
		writer.writeByteInt(this._redistributePoint);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._strength			= reader.readByteInt();
		this._constitute		= reader.readByteInt();
		this._dexterity			= reader.readByteInt();
		this._intelligence		= reader.readByteInt();
		this._mentalpower		= reader.readByteInt();
		this._redistributePoint	= reader.readByteInt();
	}
}