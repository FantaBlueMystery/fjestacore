/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharTitleBrief
 * @author FantaBlueMystery
 */
public class CharTitleBrief extends AbstractStruct {

	/**
	 * type
	 */
	protected int _type = 0;

	/**
	 * element no
	 */
	protected int _elementNo = 0;

	/**
	 * mobid
	 */
	protected int _mobId = 0;

	/**
	 * CharTitleBrief
	 */
	public CharTitleBrief() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._type);
		writer.writeByteInt(this._elementNo);
		writer.writeShort(this._mobId);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._type		= reader.readByteInt();
		this._elementNo = reader.readByteInt();
		this._mobId		= reader.readShort();
	}
}