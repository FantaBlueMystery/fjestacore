/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

/**
 * CharacterState
 * @author FantaBlueMystery
 */
public enum CharacterState {
	NONE(0),
	PLAYER(1),
	PLAYER2(2),
	DEAD(3),
	CAMP(4),
	VENDOR(5),
	RIDE(6);

	private int _value;

	/**
	 * CharacterState
	 * @param value
	 */
	private CharacterState(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getCharacterState
	 * @param state
	 * @return
	 */
	static public CharacterState getCharacterState(int state) {
		CharacterState[] states = CharacterState.values();

		for( CharacterState s: states ) {
			if( s.getValue() == state ) {
				return s;
			}
		}

		return CharacterState.NONE;
	}
}