/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.common.login.CharLogin;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharReviveOther
 * @author FantaBlueMystery
 */
public class CharReviveOther extends AbstractStruct {

	/**
	 * char revive same
	 */
	protected CharReviveSame _crs = new CharReviveSame();

	/**
	 * char login
	 */
	protected CharLogin _cl = new CharLogin();

	/**
	 * world manager handle
	 */
	protected int _wldmanhandle = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 30;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._crs.read(reader);
		this._cl.read(reader);
		this._wldmanhandle = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._crs.write(writer);
		this._cl.write(writer);
		writer.writeShort(this._wldmanhandle);
	}
}