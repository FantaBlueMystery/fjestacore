/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharStatVar
 * @author FantaBlueMystery
 */
public class CharStatVar extends AbstractStruct {

	/**
	 * conts
	 */
	static public int SIZE = 8;

	/**
	 * base
	 */
	protected long _base = 0;

	/**
	 * change
	 */
	protected long _change = 0;

	/**
	 * CharStatVar
	 */
	public CharStatVar() {}

	/**
	 * CharStatVar
	 * @param base
	 * @param change
	 */
	public CharStatVar(long base, long change) {
		this._base		= base;
		this._change	= change;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharStatVar.SIZE;
	}

	/**
	 * getBase
	 * @return
	 */
	public long getBase() {
		return this._base;
	}

	/**
	 * setBase
	 * @param base
	 */
	public void setBase(long base) {
		this._base = base;
	}

	/**
	 * getChange
	 * @return
	 */
	public long getChange() {
		return this._change;
	}

	/**
	 * setChange
	 * @param change
	 */
	public void setChange(int change) {
		this._change = change;
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._base);
		writer.writeUInt(this._change);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._base = reader.readUInt();
		this._change = reader.readUInt();
	}
}