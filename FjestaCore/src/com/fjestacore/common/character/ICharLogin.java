/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

/**
 * ICharLogin
 * @author FantaBlueMystery
 */
public interface ICharLogin {

	/**
	 * getServerIp
	 * @return
	 */
	public String getServerIp();

	/**
	 * getServerPort
	 * @return
	 */
	public int getServerPort();
}