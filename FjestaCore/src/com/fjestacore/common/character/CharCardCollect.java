/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.character;

import com.fjestacore.common.card.CardInform;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * CharCardCollect
 * @author FantaBlueMystery
 */
public class CharCardCollect extends AbstractStruct {

	/**
	 * char reg num
	 */
	protected long _chrregnum = 0;

	/**
	 * card list
	 */
	protected ArrayList<CardInform> _cardList = new ArrayList<>();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + 2 + (this._cardList.size() * CardInform.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chrregnum = reader.readUInt();

		int number = reader.readShort();

		for( int i=0; i<number; i++ ) {
			CardInform ci = new CardInform();
			ci.read(reader);

			this._cardList.add(ci);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._chrregnum);
		writer.writeShort(this._cardList.size());

		for( CardInform ci: this._cardList ) {
			ci.write(writer);
		}
	}
}
