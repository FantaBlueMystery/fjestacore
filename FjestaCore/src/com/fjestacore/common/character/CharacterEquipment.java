/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.common.inventory.Inventory;
import com.fjestacore.common.inventory.InventoryLocation;
import com.fjestacore.common.item.Item;
import com.fjestacore.common.item.ItemEquip;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * CharacterEquipment
 * @author FantaBlueMystery
 */
public class CharacterEquipment extends AbstractStruct implements IAbstractStructDump {

	/**
	 * item equip list
	 */
	static public ItemEquip[] ItemEquipList = new ItemEquip[] {
		ItemEquip.ITEMEQUIP_HAT,
		ItemEquip.ITEMEQUIP_MOUTH,
		ItemEquip.ITEMEQUIP_RIGHTHAND,
		ItemEquip.ITEMEQUIP_BODY,
		ItemEquip.ITEMEQUIP_LEFTHAND,
		ItemEquip.ITEMEQUIP_LEG,
		ItemEquip.ITEMEQUIP_SHOES,
		ItemEquip.ITEMEQUIP_SHOESACC,
		ItemEquip.ITEMEQUIP_LEGACC,
		ItemEquip.ITEMEQUIP_BODYACC,
		ItemEquip.ITEMEQUIP_HATACC,
		ItemEquip.ITEMEQUIP_MINIMON_R,
		ItemEquip.ITEMEQUIP_EYE,
		ItemEquip.ITEMEQUIP_LEFTHANDACC,
		ItemEquip.ITEMEQUIP_RIGHTHANDACC,
		ItemEquip.ITEMEQUIP_BACK,
		ItemEquip.ITEMEQUIP_COSEFF,
		ItemEquip.ITEMEQUIP_TAIL,
		ItemEquip.ITEMEQUIP_MINIMON,
		ItemEquip.ITEMEQUIP_SHIELDACC
		};

	/**
	 * equipments
	 */
	protected Map<ItemEquip, Integer> _equipments = new HashMap<>();

	/**
	 * upgrade
	 */
	protected CharacterEquipmentUpgrade _upgrade = new CharacterEquipmentUpgrade();

	/**
	 * CharacterEquipment
	 */
	public CharacterEquipment() {}

	/**
	 * CharacterEquipment
	 * @param inv
	 */
	public CharacterEquipment(Inventory inv) {
		Map<Integer, Item> items = inv.getItems();

		for( Map.Entry<Integer, Item> entry: items.entrySet() ) {
			int location = entry.getKey();

			int index = location - 8192;

			if( (index > 0) && (index < CharacterEquipment.ItemEquipList.length) ) {
				this._equipments.put(CharacterEquipment.ItemEquipList[index], entry.getValue().getId());
			}
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 43;
	}

	/**
	 * _id
	 * @param id
	 * @return
	 */
	protected int _id(int id) {
		if( id == 0 ) {
			return Item.ITEM_ID_MAX;
		}

		return id;
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		for( ItemEquip ie: CharacterEquipment.ItemEquipList ) {
			Integer id = this._equipments.get(ie);

			if( id == null ) {
				id = Item.ITEM_ID_MAX;
			}

			writer.writeShort(id);
		}

		this._upgrade.write(writer);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		for( ItemEquip ie: CharacterEquipment.ItemEquipList ) {
			this._equipments.put(ie, reader.readShort());
		}

		this._upgrade.read(reader);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String msg = "CharacterEquipment: [";

		for( ItemEquip ie: CharacterEquipment.ItemEquipList ) {
			Integer id = this._equipments.get(ie);

			if( id == null ) {
				id = Item.ITEM_ID_MAX;
			}

			msg += " " + ie.name() + ": " + Integer.toString(id);
		}

		msg += "]";

		String dump = MsgDump.getMsgDump(deep, msg);



		return dump;
	}
}