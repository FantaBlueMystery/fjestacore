/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

/**
 * CharacterClass
 * @author FantaBlueMystery
 */
public enum CharacterClass {
	FIGHTER(1),
	CLEVER_FIGHTER(2),
	WARRIOR(3),
	GLADIATOR(4),
	KNIGHT(5),

	CLERIC(6),
	HIGH_CLERIC(7),
	PALADIN(8),
	HOLY_KNIGHT(9),
	GUARDIAN(10),

	ARCHER(11),
	HAWK_ARCHER(12),
	SCOUT(13),
	SHARP_SHOOTER(14),
	RANGER(15),

	MAGE(16),
	WIZ_MAGE(17),
	ENCHANTER(18),
	WARLOCK(19),
	WIZARD(20),

	TRICKSTER(21),
	GAMBIT(22),
	RENEGADE(23),
	SPECTRE(24),
	REAPER(25),

    CRUSADER(26),
    TEMPLAR(27),

	NONE(255)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CharacterClass
	 * @param value
	 */
	private CharacterClass(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToCharacterClass
	 * @param value
	 * @return
	 */
	static public CharacterClass intToCharacterClass(int value) {
		CharacterClass[] classes = CharacterClass.values();

		for( CharacterClass tclass : classes ) {
			if( tclass.getValue() == value ) {
				return tclass;
			}
		}

		return CharacterClass.NONE;
	}
}