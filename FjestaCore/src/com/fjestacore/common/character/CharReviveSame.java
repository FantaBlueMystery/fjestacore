/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharReviveSame
 * @author FantaBlueMystery
 */
public class CharReviveSame extends AbstractStruct {

	/**
	 * map id
	 */
	protected int _mapId = 0;

	/**
	 * position xy
	 */
	protected PositionXY _pos = new PositionXY();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 10;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._mapId = reader.readShort();
		this._pos.read(reader);

	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._mapId);
		this._pos.write(writer);
	}
}