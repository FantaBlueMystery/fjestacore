/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * CharacterLook
 * @author FantaBlueMystery
 */
public class CharacterLook extends AbstractStruct implements IAbstractStructDump {

	/**
	 * is maile
	 */
	protected boolean _isMale = false;

	/**
	 * class
	 */
	protected CharacterClass _class = CharacterClass.NONE;

	/**
	 * hair
	 */
	protected int _hair = 0;

	/**
	 * hair color
	 */
	protected int _hairColor = 0;

	/**
	 * face
	 */
	protected int _face = 0;

	/**
	 * CharacterLook
	 */
	public CharacterLook() {}

	/**
	 * isMale
	 * @return
	 */
	public boolean isMale() {
		return this._isMale;
	}

	/**
	 * setMale
	 * @param male
	 */
	public void setMale(boolean male) {
		this._isMale = male;
	}

	/**
	 * getClassId
	 * @return
	 */
	public int getClassId() {
		return this._class.getValue();
	}

	/**
	 * setClassId
	 * @param classid
	 */
	public void setClassId(int classid) {
		this._class = CharacterClass.intToCharacterClass(classid);
	}

	/**
	 * getPlayerClass
	 * @return
	 */
	public CharacterClass getPlayerClass() {
		return this._class;
	}

	/**
	 * setPlayerClass
	 * @param pclass
	 */
	public void setPlayerClass(CharacterClass pclass) {
		this._class = pclass;
	}

	/**
	 * hair
	 * @return
	 */
	public int getHair() {
		return this._hair;
	}

	/**
	 * setHair
	 * @param hair
	 */
	public void setHair(int hair) {
		this._hair = hair;
	}

	/**
	 * getHairColor
	 * @return
	 */
	public int getHairColor() {
		return this._hairColor;
	}

	/**
	 * setHairColor
	 * @param color
	 */
	public void setHairColor(int color) {
		this._hairColor = color;
	}

	/**
	 * getFace
	 * @return
	 */
	public int getFace() {
		return this._face;
	}

	/**
	 * setFace
	 * @param face
	 */
	public void setFace(int face) {
		this._face = face;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		byte isMaleByte = 0x0;

		if( this._isMale ) {
			isMaleByte = 0x1;
		}

		Integer classId = this._class.getValue();
		byte classIdByte = classId.byteValue();

		writer.writeByte((byte) (0x01 | (classIdByte << 2) | (isMaleByte) << 7));	// +1

		writer.writeByteInt(this._hair);
		writer.writeByteInt(this._hairColor);
		writer.writeByteInt(this._face);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		byte jobGender	= reader.readByte();

		byte isMaleByte		= (byte)((jobGender >> 7) & 0x01);
		byte classIdByte	= (byte)((jobGender >> 2) & 0x1F);

		this._isMale	= (isMaleByte == 0x01);
		this._class	= CharacterClass.intToCharacterClass(Byte.toUnsignedInt(classIdByte));

		this._hair		= reader.readByteInt();
		this._hairColor = reader.readByteInt();
		this._face		= reader.readByteInt();
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"CharacterLook: [" +
			"isMale: " + (this._isMale ? "True" : "False") +
			" Class: " + this._class.name() +
			" Hair: " + Integer.toString(this._hair) +
			" HairColor: " + Integer.toString(this._hairColor) +
			" Face: " + Integer.toString(this._face) +
			"]"
			);

		return dump;
	}
}