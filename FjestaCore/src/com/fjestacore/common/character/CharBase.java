/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.character;

import com.fjestacore.common.coin.Coin;
import com.fjestacore.common.name.Name3;
import com.fjestacore.common.name.Name5;
import com.fjestacore.common.position.PositionXYR;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * CharBase
 * @author FantaBlueMystery
 */
public class CharBase extends AbstractStruct implements IAbstractStructDump, ICharacterHandle {

	/**
	 * SIZE
	 */
	static public int SIZE = 106;

	/**
	 * character handle
	 */
	protected long _characterHandle = 0;

	/**
	 * charname
	 */
	protected Name5 _charname = new Name5();

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * level
	 */
	protected int _level = 1;

	/**
	 * exp
	 */
	protected ULong _exp = ULong.MIN;

	/**
	 * stones
	 */
	protected CharStone _stones = new CharStone();

	/**
	 * character stats
	 */
	protected CharBars _bars = new CharBars();

	/**
	 * unknown (is new)
	 */
	protected int _unknown = 1;

	/**
	 * fame
	 */
	protected long _fame = 0;

	/**
	 * mapname
	 */
	protected Name3 _mapname = new Name3();

	/**
	 * money
	 */
	protected Coin _money = new Coin();

	/**
	 * position
	 */
	protected PositionXYR _pos = new PositionXYR();

	/**
	 * boni
	 */
	protected CharacterBoni _boni = new CharacterBoni();

	/**
	 * pkyellowtime
	 * player kill yellow time
	 */
	protected int _pkyellowtime = 0;

	/**
	 * player kill count
	 */
	protected long _pkcount = 0;

	/**
	 * admin level
	 */
	protected int _adminLevel = 150;	// TODO set 0

	/**
	 * prisonmin
	 */
	protected int _prisonmin = 0;

	/**
	 * flags
	 */
	protected long _flags = 0;

	/**
	 * getCharacterHandle
	 * @return
	 */
	@Override
	public long getCharacterHandle() {
		return this._characterHandle;
	}

	/**
	 * setCharacterHandle
	 * @param handle
	 */
	@Override
	public void setCharacterHandle(long handle) {
		this._characterHandle = handle;
	}

	/**
	 * getCharname
	 * @return
	 */
	public String getCharname() {
		return this._charname.getContent();
	}

	/**
	 * setCharname
	 * @param name
	 */
	public void setCharname(String name) {
		this._charname.setContent(name);
	}

	/**
	 * getSlot
	 * @return
	 */
	public int getSlot() {
		return this._slot;
	}

	/**
	 * setSlot
	 * @param slot
	 */
	public void setSlot(int slot) {
		this._slot = slot;
	}

	/**
	 * getLevel
	 * @return
	 */
	public int getLevel() {
		return this._level;
	}

	/**
	 * setLevel
	 * @param lvl
	 */
	public void setLevel(int lvl) {
		this._level = lvl;
	}

	/**
	 * getExp
	 * @return
	 */
	public ULong getExp() {
		return this._exp;
	}

	/**
	 * setExp
	 * @param exp
	 */
	public void setExp(ULong exp) {
		this._exp = exp;
	}

	/**
	 * getStones
	 * @return
	 */
	public CharStone getStones() {
		return this._stones;
	}

	/**
	 * setStones
	 * @param stones
	 */
	public void setStones(CharStone stones) {
		this._stones = stones;
	}

	/**
	 * getBars
	 * @return
	 */
	public CharBars getBars() {
		return this._bars;
	}

	/**
	 * setBars
	 * @param bars
	 */
	public void setBars(CharBars bars) {
		this._bars = bars;
	}

	/**
	 * getFame
	 * @return
	 */
	public long getFame() {
		return this._fame;
	}

	/**
	 * setFame
	 * @param fame
	 */
	public void setFame(long fame) {
		this._fame = fame;
	}

	/**
	 * getMapname
	 * @return
	 */
	public String getMapname() {
		return this._mapname.getContent();
	}

	/**
	 * setMapname
	 * @param name
	 */
	public void setMapname(String name) {
		this._mapname.setContent(name);
	}

	/**
	 * getMoney
	 * @return
	 */
	public Coin getMoney() {
		return this._money;
	}

	/**
	 * setMoney
	 * @param money
	 */
	public void setMoney(Coin money) {
		this._money = money;
	}

	/**
	 * getPosition
	 * @return
	 */
	public PositionXYR getPosition() {
		return this._pos;
	}

	/**
	 * setPosition
	 * @param pos
	 */
	public void setPosition(PositionXYR pos) {
		this._pos = pos;
	}

	/**
	 * getBoni
	 * @return
	 */
	public CharacterBoni getBoni() {
		return this._boni;
	}

	/**
	 * setBoni
	 * @param boni
	 */
	public void setBoni(CharacterBoni boni) {
		this._boni = boni;
	}

	/**
	 * getAdminLevel
	 * @return
	 */
	public int getAdminLevel() {
		return this._adminLevel;
	}

	/**
	 * setAdminLevel
	 * @param level
	 */
	public void setAdminLevel(int level) {
		this._adminLevel = level;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CharBase.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._characterHandle	= reader.readUInt();

		this._charname.read(reader);

		this._slot		= reader.readByteInt();
		this._level		= reader.readByteInt();
		this._exp		= reader.readULong();

		this._stones.read(reader);
		this._bars.read(reader);

		this._unknown = reader.readByteInt();

		this._fame = reader.readUInt();
		this._money.read(reader);
		this._mapname.read(reader);
		this._pos.read(reader);
		this._boni.read(reader);

		this._pkyellowtime	= reader.readByteInt();
		this._pkcount		= reader.readUInt();
		this._prisonmin		= reader.readShort();
		this._adminLevel	= reader.readByteInt();
		this._flags			= reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._characterHandle);			// +4

		this._charname.write(writer);						// +20

		writer.writeByteInt(this._slot);					// +1
		writer.writeByteInt(this._level);					// +1
		writer.writeULong(this._exp);						// +8

		this._stones.write(writer);				// +8
		this._bars.write(writer);				// +12

		// ?
		writer.writeByteInt(this._unknown);					// +1
		writer.writeUInt(this._fame);						// +4

		this._money.write(writer);							// +8
		this._mapname.write(writer);						// +12
		this._pos.write(writer);							// +9
		this._boni.write(writer);							// +6

		writer.writeByteInt(this._pkyellowtime);			// +1
		writer.writeUInt(this._pkcount);					// +4
		writer.writeShort(this._prisonmin);					// +2
		writer.writeByteInt(this._adminLevel);				// +1
		writer.writeUInt(this._flags);						// +4
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"CharBase: [" +
			"CharacterHandle: " + Long.toString(this._characterHandle) +
			" Charname: " + this._charname.getContent()+
			" Slot: " + Integer.toString(this._slot) +
			" Level: " + Integer.toString(this._level) +
			" Exp: " + this._exp.toString() +
			" Unknow: " + Integer.toString(this._unknown) +
			" Fame: " + Long.toString(this._fame) +
			" Money: [" + this._money.toStrFormat() + "]" +
			" MapName: " + this._mapname.getContent() +
			" PkYellowTime: " + Integer.toString(this._pkyellowtime) +
			" PkCount: " + Long.toString(this._pkcount) +
			" PrisonMin: " + Integer.toString(this._prisonmin) +
			" AdminLevel: " + Integer.toString(this._adminLevel) +
			" Flags: " + Long.toString(this._flags) +
			"]"
			);

		dump += this._pos.dump(deep+1);
		//dump += this._param.dump(deep+1);

		return dump;
	}
}