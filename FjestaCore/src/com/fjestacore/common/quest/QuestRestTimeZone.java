/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * QuestRestTimeZone
 * @author FantaBlueMystery
 */
public class QuestRestTimeZone extends AbstractStruct {

	/**
	 * n char no
	 */
	protected int _nCharNo = 0;

	/**
	 * t reset year quest
	 */
	protected int _tResetYearQuest = 0;

	/**
	 * t reset month quest
	 */
	protected int _tResetMonthQuest = 0;

	/**
	 * t reset week quest
	 */
	protected int _tResetWeekQuest = 0;

	/**
	 * t reset day quest
	 */
	protected int _tResetDayQuest = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 18;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nCharNo = reader.readShort();
		this._tResetYearQuest = reader.readInt();
		this._tResetMonthQuest = reader.readInt();
		this._tResetWeekQuest = reader.readInt();
		this._tResetDayQuest = reader.readInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nCharNo);
		writer.writeInt(this._tResetYearQuest);
		writer.writeInt(this._tResetMonthQuest);
		writer.writeInt(this._tResetWeekQuest);
		writer.writeInt(this._tResetDayQuest);
	}
}