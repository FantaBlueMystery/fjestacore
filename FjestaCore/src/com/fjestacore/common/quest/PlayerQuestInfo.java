/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * PlayerQuestInfo
 * @author FantaBlueMystery
 */
public class PlayerQuestInfo extends AbstractStruct {

	/**
	 * size
	 */
	static public int SIZE = 32;

	/**
	 * id
	 */
	protected int _id = 0;

	/**
	 * status
	 */
	protected int _status = 0;

	/**
	 * data
	 */
	protected PlayerQuestData _data = new PlayerQuestData();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return PlayerQuestInfo.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._id		= reader.readShort();
		this._status	= reader.readByteInt();

		this._data.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._id);
		writer.writeByteInt(this._status);

		this._data.write(writer);
	}
}