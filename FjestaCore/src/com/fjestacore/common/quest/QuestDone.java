/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * QuestDone
 * @author FantaBlueMystery
 */
public class QuestDone extends AbstractStruct {

	/**
	 * chr reg num
	 * char register number
	 */
	protected long _chrregnum = 0;

	/**
	 * totel done quest
	 */
	protected int _nTotalDoneQuest = 0;

	/**
	 * n total done quest size
	 */
	protected int _nTotalDoneQuestSize = 0;

	/**
	 * n done quest count
	 */
	protected int _nDoneQuestCount = 0;

	/**
	 * n index
	 */
	protected int _nIndex = 0;

	/**
	 * quest done array
	 */
	protected ArrayList<PlayerQuestDoneInfo> _questDoneArray = new ArrayList<>();

	/**
	 * QuestDone
	 */
	public QuestDone() {}

	/**
	 * QuestDone
	 * @param charid
	 */
	public QuestDone(long charid) {
		this._chrregnum = charid;
	}

	/**
	 * QuestDone
	 * @param character
	 */
	public QuestDone(ICharacterHandle character) {
		this._chrregnum = character.getCharacterHandle();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 12 + (this._questDoneArray.size() * PlayerQuestDoneInfo.SIZE);
	}

	/**
	 * read
	 * TODO
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chrregnum				= reader.readUInt();
		this._nTotalDoneQuest		= reader.readShort();
		this._nTotalDoneQuestSize	= reader.readShort();
		this._nDoneQuestCount		= reader.readShort();
		this._nIndex				= reader.readShort();

		for( int i=0; i<this._nDoneQuestCount; i++ ) {
			PlayerQuestDoneInfo pqdi = new PlayerQuestDoneInfo();
			pqdi.read(reader);

			this._questDoneArray.add(pqdi);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._chrregnum);
		writer.writeShort(this._nTotalDoneQuest);
		writer.writeShort(this._nTotalDoneQuestSize);
		writer.writeShort(this._questDoneArray.size());
		writer.writeShort(this._nIndex);

		for( PlayerQuestDoneInfo pqdi: this._questDoneArray ) {
			pqdi.write(writer);
		}
	}
}