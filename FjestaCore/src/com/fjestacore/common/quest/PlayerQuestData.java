/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * PlayerQuestData
 * @author FantaBlueMystery
 */
public class PlayerQuestData extends AbstractStruct {

	/**
	 * start time
	 */
	protected ULong _startTime = ULong.MIN;

	/**
	 * end time
	 */
	protected ULong _endTime = ULong.MIN;

	/**
	 * repeat count
	 */
	protected long _repeatCount = 0;

	/**
	 * progress step
	 */
	protected int _progressStep = 0;

	/**
	 * end NPC mob count
	 */
	protected int[] _end_NPCMobCount = new int[5];

	/**
	 * bf26
	 */
	protected int _bf26 = 0;

	/**
	 * end running time sec
	 */
	protected int _end_RunningTimeSec = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 29;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._startTime		= reader.readULong();	// +8
		this._endTime		= reader.readULong();	// +8
		this._repeatCount	= reader.readUInt();	// +4
		this._progressStep	= reader.readByteInt();	// +1

		// +5
		for( int i=0; i<this._end_NPCMobCount.length; i++ ) {
			this._end_NPCMobCount[i] = reader.readByteInt();
		}

		this._bf26					= reader.readByteInt();	// +1
		this._end_RunningTimeSec	= reader.readShort(); // +2
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeULong(this._startTime);
		writer.writeULong(this._endTime);
		writer.writeUInt(this._repeatCount);
		writer.writeByteInt(this._progressStep);

		for( int i=0; i<this._end_NPCMobCount.length; i++ ) {
			writer.writeByteInt(this._end_NPCMobCount[i]);
		}

		writer.writeByteInt(this._bf26);
		writer.writeShort(this._end_RunningTimeSec);
	}
}