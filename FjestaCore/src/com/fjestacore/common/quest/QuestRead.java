/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * QuestRead
 * @author FantaBlueMystery
 */
public class QuestRead extends AbstractStruct {

	/**
	 * chr reg num
	 * char register number
	 */
	protected long _chrregnum = 0;

	/**
	 * _questReadIDArray
	 */
	protected ArrayList<Integer> _questReadIDArray = new ArrayList<>();

	/**
	 * QuestRead
	 */
	public QuestRead() {}

	/**
	 * QuestRead
	 * @param charid
	 */
	public QuestRead(long charid) {
		this._chrregnum = charid;
	}

	/**
	 * QuestRead
	 * @param character
	 */
	public QuestRead(ICharacterHandle character) {
		this._chrregnum = character.getCharacterHandle();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6 + (this._questReadIDArray.size() * 2);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chrregnum	= reader.readUInt();

		int nNumOfReadQuest	= reader.readShort();

		for( int i=0; i<nNumOfReadQuest; i++ ) {
			this._questReadIDArray.add(reader.readShort());
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._chrregnum);
		writer.writeShort(this._questReadIDArray.size());

		for( Integer id: this._questReadIDArray ) {
			writer.writeShort(id);
		}
	}
}