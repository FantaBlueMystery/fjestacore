/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * QuestDoing
 * @author FantaBlueMystery
 */
public class QuestDoing extends AbstractStruct {

	/**
	 * chr reg num
	 * char register number
	 */
	protected long _chrregnum = 0;

	/**
	 * need clear
	 */
	protected int _bNeedClear = 0;

	/**
	 * quest doing array
	 */
	protected ArrayList<PlayerQuestInfo> _questDoingArray = new ArrayList<>();

	/**
	 * QuestDoing
	 */
	public QuestDoing() {}

	/**
	 * QuestDoing
	 * @param charId
	 */
	public QuestDoing(long charId) {
		this._chrregnum = charId;
	}

	/**
	 * QuestDoing
	 * @param character
	 */
	public QuestDoing(ICharacterHandle character) {
		this._chrregnum = character.getCharacterHandle();
	}

	/**
	 * setChrRegNum
	 * @param num
	 */
	public void setChrRegNum(long num) {
		this._chrregnum = num;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6 +(this._questDoingArray.size()*PlayerQuestInfo.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chrregnum		= reader.readUInt();
		this._bNeedClear	= reader.readByteInt();

		int nNumOfDoingQuest = reader.readByteInt();

		for( int i=0; i<nNumOfDoingQuest; i++ ) {
			PlayerQuestInfo pqi = new PlayerQuestInfo();
			pqi.read(reader);

			this._questDoingArray.add(pqi);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._chrregnum);
		writer.writeByteInt(this._bNeedClear);

		writer.writeByteInt(this._questDoingArray.size());

		for( PlayerQuestInfo pqi: this._questDoingArray ) {
			pqi.write(writer);
		}
	}
}