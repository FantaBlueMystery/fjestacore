/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * QuestRepeat
 * @author FantaBlueMystery
 */
public class QuestRepeat extends AbstractStruct {

	/**
	 * chr reg num
	 * char register number
	 */
	protected long _chrregnum = 0;

	/**
	 * quest repeat array
	 */
	protected ArrayList<PlayerQuestInfo> _questRepeatArray = new ArrayList<>();

	/**
	 * QuestRepeat
	 */
	public QuestRepeat() {}

	/**
	 * QuestRepeat
	 * @param charId
	 */
	public QuestRepeat(long charId) {
		this._chrregnum = charId;
	}

	/**
	 * QuestRepeat
	 * @param character
	 */
	public QuestRepeat(ICharacterHandle character) {
		this._chrregnum = character.getCharacterHandle();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + 2 + (this._questRepeatArray.size() * PlayerQuestInfo.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chrregnum = reader.readUInt();

		int nNumOfRepeatQuest = reader.readShort();

		for( int i=0; i<nNumOfRepeatQuest; i++ ) {
			PlayerQuestInfo pqi = new PlayerQuestInfo();
			pqi.read(reader);

			this._questRepeatArray.add(pqi);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._chrregnum);
		writer.writeShort(this._questRepeatArray.size());

		for( PlayerQuestInfo pqi: this._questRepeatArray ) {
			pqi.write(writer);
		}
	}
}