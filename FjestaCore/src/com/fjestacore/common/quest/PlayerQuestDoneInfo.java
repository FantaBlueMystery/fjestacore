/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.quest;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * PlayerQuestDoneInfo
 * @author FantaBlueMystery
 */
public class PlayerQuestDoneInfo extends AbstractStruct {

	/**
	 * size
	 */
	static public int SIZE = 10;

	/**
	 * id
	 */
	protected int _id = 0;

	/**
	 * end time
	 */
	protected ULong _endTime = ULong.MIN;

	/**
	 * getId
	 * @return
	 */
	public int getId() {
		return this._id;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return PlayerQuestDoneInfo.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._id = reader.readShort();
		this._endTime = reader.readULong();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._id);
		writer.writeULong(this._endTime);
	}
}