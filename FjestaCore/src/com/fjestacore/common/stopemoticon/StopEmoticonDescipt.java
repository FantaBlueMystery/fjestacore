/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.stopemoticon;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * StopemoticonDescipt
 * @author FantaBlueMystery
 */
public class StopEmoticonDescipt extends AbstractStruct {

	/**
	 * emoticon
	 */
	protected int _emoticonId = 0;

	/**
	 * emoticon frame
	 */
	protected int _emoticonFrame = 0;

	/**
	 * StopemotionDescipt
	 */
	public StopEmoticonDescipt() {}

	/**
	 * setEmoticonId
	 * @param id
	 */
	public void setEmoticonId(int id) {
		this._emoticonId = id;
	}

	/**
	 * getEmoticonId
	 */
	public int getEmoticonId() {
		return this._emoticonId;
	}

	/**
	 * setEmoticonFrame
	 * @param frame
	 */
	public void setEmoticonFrame(int frame) {
		this._emoticonFrame = frame;
	}

	/**
	 * getEmoticonFrame
	 * @return
	 */
	public int getEmoticonFrame() {
		return this._emoticonFrame;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._emoticonId	= reader.readByteInt();
		this._emoticonFrame = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._emoticonId);
		writer.writeShort(this._emoticonFrame);
	}
}