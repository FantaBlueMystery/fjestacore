/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.camp;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Camp
 * @author FantaBlueMystery
 */
public class Camp extends AbstractStruct {

	/**
	 * minihouse id
	 */
	protected int _minihouseId = 0;

	/**
	 * dummy ?
	 */
	protected byte[] _dummy = new byte[10];

	/**
	 * Camp
	 */
	public Camp() {}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._minihouseId = reader.readShort();
		this._dummy = reader.readBuffer(10);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 12;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._minihouseId);
		writer.writeBuffer(this._dummy);
	}
}