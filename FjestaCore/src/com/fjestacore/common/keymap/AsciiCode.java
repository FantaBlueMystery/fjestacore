/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.keymap;

/**
 * AsciiCode
 * @author FantaBlueMystery
 */
public enum AsciiCode {
	NUL(0),
	CR(13),
	Esc(27),
	SP(32),
	SH(35),
	DO(36),
	PE(37),
	AND(38),
	AB(39),
	CO(40),
	Nil(48),
	One(49),
	Two(50),
	Three(51),
	Four(52),
	Five(53),
	Six(54),
	Seven(55),
	Eight(56),
	Nine(57),
	A(65),
	B(66),
	C(67),
	D(68),
	E(69),
	F(70),
	G(71),
	H(72),
	I(73),
	K(75),
	L(76),
	M(77),
	N(78),
	P(80),
	Q(81),
	R(82),
	S(83),
	T(84),
	U(85),
	V(86),
	W(87),
	X(88),
	Z(90),
	y(121),
	DE(222);

	/**
	 * value
	 */
	private int _value;

	/**
	 * AsciiCode
	 * @param value
	 */
	private AsciiCode(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}