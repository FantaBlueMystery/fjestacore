/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.keymap;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * KeyMap
 * @author FantaBlueMystery
 */
public class KeyMap extends AbstractStruct {

	/**
	 * function num
	 */
	protected int _functionNum = 0;

	/**
	 * extend key
	 */
	protected int _extendKey = 0;

	/**
	 * ascii code
	 */
	protected int _asciiCode = 0;

	/**
	 * KeyMap
	 */
	public KeyMap() {}

	/**
	 * KeyMap
	 * @param functionNum
	 * @param asciiCode
	 */
	public KeyMap(int functionNum, int asciiCode) {
		this._functionNum = functionNum;
		this._asciiCode = asciiCode;
	}

	/**
	 * KeyMap
	 * @param functionNum
	 * @param asciiCode
	 */
	public KeyMap(int functionNum, AsciiCode asciiCode) {
		this._functionNum = functionNum;
		this._asciiCode = asciiCode.getValue();
	}

	/**
	 * KeyMap
	 * @param functionNum
	 * @param asciiCode
	 * @param extendKey
	 */
	public KeyMap(int functionNum, int asciiCode, int extendKey) {
		this._functionNum = functionNum;
		this._asciiCode = asciiCode;
		this._extendKey = extendKey;
	}

	/**
	 * KeyMap
	 * @param functionNum
	 * @param asciiCode
	 * @param extendKey
	 */
	public KeyMap(int functionNum, AsciiCode asciiCode, int extendKey) {
		this._functionNum = functionNum;
		this._asciiCode = asciiCode.getValue();
		this._extendKey = extendKey;
	}

	/**
	 * getFunctionNum
	 * @return
	 */
	public int getFunctionNum() {
		return this._functionNum;
	}

	/**
	 * setFunctionNum
	 * @param functionNum
	 */
	public void setFunctionNum(int functionNum) {
		this._functionNum = functionNum;
	}

	/**
	 * getExtendKey
	 * @return
	 */
	public int getExtendKey() {
		return this._extendKey;
	}

	/**
	 * setExtendKey
	 * @param extendKey
	 */
	public void setExtendKey(int extendKey) {
		this._extendKey = extendKey;
	}

	/**
	 * getAsciiCode
	 * @return
	 */
	public int getAsciiCode() {
		return this._asciiCode;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._functionNum	= reader.readShort();
		this._extendKey		= reader.readByteInt();
		this._asciiCode		= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((short) this._functionNum);
		writer.writeByteInt(this._extendKey);
		writer.writeByteInt(this._asciiCode);
	}
}