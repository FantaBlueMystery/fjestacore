/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.keymap;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * KeyMapList
 * @author FantaBlueMystery
 */
public class KeyMapList extends AbstractStruct {

	/**
	 * keymaps
	 */
	protected ArrayList<KeyMap> _list = new ArrayList<>();

	/**
	 * KeyMapList
	 */
	public KeyMapList() {}

	/**
	 * getList
	 * @return
	 */
	public ArrayList<KeyMap> getList() {
		return this._list;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int count = reader.readShort();

		for( int i=0; i<count; i++ ) {
			KeyMap km = new KeyMap();
			km.read(reader);

			this._list.add(km);
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (this._list.size()*4);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._list.size());

		for( KeyMap km: this._list ) {
			km.write(writer);
		}
	}
}