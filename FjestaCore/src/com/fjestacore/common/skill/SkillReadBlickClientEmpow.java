/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.skill;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SkillReadBlickClientEmpow
 * @author FantaBlueMystery
 */
public class SkillReadBlickClientEmpow extends AbstractStruct {

	/**
	 * TODO _BYTE gap0[1]; orginal is a array
	 */
	protected int _gap0 = 0;

	/**
	 * bf1
	 */
	protected int _bf1 = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._gap0	= reader.readByteInt();
		this._bf1	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._gap0);
		writer.writeByteInt(this._bf1);
	}
}