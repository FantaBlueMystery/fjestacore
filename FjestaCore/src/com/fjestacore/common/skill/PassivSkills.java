/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.skill;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * PassivSkills
 * @author FantaBlueMystery
 */
public class PassivSkills extends AbstractStruct {

	/**
	 * passive
	 */
	protected ArrayList<Integer> _passive = new ArrayList<>();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (2 * this._passive.size());
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int number = reader.readShort();

		for( int i=0; i<number; i++ ) {
			this._passive.add(reader.readShort());
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._passive.size());

		for( Integer passiv: this._passive ) {
			writer.writeShort(passiv);
		}
	}
}