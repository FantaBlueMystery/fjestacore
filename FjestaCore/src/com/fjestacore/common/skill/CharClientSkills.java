/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.skill;

import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharClientSkills
 * @author FantaBlueMystery
 */
public class CharClientSkills extends AbstractStruct {

	/**
	 * restempow
	 */
	protected int _restempow = 0;

	/**
	 * part mark
	 */
	protected int _partMark = 0;

	/**
	 * n max num?
	 */
	protected int _nMaxNum = 0;

	/**
	 * skills
	 */
	protected CharSkills _skills;

	/**
	 * CharClientSkill
	 */
	public CharClientSkills() {
		this._skills = new CharSkills();
	}

	/**
	 * CharClientSkills
	 * @param charid
	 */
	public CharClientSkills(long charid) {
		this._skills = new CharSkills(charid);
	}

	/**
	 * CharClientSkills
	 * @param character
	 */
	public CharClientSkills(ICharacterHandle character) {
		this._skills = new CharSkills(character.getCharacterHandle());
	}

	/**
	 * getCharSkills
	 * @return
	 */
	public CharSkills getCharSkills() {
		return this._skills;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + this._skills.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._restempow = reader.readByteInt();
		this._partMark = reader.readByteInt();
		this._nMaxNum = reader.readShort();
		this._skills.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._restempow);
		writer.writeByteInt(this._partMark);
		writer.writeShort(this._nMaxNum);
		this._skills.write(writer);
	}
}