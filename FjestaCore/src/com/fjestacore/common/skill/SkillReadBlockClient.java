/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.skill;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SkillReadBlockClient
 * @author FantaBlueMystery
 */
public class SkillReadBlockClient extends AbstractStruct {

	/**
	 * SkillReadBlockClient
	 */
	static public int SIZE = 12;

	/**
	 * skill id
	 */
	protected int _skillid = 0;

	/**
	 * cooltime
	 */
	protected long _cooltime = 0;

	/**
	 * empow
	 */
	protected SkillReadBlickClientEmpow _empow = new SkillReadBlickClientEmpow();

	/**
	 * mastery
	 */
	protected long _mastery = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return SkillReadBlockClient.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._skillid	= reader.readShort();
		this._cooltime	= reader.readUInt();
		this._empow.read(reader);
		this._mastery	= reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._skillid);
		writer.writeUInt(this._cooltime);
		this._empow.write(writer);
		writer.writeUInt(this._mastery);
	}
}