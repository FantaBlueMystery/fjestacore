/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.skill;

import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CharSkills
 * @author FantaBlueMystery
 */
public class CharSkills extends AbstractStruct {

	/**
	 * chrregnum
	 */
	protected long _chrregnum = 0;

	/**
	 * number
	 */
	protected int _number = 0;

	/**
	 * skill
	 */
	protected SkillReadBlockClient[] _skill = new SkillReadBlockClient[0];

	/**
	 * CharSkills
	 */
	protected CharSkills() {}

	/**
	 * CharSkills
	 * @param charid
	 */
	public CharSkills(long charid) {
		this._chrregnum = charid;
	}

	/**
	 * setCharRegnum
	 * @param charregnum
	 */
	public void setCharRegnum(long charregnum) {
		this._chrregnum = charregnum;
	}

	/**
	 * setCharRegnum
	 * @param character
	 */
	public void setCharRegnum(ICharacterHandle character) {
		this._chrregnum = character.getCharacterHandle();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6 + (this._skill.length * SkillReadBlockClient.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chrregnum = reader.readUInt();
		this._number = reader.readShort();

		this._skill = new SkillReadBlockClient[this._number];

		for( int i=0; i<this._skill.length; i++ ) {
			this._skill[i] = new SkillReadBlockClient();
			this._skill[i].read(reader);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._chrregnum);
		writer.writeShort(this._number);

		for( SkillReadBlockClient askill: this._skill ) {
			askill.write(writer);
		}
	}
}