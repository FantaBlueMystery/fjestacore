/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.briefinfo;

import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Unequip
 * @author FantaBlueMystery
 */
public class Unequip extends AbstractStruct implements IObjectHandle {

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._handle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * getSlot
	 * @return
	 */
	public int getSlot() {
		return this._slot;
	}

	/**
	 * setSlot
	 * @param slot
	 */
	public void setSlot(int slot) {
		this._slot = slot;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle = reader.readShort();
		this._slot = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);
		writer.writeByteInt(this._slot);
	}
}