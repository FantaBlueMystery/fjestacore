/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.briefinfo;

import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * BriefInfoDelete
 * @author FantaBlueMystery
 */
public class BriefInfoDelete extends AbstractStruct implements IObjectHandle {

	/**
	 * object id
	 */
	protected int _objectId = 0;

	/**
	 * BriefInfoDelete
	 */
	public BriefInfoDelete() {}

	/**
	 * BriefInfoDelete
	 * @param objectId
	 */
	public BriefInfoDelete(int objectId) {
		this._objectId = objectId;
	}

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._objectId;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._objectId = handle;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._objectId = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._objectId);
	}
}