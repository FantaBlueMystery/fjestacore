/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.briefinfo;

/**
 * DropedItemState
 * @author FantaBlueMystery
 */
public enum DropedItemState {
	NOLOOT(0),
	CANLOOT(8);

	/**
	 * value
	 */
	private int _value;

	/**
	 * DropedItemState
	 * @param value
	 */
	private DropedItemState(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getDropItemState
	 * @param value
	 * @return
	 */
	static public DropedItemState getDropItemState(int value) {
		DropedItemState[] flags = DropedItemState.values();

		for( DropedItemState type: flags ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return DropedItemState.NOLOOT;
	}
}
