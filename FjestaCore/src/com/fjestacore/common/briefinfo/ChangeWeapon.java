/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.briefinfo;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ChangeWeapon
 * @author FantaBlueMystery
 */
public class ChangeWeapon extends AbstractStruct {

	/**
	 * upgrade info
	 */
	protected ChangeUpgrade _upgradeinfo = new ChangeUpgrade();

	/**
	 * current mob id
	 */
	protected int _currentmobid = 0;

	/**
	 * current kill level
	 */
	protected int _currentkilllevel = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._upgradeinfo.getSize() + 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._upgradeinfo.read(reader);
		this._currentmobid		= reader.readShort();
		this._currentkilllevel	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._upgradeinfo.write(writer);
		writer.writeShort(this._currentmobid);
		writer.writeByteInt(this._currentkilllevel);
	}
}