/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.briefinfo;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.common.position.PositionXYR;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * DropedItem
 * @author FantaBlueMystery
 */
public class DropedItem extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * SIZE
	 */
	static public int SIZE = 15;

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * item id
	 */
	protected int _itemid = 0;

	/**
	 * location
	 */
	protected PositionXY _location = new PositionXY();

	/**
	 * drop mob handle
	 */
	protected int _dropmobhandle = 0;

	/**
	 * attr
	 */
	protected DropedItemState _attr = DropedItemState.NOLOOT;

	/**
	 * DropedItem
	 */
	public DropedItem() {}

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._handle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * getItemId
	 * @return
	 */
	public int getItemId() {
		return this._itemid;
	}

	/**
	 * setItemId
	 * @param id
	 */
	public void setItemId(int id) {
		this._itemid = id;
	}

	/**
	 * getLocation
	 * @return
	 */
	public PositionXY getLocation() {
		return this._location;
	}

	/**
	 * setLocation
	 * @param location
	 */
	public void setLocation(PositionXY location) {
		this._location = location;
	}

	/**
	 * getDropMobHandle
	 * @return
	 */
	public int getDropMobHandle() {
		return this._dropmobhandle;
	}

	/**
	 * setDropMobHandle
	 * @param handle
	 */
	public void setDropMobHandle(int handle) {
		this._dropmobhandle = handle;
	}

	/**
	 * getAttr
	 * @return
	 */
	public DropedItemState getAttr() {
		return this._attr;
	}

	/**
	 * setAttr
	 * @param attr
	 */
	public void setAttr(DropedItemState attr) {
		this._attr = attr;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return DropedItem.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle = reader.readShort();
		this._itemid = reader.readShort();
		this._location.read(reader);
		this._dropmobhandle = reader.readShort();
		this._attr = DropedItemState.getDropItemState(reader.readByteInt());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);
		writer.writeShort(this._itemid);

		if( this._location instanceof PositionXYR ) {
			((PositionXYR) this._location).getPositionXY().write(writer);
		}
		else {
			this._location.write(writer);
		}

		writer.writeShort(this._dropmobhandle);
		writer.writeByteInt(this._attr.getValue());
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"DropedItem: [" +
			" Handle: " + Integer.toString(this._handle) +
			" ItemId: " + Integer.toString(this._itemid) +
			" Location-X: " + Integer.toString(this._location.getX()) +
			" Location-Y: " + Integer.toString(this._location.getY()) +
			" DropMobHandle: " + Integer.toString(this._dropmobhandle) +
			" Attr: " + Integer.toString(this._attr.getValue()) +
			"]");

		return dump;
	}
}