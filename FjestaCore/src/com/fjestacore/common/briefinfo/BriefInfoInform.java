/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.briefinfo;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.net.NetCommand;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * BriefInfoInform
 * @author FantaBlueMystery
 */
public class BriefInfoInform extends AbstractStruct implements IAbstractStructDump {

	/**
	 * my hnd
	 */
	protected int _nMyHnd = 0;

	/**
	 * receive net command
	 */
	protected NetCommand _receiveNetCommand = new NetCommand();

	/**
	 * hnd
	 */
	protected int _hnd = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nMyHnd = reader.readShort();
		this._receiveNetCommand.read(reader);
		this._hnd = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nMyHnd);
		this._receiveNetCommand.write(writer);
		writer.writeShort(this._hnd);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"BriefInfoInform: [" +
			" MyHnd: " + Integer.toString(this._nMyHnd) +
			" receiveNetCommand: " + Integer.toString(this._receiveNetCommand.getProtocol()) +
			" hnd: " + Integer.toString(this._hnd) +
			"]"
			);

		return dump;
	}
}