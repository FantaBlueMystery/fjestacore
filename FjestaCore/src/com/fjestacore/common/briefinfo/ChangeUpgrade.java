/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.briefinfo;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * ChangeUpgrade
 * @author FantaBlueMystery
 */
public class ChangeUpgrade extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * item
	 */
	protected int _item = 0;

	/**
	 * upgrade
	 */
	protected int _upgrade = 0;

	/**
	 * n slot num
	 */
	protected int _nSlotNum = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._handle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle	= reader.readShort();
		this._item		= reader.readShort();
		this._upgrade	= reader.readByteInt();
		this._nSlotNum	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);
		writer.writeShort(this._item);
		writer.writeByteInt(this._upgrade);
		writer.writeByteInt(this._nSlotNum);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ChangeUpgrade: [" +
			" handle: " + Integer.toString(this._handle) +
			" item: " + Integer.toString(this._item) +
			" upgrade: " + Integer.toString(this._upgrade) +
			" nSlotNum: " + Integer.toString(this._nSlotNum) +
			"]"
			);

		return dump;
	}
}