/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.briefinfo;

import com.fjestacore.common.abstate.AbnormalStateBit;
import com.fjestacore.common.animation.SAnimation;
import com.fjestacore.common.booth.Booth;
import com.fjestacore.common.camp.Camp;
import com.fjestacore.common.character.CharBars;
import com.fjestacore.common.character.CharTitleBrief;
import com.fjestacore.common.character.CharacterClass;
import com.fjestacore.common.character.CharacterEquipment;
import com.fjestacore.common.character.CharacterLook;
import com.fjestacore.common.character.CharacterState;
import com.fjestacore.common.kq.KQTeamType;
import com.fjestacore.common.name.Name5;
import com.fjestacore.common.position.PositionXY;
import com.fjestacore.common.position.PositionXYR;
import com.fjestacore.common.ride.RideInfo;
import com.fjestacore.common.stopemoticon.StopEmoticonDescipt;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * LoginCharacter
 * @author FantaBlueMystery
 */
public class LoginCharacter extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * SIZE
	 */
	static public int SIZE = 248;

	/**
	 * map object id
	 */
	protected int _objectHandle = 0;

	/**
	 * name
	 */
	protected Name5 _name = new Name5();

	/**
	 * position
	 */
	protected PositionXYR _position = new PositionXYR();

	/**
	 * state
	 */
	protected CharacterState _state = CharacterState.NONE;

	/**
	 * char class
	 */
	protected CharacterClass _charClass = CharacterClass.NONE;

	/**
	 * look
	 */
	protected CharacterLook _look = new CharacterLook();

	/**
	 * equipment
	 */
	protected CharacterEquipment _equipment = new CharacterEquipment();

	/**
	 * ride
	 */
	protected RideInfo _ride = null;

	/**
	 * camp
	 */
	protected Camp _camp = null;

	/**
	 * booth
	 */
	protected Booth _booth = null;

	/**
	 * polymorph
	 * default = 65535
	 */
	protected int _polymorph = 65535;

	/**
	 * emoticon
	 */
	protected StopEmoticonDescipt _emoticon = new StopEmoticonDescipt();

	/**
	 * title
	 */
	protected CharTitleBrief _title = new CharTitleBrief();

	/**
	 * abstatebit
	 */
	protected AbnormalStateBit _abstatebit = new AbnormalStateBit();

	/**
	 * guild id
	 */
	protected int _guildId = 0;

	/**
	 * type
	 */
	protected int _type = 0;

	/**
	 * guild academy member
	 */
	protected Boolean _isGuildAcademyMember = false;

	/**
	 * is auto pick
	 */
	protected Boolean _isAutoPick = false;

	/**
	 * level
	 */
	protected int _level = 1;

	/**
	 * sanimation
	 */
	protected SAnimation _sanimation = new SAnimation();

	/**
	 * bars
	 */
	protected CharBars _bars = new CharBars();

	/**
	 * n mover hnd
	 */
	protected int _nMoverHnd = 0;

	/**
	 * n mover slot
	 */
	protected int _nMoverSlot = 0;

	/**
	 * n KQ TeamType
	 */
	protected KQTeamType _nKQTeamType = KQTeamType.KQTT_MAX;

	/**
	 * is use item minimon
	 */
	protected boolean _isUseItemMinimon = false;

	/**
	 * unknown ?
	 */
	protected int _unknown = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._objectHandle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._objectHandle = handle;
	}

	/**
	 * getName
	 * @return
	 */
	public String getName() {
		return this._name.getContent();
	}

	/**
	 * setName
	 * @param name
	 */
	public void setName(Name5 name) {
		this._name = name;
	}

	/**
	 * setName
	 * @param name
	 */
	public void setName(String name) {
		this._name.setContent(name);
	}

	/**
	 * getPosition
	 * @return
	 */
	public PositionXYR getPosition() {
		return this._position;
	}

	/**
	 * setPosition
	 * @param pos
	 */
	public void setPosition(PositionXYR pos) {
		this._position = pos;
	}

	/**
	 * setPosition
	 * @param pos
	 */
	public void setPosition(PositionXY pos) {
		this._position.setX(pos.getX());
		this._position.setY(pos.getY());
	}

	/**
	 * getState
	 * @return
	 */
	public CharacterState getState() {
		return this._state;
	}

	/**
	 * setState
	 * @param state
	 */
	public void setState(CharacterState state) {
		this._state = state;
	}

	/**
	 * getCharClass
	 * @return
	 */
	public CharacterClass getCharClass() {
		return this._charClass;
	}

	/**
	 * setCharClass
	 * @param charClass
	 */
	public void setCharClass(CharacterClass charClass) {
		this._charClass = charClass;
	}

	/**
	 * getLook
	 * @return
	 */
	public CharacterLook getLook() {
		return this._look;
	}

	/**
	 * setLook
	 * @param look
	 */
	public void setLook(CharacterLook look) {
		this._look = look;
	}

	/**
	 * getEquipment
	 * @return
	 */
	public CharacterEquipment getEquipment() {
		return this._equipment;
	}

	/**
	 * setEquipment
	 * @param equipment
	 */
	public void setEquipment(CharacterEquipment equipment) {
		this._equipment = equipment;
	}

	/**
	 * getRideInfo
	 * @return
	 */
	public RideInfo getRideInfo() {
		return this._ride;
	}

	/**
	 * setRideInfo
	 * @param info
	 */
	public void setRideInfo(RideInfo info) {
		this._ride = info;
	}

	/**
	 * getCamp
	 * @return
	 */
	public Camp getCamp() {
		return this._camp;
	}

	/**
	 * getLevel
	 * @return
	 */
	public int getLevel() {
		return this._level;
	}

	/**
	 * setLevel
	 * @param level
	 */
	public void setLevel(int level) {
		this._level = level;
	}

	/**
	 * getPolymorph
	 * @return
	 */
	public int getPolymorph() {
		return this._polymorph;
	}

	/**
	 * setPolymorph
	 * @param polymorph
	 */
	public void setPolymorph(int polymorph) {
		this._polymorph = polymorph;
	}

	/**
	 * getGuildId
	 * @return
	 */
	public int getGuildId() {
		return this._guildId;
	}

	/**
	 * setGuildId
	 * @param guildId
	 */
	public void setGuildId(int guildId) {
		this._guildId = guildId;
	}

	/**
	 * isGuildAcademyMember
	 * @return
	 */
	public Boolean isGuildAcademyMember() {
		return this._isGuildAcademyMember;
	}

	/**
	 * setIsGuildAcademyMember
	 * @param is
	 */
	public void setIsGuildAcademyMember(boolean is) {
		this._isGuildAcademyMember = is;
	}

	/**
	 * getBars
	 * @return
	 */
	public CharBars getBars() {
		return this._bars;
	}

	/**
	 * setBars
	 * @param bars
	 */
	public void setBars(CharBars bars) {
		this._bars = bars;
	}

	/**
	 * getEmoticon
	 * @return
	 */
	public StopEmoticonDescipt getEmoticon() {
		return this._emoticon;
	}

	/**
	 * setEmoticon
	 * @param emoticon
	 */
	public void setEmoticon(StopEmoticonDescipt emoticon) {
		this._emoticon = emoticon;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._objectHandle	= reader.readShort();	// +2

		if( this._objectHandle < 1 ) {
			throw new IOException("None positiv map object id (" + Long.toString(this._objectHandle) +  ") is read!");
		}

		this._name.read(reader);				// +20
		this._position.read(reader);			// +9

		this._state		= CharacterState.getCharacterState(reader.readByteInt());		// +1
		this._charClass = CharacterClass.intToCharacterClass(reader.readByteInt());		// +1

		this._look.read(reader);	// + 4

		this._camp = null;
		this._booth = null;

		switch( this._state ) {
			case PLAYER:
			case PLAYER2:
			case DEAD:
				this._equipment.read(reader);	// +43

				// read padding
				reader.readBuffer(2);			// +2
				break;

			case RIDE:
				this._equipment.read(reader);

				this._ride = new RideInfo();
				this._ride.read(reader);

				break;

			case CAMP:
				this._camp = new Camp();
				this._camp.read(reader);

				// read padding
				reader.readBuffer(45-this._camp.getSize());

				break;

			case VENDOR:
				this._booth = new Booth();
				this._booth.read(reader);

				// read padding
				reader.readBuffer(45-this._booth.getSize());
				break;
		}

		this._polymorph = reader.readShort();	// +2

		this._emoticon.read(reader);			// +3
		this._title.read(reader);				// +4
		this._abstatebit.read(reader);

		this._guildId				= (int) reader.readUInt();	// myGuild	// +4
		this._type					= reader.readByteInt();		// Type	// +1
		this._isGuildAcademyMember	= reader.readBoolean();		// isGuildAcademyMember	// +1
		this._isAutoPick			= reader.readBoolean();		// isAutoPick	// +1
		this._level					= reader.readByteInt();		// level	// +1

		this._sanimation.read(reader);		// sAnimation	// +32

		this._nMoverHnd				= reader.readShort();		// nMoverHnd	// +2
		this._nMoverSlot			= reader.readByteInt();		// nMoverSlot	// +1
		this._nKQTeamType			= KQTeamType.intToType(reader.readByteInt());	// +1
		this._isUseItemMinimon		= reader.readBoolean();		// IsUseItemMinimon	// +1
		this._unknown				= reader.readByteInt();		// ? use hat?	// +1
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return LoginCharacter.SIZE;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((int) this._objectHandle);

		this._name.write(writer);
		this._position.write(writer);

		writer.writeByteInt(this._state.getValue());
		writer.writeByteInt(this._charClass.getValue());

		this._look.write(writer);

		switch( this._state ) {
			case PLAYER:
			case PLAYER2:
			case DEAD:
				this._equipment.write(writer);

				writer.writeString("", 2);	// padding
				break;

			case RIDE:
				this._equipment.write(writer);
				this._ride.write(writer);
				break;

			case CAMP:
				this._camp.write(writer);

				writer.writeString("", 45-this._camp.getSize());	// padding
				break;

			case VENDOR:
				this._booth.write(writer);

				writer.writeString("", 45-this._booth.getSize());	// padding
		}

		writer.writeShort(this._polymorph);

		this._emoticon.write(writer);
		this._title.write(writer);
		this._abstatebit.write(writer);

		writer.writeUInt(this._guildId);
		writer.writeByteInt(this._type);
		writer.writeBoolean(this._isGuildAcademyMember);
		writer.writeBoolean(this._isAutoPick);
		writer.writeByteInt(this._level);

		this._sanimation.write(writer);

		writer.writeShort(this._nMoverHnd);
		writer.writeByteInt(this._nMoverSlot);
		writer.writeByteInt(this._nKQTeamType.getValue());
		writer.writeBoolean(this._isUseItemMinimon);
		writer.writeByteInt(this._unknown);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"CharacterObject: [" +
			"Handle: " + Integer.toString((int) this._objectHandle)
			+ " Name: " + this._name.getContent()
			+ " Position: X: " + Integer.toString(this._position.getX())
			+ " Position: Y: " + Integer.toString(this._position.getY())
			+ " Position: R: " + Integer.toString(this._position.getRotation())
			+ " State: " + this._state.name()
			+ " Class: " + this._charClass.name()
			+ " GuildId: " + Integer.toString(this._guildId)
			+ " Type: " + Integer.toString(this._type)
			+ " Lvl: " + Integer.toString(this._level)
			+ " Unk: " + Integer.toString(this._unknown)
			+ " Polymorph: " + Integer.toString(this._polymorph)
			+ " nKQTeamType: " + this._nKQTeamType.name()
			+ " Abstatebit: " + this._abstatebit.toHexString()
			+ "]"
			);

		this._equipment.dump(deep+1);

		return dump;
	}
}