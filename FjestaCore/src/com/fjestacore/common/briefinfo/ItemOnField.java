/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.briefinfo;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ItemOnField
 * @author FantaBlueMystery
 */
public class ItemOnField extends AbstractStruct{

	/**
	 * items
	 */
	protected ArrayList<DropedItem> _items = new ArrayList<>();

	/**
	 * ItemOnField
	 */
	public ItemOnField() {}

	/**
	 * ItemOnField
	 * @param items
	 */
	public ItemOnField(ArrayList<DropedItem> items) {
		this._items = items;
	}

	/**
	 * getItems
	 * @return
	 */
	public ArrayList<DropedItem> getItems() {
		return this._items;
	}

	/**
	 * setItems
	 * @param items
	 */
	public void setItems(ArrayList<DropedItem> items) {
		this._items = items;
	}

	/**
	 * addItem
	 * @param item
	 */
	public void addItem(DropedItem item) {
		this._items.add(item);
	}

	/**
	 * getListSize
	 * @return
	 */
	public int getListSize() {
		return this._items.size();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1 + (this._items.size() * DropedItem.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int itemnum = reader.readByteInt();

		for( int i=0; i<itemnum; i++ ) {
			DropedItem di = new DropedItem();

			di.read(reader);

			this._items.add(di);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._items.size());

		for( DropedItem di: this._items ) {
			di.write(writer);
		}
	}
}