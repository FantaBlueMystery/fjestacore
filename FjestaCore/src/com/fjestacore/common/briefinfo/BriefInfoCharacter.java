/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.briefinfo;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * BriefInfoCharacter
 * @author FantaBlueMystery
 */
public class BriefInfoCharacter extends AbstractStruct {

	/**
	 * characters
	 */
	protected ArrayList<LoginCharacter> _characters = new ArrayList<>();

	/**
	 * BriefInfoCharacter
	 */
	public BriefInfoCharacter() {}

	/**
	 * BriefInfoCharacter
	 * @param characters
	 */
	public BriefInfoCharacter(ArrayList<LoginCharacter> characters) {
		this._characters = characters;
	}

	/**
	 * getCharacterObjects
	 * @return
	 */
	public ArrayList<LoginCharacter> getCharacterObjects() {
		return this._characters;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1 + (this._characters.size() * LoginCharacter.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int charcount = reader.readByteInt();

		for( int i=0; i<charcount; i++ ) {
			LoginCharacter tchar = new LoginCharacter();
			tchar.read(reader);

			this._characters.add(tchar);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._characters.size());

		for( LoginCharacter tchar: this._characters ) {
			tchar.write(writer);
		}
	}
}
