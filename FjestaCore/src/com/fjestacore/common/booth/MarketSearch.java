/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.booth;

/**
 * MarketSearch
 * @author FantaBlueMystery
 */
public enum MarketSearch {
	MS_ALL(0x0),
	MS_WEAPON(0x1),
	MS_ARMOR(0x2),
	MS_ACCESSORY(0x3),
	MS_SCROLL(0x4),
	MS_ENCHANT(0x5),
	MS_POTION(0x6),
	MS_ETC(0x7),
	MAX_MARKETSEARCH(0x08);

	/**
	 * value
	 */
	private int _value;

	/**
	 * MobBriefFlag
	 * @param value
	 */
	private MarketSearch(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getMarktSearch
	 * @param value
	 * @return
	 */
	static public MarketSearch getMarktSearch(int value) {
		MarketSearch[] searchs = MarketSearch.values();

		for( MarketSearch search: searchs ) {
			if( search.getValue() == value ) {
				return search;
			}
		}

		return MarketSearch.MS_ALL;
	}
}