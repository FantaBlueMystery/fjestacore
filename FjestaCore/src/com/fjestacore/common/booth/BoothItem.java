/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.booth;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * BoothItem
 * @author FantaBlueMystery
 */
public class BoothItem extends AbstractStruct {

	/**
	 * SIZE
	 */
	static public int SIZE = 12;

	/**
	 * inven slot
	 */
	protected int _invenslot = 0;

	/**
	 * board slot
	 */
	protected int _boardslot = 0;

	/**
	 * unit cost
	 */
	protected ULong _unitcost = ULong.MIN;

	/**
	 * total lot
	 */
	protected int _totallot = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return BoothItem.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._invenslot = reader.readByteInt();
		this._boardslot = reader.readByteInt();
		this._unitcost	= reader.readULong();
		this._totallot	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._invenslot);
		writer.writeByteInt(this._boardslot);
		writer.writeULong(this._unitcost);
		writer.writeShort(this._totallot);
	}
}