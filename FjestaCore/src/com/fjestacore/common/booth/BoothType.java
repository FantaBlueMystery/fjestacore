/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.booth;

/**
 * BoothType
 * @author FantaBlueMystery
 */
public enum  BoothType {
	BT_BUY(0),
	BT_SELL(1);

	/**
	 * value
	 */
	private int _value;

	/**
	 * BoothType
	 * @param value
	 */
	private BoothType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}