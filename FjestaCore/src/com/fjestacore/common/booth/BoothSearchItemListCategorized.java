/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.booth;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * BoothSearchItemListCategorized
 * @author FantaBlueMystery
 */
public class BoothSearchItemListCategorized extends AbstractStruct {

	/**
	 * search
	 */
	protected MarketSearch _search = MarketSearch.MS_ALL;

	/**
	 * only my class
	 */
	protected boolean _onlyMyClass = false;

	/**
	 * b first?
	 */
	protected boolean _bFirst = false;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._search		= MarketSearch.getMarktSearch((int) reader.readUInt());
		this._onlyMyClass	= reader.readBoolean();
		this._bFirst		= reader.readBoolean();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._search.getValue());
		writer.writeBoolean(this._onlyMyClass);
		writer.writeBoolean(this._bFirst);
	}
}