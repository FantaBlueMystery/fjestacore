/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.booth;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * BoothSearchItemList
 * @author FantaBlueMystery
 */
public class BoothSearchItemList extends AbstractStruct {

	/**
	 * error
	 */
	protected int _error = 0;

	/**
	 * booth item list
	 */
	protected ArrayList<BoothItem> _list = new ArrayList<>();

	/**
	 * BoothSearchItemList
	 */
	public BoothSearchItemList() {}

	/**
	 * getError
	 * @return
	 */
	public int getError() {
		return this._error;
	}

	/**
	 * getItemList
	 * @return
	 */
	public ArrayList<BoothItem> getItemList() {
		return this._list;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._error = reader.readShort();

		int numOfItems = reader.readByteInt();

		for( int i=0; i<numOfItems; i++ ) {
			BoothItem bi = new BoothItem();
			bi.read(reader);

			this._list.add(bi);
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._error);
		writer.writeByteInt(this._list.size());

		for( BoothItem _list1 : this._list ) {
			writer.appendBuffer(_list1.write());
		}
	}
}