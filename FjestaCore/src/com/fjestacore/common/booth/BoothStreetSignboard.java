/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.booth;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * BoothStreetSignboard
 * @author FantaBlueMystery
 */
public class BoothStreetSignboard extends AbstractStruct {

	/**
	 * signboard
	 */
	protected String _signboard = "";

	/**
	 * getString
	 * @return
	 */
	public String getString() {
		return this._signboard;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 30;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._signboard = reader.readString(this.getSize());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeString(this._signboard, this.getSize());
	}
}