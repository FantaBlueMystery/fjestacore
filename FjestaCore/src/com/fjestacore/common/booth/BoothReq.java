/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.booth;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * BoothReq
 * @author FantaBlueMystery
 */
public class BoothReq extends AbstractStruct {

	/**
	 * signboard
	 */
	protected BoothStreetSignboard _signboard = new BoothStreetSignboard();

	/**
	 * type
	 */
	protected BoothType _type = BoothType.BT_SELL;

	/**
	 * items
	 */
	protected ArrayList<BoothItem> _items = new ArrayList<>();

	/**
	 * getType
	 * @return
	 */
	public BoothType getType() {
		return this._type;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._signboard.getSize() + 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._signboard.read(reader);
		int flag = reader.readByteInt();

		byte isSell	= (byte)((flag >> 7) & 0x01);
		int	size	= ((flag >> 2) & 0x1F);

		this._type = (isSell == 0x01 ? BoothType.BT_SELL : BoothType.BT_BUY );

		for( int i=0; i<size; i++ ) {
			BoothItem bi = new BoothItem();
			bi.read(reader);

			this._items.add(bi);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._signboard.write(writer);

		byte isSell = 0x0;

		if( this._type == BoothType.BT_SELL ) {
			isSell = 0x1;
		}

		Integer listsize = this._items.size();
		byte size = listsize.byteValue();

		writer.writeByte((byte) (0x01 | (size << 2) | (isSell) << 7));

		for( BoothItem bi: this._items ) {
			bi.write(writer);
		}
	}
}
