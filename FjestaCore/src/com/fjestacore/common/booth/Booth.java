/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.booth;

import com.fjestacore.common.camp.Camp;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Booth
 * @author FantaBlueMystery
 */
public class Booth extends AbstractStruct {

	/**
	 * camp
	 */
	protected Camp _camp = new Camp();

	/**
	 * is sell
	 */
	protected boolean _isSell = false;

	/**
	 * signboard
	 */
	protected BoothStreetSignboard _signboard = new BoothStreetSignboard();

	/**
	 * Booth
	 */
	public Booth() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._camp.getSize() + this._signboard.getSize() + 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._camp.read(reader);

		this._isSell = reader.readBoolean();

		this._signboard.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._camp.write(writer);

		writer.writeBoolean(this._isSell);

		this._signboard.write(writer);
	}
}