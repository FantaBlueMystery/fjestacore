/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.winpos;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * WinPosStPos
 * @author FantaBlueMystery
 */
public class WinPosStPos extends AbstractStruct {

	/**
	 * pos x rate
	 */
	protected float _fPosXRate = 0;

	/**
	 * pos y rate
	 */
	protected float _fPosYRate = 0;

	/**
	 * getPosXRate
	 * @return
	 */
	public float getPosXRate() {
		return this._fPosXRate;
	}

	/**
	 * getPosYRate
	 * @return
	 */
	public float getPosYRate() {
		return this._fPosYRate;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 8;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._fPosXRate = reader.readFloat();
		this._fPosYRate = reader.readFloat();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeFloat(this._fPosXRate);
		writer.writeFloat(this._fPosYRate);
	}
}