/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.winpos;

import com.fjestacore.common.Version;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * WinPosOption
 * @author FantaBlueMystery and setnr12/mine
 */
public class WinPosOption extends AbstractStruct {

	/**
	 * mkversion
	 */
	protected Version _mkVersion = new Version();

	/**
	 * ui total num
	 */
	protected long _uiTotalNum = 0;

	/**
	 * stpos
	 */
	protected WinPosStPos[] _stPos = new WinPosStPos[27];

	/**
	 * ui window width
	 */
	protected long _uiWindowWidth = 0;

	/**
	 * ui sys window width
	 */
	protected long _uiSysWindowWidth = 0;

	/**
	 * ui window height
	 */
	protected long _uiWindowHeight = 0;

	/**
	 * ui sys window height
	 */
	protected long _uiSysWindowHeight = 0;

	/**
	 * ui display resolution width
	 */
	protected long _uiDisplayResolutionWidth = 0;

	/**
	 * ui display resolution height
	 */
	protected long _uiDisplayResolutionHeight = 0;

	/**
	 * WinPosOption
	 */
	public WinPosOption() {
		for( int i=0; i<this._stPos.length; i++ ) {
			this._stPos[i] = new WinPosStPos();
		}
	}

	/**
	 * getStPos
	 * @param swp
	 * @return
	 */
	public WinPosStPos getStPos(SaveWinPos swp) {
		return this.getStPos(swp.getValue());
	}

	/**
	 * getStPos
	 * patchby setnr12/mine
	 * @param index
	 * @return
	 */
	public WinPosStPos getStPos(int index) {
        if( this._stPos.length <= index) {
			return this._stPos[index];
		}

        return null;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return (7*4) + 1 + this._mkVersion.getSize() + (27*8);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._mkVersion.read(reader);

		// TODO
		// enum? enum $05308AE399C5B13E308E54D34DAC2F98
		int tmp = reader.readByteInt();	// ??? a part of version?

		this._uiTotalNum = reader.readUInt();

		for( int i=0; i<this._stPos.length; i++ ) {
			this._stPos[i].read(reader);
		}

		this._uiWindowWidth				= reader.readUInt();
		this._uiSysWindowWidth			= reader.readUInt();
		this._uiWindowHeight			= reader.readUInt();
		this._uiSysWindowHeight			= reader.readUInt();
		this._uiDisplayResolutionWidth	= reader.readUInt();
		this._uiDisplayResolutionHeight	= reader.readUInt();
	}

	/**
	 *
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._mkVersion.write(writer);
		writer.writeByteInt(0); // ??? a part of version?	// TODO
		writer.writeUInt(this._uiTotalNum);

		for( int i=0; i<this._stPos.length; i++ ) {
			this._stPos[i].write(writer);
		}

		writer.writeUInt(this._uiWindowWidth);
		writer.writeUInt(this._uiSysWindowWidth);
		writer.writeUInt(this._uiWindowHeight);
		writer.writeUInt(this._uiSysWindowHeight);
		writer.writeUInt(this._uiDisplayResolutionWidth);
		writer.writeUInt(this._uiDisplayResolutionHeight);
	}
}