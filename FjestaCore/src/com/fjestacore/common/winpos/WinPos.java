/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.winpos;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * WinPos
 * @author FantaBlueMystery
 */
public class WinPos extends AbstractStruct {

	/**
	 * bSuccess
	 */
	protected boolean _bSuccess = false;

	/**
	 * data
	 */
	protected WinPosOption _data = new WinPosOption();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 393;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._bSuccess = reader.readBoolean();
		this._data.read(reader);

		// read padding
		reader.readBuffer(this.getSize()-1-this._data.getSize());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBoolean(this._bSuccess);
		this._data.write(writer);

		writer.writeString("", this.getSize()-1-this._data.getSize());
	}
}