/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.winpos;

/**
 * SaveWinPos
 * @author FantaBlueMystery
 */
public enum SaveWinPos {
	StatusWin(0),
	TargetWin(1),
	ShortCutWin(2),
	ShortCutWinV(3),
	ShortCutSubWin1(4),
	ShortCutSubWin2(5),
	ShortCutSubWin3(6),
	ShortCutSubWin4(7),
	ShortCutSubWinV1(8),
	ShortCutSubWinV2(9),
	ShortCutSubWinV3(10),
	ShortCutSubWinV4(11),
	ItemUpgradeWin(12),
	MoverUpgradeWin(13),
	FriendWin(14),
	FriendAddWin(15),
	ActionWin(16),
	KingDomQuest(17),
	OptionWin(18),
	CharInfoWin(19),
	InventoryWin(20),
	SkillWin(21),
	MiniMapWin(22),
	ChatDisplayWin(23),
	SysMsgDisplayWin(24),
	TOTWin(25),
	ItemMixWin(26),
	WIN_MAX(27);

	/**
	 * value
	 */
	private int _value;

	/**
	 * PlayerState
	 * @param value
	 */
	private SaveWinPos(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}