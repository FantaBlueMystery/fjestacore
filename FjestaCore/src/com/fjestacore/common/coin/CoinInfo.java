/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.coin;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CoinInfo
 * @author FantaBlueMystery
 */
public class CoinInfo extends AbstractStruct {

	/**
	 * n coin
	 */
	protected Coin _nCoin = new Coin();

	/**
	 * n exchanged coin
	 */
	protected Coin _nExchangedCoin = new Coin();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 16;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nCoin.read(reader);
		this._nExchangedCoin.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._nCoin.write(writer);
		this._nExchangedCoin.write(writer);
	}
}