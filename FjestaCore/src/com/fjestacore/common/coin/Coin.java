/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.coin;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * Coin
 * Base object Coin
 *
 * @author FantaBlueMystery, EndOfFile
 */
public class Coin extends AbstractStruct {

	/**
	 * money
	 */
	protected ULong _money = ULong.MIN;

	/**
	 * Money
	 */
	public Coin() {}

	/**
	 * Money
	 * @param money
	 */
	public Coin(ULong money) {
		this._money = money;
	}

	/**
	 * Money
	 * @param money
	 */
	public Coin(long money) {
		this._money = ULong.valueOf(money);
	}

	/**
	 * Money
	 * @param money
	 */
	public Coin(int money) {
		this._money = ULong.valueOf(money);
	}

	/**
	 * getValue
	 * @return
	 */
	public ULong getValue() {
		return this._money;
	}

	/**
	 * setValue
	 * @param value
	 */
	public void setValue(ULong value) {
		this._money = value;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._money = reader.readULong();
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeULong(this._money);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 8;
	}

	/**
	 * toStrFormat
	 * convert int64 Coin to readable string
	 * @return
	 */
	public String toStrFormat() {
		long val = this._money.longValue();

		short gem = (short)(val / 100_000_000L);
		short gold = (short)((val / 1_000_000L) % 100);
		short silver = (short)((val / 1_000L) % 1000);
		short copper = (short)(val % 1000);

		String format = "";

		if( gem > 0 ) {
			format += gem + " Gem ";
		}

		if( gold > 0 ) {
			format += gold + " Gold ";
		}

		if( silver > 0 ) {
			format += silver + " Silver ";
		}

		if( copper > 0 ) {
			format += copper + " Copper ";
		}

		return format.trim();
	}
}