/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.gamesetting;

/**
 * GameOpt
 * @author FantaBlueMystery
 */
public enum GameOpt {
	SHOW_MY_CHAR_NAME(0x0),
	SHOW_OTHERS_CHAR_NAME(0x1),
	SHOW_NPC_NAME(0x2),
	SHOW_MONSTER_NAME(0x3),
	SHOW_ITEM_NAME(0x4),
	SHOW_HP_BAR(0x5),
	SHOW_SP_BAR(0x6),
	SHOW_INTERFACE(0x7),
	SHOW_SPEECH_BUBBLE(0x8),
	SHOW_BILLBOARD_NAMEPANEL(0x9),
	SHOW_BASICINFO_TIP(0xA),
	SHOW_PLAYGUIDE(0xB),
	REFUSE_WHISPER(0xC),
	REFUSE_TRADE(0xD),
	REFUSE_PARTY_INVITE(0xE),
	REFUSE_GUILD_INVITE(0xF),
	REFUSE_SYSTEM_MSG(0x10),
	REFUSE_CONFIRM_MSG(0x11),
	AUTOHIDE_SYSTEMWIN(0x12),
	AUTOHIDE_CHATWIN(0x13),
	NOTICE_LOGIN_GUILDMEMBER(0x14),
	START_CHATTING_ENTER(0x15),
	INIT_INTERFACEPOS(0x16),
	ENABLE_SKILLLOCK(0x17),

	SHOW_AUTOSTACK(0x1B),

	SHINE_GAME_OPT_MAX(0x1C)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * GameOpt
	 * @param value
	 */
	private GameOpt(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}