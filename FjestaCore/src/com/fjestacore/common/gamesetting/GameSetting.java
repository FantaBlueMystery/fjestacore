/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.gamesetting;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * GameSetting
 * @author FantaBlueMystery
 */
public class GameSetting extends AbstractStruct {

	/**
	 * setting num
	 */
	protected int _settingNum = 0;

	/**
	 * value
	 */
	protected int _value = 0;

	/**
	 * GameSetting
	 */
	public GameSetting() {}

	/**
	 * GameSetting
	 * @param num
	 * @param value
	 */
	public GameSetting(int num, int value) {
		this._settingNum = num;
		this._value = value;
	}

	/**
	 * GameSetting
	 * @param num
	 * @param value
	 */
	public GameSetting(GameOpt num, int value) {
		this._settingNum = num.getValue();
		this._value = value;
	}

	/**
	 * getSettingNum
	 * @return
	 */
	public int getSettingNum() {
		return this._settingNum;
	}

	/**
	 * setSettingNum
	 * @param num
	 */
	public void setSettingNum(int num) {
		this._settingNum = num;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	public void setValue(int value) {
		this._value = value;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._settingNum = reader.readShort();
		this._value = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((short) this._settingNum);
		writer.writeByteInt(this._value);
	}
}