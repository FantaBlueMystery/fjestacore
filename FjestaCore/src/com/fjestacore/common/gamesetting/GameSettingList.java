/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.gamesetting;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * GameSettingList
 * @author FantaBlueMystery
 */
public class GameSettingList extends AbstractStruct {

	/**
	 * keymaps
	 */
	protected ArrayList<GameSetting> _list = new ArrayList<>();

	/**
	 * GameSettingList
	 */
	public GameSettingList() {}

	/**
	 * getList
	 * @return
	 */
	public ArrayList<GameSetting> getList() {
		return this._list;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int count = reader.readShort();

		for( int i=0; i<count; i++ ) {
			GameSetting gs = new GameSetting();
			gs.read(reader);

			this._list.add(gs);
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (this._list.size()*3);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((short) this._list.size());

		for( GameSetting gs: this._list ) {
			gs.write(writer);
		}
	}
}