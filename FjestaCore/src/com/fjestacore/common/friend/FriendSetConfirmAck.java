/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.friend;

import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * FriendSetConfirmAck
 * @author FantaBlueMystery
 */
public class FriendSetConfirmAck extends AbstractStruct {

	/**
	 * char id
	 */
	protected Name5 _charid = new Name5();

	/**
	 * friend id
	 */
	protected Name5 _friendid = new Name5();

	/**
	 * accept firend
	 */
	protected boolean _acceptFriend = false;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._charid.getSize() + this._friendid.getSize() + 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._charid.read(reader);
		this._friendid.read(reader);
		this._acceptFriend = reader.readBoolean();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._charid.write(writer);
		this._friendid.write(writer);
		writer.writeBoolean(this._acceptFriend);
	}
}