/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.friend;

import com.fjestacore.common.character.CharacterClass;
import com.fjestacore.common.name.Name3;
import com.fjestacore.common.name.Name5;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * FriendInfo
 * @author FantaBlueMystery
 */
public class FriendInfo extends AbstractStruct implements IAbstractStructDump {

	/**
	 * login info
	 */
	protected FriendDate _logininfo = new FriendDate();

	/**
	 * char id
	 */
	protected Name5 _charid = new Name5();

	/**
	 * class
	 */
	protected CharacterClass _class = CharacterClass.NONE;

	/**
	 * level
	 */
	protected int _level = 0;

	/**
	 * is party
	 */
	protected boolean _isParty = false;

	/**
	 * flag
	 */
	protected int _flag = 0;

	/**
	 * map name
	 */
	protected Name3 _mapName = new Name3();

	/**
	 * status title
	 */
	protected String _statusTitle = "";

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 52;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._logininfo.read(reader);
		this._charid.read(reader);

		this._class			= CharacterClass.intToCharacterClass(reader.readByteInt());
		this._level			= reader.readByteInt();
		this._isParty		= reader.readBoolean();
		this._flag			= reader.readByteInt();
		this._mapName.read(reader);
		this._statusTitle	= reader.readString(32).trim();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._logininfo.write(writer);
		this._charid.write(writer);

		writer.writeByteInt(this._class.getValue());
		writer.writeByteInt(this._level);
		writer.writeBoolean(this._isParty);
		writer.writeByteInt(this._flag);
		this._mapName.write(writer);
		writer.writeString(this._statusTitle, 32);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"FriendInfo: [" +
			" CharId: " + this._charid.getContent() +
			" Class: " + this._class.name() +
			" Level: " + Integer.toString(this._level) +
			" isParty: " + (this._isParty ? "True" : "False") +
			" Flag: " + Integer.toString(this._flag) +
			" MapName: " + this._mapName.getContent() +
			" StatusTitle: " + this._statusTitle +
			"]"
			);

		dump += this._logininfo.dump(deep+1);

		return dump;
	}
}