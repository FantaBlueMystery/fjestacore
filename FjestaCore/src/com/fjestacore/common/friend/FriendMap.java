/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.friend;

import com.fjestacore.common.name.Name3;
import com.fjestacore.common.name.Name5;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * FriendMap
 * @author FantaBlueMystery
 */
public class FriendMap extends AbstractStruct implements IAbstractStructDump {

	/**
	 * char id
	 */
	protected Name5 _charid = new Name5();

	/**
	 * map
	 */
	protected Name3 _map = new Name3();

	/**
	 * getCharId
	 * @return
	 */
	public String getCharId() {
		return this._charid.getContent();
	}

	/**
	 * getMap
	 * @return
	 */
	public String getMap() {
		return this._map.getContent();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._charid.getSize() + this._map.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._charid.read(reader);
		this._map.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._charid.write(writer);
		this._map.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"FriendMap: [" +
			" charid: " + this._charid.getContent() +
			" map: " + this._map.getContent() +
			"]"
			);

		return dump;
	}
}