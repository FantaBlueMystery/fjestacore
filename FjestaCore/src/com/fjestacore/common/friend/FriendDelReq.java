/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.friend;

import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * FriendDelReq
 * @author FantaBlueMystery
 */
public class FriendDelReq extends AbstractStruct {

	/**
	 * char id
	 */
	protected Name5 _charid = new Name5();

	/**
	 * firend id
	 */
	protected Name5 _friendid = new Name5();

	/**
	 * getCharId
	 * @return
	 */
	public String getCharId() {
		return this._charid.getContent();
	}

	/**
	 * getFriendId
	 * @return
	 */
	public String getFriendId() {
		return this._friendid.getContent();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._charid.getSize() + this._friendid.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._charid.read(reader);
		this._friendid.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._charid.write(writer);
		this._friendid.write(writer);
	}
}
