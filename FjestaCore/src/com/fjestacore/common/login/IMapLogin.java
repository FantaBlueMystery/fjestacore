/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.zone.IZoneChardata;

/**
 * IMapLogin
 * @author FantaBlueMystery
 */
public interface IMapLogin extends IZoneChardata {

	/**
	 * getChecksums
	 * @return
	 */
	public MapLoginCheckSum[] getChecksums();
}