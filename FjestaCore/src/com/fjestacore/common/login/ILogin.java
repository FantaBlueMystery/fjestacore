/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

/**
 * ILogin
 * @author FantaBlueMystery
 */
public interface ILogin {

	/**
	 * getUsername
	 * @return
	 */
	public String getUsername();

	/**
	 * getPassword
	 * @return
	 */
	public String getPassword();

	/**
	 * getSpawnapps
	 * @return
	 */
	public String getSpawnapps();
}