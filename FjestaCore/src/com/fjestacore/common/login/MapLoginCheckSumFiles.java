/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

/**
 * MapLoginCheckSumFiles
 * @author FantaBlueMystery
 */
public enum MapLoginCheckSumFiles {
	ABSTATE("Abstate"),
	ACTIVESKILL("ActiveSkill"),
	CHARACTERTITLEDATA("CharacterTitleData"),
	CHARGEDEFFECT("ChargedEffect"),
	CLASSNAME("ClassName"),
	GATHER("Gather"),
	GRADEITEMOPTION("GradeItemOption"),
	ITEMDISMANTLE("ItemDismantle"),
	ITEMINFO("ItemInfo"),
	MAPINFO("MapInfo"),
	MINIHOUSE("MiniHouse"),
	MINIHOUSEFURNITURE("MiniHouseFurniture"),
	MINIHOUSEOBJANI("MiniHouseObjAni"),
	MOBINFO("MobInfo"),
	PASSIVESKILL("PassiveSkill"),
	RIDING("Riding"),
	SUBABSTATE("SubAbstate"),
	UPGRADEINFO("UpgradeInfo"),
	WEAPONATTRIB("WeaponAttrib"),
	WEAPONTITLEDATA("WeaponTitleData"),
	MINIHOUSEFURNITUREOBJEFFECT("MiniHouseFurnitureObjEffect"),
	MINIHOUSEENDURE("MiniHouseEndure"),
	DICEDIVIDIND("DiceDividind"),
	ACTIONVIEWINFO("ActionViewInfo"),
	MAPLINKPOINT("MapLinkPoint"),
	MAPWAYPOINT("MapWayPoint"),
	ABSTATEVIEW("AbStateView"),
	ACTIVESKILLVIEW("ActiveSkillView"),
	CHARACTERTITLESTATEVIEW("CharacterTitleStateView"),
	EFFECTVIEWINFO("EffectViewInfo"),
	ITEMSHOPVIEW("ItemShopView"),
	ITEMVIEWINFO("ItemViewInfo"),
	MAPVIEWINFO("MapViewInfo"),
	MOBVIEWINFO("MobViewInfo"),
	NPCVIEWINFO("NPCViewInfo"),
	PASSIVESKILLVIEW("PassiveSkillView"),
	PRODUCEVIEW("ProduceView"),
	COLLECTCARDVIEW("CollectCardView"),
	GTIVIEW("GTIView"),
	ITEMVIEWEQUIPTYPEINFO("ItemViewEquipTypeInfo"),
	SINGLEDATA("SingleData"),
	MARKETSEARCHINFO("MarketSearchInfo"),
	ITEMMONEY("ItemMoney"),
	PUPMAIN("PupMain"),
	CHATCOLOR("ChatColor"),
	TERMEXTENDMATCH("TermExtendMatch"),
	MINIMONINFO("MinimonInfo"),
	MINIMONAUTOUSEITEM("MinimonAutoUseItem"),
	CHARGEDDELETABLEBUFF("ChargedDeletableBuff"),
	SLANDERFILTER("SlanderFilter"),
	DEPRECATEDFILES("DeprecatedFiles"),
	QUESTDATA("QuestData"),
	QUESTENDNPC("QuestEndNpc"),
	QUESTENDITEM("QuestEndItem"),
	QUESTACTION("QuestAction"),
	QUESTREWARD("QuestReward"),

	UNKNOWN("Unknown")
	;

	/**
	 * value
	 */
	private String _value;

	/**
	 * CGdpType
	 * @param value
	 */
	private MapLoginCheckSumFiles(String value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public String getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(String value) {
		this._value = value;
	}

	/**
	 * byIndex
	 * @param index
	 * @return
	 */
	static MapLoginCheckSumFiles byIndex(int index) {
		MapLoginCheckSumFiles[] values = MapLoginCheckSumFiles.values();

		if( index < values.length ) {
			return values[index];
		}

		return MapLoginCheckSumFiles.UNKNOWN;
	}
}