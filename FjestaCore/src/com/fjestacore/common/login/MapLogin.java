/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.zone.ZoneChardata;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.ArrayList;

/**
 * MapLogin
 * @author FantaBlueMystery
 */
public class MapLogin extends ZoneChardata implements IMapLogin, IAbstractStructDump {

	/**
	 * conts
	 */
	static public int COUNT_CHECKSUMS = 56;

	/**
	 * checksums
	 */
	protected ArrayList<MapLoginCheckSum> _checksums = new ArrayList();

	/**
	 * getChecksums
	 * @return
	 */
	@Override
	public MapLoginCheckSum[] getChecksums() {
		return this._checksums.toArray(new MapLoginCheckSum[]{});
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 22 + (MapLoginCheckSum.SIZE * MapLogin.COUNT_CHECKSUMS);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._worldHandle = reader.readShort();
		this._charname.read(reader);

		for( int i=0; i<MapLogin.COUNT_CHECKSUMS; i++ ) {
			MapLoginCheckSum cs = new MapLoginCheckSum(i);
			cs.read(reader);

			this._checksums.add(cs);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((int) this._worldHandle);
		this._charname.write(writer);

		for( MapLoginCheckSum cs: this._checksums ) {
			cs.write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"MapLogin: [" +
			" worldHandle: " + Integer.toString(this._worldHandle) +
			" Charname: " + this._charname.getContent() + "]"
			);

		for( MapLoginCheckSum cs: this._checksums ) {
			dump += cs.dump(deep+1);
		}

		return dump;
	}
}