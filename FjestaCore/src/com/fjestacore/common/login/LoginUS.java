/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * LoginUS
 * @author FantaBlueMystery
 */
public class LoginUS extends AbstractStruct implements ILogin {

	/**
	 * username
	 */
	protected String _username = "";

	/**
	 * password
	 */
	protected String _password = "";

	/**
	 * spawnapps
	 */
	protected Name5 _spawnapps = new Name5();

	/**
	 * LoginUS
	 */
	public LoginUS() {}

	/**
	 * LoginUS
	 * @param username
	 * @param password
	 * @param spawnapps
	 */
	public LoginUS(String username, String password, String spawnapps) {
		this._username = username;
		this._password = password;
		this._spawnapps.setContent(spawnapps);
	}

	/**
	 * getUsername
	 * @return
	 */
	@Override
	public String getUsername() {
		return this._username;
	}

	/**
	 * getPassword
	 * @return
	 */
	@Override
	public String getPassword() {
		return this._password;
	}

	/**
	 * getSpawnapps
	 * @return
	 */
	@Override
	public String getSpawnapps() {
		return this._spawnapps.getContent();
	}

	/**
	 * setVerify
	 * @param spawnapps
	 */
	public void setSpawnapps(String spawnapps) {
		this._spawnapps.setContent(spawnapps);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 316;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._username = reader.readString(260).trim();
		this._password = reader.readString(36).trim();
		this._spawnapps.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeString(this._username, 260);
		writer.writeString(this._password, 36);
		this._spawnapps.write(writer);
	}
}