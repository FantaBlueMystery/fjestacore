/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.avatar.AvatarInformationList;
import com.fjestacore.core.handle.IWorldHandle;

/**
 * IUserWorldLogin
 * @author FantaBlueMystery
 */
public interface IUserWorldLogin extends IWorldHandle {

	/**
	 * getAvatars
	 * @return
	 */
	public AvatarInformationList getAvatars();

	/**
	 * setAvatars
	 * @param avatars
	 */
	public void setAvatars(AvatarInformationList avatars);
}