/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.name.Name18Byte;
import com.fjestacore.common.name.Name4;
import com.fjestacore.common.name.Name5;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * LoginGer
 * @author FantaBlueMystery
 */
public class LoginGer extends AbstractStruct implements IAbstractStructDump, ILogin {

	/**
	 * consts
	 */
	static public int SIZE = 54;

	/**
	 * username
	 */
	protected Name18Byte _user = new Name18Byte();

	/**
	 * password
	 */
	protected Name4 _password = new Name4();

	/**
	 * spawnapps
	 */
	protected Name5 _spawnapps = new Name5();

	/**
	 * LoginGer
	 */
	public LoginGer() {}

	/**
	 * LoginGer
	 * @param username
	 * @param password
	 * @param spawnapps
	 */
	public LoginGer(String username, String password, String spawnapps) {
		this._user.setContent(username);
		this._password.setContent(password);
		this._spawnapps.setContent(spawnapps);
	}

	/**
	 * getUsername
	 * @return
	 */
	@Override
	public String getUsername() {
		return this._user.getContent();
	}

	/**
	 * setUsername
	 * @param username
	 */
	public void setUsername(String username) {
		this._user.setContent(username);
	}

	/**
	 * getPassword
	 * @return
	 */
	@Override
	public String getPassword() {
		return this._password.getContent();
	}

	/**
	 * setPassword
	 * @param password
	 */
	public void setPassword(String password) {
		this._password.setContent(password);
	}

	/**
	 * getSpawnapps
	 * @return
	 */
	@Override
	public String getSpawnapps() {
		return this._spawnapps.getContent();
	}

	/**
	 * setVerify
	 * @param spawnapps
	 */
	public void setSpawnapps(String spawnapps) {
		this._spawnapps.setContent(spawnapps);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return LoginGer.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._user.read(reader);
		this._password.read(reader);
		this._spawnapps.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._user.write(writer);
		this._password.write(writer);
		this._spawnapps.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"LoginGer: [" +
			" User: " + this._user.getContent() +
			" Password: " + this._password.getContent() +
			" SpawnApps: " + this._spawnapps.getContent() +
			"]"
			);

		return dump;
	}
}