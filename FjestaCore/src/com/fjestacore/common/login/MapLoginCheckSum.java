/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.name.Name8;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * MapLoginCheckSum
 * @author FantaBlueMystery
 */
public class MapLoginCheckSum extends AbstractStruct implements IAbstractStructDump {

	/**
	 * consts
	 */
	static public int SIZE = 32;

	/**
	 * file
	 */
	protected MapLoginCheckSumFiles _file = MapLoginCheckSumFiles.UNKNOWN;

	/**
	 * checksum
	 */
	protected Name8 _checksum = new Name8();

	/**
	 * MapLoginCheckSum
	 */
	public MapLoginCheckSum() {}

	/**
	 * MapLoginCheckSum
	 * @param file
	 */
	public MapLoginCheckSum(MapLoginCheckSumFiles file) {
		this._file = file;
	}

	/**
	 * MapLoginCheckSum
	 * @param fileIndex
	 */
	public MapLoginCheckSum(int fileIndex) {
		this._file = MapLoginCheckSumFiles.byIndex(fileIndex);
	}

	/**
	 * getCheckSum
	 * @return
	 */
	public String getCheckSum() {
		return this._checksum.getContent();
	}

	/**
	 * setCheckSum
	 * @param checksum
	 */
	public void setCheckSum(String checksum) {
		this._checksum.setContent(checksum);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._checksum.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._checksum.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._checksum.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"MapLoginCheckSum: [" +
			" File: " + this._file.getValue() +
			" Checksum: " + this._checksum.getContent() + "]"
			);

		return dump;
	}
}