/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.avatar.AvatarInformationList;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * UserWorldLogin
 * @author FantaBlueMystery
 */
public class UserWorldLogin extends AbstractStruct implements IAbstractStructDump, IUserWorldLogin {

	/**
	 * world handle
	 */
	protected int _worldHandle = 0;

	/**
	 * avatars
	 */
	protected AvatarInformationList _avatars;

	/**
	 * WorldLogin
	 */
	public UserWorldLogin() {
		this._avatars = new AvatarInformationList();
	}

	/**
	 * UserWorldLogin
	 * @param worldHandle
	 * @param avatars
	 */
	public UserWorldLogin(int worldHandle, AvatarInformationList avatars) {
		this._worldHandle = worldHandle;
		this._avatars = avatars;
	}

	/**
	 * getWorldHandle
	 * @return
	 */
	@Override
	public int getWorldHandle() {
		return this._worldHandle;
	}

	/**
	 * setWorldHandle
	 * @param handle
	 */
	@Override
	public void setWorldHandle(int handle) {
		this._worldHandle = handle;
	}

	/**
	 * getAvatars
	 * @return
	 */
	@Override
	public AvatarInformationList getAvatars() {
		return this._avatars;
	}

	/**
	 * setAvatars
	 * @param avatars
	 */
	@Override
	public void setAvatars(AvatarInformationList avatars) {
		this._avatars = avatars;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._worldHandle = reader.readShort();
		this._avatars.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((int) this._worldHandle);
		writer.appendBuffer(this._avatars.write());
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"UserWorldLogin: [" +
			"WorldHandle: " + Integer.toString(this._worldHandle) +
			"]"
			);

		dump += this._avatars.dump(deep+1);

		return dump;
	}
}
