/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.common.character.ICharLogin;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * CharLogin
 * @author FantaBlueMystery
 */
public class CharLogin extends AbstractStruct implements IAbstractStructDump, ICharLogin  {

	/**
	 * server ip
	 */
	protected String _serverIp = "";

	/**
	 * server port
	 */
	protected int _serverPort = 0;

	/**
	 * CharLogin
	 */
	public CharLogin() {}

	/**
	 * CharLogin
	 * @param ip
	 * @param port
	 */
	public CharLogin(String ip, int port) {
		this._serverIp		= ip;
		this._serverPort	= port;
	}

	/**
	 * getServerIp
	 * @return
	 */
	@Override
	public String getServerIp() {
		return this._serverIp;
	}

	/**
	 * setServerIp
	 * @param ip
	 */
	public void setServerIp(String ip) {
		this._serverIp = ip;
	}

	/**
	 * getServerPort
	 * @return
	 */
	@Override
	public int getServerPort() {
		return this._serverPort;
	}

	/**
	 * setServerPort
	 * @param port
	 */
	public void setServerPort(int port) {
		this._serverPort = port;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 18;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._serverIp		= reader.readString(16);
		this._serverPort	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeString(this._serverIp, 16);
		writer.writeShort(this._serverPort);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"CharLogin: [" +
			"ServerIP: " + this._serverIp +
			" ServerPort: " + Integer.toString(this._serverPort) +
			"]"
			);

		return dump;
	}
}