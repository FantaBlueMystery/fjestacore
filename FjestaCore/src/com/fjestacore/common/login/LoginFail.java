/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.login;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import com.fjestacore.server.ServerError;
import java.io.IOException;

/**
 * LoginFail
 * @author FantaBlueMystery
 */
public class LoginFail extends AbstractStruct implements IAbstractStructDump {

	/**
	 * _errorCode
	 */
	protected ServerError _errorCode = ServerError.NONE;

	/**
	 * size
	 */
	static public int SIZE = 2;

	/**
	 * LoginFail
	 */
	public LoginFail() {}

	/**
	 * LoginFail
	 * @param errorCode
	 */
	public LoginFail(ServerError errorCode) {
		this._errorCode = errorCode;
	}

	/**
	 * getErrorCode
	 * @return
	 */
	public ServerError getErrorCode() {
		return this._errorCode;
	}

	/**
	 * setErrorCode
	 * @param errorCode
	 */
	public void setErrorCode(int errorCode) {
		this._errorCode = ServerError.intToError(errorCode);
	}

	/**
	 * setErrorCode
	 * @param errorCode
	 */
	public void setErrorCode(ServerError errorCode) {
		this._errorCode = errorCode;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return LoginFail.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._errorCode = ServerError.intToError(reader.readShort());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._errorCode.getValue());
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"LoginFail: [" +
			"errorCode: '" + Integer.toString(this._errorCode.getValue()) + "' " +
				"-> Msg: " + ServerError.getMsg(this._errorCode) +
			"]"
			);

		return dump;
	}
}