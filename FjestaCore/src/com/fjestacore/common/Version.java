/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Version
 * @author FantaBlueMystery
 */
public class Version extends AbstractStruct {

	/**
	 * year
	 */
	protected int _year = 0;

	/**
	 * month
	 */
	protected int _month = 0;

	/**
	 * date
	 */
	protected int _date = 0;

	/**
	 * hour
	 */
	protected int _hour = 0;

	/**
	 * getYear
	 * @return
	 */
	public int getYear() {
		return this._year;
	}

	/**
	 * getMonth
	 * @return
	 */
	public int getMonth() {
		return this._month;
	}

	/**
	 * getDate
	 * @return
	 */
	public int getDate() {
		return this._date;
	}

	/**
	 * getHour
	 * @return
	 */
	public int getHour() {
		return this._hour;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._year	= reader.readShort();
		this._month	= reader.readByteInt();
		this._date	= reader.readByteInt();
		this._hour	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._year);
		writer.writeByteInt(this._month);
		writer.writeByteInt(this._date);
		writer.writeByteInt(this._hour);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 5;
	}
}