/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.tutorial;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * TutorialInfo
 * @author FantaBlueMystery
 */
public class TutorialInfo extends AbstractStruct {

	/**
	 * tutorial state
	 */
	protected TutorialState _tutorialState = TutorialState.TS_DONE;

	/**
	 * tutorial state
	 */
	protected byte _tutorialStep = 0x00;

	/**
	 * getState
	 * @return
	 */
	public TutorialState getState() {
		return this._tutorialState;
	}

	/**
	 * getStep
	 * @return
	 */
	public byte getStep() {
		return this._tutorialStep;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._tutorialState = TutorialState.getTutorialState((int) reader.readUInt());
		this._tutorialStep = reader.readByte();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._tutorialState.getValue());	// +4
		writer.writeByte(this._tutorialStep);				// +1
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 5;
	}
}
