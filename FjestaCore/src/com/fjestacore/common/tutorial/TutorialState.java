/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.tutorial;

/**
 * TutorialState
 * @author FantaBlueMystery
 */
public enum TutorialState {
	TS_PROGRESS(0),
	TS_DONE(1),
	TS_SKIP(2),
	TS_EXCEPTION(3),
	TS_MAX(4);

	private int _value;

	/**
	 * TutorialState
	 * @param value
	 */
	private TutorialState(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getTutorialState
	 * @param state
	 * @return
	 */
	static public TutorialState getTutorialState(int state) {
		TutorialState[] states = TutorialState.values();

		for( TutorialState t: states ) {
			if( t.getValue() == state ) {
				return t;
			}
		}

		return TutorialState.TS_DONE;
	}
}