/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

import com.fjestacore.common.name.Name4;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * GuildTournamentList
 * @author FantaBlueMystery
 */
public class GuildTournamentList extends AbstractStruct implements IAbstractStructDump {

	/**
	 * baseclass
	 */
	protected GuildTournamentListDb _baseclass0 = new GuildTournamentListDb();

	/**
	 * guild name
	 */
	protected Name4 _sGuildName = new Name4();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._baseclass0.getSize() + this._sGuildName.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._baseclass0.read(reader);
		this._sGuildName.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._baseclass0.write(writer);
		this._sGuildName.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"GuildTournamentList: [" +
			"GuildName: " + this._sGuildName.getContent() +
			"]"
			);

		dump += this._baseclass0.dump(deep+1);

		return dump;
	}
}