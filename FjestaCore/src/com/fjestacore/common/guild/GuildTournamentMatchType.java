/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

/**
 * GuildTournamentMatchType
 * @author FantaBlueMystery
 */
public enum GuildTournamentMatchType {
	GUILD_TOURNAMENT_MATCH_NONE(0),
	GUILD_TOURNAMENT_MATCH_JOIN(1),
	GUILD_TOURNAMENT_MATCH_PRACTICE(2),
	GUILD_TOURNAMENT_MATCH_PRACTICE_END(3),
	GUILD_TOURNAMENT_MATCH_161(4),
	GUILD_TOURNAMENT_MATCH_162(5),
	GUILD_TOURNAMENT_MATCH_8(6),
	GUILD_TOURNAMENT_MATCH_4(7),
	GUILD_TOURNAMENT_MATCH_2(8),
	GUILD_TOURNAMENT_MATCH_END(9),
	GUILD_TOURNAMENT_MATCH_CANCEL(10),
	GUILD_TOURNAMENT_MATCH_DBERROR(11)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * GuildTournamentMatchType
	 * @param value
	 */
	private GuildTournamentMatchType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToType
	 * @param value
	 * @return
	 */
	static public GuildTournamentMatchType intToType(int value) {
		GuildTournamentMatchType[] types = GuildTournamentMatchType.values();

		for( GuildTournamentMatchType type: types ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return GuildTournamentMatchType.GUILD_TOURNAMENT_MATCH_NONE;
	}
}