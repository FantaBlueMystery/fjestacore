/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

import com.fjestacore.common.name.Name4;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * GuildNameAck
 * @author FantaBlueMystery
 */
public class GuildNameAck extends AbstractStruct {

	/**
	 * number (id) of guild
	 */
	protected long _nNo = 0;

	/**
	 * name
	 */
	protected Name4 _name = new Name4();

	/**
	 * emblem info
	 */
	protected GuildEmblemInfo _emblemInfo = new GuildEmblemInfo();

	/**
	 * GuildNameAck
	 */
	public GuildNameAck() {}

	/**
	 * GuildNameAck
	 * @param num
	 * @param name
	 */
	public GuildNameAck(long num, String name) {
		this._nNo = num;
		this._name.setContent(name);
	}

	/**
	 * getGuildId
	 * @return
	 */
	public long getGuildId() {
		return this._nNo;
	}

	/**
	 * setGuildId
	 * @param id
	 */
	public void setGuildId(long id) {
		this._nNo = id;
	}

	/**
	 * getName
	 * @return
	 */
	public Name4 getName() {
		return this._name;
	}

	/**
	 * setName
	 * @param name
	 */
	public void setName(Name4 name) {
		this._name = name;
	}

	/**
	 * setName
	 * @param name
	 */
	public void setName(String name) {
		this._name.setContent(name);
	}

	/**
	 * getNameStr
	 * @return
	 */
	public String getNameStr() {
		return this._name.getContent();
	}

	/**
	 * getEmblemInfo
	 * @return
	 */
	public GuildEmblemInfo getEmblemInfo() {
		return this._emblemInfo;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + 16 + 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nNo = reader.readUInt();

		this._name.read(reader);
		this._emblemInfo.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._nNo);

		this._name.write(writer);
		this._emblemInfo.write(writer);
	}
}