/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * GuildTournamentListDb
 * @author FantaBlueMystery
 */
public class GuildTournamentListDb extends AbstractStruct implements IAbstractStructDump {

	/**
	 * result
	 */
	protected int _nResult = 0;

	/**
	 * guild no
	 */
	protected int _nGuildNo = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nResult	= reader.readByteInt();
		this._nGuildNo	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._nResult);
		writer.writeShort(this._nGuildNo);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"GuildTournamentListDb: [" +
			"Result: " + Integer.toString(this._nResult) +
			" GuildNo: " + Integer.toString(this._nGuildNo) +
			"]"
			);

		return dump;
	}
}