/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

import com.fjestacore.common.chat.ChatReq;
import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * GuildAcademyChat
 * @author FantaBlueMystery
 */
public class GuildAcademyChat extends AbstractStruct {

	/**
	 * guild number
	 */
	protected long _nGuildNo = 0;

	/**
	 * talker
	 */
	protected Name5 _talker = new Name5();

	/**
	 * chat
	 */
	protected ChatReq _chat = new ChatReq();

	/**
	 * getGuildId
	 * @return
	 */
	public long getGuildId() {
		return this._nGuildNo;
	}

	/**
	 * setGuildId
	 * @param id
	 */
	public void setGuildId(long id) {
		this._nGuildNo = id;
	}

	/**
	 * getTalker
	 * @return
	 */
	public Name5 getTalker() {
		return this._talker;
	}

	/**
	 * setTalker
	 * @param talker
	 */
	public void setTalker(Name5 talker) {
		this._talker = talker;
	}

	/**
	 * getChat
	 * @return
	 */
	public ChatReq getChat() {
		return this._chat;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + this._talker.getSize() + this._chat.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nGuildNo = reader.readUInt();

		this._talker.read(reader);
		this._chat.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._nGuildNo);

		this._talker.write(writer);
		this._chat.write(writer);
	}
}