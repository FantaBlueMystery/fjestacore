/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import com.fjestacore.core.type.TmTime;
import java.io.IOException;
import java.util.ArrayList;

/**
 * GuildTournamentListAck
 * @author FantaBlueMystery
 */
public class GuildTournamentListAck extends AbstractStruct implements IAbstractStructDump {

	/**
	 * error
	 */
	protected int _nError = 0;

	/**
	 * time start
	 */
	protected int _time_start = 0;

	/**
	 * time practic
	 */
	protected int _time_practic = 0;

	/**
	 * time practic end
	 */
	protected int _time_practicEnd = 0;

	/**
	 * time match 161
	 */
	protected int _time_match_161 = 0;

	/**
	 * time match 162
	 */
	protected int _time_match_162 = 0;

	/**
	 * time match 8
	 */
	protected int _time_match_8 = 0;

	/**
	 * time match 4
	 */
	protected int _time_match_4 = 0;

	/**
	 * time match 2
	 */
	protected int _time_match_2 = 0;

	/**
	 * time match end
	 */
	protected int _time_match_end = 0;

	/**
	 * tm time start
	 */
	protected TmTime _tm_time_start = new TmTime();

	/**
	 * tm time practic
	 */
	protected TmTime _tm_time_practic = new TmTime();

	/**
	 * tm time parctiv end
	 */
	protected TmTime _tm_time_practicEnd = new TmTime();

	/**
	 * tm time match 161
	 */
	protected TmTime _tm_time_match_161 = new TmTime();

	/**
	 * tm time match 162
	 */
	protected TmTime _tm_time_match_162 = new TmTime();

	/**
	 * tm time match 8
	 */
	protected TmTime _tm_time_match_8 = new TmTime();

	/**
	 * tm time match 4
	 */
	protected TmTime _tm_time_match_4 = new TmTime();

	/**
	 * tm time match 2
	 */
	protected TmTime _tm_time_match_2 = new TmTime();

	/**
	 * tm time match end
	 */
	protected TmTime _tm_time_match_end = new TmTime();

	/**
	 * match type
	 */
	protected GuildTournamentMatchType _nMatchType = GuildTournamentMatchType.GUILD_TOURNAMENT_MATCH_NONE;

	/**
	 * torunament list
	 */
	protected ArrayList<GuildTournamentList> _tournamentList = new ArrayList<>();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (9*4) + (9*9*4) + 1 + (31 * 19);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nError			= reader.readShort();
		this._time_start		= reader.readInt();
		this._time_practic		= reader.readInt();
		this._time_practicEnd	= reader.readInt();
		this._time_match_161	= reader.readInt();
		this._time_match_162	= reader.readInt();
		this._time_match_8		= reader.readInt();
		this._time_match_4		= reader.readInt();
		this._time_match_2		= reader.readInt();
		this._time_match_end	= reader.readInt();
		this._tm_time_start.read(reader);
		this._tm_time_practic.read(reader);
		this._tm_time_practicEnd.read(reader);
		this._tm_time_match_161.read(reader);
		this._tm_time_match_162.read(reader);
		this._tm_time_match_8.read(reader);
		this._tm_time_match_4.read(reader);
		this._tm_time_match_2.read(reader);
		this._tm_time_match_end.read(reader);
		this._nMatchType = GuildTournamentMatchType.intToType(reader.readByteInt());

		for( int i=0; i<31; i++ ) {
			GuildTournamentList tlist = new GuildTournamentList();
			tlist.read(reader);

			this._tournamentList.add(tlist);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nError);
		writer.writeInt(this._time_start);
		writer.writeInt(this._time_practic);
		writer.writeInt(this._time_practicEnd);
		writer.writeInt(this._time_match_161);
		writer.writeInt(this._time_match_162);
		writer.writeInt(this._time_match_8);
		writer.writeInt(this._time_match_4);
		writer.writeInt(this._time_match_2);
		writer.writeInt(this._time_match_end);
		this._tm_time_start.write(writer);
		this._tm_time_practic.write(writer);
		this._tm_time_practicEnd.write(writer);
		this._tm_time_match_161.write(writer);
		this._tm_time_match_162.write(writer);
		this._tm_time_match_8.write(writer);
		this._tm_time_match_4.write(writer);
		this._tm_time_match_2.write(writer);
		this._tm_time_match_end.write(writer);
		writer.writeByteInt(this._nMatchType.getValue());

		for( int i=0; i<31; i++ ) {
			GuildTournamentList tlist = this._tournamentList.get(i);

			if( tlist == null ) {
				tlist = new GuildTournamentList();
			}

			tlist.write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"GuildTournamentListAck: [" +
			"nError: " + Integer.toString(this._nError) +
			" time_start: " + Integer.toString(this._time_start) +
			" time_practic: " + Integer.toString(this._time_practic) +
			" time_practicEnd: " + Integer.toString(this._time_practicEnd) +
			" time_match_161: " + Integer.toString(this._time_match_161) +
			" time_match_162: " + Integer.toString(this._time_match_162) +
			" time_match_8: " + Integer.toString(this._time_match_8) +
			" time_match_4: " + Integer.toString(this._time_match_4) +
			" time_match_2: " + Integer.toString(this._time_match_2) +
			" time_match_end: " + Integer.toString(this._time_match_end) +
			"]"
			);

		// Todo

		return dump;
	}
}
