/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * GuildNameReq
 * @author FantaBlueMystery
 */
public class GuildNameReq extends AbstractStruct {

	/**
	 * number (id) of guild
	 */
	protected long _nNo = 0;

	/**
	 * GuildNameReq
	 */
	public GuildNameReq() {}

	/**
	 * GuildNameReq
	 * @param guildId
	 */
	public GuildNameReq(long guildId) {
		this._nNo = guildId;
	}

	/**
	 * getGuildId
	 * @return
	 */
	public long getGuildId() {
		return this._nNo;
	}

	/**
	 * setGuildId
	 * @param id
	 */
	public void setGuildId(long id) {
		this._nNo = id;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nNo = reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._nNo);
	}
}