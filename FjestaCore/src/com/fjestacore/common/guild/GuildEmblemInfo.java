/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.guild;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * GuildEmblemInfo
 * @author FantaBlueMystery
 */
public class GuildEmblemInfo extends AbstractStruct {

	/**
	 * file number
	 */
	protected int _nPTFileNo = 0;

	/**
	 * icon number
	 */
	protected int _nPTIconNo = 0;

	/**
	 * getFileNo
	 * @return
	 */
	public int getFileNo() {
		return this._nPTFileNo;
	}

	/**
	 * setFileNo
	 * @param fileNo
	 */
	public void setFileNo(int fileNo) {
		this._nPTFileNo = fileNo;
	}

	/**
	 * getIconNo
	 * @return
	 */
	public int getIconNo() {
		return this._nPTIconNo;
	}

	/**
	 * setIconNo
	 * @param iconNo
	 */
	public void setIconNo(int iconNo) {
		this._nPTIconNo = iconNo;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nPTFileNo = reader.readByteInt();
		this._nPTIconNo = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._nPTFileNo);
		writer.writeByteInt(this._nPTIconNo);
	}
}