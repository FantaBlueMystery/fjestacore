/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.zone;

import com.fjestacore.core.handle.IWorldHandle;

/**
 * IZoneChardata
 * @author FantaBlueMystery
 */
public interface IZoneChardata extends IWorldHandle {

	/**
	 * getCharname
	 * return a charname as string of map login
	 * @return
	 */
	public String getCharname();
}