/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.zone;

import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ZoneChardata
 * @author FantaBlueMystery
 */
public class ZoneChardata extends AbstractStruct implements IZoneChardata {

	/**
	 * world handle
	 */
	protected int _worldHandle = 0;

	/**
	 * charname
	 */
	protected Name5 _charname = new Name5();

	/**
	 * getWorldHandle
	 * @return
	 */
	@Override
	public int getWorldHandle() {
		return this._worldHandle;
	}

	/**
	 * setWorldHandle
	 * @param handle
	 */
	@Override
	public void setWorldHandle(int handle) {
		this._worldHandle = handle;
	}

	/**
	 * getCharname
	 * @return
	 */
	@Override
	public String getCharname() {
		return this._charname.getContent();
	}

	/**
	 * setCharname
	 * @param charname
	 */
	public void setCharname(String charname) {
		this._charname.setContent(charname);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 22;
	}

	/**
	 *
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._worldHandle = reader.readShort();
		this._charname.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((int) this._worldHandle);
		this._charname.write(writer);
	}
}