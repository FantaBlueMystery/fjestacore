/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.item;

import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ItemChargedItemInfo
 * @author FantaBlueMystery
 */
public class ItemChargedItemInfo extends AbstractStruct {

	/**
	 * SIZE
	 */
	static public int SIZE = 16;

	/**
	 * item order no
	 */
	protected long _itemOrderNo = 0;

	/**
	 * item code
	 */
	protected long _itemCode = 0;

	/**
	 * item amount
	 */
	protected long _itemAmount = 0;

	/**
	 * item register date
	 */
	protected DateTime _itemRegisterDate = new DateTime();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return ItemChargedItemInfo.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._itemOrderNo	= reader.readUInt();
		this._itemCode		= reader.readUInt();
		this._itemAmount	= reader.readUInt();
		this._itemRegisterDate.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._itemOrderNo);
		writer.writeUInt(this._itemCode);
		writer.writeUInt(this._itemAmount);
		this._itemRegisterDate.write(writer);
	}
}