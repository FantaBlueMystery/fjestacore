/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

/**
 * IItem
 * @author FantaBlueMystery
 */
public interface IItem {

	/**
	 * conts
	 */
	public static int ITEM_MAX_SIZE = 130;

	/**
	 * getId
	 * id of item
	 * @return
	 */
	public int getId();

	/**
	 * getAttr
	 * @return
	 */
	public IItemAttr getAttr();
}
