/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.common.item.attr.ActionItem;
import com.fjestacore.common.item.attr.ActiveSkill;
import com.fjestacore.common.item.attr.Amount;
import com.fjestacore.common.item.attr.Amulet;
import com.fjestacore.common.item.attr.Armor;
import com.fjestacore.common.item.attr.BindItem;
import com.fjestacore.common.item.attr.Boot;
import com.fjestacore.common.item.attr.Bracelet;
import com.fjestacore.common.item.attr.ByteLot;
import com.fjestacore.common.item.attr.Capsule;
import com.fjestacore.common.item.attr.CostumShield;
import com.fjestacore.common.item.attr.CostumWeapon;
import com.fjestacore.common.item.attr.Decoration;
import com.fjestacore.common.item.attr.DwrdLot;
import com.fjestacore.common.item.attr.Enchant;
import com.fjestacore.common.item.attr.Feed;
import com.fjestacore.common.item.attr.Furniture;
import com.fjestacore.common.item.attr.GBCoin;
import com.fjestacore.common.item.attr.ItemChest;
import com.fjestacore.common.item.attr.KQStep;
import com.fjestacore.common.item.attr.KingdomQuest;
import com.fjestacore.common.item.attr.MiniHouseSkin;
import com.fjestacore.common.item.attr.MobCardCollect;
import com.fjestacore.common.item.attr.MobCardCollectUnident;
import com.fjestacore.common.item.attr.NoEffect;
import com.fjestacore.common.item.attr.Pet;
import com.fjestacore.common.item.attr.QuestItem;
import com.fjestacore.common.item.attr.RecallScroll;
import com.fjestacore.common.item.attr.Ridding;
import com.fjestacore.common.item.attr.Shield;
import com.fjestacore.common.item.attr.SkillScroll;
import com.fjestacore.common.item.attr.UpBlue;
import com.fjestacore.common.item.attr.UpGold;
import com.fjestacore.common.item.attr.UpRed;
import com.fjestacore.common.item.attr.UpSource;
import com.fjestacore.common.item.attr.Weapon;
import com.fjestacore.common.item.attr.WeaponTitleLicence;
import com.fjestacore.common.item.attr.WordLot;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ItemAttr
 * @author FantaBlueMystery
 */
public class ItemAttr extends AbstractStruct implements IItemAttr {

	/**
	 * buffer
	 */
	protected byte[] _buffer = null;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		if( this._buffer != null ) {
			return this._buffer.length;
		}

		return 0;
	}

	/**
	 * getBuffer
	 * @return
	 */
	public byte[] getBuffer() {
		return this._buffer;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._buffer = reader.readRemaining();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBuffer(this._buffer);
	}

	/**
	 * getAttr
	 * @param iclass
	 * @return
	 */
	static public ItemAttr getAttr(int iclass) {
		return ItemAttr.getAttr(ItemClass.intToItemClass(iclass));
	}

	/**
	 * getAttr
	 * @param iclass
	 * @return
	 */
	static public ItemAttr getAttr(ItemClass iclass) {
		switch( iclass ) {
			case BYTELOT:
				return new ByteLot();

			case WORDLOT:
				return new WordLot();

			case DWRDLOT:
				return new DwrdLot();

			case QUESTITEM:
				return new QuestItem();

			case AMULET:
				return new Amulet();

			case WEAPON:
				return new Weapon();

			case ARMOR:
				return new Armor();

			case SHIELD:
				return new Shield();

			case BOOT:
				return new Boot();

			case FURNITURE:
				return new Furniture();

			case DECORATION:
				return new Decoration();

			case SKILLSCROLL:
				return new SkillScroll();

			case RECALLSCROLL:
				return new RecallScroll();

			case BINDITEM:
				return new BindItem();

			case UPSOURCE:
				return new UpSource();

			case ITEMCHEST:
				return new ItemChest();

			case WTLICENCE:
				return new WeaponTitleLicence();

			case KQ:
				return new KingdomQuest();

			case HOUSESKIN:
				return new MiniHouseSkin();

			case UPRED:
				return new UpRed();

			case UPBLUE:
				return new UpBlue();

			case KQSTEP:
				return new KQStep();

			case FEED:
				return new Feed();

			case RIDING:
				return new Ridding();

			case AMOUNT:
				return new Amount();

			case UPGOLD:
				return new UpGold();

			case COSWEAPON:
				return new CostumWeapon();

			case ACTIONITEM:
				return new ActionItem();

			case CAPSULE:
				return new Capsule();

			case GBCOIN:
				return new GBCoin();

			case CLOSEDCARD:
				return new MobCardCollectUnident();

			case OPENCARD:
				return new MobCardCollect();

			case NOEFFECT:
				return new NoEffect();

			case ACTIVESKILL:
				return new ActiveSkill();

			case ENCHANT:
				return new Enchant();

			case PET:
				return new Pet();

			case COSSHIELD:
				return new CostumShield();

			case BRACELET:
				return new Bracelet();
		}

		return null;
	}
}