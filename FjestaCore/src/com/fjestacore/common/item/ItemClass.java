/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

/**
 * ItemClass
 * @author FantaBlueMystery
 */
public enum ItemClass {
	BYTELOT(0x0),
	WORDLOT(0x1),
	DWRDLOT(0x2),
	QUESTITEM(0x3),
	AMULET(0x4),
	WEAPON(0x5),
	ARMOR(0x6),
	SHIELD(0x7),
	BOOT(0x8),
	FURNITURE(0x9),
	DECORATION(0xA),
	SKILLSCROLL(0xB),
	RECALLSCROLL(0xC),
	BINDITEM(0xD),
	UPSOURCE(0xE),
	ITEMCHEST(0xF),
	WTLICENCE(0x10),
	KQ(0x11),
	HOUSESKIN(0x12),
	UPRED(0x13),
	UPBLUE(0x14),
	KQSTEP(0x15),
	FEED(0x16),
	RIDING(0x17),
	AMOUNT(0x18),
	UPGOLD(0x19),
	COSWEAPON(0x1A),
	ACTIONITEM(0x1B),
	CAPSULE(0x1C),
	GBCOIN(0x1D),
	CLOSEDCARD(0x1E),
	OPENCARD(0x1F),
	MONEY(0x20),
	NOEFFECT(0x21),
	ACTIVESKILL(0x22),
	ENCHANT(0x23),
	PET(0x24),
	COSSHIELD(0x25),
	BRACELET(0x26),
	MAX_ITEMCLASSENUM(0x27)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * ItemClass
	 * @param value
	 */
	private ItemClass(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToItemClass
	 * @param iclass
	 * @return
	 */
	static public ItemClass intToItemClass(int iclass) {
		ItemClass[] classes = ItemClass.values();

		for( ItemClass tmp: classes ) {
			if( tmp.getValue() == iclass ) {
				return tmp;
			}
		}

		return ItemClass.MAX_ITEMCLASSENUM;
	}
}