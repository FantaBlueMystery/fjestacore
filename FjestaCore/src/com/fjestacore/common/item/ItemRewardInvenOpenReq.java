/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ItemRewardInvenOpenReq
 * @author FantaBlueMystery
 */
public class ItemRewardInvenOpenReq extends AbstractStruct {

	/**
	 * page
	 */
	protected int _page = 0;

	/**
	 * getPage
	 * @return
	 */
	public int getPage() {
		return this._page;
	}

	/**
	 * setPage
	 * @param page
	 */
	public void setPage(int page) {
		this._page = page;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._page = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._page);
	}
}