/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * ItemPacketInform
 * @author FantaBlueMystery
 */
public class ItemPacketInform extends AbstractStruct implements IAbstractStructDump {

	/**
	 * location
	 */
	protected ItemInven _location = new ItemInven();

	/**
	 * info
	 */
	protected Item _info = new Item();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1 + this._location.getSize() + this._info.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int datasize = reader.readByteInt();

		this._location.read(reader);

		ReaderStream treader = new ReaderStream(reader.readBuffer(datasize-2));

		this._info.read(treader);

		if( !treader.isEOS() ) {
			throw new IOException(
				"Struct not ready read, position end by: " +
				Long.toString(reader.getOffset())
				);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._info.getSize());
		this._location.write(writer);
		this._info.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ItemPacketInform:" +
			" Location: " + Integer.toString(this._location.getInven())
			);

		dump += this._info.dump(deep+1);

		return dump;
	}
}