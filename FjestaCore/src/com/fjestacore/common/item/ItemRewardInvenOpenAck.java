/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ItemRewardInvenOpenAck
 * @author FantaBlueMystery
 */
public class ItemRewardInvenOpenAck extends AbstractStruct implements IAbstractStructDump {

	/**
	 * item array list
	 */
	protected ArrayList<ItemPacketInform> _itemarray = new ArrayList();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 1;

		for( ItemPacketInform ipi: this._itemarray ) {
			size += ipi.getSize();
		}

		return size;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int itemcounter = reader.readByteInt();

		for(int i=0; i<itemcounter; i++ ) {
			ItemPacketInform ipi = new ItemPacketInform();
			ipi.read(reader);

			this._itemarray.add(ipi);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._itemarray.size());

		for( ItemPacketInform ipi: this._itemarray ) {
			ipi.write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ItemRewardInvenOpenAck:" +
			"count: " + Integer.toString(this._itemarray.size())
			);

		for( ItemPacketInform ipi: this._itemarray ) {
			dump += ipi.dump(deep+1);
			dump += "\r\n";
		}

		return dump;
	}
}