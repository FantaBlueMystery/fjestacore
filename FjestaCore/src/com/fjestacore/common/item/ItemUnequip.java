/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.item;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * ItemUnequip
 * @author FantaBlueMystery
 */
public class ItemUnequip extends AbstractStruct implements IAbstractStructDump {

	/**
	 * slot equip
	 */
	protected int _slotequip = 0;

	/**
	 * slot inven
	 */
	protected int _slotinven = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._slotequip = reader.readByteInt();
		this._slotinven = reader.readByteInt();
	}

	/**
	 *
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._slotequip);
		writer.writeByteInt(this._slotinven);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ItemUnequip: [" +
			"SlotEguip: " + Integer.toString(this._slotequip) +
			" SlotInven: " + Integer.toString(this._slotinven) +
			"]"
			);

		return dump;
	}
}