/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ItemSellReq
 * @author FantaBlueMystery
 */
public class ItemSellReq extends AbstractStruct {

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * lot
	 */
	protected int _lot = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._slot = reader.readByteInt();
		this._lot = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._slot);
		writer.writeShort(this._lot);
	}
}