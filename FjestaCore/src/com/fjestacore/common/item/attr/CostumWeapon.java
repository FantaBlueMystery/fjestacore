/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * CostumWeapon
 * @author FantaBlueMystery
 */
public class CostumWeapon extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 4;

	/**
	 * costum charged
	 */
	protected long _costumCharged = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return CostumWeapon.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._costumCharged = reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._costumCharged);
	}
}