/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.shine.ShinePutOnBelongedItem;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * Decoration
 * @author FantaBlueMystery
 */
public class Decoration extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 10;

	/**
	 * delete time
	 */
	protected DateTime _deletetime = new DateTime();

	/**
	 * is put on belonged
	 */
	protected ShinePutOnBelongedItem _isPutOnBelonged = ShinePutOnBelongedItem.SPOBI_NOT_BELONGED;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Decoration.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._deletetime.read(reader);

		this._isPutOnBelonged	= ShinePutOnBelongedItem.intToSPOBI(reader.readByteInt());

		// ?? new 10.12.2019
		reader.readBuffer(5);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._deletetime.write(writer);

		writer.writeByteInt(this._isPutOnBelonged.getValue());

		// ?? new 10.12.2019
		writer.writeString("", 5);
	}
}