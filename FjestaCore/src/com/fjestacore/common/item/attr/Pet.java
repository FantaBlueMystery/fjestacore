/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.name.Name4P;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * Pet
 * @author FantaBlueMystery
 */
public class Pet extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 12;

	/**
	 * n pet reg num
	 */
	protected int _nPetRegNum = 0;

	/**
	 * n pet id
	 */
	protected int _nPetId = 0;

	/**
	 * name
	 */
	protected Name4P _name = new Name4P();

	/**
	 * summoning
	 */
	protected int _bSummoning = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Pet.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nPetRegNum	= reader.readShort();
		this._nPetId		= reader.readShort();

		this._name.read(reader);

		this._bSummoning	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nPetRegNum);
		writer.writeShort(this._nPetId);

		this._name.write(writer);

		writer.writeByteInt(this._bSummoning);
	}
}