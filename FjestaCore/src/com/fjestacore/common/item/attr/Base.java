/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.item.attr.option.Storage;
import com.fjestacore.common.shine.ShinePutOnBelongedItem;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * ItemAttrBase1
 * @author FantaBlueMystery
 */
public class Base extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 35;

	/**
	 * uppgrade
	 */
	protected int _upgrade = 0;

	/**
	 * strengthen
	 */
	protected int _strengthen = 0;

	/**
	 * upgrade fail count
	 */
	protected int _upgradefailcount = 0;

	/**
	 * is put on belonged
	 */
	protected ShinePutOnBelongedItem _isPutOnBelonged = ShinePutOnBelongedItem.SPOBI_NOT_BELONGED;

	/**
	 * delete time
	 */
	protected DateTime _deletetime = new DateTime();

	/**
	 * random option changed count
	 */
	protected int _randomOptionChangedCount = 0;

	/**
	 * option
	 */
	protected Storage _option = new Storage();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Base.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._upgrade			= reader.readByteInt();
		this._strengthen		= reader.readByteInt();
		this._upgradefailcount	= reader.readByteInt();
		this._isPutOnBelonged	= ShinePutOnBelongedItem.intToSPOBI(reader.readByteInt());

		this._deletetime.read(reader);

		this._randomOptionChangedCount = reader.readByteInt();

		this._option.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._upgrade);
		writer.writeByteInt(this._strengthen);
		writer.writeByteInt(this._upgradefailcount);
		writer.writeByteInt(this._isPutOnBelonged.getValue());

		this._deletetime.write(writer);

		writer.writeByteInt(this._randomOptionChangedCount);

		this._option.write(writer);
	}
}