/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * ActionItem
 * @author FantaBlueMystery
 */
public class ActionItem extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 4;

	/**
	 * delete time
	 */
	protected DateTime _deletetime = new DateTime();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return ActionItem.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._deletetime.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._deletetime.write(writer);
	}
}