/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.shine.ShinePutOnBelongedItem;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * Ridding
 * @author FantaBlueMystery
 */
public class Ridding extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 21;

	/**
	 * hungrypoint
	 */
	protected int _hungrypoint = 0;

	/**
	 * delete time
	 */
	protected DateTime _deletetime = new DateTime();

	/**
	 * bit flag
	 */
	protected int _bitflag = 0;

	/**
	 * is put on belonged
	 */
	protected ShinePutOnBelongedItem _isPutOnBelonged = ShinePutOnBelongedItem.SPOBI_NOT_BELONGED;

	/**
	 *
	 */
	protected long _nHP = 0;

	/**
	 * grade
	 */
	protected int _nGrade = 0;

	/**
	 * rare fail count
	 */
	protected int _nRareFailCount = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Ridding.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._hungrypoint = reader.readShort();

		this._deletetime.read(reader);

		this._bitflag			= reader.readShort();
		this._isPutOnBelonged	= ShinePutOnBelongedItem.intToSPOBI(reader.readByteInt());
		this._nHP				= reader.readUInt();
		this._nGrade			= reader.readByteInt();
		this._nRareFailCount	= reader.readShort();

		// ?? new 10.12.2019
		reader.readBuffer(3);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._hungrypoint);

		this._deletetime.write(writer);

		writer.writeShort(this._bitflag);
		writer.writeByteInt(this._isPutOnBelonged.getValue());
		writer.writeUInt(this._nHP);
		writer.writeByteInt(this._nGrade);
		writer.writeShort(this._nRareFailCount);

		// ?? new 10.12.2019
		writer.writeString("", 3);
	}
}