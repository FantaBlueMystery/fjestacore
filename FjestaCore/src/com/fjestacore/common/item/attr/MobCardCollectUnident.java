/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * MobCardCollectUnident
 * @author FantaBlueMystery
 */
public class MobCardCollectUnident extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 9;

	/**
	 * serial number
	 */
	protected long _serialNumber = 0;

	/**
	 * card id
	 */
	protected int _cardID = 0;

	/**
	 * star
	 */
	protected int _star = 0;

	/**
	 * group
	 */
	protected int _group = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return MobCardCollectUnident.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._serialNumber	= reader.readUInt();
		this._cardID		= reader.readShort();
		this._star			= reader.readByteInt();
		this._group			= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._serialNumber);
		writer.writeShort(this._cardID);
		writer.writeByteInt(this._star);
		writer.writeShort(this._group);
	}
}