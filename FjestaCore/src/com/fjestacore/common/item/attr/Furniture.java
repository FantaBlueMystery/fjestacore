/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * Furniture
 * @author FantaBlueMystery
 */
public class Furniture extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 32;

	/**
	 * flag
	 */
	protected int _flag = 0;

	/**
	 * furnicher id
	 */
	protected int _furnicherId = 0;

	/**
	 * delete time
	 */
	protected DateTime _deletetime = new DateTime();

	/**
	 * loc x
	 */
	protected float _locX = 0;

	/**
	 * loc y
	 */
	protected float _locY = 0;

	/**
	 * loc z
	 */
	protected float _locZ = 0;

	/**
	 * dendure end time
	 */
	protected DateTime _dEndureEndTime = new DateTime();

	/**
	 * nendure grade
	 */
	protected int _nEndureGrade = 0;

	/**
	 * reward money
	 */
	protected ULong _nRewardMoney = ULong.MIN;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Furniture.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._flag			= reader.readByteInt();
		this._furnicherId	= reader.readShort();

		this._deletetime.read(reader);

		this._locX			= reader.readFloat();
		this._locY			= reader.readFloat();
		this._locZ			= reader.readFloat();

		this._dEndureEndTime.read(reader);

		this._nEndureGrade	= reader.readByteInt();
		this._nRewardMoney	= reader.readULong();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._flag);
		writer.writeShort((short) this._furnicherId);

		this._deletetime.write(writer);

		writer.writeFloat(this._locX);
		writer.writeFloat(this._locY);
		writer.writeFloat(this._locZ);

		this._dEndureEndTime.write(writer);

		writer.writeByteInt(this._nEndureGrade);
		writer.writeULong(this._nRewardMoney);
	}
}
