/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * ByteLot
 * @author FantaBlueMystery
 */
public class ByteLot extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 1;

	/**
	 * lot
	 */
	protected int _lot = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return ByteLot.SIZE;
	}

	/**
	 * getLot
	 * @return
	 */
	public int getLot() {
		return this._lot;
	}

	/**
	 * setLot
	 * @param lot
	 */
	public void setLot(int lot) {
		this._lot = lot;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._lot = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._lot);
	}
}