/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.item.attr.bind.Bind;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * BindItem
 * @author FantaBlueMystery
 */
public class BindItem extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 101;

	/**
	 * portal num
	 */
	protected int _portalnum = 0;

	/**
	 * bind
	 */
	protected Bind[] _bind = new Bind[10];

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return BindItem.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._portalnum = reader.readByteInt();

		for( int i=0; i<this._bind.length; i++ ) {
			this._bind[i] = new Bind();
			this._bind[i].read(reader);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._portalnum);

		for( int i=0; i<this._bind.length; i++ ) {
			if( this._bind[i] == null ) {
				this._bind[i] = new Bind();
			}

			this._bind[i].write(writer);
		}
	}
}