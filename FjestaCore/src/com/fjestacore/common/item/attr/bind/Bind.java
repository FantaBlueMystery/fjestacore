/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fjestacore.common.item.attr.bind;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Bind
 * @author FantaBlueMystery
 */
public class Bind extends AbstractStruct {

	/**
	 * map id
	 */
	protected int _mapid = 0;

	/**
	 * position
	 */
	protected PositionXY _position = new PositionXY();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 10;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._mapid = reader.readShort();
		this._position.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._mapid);
		this._position.write(writer);
	}
}