/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.item.attr.item.RegistNumber;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * Capsule
 * @author FantaBlueMystery
 */
public class Capsule extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 28;

	/**
	 * content
	 */
	protected RegistNumber _content = new RegistNumber();

	/**
	 * useabletime
	 */
	protected DateTime _useabletime = new DateTime();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Capsule.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._content.read(reader);
		this._useabletime.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._content.write(writer);
		this._useabletime.write(writer);
	}
}