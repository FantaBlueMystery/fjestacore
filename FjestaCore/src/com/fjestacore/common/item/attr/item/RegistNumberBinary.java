/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr.item;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * RegistNumberBinary
 * @author FantaBlueMystery
 */
public class RegistNumberBinary extends AbstractStruct {

	/**
	 * ?
	 */
	protected byte[] _gap0 = new byte[4];

	/**
	 * bf4
	 */
	protected long _bf4 = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 8;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._gap0 = reader.readBuffer(4);
		this._bf4 = reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBuffer(this._gap0);
		writer.writeUInt(this._bf4);
	}
}