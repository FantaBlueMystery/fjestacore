/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr.item;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.ULong;
import java.io.IOException;

/**
 * Registnumber
 * @author FantaBlueMystery
 */
public class RegistNumber extends AbstractStruct {

	/**
	 * binary
	 */
	protected RegistNumberBinary _binary = new RegistNumberBinary();

	/**
	 * ?
	 */
	protected long[] _dwrdkey = new long[2];

	/**
	 * key
	 */
	protected ULong _key = ULong.MIN;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 24;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._binary.read(reader);	//+8

		// +2*4
		for( int i=0; i<this._dwrdkey.length; i++ ) {
			this._dwrdkey[i] = reader.readUInt();
		}

		// +8
		this._key = reader.readULong();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._binary.write(writer);

		for( int i=0; i<this._dwrdkey.length; i++ ) {
			writer.writeUInt(this._dwrdkey[i]);
		}

		writer.writeULong(this._key);
	}
}