/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.item.attr.option.Storage;
import com.fjestacore.common.item.attr.weapon.GemSocket;
import com.fjestacore.common.item.attr.weapon.Mobkill;
import com.fjestacore.common.shine.ShinePutOnBelongedItem;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * Weapon
 * @author FantaBlueMystery
 */
public class Weapon extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 87;

	/**
	 * uppgrade
	 */
	protected int _upgrade = 0;

	/**
	 * strengthen
	 */
	protected int _strengthen = 0;

	/**
	 * upgrade fail count
	 */
	protected int _upgradefailcount = 0;

	/**
	 * is put on belonged
	 */
	protected ShinePutOnBelongedItem _isPutOnBelonged = ShinePutOnBelongedItem.SPOBI_NOT_BELONGED;

	/**
	 * mobkills
	 */
	protected Mobkill[] _mobkills = new Mobkill[3];

	/**
	 * character title mob id
	 */
	protected int _characterTitleMobID = 0;

	/**
	 * user title
	 */
	protected String _usertitle = "";

	/**
	 * gem socket
	 */
	protected GemSocket[] _gemSockets = new GemSocket[3];

	/**
	 * max socket count
	 */
	protected int _maxSocketCount = 0;

	/**
	 * created socket count
	 */
	protected int _createdSocketCount = 0;

	/**
	 * delete time
	 */
	protected DateTime _deletetime = new DateTime();

	/**
	 * random option changed count
	 */
	protected int _randomOptionChangedCount = 0;

	/**
	 * option
	 */
	protected Storage _option = new Storage();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Weapon.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._upgrade			= reader.readByteInt();	// +1
		this._strengthen		= reader.readByteInt();	// +1
		this._upgradefailcount	= reader.readByteInt();	// +1

		this._isPutOnBelonged	= ShinePutOnBelongedItem.intToSPOBI(reader.readByteInt());	// +1

		// +6 *3
		for( int i=0; i<this._mobkills.length; i++ ) {
			this._mobkills[i] = new Mobkill();
			this._mobkills[i].read(reader);
		}

		this._characterTitleMobID	= reader.readShort();	// +2
		this._usertitle				= reader.readString(21).trim();	// +21

		// +3 * 3
		for( int i=0; i<this._gemSockets.length; i++ ) {
			this._gemSockets[i] = new GemSocket();
			this._gemSockets[i].read(reader);
		}

		this._maxSocketCount		= reader.readByteInt();	//  +1
		this._createdSocketCount	= reader.readByteInt();	// +1

		this._deletetime.read(reader);	// +4

		this._randomOptionChangedCount = reader.readByteInt();	// +1

		this._option.read(reader);	// 26
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._upgrade);
		writer.writeByteInt(this._strengthen);
		writer.writeByteInt(this._upgradefailcount);

		for( int i=0; i<this._mobkills.length; i++ ) {
			if( this._mobkills[i] == null ) {
				this._mobkills[i] = new Mobkill();
			}

			this._mobkills[i].write(writer);
		}

		writer.writeShort((short) this._characterTitleMobID);
		writer.writeString(this._usertitle, 21);

		for( int i=0; i<this._gemSockets.length; i++ ) {
			if( this._gemSockets[i] == null ) {
				this._gemSockets[i] = new GemSocket();
			}

			this._gemSockets[i].write(writer);
		}

		writer.writeByteInt(this._maxSocketCount);
		writer.writeByteInt(this._createdSocketCount);

		this._deletetime.write(writer);

		writer.writeByteInt(this._randomOptionChangedCount);

		this._option.write(writer);
	}
}