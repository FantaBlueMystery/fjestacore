/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr.option;

/**
 * ItemOptionType
 * @author FantaBlueMystery
 */
public enum ItemOptionType {
	NUMBER(0),
	STR(1),
	INT(2),
	DEX(3),
	MEN(4),
	CON(5),
	TH(6),
	TB(7),
	AC(8),
	WC(9),
	MA(10),
	MR(11),
	MAX(12)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * ItemOptionType
	 * @param value
	 */
	private ItemOptionType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getItemOptionType
	 * @param value
	 * @return
	 */
	static public ItemOptionType getItemOptionType(int value) {
		ItemOptionType[] types = ItemOptionType.values();

		for( ItemOptionType type: types ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return ItemOptionType.NUMBER;
	}
}