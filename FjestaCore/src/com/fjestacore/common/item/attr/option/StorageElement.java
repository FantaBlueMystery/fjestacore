/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr.option;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * StorageElement
 * @author FantaBlueMystery
 */
public class StorageElement extends AbstractStruct {

	/**
	 * type
	 */
	protected ItemOptionType _type = ItemOptionType.NUMBER;

	/**
	 * value
	 */
	protected int _value = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._type = ItemOptionType.getItemOptionType(reader.readByteInt());
		this._value = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._type.getValue());
		writer.writeShort((short) this._value);
	}
}