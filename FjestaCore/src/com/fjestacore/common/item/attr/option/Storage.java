/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr.option;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Storage
 * @author FantaBlueMystery
 */
public class Storage extends AbstractStruct {

	/**
	 * fixed
	 */
	protected StorageFixedInfo _fixed = new StorageFixedInfo();

	/**
	 * optionlist
	 */
	protected StorageElement[] _optionlist = new StorageElement[8];

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (3*8);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._fixed.read(reader);	// +2

		for( int i=0; i<this._optionlist.length; i++ ) {
			this._optionlist[i] = new StorageElement();
			this._optionlist[i].read(reader);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._fixed.write(writer);

		for( StorageElement _optionlist1 : this._optionlist ) {
			_optionlist1.write(writer);
		}
	}
}