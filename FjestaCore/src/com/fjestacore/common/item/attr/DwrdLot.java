/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * DwrdLot
 * @author FantaBlueMystery
 */
public class DwrdLot extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 4;

	/**
	 * lot
	 */
	protected long _lot = 0;

	/**
	 * getLot
	 * @return
	 */
	public long getLot() {
		return this._lot;
	}

	/**
	 * setLot
	 * @param lot
	 */
	public void setLot(long lot) {
		this._lot = lot;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._lot = reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._lot);
	}
}
