/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * MobCardCollect
 * @author FantaBlueMystery
 */
public class MobCardCollect extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 5;

	/**
	 * serial number
	 */
	protected long _serialNumber = 0;

	/**
	 * star
	 */
	protected int _star = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return MobCardCollectUnident.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._serialNumber	= reader.readUInt();
		this._star			= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._serialNumber);
		writer.writeByteInt(this._star);
	}
}