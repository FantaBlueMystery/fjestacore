/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.item.attr.item.RegistNumber;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * ItemChest
 * @author FantaBlueMystery
 */
public class ItemChest extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 193;

	/**
	 * type
	 */
	protected int _type = 0;

	/**
	 * content
	 */
	protected RegistNumber[] _content = new RegistNumber[8];

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return ItemChest.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._type = reader.readByteInt();

		for( int i=0; i<this._content.length; i++ ) {
			if( this._content[i] == null ) {
				this._content[i] = new RegistNumber();
			}

			this._content[i].read(reader);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._type);

		for( int i=0; i<this._content.length; i++ ) {
			if( this._content[i] == null ) {
				this._content[i] = new RegistNumber();
			}

			this._content[i].write(writer);
		}
	}
}