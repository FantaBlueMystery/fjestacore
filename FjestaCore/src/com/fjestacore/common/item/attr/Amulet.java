/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item.attr;

import com.fjestacore.common.item.ItemAttr;
import com.fjestacore.common.item.attr.option.Storage;
import com.fjestacore.common.shine.ShinePutOnBelongedItem;
import com.fjestacore.common.time.DateTime;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * Amulet
 * @author FantaBlueMystery
 */
public class Amulet extends ItemAttr {

	/**
	 * size of object
	 */
	static public Integer SIZE = 61;

	/**
	 * delete time
	 */
	protected DateTime _deletetime = new DateTime();

	/**
	 * is put on belonged
	 */
	protected ShinePutOnBelongedItem _isPutOnBelonged = ShinePutOnBelongedItem.SPOBI_NOT_BELONGED;

	/**
	 * uppgrade
	 */
	protected int _upgrade = 0;

	/**
	 * strengthen
	 */
	protected int _strengthen = 0;

	/**
	 * upgrade fail count
	 */
	protected int _upgradefailcount = 0;

	/**
	 * upgrade option
	 */
	protected Storage _upgradeOption = new Storage();

	/**
	 * random option changed count
	 */
	protected int _randomOptionChangedCount = 0;

	/**
	 * option
	 */
	protected Storage _option = new Storage();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return Amulet.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._deletetime.read(reader);

		this._isPutOnBelonged	= ShinePutOnBelongedItem.intToSPOBI(reader.readByteInt());
		this._upgrade			= reader.readByteInt();
		this._strengthen		= reader.readByteInt();
		this._upgradefailcount	= reader.readByteInt();

		this._upgradeOption.read(reader);

		this._randomOptionChangedCount = reader.readByteInt();

		this._option.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._deletetime.write(writer);

		writer.writeByteInt(this._isPutOnBelonged.getValue());
		writer.writeByteInt(this._upgrade);
		writer.writeByteInt(this._strengthen);
		writer.writeByteInt(this._upgradefailcount);

		this._upgradeOption.write(writer);

		writer.writeByteInt(this._randomOptionChangedCount);

		this._option.write(writer);
	}
}