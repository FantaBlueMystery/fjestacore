/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.item;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ItemChargedInvenOpenAck
 * @author FantaBlueMystery
 */
public class ItemChargedInvenOpenAck extends AbstractStruct {

	/**
	 * error code
	 */
	protected int _errorCode = 0;

	/**
	 * part mark
	 */
	protected int _nPartMark = 0;

	/**
	 * charged item info list
	 */
	protected ArrayList<ItemChargedItemInfo> _chargedItemInfoList = new ArrayList<>();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 5 + (this._chargedItemInfoList.size() * ItemChargedItemInfo.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._errorCode = reader.readShort();
		this._nPartMark = reader.readByteInt();

		int numOfChargedItem = reader.readShort();

		for( int i=0; i<numOfChargedItem; i++ ) {
			ItemChargedItemInfo icif = new ItemChargedItemInfo();

			icif.read(reader);

			this._chargedItemInfoList.add(icif);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._errorCode);
		writer.writeByteInt(this._nPartMark);
		writer.writeShort(this._chargedItemInfoList.size());

		for( ItemChargedItemInfo icif: this._chargedItemInfoList ) {
			icif.write(writer);
		}
	}
}