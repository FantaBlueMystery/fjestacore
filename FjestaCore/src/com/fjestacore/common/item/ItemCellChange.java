/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ItemCellChange
 * @author FantaBlueMystery
 */
public class ItemCellChange extends AbstractStruct {

	/**
	 * exchange
	 */
	protected ItemInven _exchange = new ItemInven();

	/**
	 * location
	 */
	protected ItemInven _location = new ItemInven();

	/**
	 * item
	 */
	protected ItemVarStruct _item = new ItemVarStruct();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._exchange.getSize() + this._location.getSize() + this._item.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._exchange.read(reader);
		this._location.read(reader);
		this._item.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._exchange.write(writer);
		this._location.write(writer);
		this._item.write(writer);
	}
}