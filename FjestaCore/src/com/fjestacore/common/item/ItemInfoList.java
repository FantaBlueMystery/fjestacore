/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.files.shn.CDataData;
import java.util.HashMap;
import java.util.Map;

/**
 * ItemInfoList
 * @author FantaBlueMystery
 */
public class ItemInfoList {

	/**
	 * SingletonHolder
	 */
	static private final class SingletonHolder {
		private static ItemInfoList INSTANCE = null;
	}

	/**
	 * setInstance
	 * @param list
	 */
	static public void setInstance(ItemInfoList list) {
		ItemInfoList.SingletonHolder.INSTANCE = list;
	}

	/**
	 * getInstance
	 * @return
	 */
	static public ItemInfoList getInstance() {
		return ItemInfoList.SingletonHolder.INSTANCE;
	}

	/**
	 * item info list
	 */
	protected HashMap<Integer, ItemInfo> _list = new HashMap<>();

	/**
	 * putRows
	 * @param rows
	 */
	public void putRows(HashMap<Object, CDataData> rows) {
		for( Map.Entry<Object, CDataData> entry : rows.entrySet() ) {
			this.put((ItemInfo) entry.getValue().cast(ItemInfo.class));
		}
	}

	/**
	 * put
	 * @param ii
	 */
	public void put(ItemInfo ii) {
		this._list.put(ii.getId(), ii);
	}

	/**
	 * get
	 * @param id
	 * @return
	 */
	public ItemInfo get(int id) {
		return this._list.get(id);
	}

	/**
	 * getSize
	 * @return
	 */
	public int getSize() {
		return this._list.size();
	}
}
