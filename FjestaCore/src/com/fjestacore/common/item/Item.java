/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Item
 * @author FantaBlueMystery
 */
public class Item extends AbstractStruct implements IAbstractStructDump, IItem {

	// item max size
	static public int ITEM_MAX_SIZE = 130;

	// empty slot (max id)
	static public int ITEM_ID_MAX = 65535;

	/**
	 * id
	 */
	protected int _id = 0;

	/**
	 * item attr
	 */
	protected ItemAttr _attr = null;

	/**
	 * Item
	 */
	public Item() {}

	/**
	 * Item
	 * @param id
	 */
	public Item(int id) {
		this._id = id;
	}

	/**
	 * Item
	 * @param id
	 * @param attr
	 */
	public Item(int id, ItemAttr attr) {
		this._id	= id;
		this._attr	= attr;
	}

	/**
	 * setId
	 * @param id
	 */
	public void setId(int id) {
		this._id = id;
	}

	/**
	 * getId
	 * @return
	 */
	@Override
	public int getId() {
		return this._id;
	}

	/**
	 * setAttr
	 * @param attr
	 */
	public void setAttr(ItemAttr attr) {
		this._attr = attr;
	}

	/**
	 * getAttr
	 * @return
	 */
	@Override
	public ItemAttr getAttr() {
		return this._attr;
	}

	/**
	 * isEmpty
	 * @return
	 */
	public boolean isEmpty() {
		return this._id == Item.ITEM_ID_MAX;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._id = reader.readShort();

		if( this._id == Item.ITEM_ID_MAX ) {
			this._attr = new ItemAttr();
		}
		else {
			ItemInfoList list = ItemInfoList.getInstance();
			ItemInfo info = null;

			if( list != null ) {
				info = list.get(this._id);

				if( info != null ) {
					this._attr = ItemAttr.getAttr((int)info.getClassNum());
				}
				else {
					Logger.getLogger(Item.class.getName()).log(Level.SEVERE,
						"Item({0}) not found! ", Integer.toString(this._id));
				}
			}
			else {
				Logger.getLogger(Item.class.getName()).log(Level.SEVERE,
					"Please set ItemInfoList instance!");
			}

			if( this._attr == null ) {
				if( info != null ) {
					Logger.getLogger(Item.class.getName()).log(Level.SEVERE,
						"ItemAttr by Class({0}) is null!", Integer.toString((int)info.getClassNum()));
				}
				else {
					Logger.getLogger(Item.class.getName()).log(Level.SEVERE,
						"ItemAttr is null!");
				}

				this._attr = new ItemAttr();
			}
		}

		this._attr.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort((short) this._id);

		if( this._attr != null ) {
			this._attr.write(writer);
		}
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 2;

		if( this._attr != null ) {
			size += this._attr.getSize();
		}

		return size;
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"Item: [ItemId: " + Integer.toString(this._id) + "]"
			);

		ItemInfoList iil = ItemInfoList.getInstance();

		if( iil != null ) {
			ItemInfo itf = iil.get(_id);

			if( itf != null ) {
				dump += itf.dump(deep+1);
			}
			else {
				dump += "Item not found!";
			}
		}
		else {
			dump += "Please set ItemInfoList instance!";
		}

		return dump;
	}
}