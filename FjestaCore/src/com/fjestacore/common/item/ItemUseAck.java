/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.item;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ItemUseAck
 * @author FantaBlueMystery
 */
public class ItemUseAck extends AbstractStruct {

	/**
	 * error
	 */
	protected int _error = 0;

	/**
	 * used item
	 */
	protected int _useditem = 0;

	/**
	 * inven type
	 */
	protected int _invenType = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 5;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._error		= reader.readShort();
		this._useditem	= reader.readShort();
		this._invenType = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._error);
		writer.writeShort(this._useditem);
		writer.writeByteInt(this._useditem);
	}
}