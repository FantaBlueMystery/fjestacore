/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.item;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.grade.GradeType;
import com.fjestacore.files.shn.CDataData;

/**
 * ItemInfo
 * @author FantaBlueMystery
 */
public class ItemInfo extends CDataData {

	/**
	 * getId
	 * @return
	 */
	public int getId() {
		return (int) this.getData(0);
	}

	/**
	 * getInxName
	 * @return
	 */
	public String getInxName() {
		return (String) this.getData(1);
	}

	/**
	 * getName
	 * @return
	 */
	public String getName() {
		return (String) this.getData(2);
	}

	/**
	 * getType
	 * @return
	 */
	public long getType() {
		return (long) this.getData(3);
	}

	/**
	 * getClassNum
	 * @return
	 */
	public long getClassNum() {
		return (long) this.getData(4);
	}

	/**
	 * getMaxLot
	 * @return
	 */
	public long getMaxLot() {
		return (long) this.getData(5);
	}

	/**
	 * getEquip
	 * @return
	 */
	public long getEquip() {
		return (long) this.getData(6);
	}

	/**
	 * getItemAuctionGroup
	 * @return
	 */
	public long getItemAuctionGroup() {
		return (long) this.getData(7);
	}

	/**
	 * getItemGradeType
	 * @return
	 */
	public long getItemGradeType() {
		return (long) this.getData(8);
	}

	/**
	 * getItemGradeTypeGT
	 * @return
	 */
	public GradeType getItemGradeTypeGT() {
		return GradeType.getGradeType((int) this.getItemGradeType());
	}

	/**
	 * getTwoHand
	 * @return
	 */
	public boolean getTwoHand() {
		return ((byte)this.getData(9) > 0);
	}

	/**
	 * getAtkSpeed
	 * @return
	 */
	public long getAtkSpeed() {
		return (long) this.getData(10);
	}

	/**
	 * getDemandLv
	 * @return
	 */
	public long getDemandLv() {
		return (long) this.getData(11);
	}

	/**
	 * getGrade
	 * @return
	 */
	public long getGrade() {
		return (long) this.getData(12);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ItemInfo: [" + "ID: " + Integer.toString(this.getId()) + " " +
			"inxName: '" + this.getInxName() + "' " +
			"name: '" + this.getName() + "' " +
			"type: " + Long.toString(this.getType()) + " " +
			"class: " + Long.toString(this.getClassNum()) + " <" + ItemClass.intToItemClass((int) this.getClassNum()).name() + "> " +
			"maxLot: " + Long.toString(this.getMaxLot()) + " " +
			"equip: " + Long.toString(this.getEquip()) + " " +
			"itemAuctionGroup: " + Long.toString(this.getItemAuctionGroup()) + " " +
			"itemGradeType: " + Long.toString(this.getItemGradeType()) + " " +
			"twoHand: " + (this.getTwoHand() ? "true" : "false") + " " +
			"atkSpeed: " + Long.toString(this.getAtkSpeed()) + " " +
			"demandLv: " + Long.toString(this.getDemandLv()) + " " +
			"grade: " + Long.toString(this.getGrade()) + "]"
			);

		return dump;
	}
}