/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.item;

/**
 * ItemEquip
 * @author FantaBlueMystery
 */
public enum ItemEquip {
	ITEMEQUIP_NONE(0),
	ITEMEQUIP_HAT(1),
	ITEMEQUIP_NOUSE03(2),
	ITEMEQUIP_NOUSE01(3),
	ITEMEQUIP_NOUSE02(4),
	ITEMEQUIP_FACETATTOO(5),
	ITEMEQUIP_NECKLACE(6),
	ITEMEQUIP_BODY(7),
	ITEMEQUIP_BODYACC(8),
	ITEMEQUIP_BACK(9),
	ITEMEQUIP_LEFTHAND(10),
	ITEMEQUIP_LEFTHANDACC(11),
	ITEMEQUIP_RIGHTHAND(12),
	ITEMEQUIP_RIGHTHANDACC(13),
	ITEMEQUIP_BRACELET(14),
	ITEMEQUIP_LEFTRING(15),
	ITEMEQUIP_RIGHTRING(16),
	ITEMEQUIP_COSEFF(17),
	ITEMEQUIP_TAIL(18),
	ITEMEQUIP_LEG(19),
	ITEMEQUIP_LEGACC(20),
	ITEMEQUIP_SHOES(21),
	ITEMEQUIP_SHOESACC(22),
	ITEMEQUIP_EARRING(23),
	ITEMEQUIP_MOUTH(24),
	ITEMEQUIP_MINIMON(25),
	ITEMEQUIP_EYE(26),
	ITEMEQUIP_HATACC(27),
	ITEMEQUIP_MINIMON_R(28),
	ITEMEQUIP_SHIELDACC(29)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * ItemEquip
	 * @param value
	 */
	private ItemEquip(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}