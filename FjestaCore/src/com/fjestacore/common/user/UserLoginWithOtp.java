/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.user;

import com.fjestacore.common.name.Name8;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * UserLoginWithOtp
 * @author FantaBlueMystery
 */
public class UserLoginWithOtp extends AbstractStruct {

	/**
	 * sOTP
	 */
	protected Name8 _sOTP = new Name8();

	/**
	 * UserLoginWithOtp
	 */
	public UserLoginWithOtp() {}

	/**
	 * getOtp
	 * @return
	 */
	public String getOtp() {
		return this._sOTP.getContent();
	}

	/**
	 * setOtp
	 * @param otp
	 */
	public void setOtp(String otp) {
		this._sOTP.setContent(otp);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._sOTP.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._sOTP.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._sOTP.write(writer);
	}
}