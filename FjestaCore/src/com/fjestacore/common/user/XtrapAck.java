/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.user;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * XtrapAck
 * @author FantaBlueMystery
 */
public class XtrapAck extends AbstractStruct implements IAbstractStructDump {

	/**
	 * allow
	 */
	protected boolean _allow = false;

	/**
	 * XtrapAck
	 */
	public XtrapAck() {}

	/**
	 * XtrapAck
	 * @param allow
	 */
	public XtrapAck(boolean allow) {
		this._allow = allow;
	}

	/**
	 * isAllow
	 * @return
	 */
	public boolean isAllow() {
		return this._allow;
	}

	/**
	 * setAllow
	 * @param allow
	 */
	public void setAllow(boolean allow) {
		this._allow = allow;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._allow = reader.readBoolean();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBoolean(this._allow);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"XtrapAck: [" +
			"allow: " + (this._allow ? "true" : "false") + " " +
			"]"
			);

		return dump;
	}
}