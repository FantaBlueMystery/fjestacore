/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.user;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.Arrays;

/**
 * XtrapReq
 * @author FantaBlueMystery
 */
public class XtrapReq extends AbstractStruct implements IAbstractStructDump {

	/**
	 * xtrapClientkey
	 */
	protected byte[] _XTrapClientKey = null;

	/**
	 * XtrapReq
	 */
	public XtrapReq() {
		this._XTrapClientKey = new byte[29];


	}

	/**
	 * getXtrapClientKey
	 * @return
	 */
	public byte[] getXtrapClientKey() {
		return this._XTrapClientKey;
	}

	/**
	 * getHash
	 * @return
	 */
	public String getHash() {
		return new String(this._XTrapClientKey);
	}

	/**
	 * setHash
	 * @param hash
	 */
	public void setHash(String hash) {
		// hash (md5) of exe
		this._XTrapClientKey = hash.getBytes();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 1;

		if( this._XTrapClientKey != null ) {
			size += this._XTrapClientKey.length;
		}

		return size;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int xTrapClientKeyLength = reader.readByteInt();

		if( xTrapClientKeyLength > 0 ) {
			this._XTrapClientKey = reader.readBuffer(xTrapClientKeyLength);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		if( this._XTrapClientKey != null ) {
			writer.writeByteInt(this._XTrapClientKey.length);
			writer.writeBuffer(this._XTrapClientKey);
		}
		else {
			writer.writeByteInt(0);
		}
	}

	/**
	 * compare
	 * @param otherReq
	 * @return
	 */
	public boolean compare(XtrapReq otherReq) {
		return Arrays.equals(otherReq.getXtrapClientKey(), this.getXtrapClientKey());
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"XtrapReq: [" +
			"xtrapClientkey: " + this.getHash() +
			"]"
			);

		return dump;
	}
}