/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.user;

import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.Arrays;

/**
 * UserClientVersion
 * @author FantaBlueMystery
 */
public class UserClientVersion extends AbstractStruct implements IAbstractStructDump {

	/**
	 * size
	 */
	static public int SIZE = 64;

	/**
	 * server verison key
	 */
	protected byte[] _sVersionKey = new byte[UserClientVersion.SIZE];

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return UserClientVersion.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._sVersionKey = reader.readBuffer(UserClientVersion.SIZE);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBuffer(this._sVersionKey);
	}

	/**
	 * getHash
	 * @return
	 */
	public byte[] getHash() {
		return Arrays.copyOf(this._sVersionKey, 20);
	}

	/**
	 * getHashStr
	 * @return
	 */
	public String getHashStr() {
		return HexString.byteArrayToHexString(this.getHash());
	}

	/**
	 * compareHash
	 * @param otherClientVersion
	 * @return
	 */
	public int compareHash(UserClientVersion otherClientVersion) {
		return this.getHashStr().compareTo(otherClientVersion.getHashStr());
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"UserClientVersion: [" +
			"sVersionKey: '" + new String(this._sVersionKey) + "'" +
			"]"
			);

		return dump;
	}
}