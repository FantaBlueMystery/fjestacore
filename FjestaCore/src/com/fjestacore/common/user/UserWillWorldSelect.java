/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.user;

import com.fjestacore.common.name.Name8;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * UserWillWorldSelect
 * @author FantaBlueMystery
 */
public class UserWillWorldSelect extends AbstractStruct {

	/**
	 * error code
	 */
	protected int _nError = 0;

	/**
	 * s otp
	 */
	protected Name8 _sOTP = new Name8();

	/**
	 * UserWillWorldSelect
	 */
	public UserWillWorldSelect() {}

	/**
	 * UserWillWorldSelect
	 * @param nError
	 * @param sOtp
	 */
	public UserWillWorldSelect(int nError, String sOtp) {
		this._nError = nError;
		this._sOTP.setContent(sOtp);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + 32;
	}

	/**
	 * getError
	 * @return
	 */
	public int getError() {
		return this._nError;
	}

	/**
	 * setError
	 * @param error
	 */
	public void setError(int error) {
		this._nError = error;
	}

	/**
	 * getOtp
	 * @return
	 */
	public String getOtp() {
		return this._sOTP.getContent();
	}

	/**
	 * setOtp
	 * @param otp
	 */
	public void setOtp(String otp) {
		this._sOTP.setContent(otp);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nError = reader.readShort();
		this._sOTP.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._nError);
		this._sOTP.write(writer);
	}
}