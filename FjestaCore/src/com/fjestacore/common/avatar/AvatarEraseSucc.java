/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.avatar;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AvatarEraseSucc
 * @author FantaBlueMystery
 */
public class AvatarEraseSucc extends AbstractStruct {

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * AvatarEraseSucc
	 */
	public AvatarEraseSucc() {}

	/**
	 * AvatarEraseSucc
	 * @param slot
	 */
	public AvatarEraseSucc(int slot) {
		this._slot = slot;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._slot = reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._slot);
	}
}