/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.avatar;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.ArrayList;

/**
 * CharacterShortList
 * @author FantaBlueMystery
 */
public class AvatarInformationList extends AbstractStruct implements IAbstractStructDump {

	/**
	 * list
	 */
	protected ArrayList<AvatarInformation> _list = new ArrayList<>();

	/**
	 * CharacterShortList
	 */
	public AvatarInformationList() {}

	/**
	 * getList
	 * @return
	 */
	public ArrayList<AvatarInformation> getList() {
		return this._list;
	}

	/**
	 * setList
	 * @param list
	 */
	public void setList(ArrayList<AvatarInformation> list) {
		this._list = list;
	}

	/**
	 * addCharacter
	 * @param character
	 */
	public void addCharacter(AvatarInformation character) {
		this._list.add(character);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1 + (this._list.size()*AvatarInformation.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int count = reader.readByteInt();

		for( int i=0; i<count; i++ ) {
			AvatarInformation character = new AvatarInformation();
			character.read(reader);

			this._list.add(character);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._list.size());

		for( AvatarInformation tcharacter: this._list ) {
			((AvatarInformation)tcharacter).write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"AvatarInformationList: [" +
				"Size: " + Integer.toString(this._list.size()) +
				"]");

		for( AvatarInformation tcharacter: this._list ) {
			dump += tcharacter.dump(deep+1);
		}

		return dump;
	}
}
