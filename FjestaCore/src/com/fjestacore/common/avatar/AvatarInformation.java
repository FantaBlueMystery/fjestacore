/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.avatar;

import com.fjestacore.common.character.CharDeleteInfo;
import com.fjestacore.common.character.CharIdChangeData;
import com.fjestacore.common.character.CharacterEquipment;
import com.fjestacore.common.character.CharacterLook;
import com.fjestacore.common.kq.KQInformation;
import com.fjestacore.common.name.Name3;
import com.fjestacore.common.name.Name5;
import com.fjestacore.common.tutorial.TutorialInfo;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.ICharacterHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * AvatarInformation
 * @author FantaBlueMystery
 */
public class AvatarInformation extends AbstractStruct implements IAbstractStructDump, ICharacterHandle {

	/**
	 * struct size
	 */
	static public final int SIZE = 130;

	/**
	 * character handle
	 */
	protected long _characterHandle = 0;

	/**
	 * charname
	 */
	protected Name5 _charname = new Name5();

	/**
	 * level
	 */
	protected int _level = 1;

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * mapname
	 */
	protected Name3 _mapname = new Name3();

	/**
	 * delete info
	 */
	protected CharDeleteInfo _deleteInfo = new CharDeleteInfo();

	/**
	 * character look
	 */
	protected CharacterLook _look = new CharacterLook();

	/**
	 * equipment
	 */
	protected CharacterEquipment _equipment = new CharacterEquipment();

	/**
	 * kq information
	 */
	protected KQInformation _kqinformation = new KQInformation();

	/**
	 * char id change data
	 */
	protected CharIdChangeData _charIdChangeData = new CharIdChangeData();

	/**
	 * tutorial info
	 */
	protected TutorialInfo _tutorialInfo = new TutorialInfo();

	/**
	 * getCharacterHandle
	 * return character handle (id of server)
	 * @return
	 */
	@Override
	public long getCharacterHandle() {
		return this._characterHandle;
	}

	/**
	 * setCharacterHandle
	 * set character handle (id of server)
	 * @param handle
	 */
	@Override
	public void setCharacterHandle(long handle) {
		this._characterHandle = handle;
	}

	/**
	 * getCharname
	 * @return
	 */
	public String getCharname() {
		return this._charname.getContent();
	}

	/**
	 * setCharname
	 * @param charname
	 */
	public void setCharname(String charname) {
		this._charname.setContent(charname);
	}

	/**
	 * getLevel
	 * @return
	 */
	public int getLevel() {
		return this._level;
	}

	/**
	 * setLevel
	 * @param lvl
	 */
	public void setLevel(int lvl) {
		this._level = lvl;
	}

	/**
	 * getSlot
	 * @return
	 */
	public int getSlot() {
		return this._slot;
	}

	/**
	 * setSlot
	 * @param slot
	 */
	public void setSlot(int slot) {
		this._slot = slot;
	}

	/**
	 * getMapname
	 * @return
	 */
	public String getMapname() {
		return this._mapname.getContent();
	}

	/**
	 * setMapname
	 * @param mapname
	 */
	public void setMapname(String mapname) {
		this._mapname.setContent(mapname);
	}

	/**
	 * getDeleteInfo
	 * @return
	 */
	public CharDeleteInfo getDeleteInfo() {
		return this._deleteInfo;
	}

	/**
	 * getLook
	 * @return
	 */
	public CharacterLook getLook() {
		return this._look;
	}

	/**
	 * setLook
	 * @param look
	 */
	public void setLook(CharacterLook look) {
		this._look = look;
	}

	/**
	 * getEquipment
	 * @return
	 */
	public CharacterEquipment getEquipment() {
		return this._equipment;
	}

	/**
	 * getKQInformation
	 * @return
	 */
	public KQInformation getKQInformation() {
		return this._kqinformation;
	}

	/**
	 * getCharIdChangeData
	 * @return
	 */
	public CharIdChangeData getCharIdChangeData() {
		return this._charIdChangeData;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return AvatarInformation.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._characterHandle	= reader.readUInt();

		this._charname.read(reader);

		this._level		= reader.readShort();
		this._slot		= reader.readByteInt();

		this._mapname.read(reader);
		this._deleteInfo.read(reader);
		this._look.read(reader);
		this._equipment.read(reader);
		this._kqinformation.read(reader);
		this._charIdChangeData.read(reader);
		this._tutorialInfo.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._characterHandle);			// +4

		this._charname.write(writer);						// +20

		writer.writeShort((short) this._level);				// +2
		writer.writeByteInt(this._slot);					// +1

		this._mapname.write(writer);						// +12
		this._deleteInfo.write(writer);						// +5
		this._look.write(writer);							// +4
		this._equipment.write(writer);						// +43
		this._kqinformation.write(writer);					// +28
		this._charIdChangeData.write(writer);				// +6
		this._tutorialInfo.write(writer);					// +5
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"AvatarInformation: [" +
			"CharacterHandle: " + Long.toString(this._characterHandle) +
			" Charname: " + this._charname.getContent() +
			" Level: " + Integer.toString(this._level) +
			" Slot: " + Integer.toString(this._slot) +
			" Mapname: " + this._mapname.getContent() +
			"]");

		dump += this._equipment.dump(deep+1);
		dump += this._look.dump(deep+1);

		return dump;
	}
}