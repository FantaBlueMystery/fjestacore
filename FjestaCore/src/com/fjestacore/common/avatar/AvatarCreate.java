/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.avatar;

import com.fjestacore.common.character.CharacterLook;
import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AvatarCreate
 * @author FantaBlueMystery
 */
public class AvatarCreate extends AbstractStruct {

	/**
	 * slot
	 */
	protected int _slot = 0;

	/**
	 * charname
	 */
	protected Name5 _charname = new Name5();

	/**
	 * look
	 */
	protected CharacterLook _look = new CharacterLook();

	/**
	 * getSlot
	 * @return
	 */
	public int getSlot() {
		return this._slot;
	}

	/**
	 * setSlot
	 * @param slot
	 */
	public void setSlot(int slot) {
		this._slot = slot;
	}

	/**
	 * getCharname
	 * @return
	 */
	public String getCharname() {
		return this._charname.getContent();
	}

	/**
	 * setCharname
	 * @param charname
	 */
	public void setCharname(String charname) {
		this._charname.setContent(charname);
	}

	/**
	 * getLook
	 * @return
	 */
	public CharacterLook getLook() {
		return this._look;
	}

	/**
	 * setLook
	 * @param look
	 */
	public void setLook(CharacterLook look) {
		this._look = look;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 25;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._slot	= reader.readByteInt();

		this._charname.read(reader);
		this._look.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._slot);

		this._charname.write(writer);
		this._look.write(writer);
	}
}