/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.avatar;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.server.ServerError;
import java.io.IOException;

/**
 * AvatarCreateFail
 * @author FantaBlueMystery
 */
public class AvatarCreateFail extends AbstractStruct {

	/**
	 * error
	 */
	protected ServerError _err = ServerError.NONE;

	/**
	 * AvatarCreateFail
	 */
	public AvatarCreateFail() {}

	/**
	 * AvatarCreateFail
	 * @param err
	 */
	public AvatarCreateFail(ServerError err) {
		this._err = err;
	}

	/**
	 * getError
	 * @return
	 */
	public ServerError getError() {
		return this._err;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._err = ServerError.intToError(reader.readShort());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._err.getValue());
	}
}