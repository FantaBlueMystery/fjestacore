/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.avatar;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AvatarCreateSucc
 * @author FantaBlueMystery
 */
public class AvatarCreateSucc extends AbstractStruct {

	/**
	 * character count
	 */
	protected int _characterCount = 0;

	/**
	 * characters
	 */
	protected AvatarInformation _character = new AvatarInformation();

	/**
	 * AvatarCreateSucc
	 */
	public AvatarCreateSucc() {}

	/**
	 * AvatarCreateSucc
	 * @param count
	 * @param character
	 */
	public AvatarCreateSucc(int count, AvatarInformation character) {
		this._characterCount = count;
		this._character = character;
	}

	/**
	 * getCharacterCount
	 * @return
	 */
	public int getCharacterCount() {
		return this._characterCount;
	}

	/**
	 * setCharacterCount
	 * @param count
	 */
	public void setCharacterCount(int count) {
		this._characterCount = count;
	}

	/**
	 * getCharacter
	 * @return
	 */
	public AvatarInformation getCharacter() {
		return this._character;
	}

	/**
	 * setCharacter
	 * @param character
	 */
	public void setCharacter(AvatarInformation character) {
		this._character = character;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 131;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._characterCount = reader.readByteInt();
		this._character.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._characterCount);
		this._character.write(writer);
	}
}