/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.ride;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * RideInfo
 * @author FantaBlueMystery
 */
public class RideInfo extends AbstractStruct {

	/**
	 * horse id
	 */
	protected int _horseId = 0;

	/**
	 * getHorseId
	 * @return
	 */
	public int getHorseId() {
		return this._horseId;
	}

	/**
	 * setHorseId
	 * @param id
	 */
	public void setHorseId(int id) {
		this._horseId = id;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._horseId = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._horseId);
	}
}