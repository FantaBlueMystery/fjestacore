/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.position;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * PositionXY
 * @author FantaBlueMystery
 */
public class PositionXY extends AbstractStruct implements IAbstractStructDump, Cloneable {

	/**
	 * const
	 */
	static public int SIZE = 8;

	/**
	 * x
	 */
	protected int _x = 0;

	/**
	 * y
	 */
	protected int _y = 0;

	/**
	 * PositionXY
	 */
	public PositionXY() {
		this._x = 0;
		this._y = 0;
	}

	/**
	 * PositionXY
	 * @param x
	 * @param y
	 */
	public PositionXY(int x, int y) {
		this._x = x;
		this._y = y;
	}

	/**
	 * getX
	 * @return
	 */
	public int getX() {
		return this._x;
	}

	/**
	 * getY
	 * @return
	 */
	public int getY() {
		return this._y;
	}

	/**
	 * setX
	 * @param x
	 */
	public void setX(int x) {
		this._x = x;
	}

	/**
	 * setY
	 * @param y
	 */
	public void setY(int y) {
		this._y = y;
	}

	/**
	 * setPos
	 * @param pos
	 */
	public void setPos(PositionXY pos) {
		this._x = pos.getX();
		this._y = pos.getY();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return PositionXY.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._x = (int) reader.readUInt();
		this._y = (int) reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._x);
		writer.writeUInt(this._y);
	}

	/**
	 * getDistance
	 * @param position
	 * @return
	 */
	public int getDistance(PositionXY position) {
		return this.getDistance(position.getX(), position.getY());
	}

	/**
	 * getDistance
	 * @param x
	 * @param y
	 * @return
	 */
	public int getDistance(int x, int y) {
		return (int) Math.sqrt((this._y-y) * (this._y-y) + (this._x-x) * (this._x-x));
	}

	/**
	 * inRange
	 * @param position
	 * @return
	 */
	public boolean inRange(PositionXY position) {
		return this.inRange(position.getX(), position.getY());
	}

	/**
	 * inRange
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean inRange(int x, int y) {
		return this.inRange(x, y, 2100);
	}

	/**
	 * inRange
	 * @param x
	 * @param y
	 * @param distance
	 * @return
	 */
	public boolean inRange(int x, int y, int distance) {
		int tdistance = this.getDistance(x, y);

		if( tdistance > distance ) {
			return false;
		}

		return true;
	}

	/**
	 * clone
	 * @return
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		PositionXY c = (PositionXY) super.clone();

		c.setX(this._x);
		c.setY(this._y);

		return c;
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"PositionXY: [" +
			"X: " + Integer.toString(this._x) +
			" Y: " + Integer.toString(this._y) +
			"]"
			);

		return dump;
	}
}