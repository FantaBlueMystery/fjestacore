/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.position;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * PositionXYR
 * @author FantaBlueMystery
 */
public class PositionXYR extends PositionXY implements IAbstractStructDump {

	/**
	 * rotation
	 */
	protected int _rotation = 0;

	/**
	 * PositionXYR
	 */
	public PositionXYR() {
		this._x = 0;
		this._y = 0;
	}

	/**
	 * PositionXYR
	 * @param x
	 * @param y
	 */
	public PositionXYR(int x, int y) {
		this._x = x;
		this._y = y;
	}

	/**
	 * PositionXYR
	 * @param x
	 * @param y
	 * @param rotation
	 */
	public PositionXYR(int x, int y, int rotation) {
		this._x = x;
		this._y = y;
		this._rotation = rotation;
	}

	/**
	 * getPositionXY
	 * @return
	 */
	public PositionXY getPositionXY() {
		return new PositionXY(this._x, this._y);
	}

	/**
	 * setPosition
	 * @param pos
	 */
	public void setPosition(PositionXY pos) {
		this._x = pos.getX();
		this._y = pos.getY();
	}

	/**
	 * getRotation
	 * @return
	 */
	public int getRotation() {
		return this._rotation;
	}

	/**
	 * setRotation
	 * @param rotation
	 */
	public void setRotation(int rotation) {
		this._rotation = rotation;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 9;
	}

	/**
	 * writeR
	 * write x,y and rotation
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._x);
		writer.writeUInt(this._y);
		writer.writeByteInt(this._rotation);
	}

	/**
	 * readR
	 * read x, y and rotation
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._x = (int) reader.readUInt();
		this._y = (int) reader.readUInt();
		this._rotation = reader.readByteInt();
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"PositionXYR: [" +
			"X: " + Integer.toString(this._x) +
			" Y: " + Integer.toString(this._y) +
			" Rotation: " + Integer.toString(this._rotation) +
			"]"
			);

		return dump;
	}
}