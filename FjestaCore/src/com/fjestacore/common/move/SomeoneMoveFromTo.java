/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.move;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * SomeoneMoveFromTo
 * @author FantaBlueMystery
 */
public class SomeoneMoveFromTo extends AbstractStruct implements IAbstractStructDump {

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * move
	 */
	protected MoveFromTo _move = new MoveFromTo();

	/**
	 * speed
	 */
	protected int _speed = 0;

	/**
	 * moveattr
	 */
	protected int _moveattr = 0;

	/**
	 * SomeoneMoveFromTo
	 */
	public SomeoneMoveFromTo() {}

	/**
	 * SomeoneMoveFromTo
	 * @param handle
	 * @param move
	 * @param speed
	 */
	public SomeoneMoveFromTo(int handle, MoveFromTo move, int speed) {
		this._handle = handle;
		this._move = move;
		this._speed = speed;
	}

	/**
	 * SomeoneMoveFromTo
	 * @param handle
	 * @param from
	 * @param to
	 * @param speed
	 */
	public SomeoneMoveFromTo(int handle, PositionXY from, PositionXY to, int speed) {
		this._handle = handle;
		this._move = new MoveFromTo(from, to);
		this._speed = speed;
	}

	/**
	 * getHandle
	 * @return
	 */
	public int getHandle() {
		return this._handle;
	}

	/**
	 * setHandle
	 * @param handle
	 */
	public void setHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * getMove
	 * @return
	 */
	public MoveFromTo getMove() {
		return this._move;
	}

	/**
	 * setMove
	 * @param move
	 */
	public void setMove(MoveFromTo move) {
		this._move = move;
	}

	/**
	 * getSpeed
	 * @return
	 */
	public int getSpeed() {
		return this._speed;
	}

	/**
	 * setSpeed
	 * @param speed
	 */
	public void setSpeed(int speed) {
		this._speed = speed;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 22;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle = reader.readShort();

		this._move.read(reader);

		this._speed = reader.readShort();
		this._moveattr = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);

		this._move.write(writer);

		writer.writeShort(this._speed);
		writer.writeShort(this._moveattr);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"SomeoneMoveFromTo: [" +
			"Handle: " + Integer.toString(this._handle) +
			" Speed: " + Integer.toString(this._speed) +
			" MoveAttr: " + Integer.toString(this._moveattr) +
			"]"
			);

		dump += this._move.dump(deep+1);

		return dump;
	}
}