/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.move;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * MoveFromTo
 * @author FantaBlueMystery
 */
public class MoveFromTo extends AbstractStruct implements IAbstractStructDump {

	/**
	 * form
	 */
	protected PositionXY _from = new PositionXY();

	/**
	 * to
	 */
	protected PositionXY _to = new PositionXY();

	/**
	 * MoveFromTo
	 */
	public MoveFromTo() {}

	/**
	 * MoveFromTo
	 * @param from
	 * @param to
	 */
	public MoveFromTo(PositionXY from, PositionXY to) {
		this._from = from;
		this._to = to;
	}

	/**
	 * getFrom
	 * @return
	 */
	public PositionXY getFrom() {
		return this._from;
	}

	/**
	 * setFrom
	 * @param pos
	 */
	public void setFrom(PositionXY pos) {
		this._from = pos;
	}

	/**
	 * getTo
	 * @return
	 */
	public PositionXY getTo() {
		return this._to;
	}

	/**
	 * setTo
	 * @param pos
	 */
	public void setTo(PositionXY pos) {
		this._to = pos;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 16;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._from.read(reader);
		this._to.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._from.write(writer);
		this._to.write(writer);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"MoveFromTo: "
			);

		dump += "From: \r\n";
		dump += this._from.dump(deep+1);

		dump += "To: \r\n";
		dump += this._to.dump(deep+1);

		return dump;
	}
}