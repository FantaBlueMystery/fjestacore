/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.swing;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * SwingStart
 * @author FantaBlueMystery
 */
public class SwingStart extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * attacker
	 */
	protected int _attacker = 0;

	/**
	 * defender
	 */
	protected int _defender = 0;

	/**
	 * actioncode
	 */
	protected int _actioncode = 0;

	/**
	 * attackspeed
	 */
	protected int _attackspeed = 0;

	/**
	 * damage index
	 */
	protected int _damageindex = 0;

	/**
	 * attack sequence
	 */
	protected int _attacksequence = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._attacker;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._attacker = handle;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 9;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._attacker			= reader.readShort();
		this._defender			= reader.readShort();
		this._actioncode		= reader.readByteInt();
		this._attackspeed		= reader.readShort();
		this._damageindex		= reader.readByteInt();
		this._attacksequence	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._attacker);
		writer.writeShort(this._defender);
		writer.writeByteInt(this._actioncode);
		writer.writeShort(this._attackspeed);
		writer.writeByteInt(this._damageindex);
		writer.writeByteInt(this._attacksequence);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"SwingStart: [" +
			" Attacker: " + Integer.toString(this._attacker) +
			" Defender: " + Integer.toString(this._defender) +
			" Actioncode: " + Integer.toString(this._actioncode) +
			" DamageIndex: " + Integer.toString(this._damageindex) +
			" AttackSequence: " + Integer.toString(this._attacksequence) +
			"]");

		return dump;
	}
}
