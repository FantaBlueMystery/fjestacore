/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.swing;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * SwingDamage
 * @author FantaBlueMystery
 */
public class SwingDamage extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * attacker
	 */
	protected int _attacker = 0;

	/**
	 * defender
	 */
	protected int _defender = 0;

	/**
	 * flag
	 */
	protected SwingDamageFlag _flag = new SwingDamageFlag();

	/**
	 * damage
	 */
	protected int _damage = 0;

	/**
	 * rest hp
	 */
	protected long _resthp = 0;

	/**
	 * hp change order
	 */
	protected int _hpchangeorder = 0;

	/**
	 * damage index
	 */
	protected int _damageindex = 0;

	/**
	 * attack sequence
	 */
	protected int _attacksequence = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._attacker;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._attacker = handle;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 16 + this._flag.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._attacker = reader.readShort();
		this._defender = reader.readShort();
		this._flag.read(reader);
		this._damage			= reader.readShort();
		this._resthp			= reader.readUInt();
		this._hpchangeorder		= reader.readShort();
		this._damageindex		= reader.readShort();
		this._attacksequence	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._attacker);
		writer.writeShort(this._defender);
		this._flag.write(writer);
		writer.writeShort(this._damage);
		writer.writeUInt(this._resthp);
		writer.writeShort(this._hpchangeorder);
		writer.writeShort(this._damageindex);
		writer.writeShort(this._attacksequence);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"SwingDamage: [" +
			" Attacker: " + Integer.toString(this._attacker) +
			" Defender: " + Integer.toString(this._defender) +
			" Damage: " + Integer.toString(this._damage) +
			" RestHp: " + Long.toString(this._resthp) +
			" HpChangeOrder: " + Integer.toString(this._hpchangeorder) +
			" DamageIndex: " + Integer.toString(this._damageindex) +
			" AttackSequence: " + Integer.toString(this._attacksequence) +
			"]"
			);

		return dump;
	}
}