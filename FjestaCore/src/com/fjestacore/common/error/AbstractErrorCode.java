/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.error;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AbstractErrorCode
 * @author FantaBlueMystery
 */
abstract public class AbstractErrorCode extends AbstractStruct {

	/**
	 * error code
	 */
	protected int _err = 0;

	/**
	 * getErrorCode
	 * @return
	 */
	public int getErrorCode() {
		return this._err;
	}

	/**
	 * setErrorCode
	 * @param error
	 */
	public void setErrorCode(int error) {
		this._err = error;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._err = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._err);
	}
}