/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.announce;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AnnounceW2C
 * @author FantaBlueMystery
 */
public class AnnounceMessage extends AbstractStruct {

	/**
	 * announce type
	 */
	protected int _announceType = 0;

	/**
	 * message
	 */
	protected String _message = "";

	/**
	 * AnnounceMessage
	 */
	public AnnounceMessage() {}

	/**
	 * AnnounceMessage
	 * @param announceType
	 * @param message
	 */
	public AnnounceMessage(int announceType, String message) {
		this._announceType = announceType;
		this._message = message;
	}

	/**
	 * getAnnounceType
	 * @return
	 */
	public int getAnnounceType() {
		return this._announceType;
	}

	/**
	 * getAnnounceTypeAT
	 * @return
	 */
	public AnnounceType getAnnounceTypeAT() {
		return AnnounceType.intToType(this._announceType);
	}

	/**
	 * getMessage
	 * @return
	 */
	public String getMessage() {
		return this._message;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + this._message.length();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._announceType = reader.readByteInt();

		int size = reader.readByteInt();

		this._message = reader.readString(size);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._announceType);
		writer.writeByteInt(this._message.length());
		writer.writeString(this._message, this._message.length());
	}
}