/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery, SeerOfVoid420
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.announce;

/**
 * AnnounceType
 * @author FantaBlueMystery
 */
public enum AnnounceType {
	AT_ENHANCE(1),
	AT_ACQUIRE(2),
	AT_PRODUCE(3),
	AT_LV20(4),
	AT_PROMOTE(5),
	AT_TITLEACQUIRE(6),
	AT_PETEVOLVE(7),
	AT_GUILDWARBEGIN(8),
	AT_GUILDWAREND(9),
	AT_GUILDRANKUP(10),
	AT_ROAR(11),
	AT_PROPOSALACCEPT(12),
	AT_MARRIAGE(13);

	/**
	 * value
	 */
	private int _value;

	/**
	 * AnnounceType
	 * @param value
	 */
	private AnnounceType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToType
	 * @param value
	 * @return
	 */
	static public AnnounceType intToType(int value) {
		AnnounceType[] types = AnnounceType.values();

		for( AnnounceType type: types ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return AnnounceType.AT_ENHANCE;
	}
}