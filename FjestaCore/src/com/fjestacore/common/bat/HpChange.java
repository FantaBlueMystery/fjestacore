/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.bat;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * HpChange
 * @author FantaBlueMystery
 */
public class HpChange extends AbstractStruct {

	/**
	 * hp
	 */
	protected long _hp = 0;

	/**
	 * hp changeorder
	 */
	protected int _changeorder = 0;

	/**
	 * HpChange
	 */
	public HpChange() {}

	/**
	 * HpChange
	 * @param hp
	 * @param changeOrder
	 */
	public HpChange(long hp, int changeOrder) {
		this._hp = hp;
		this._changeorder = changeOrder;
	}

	/**
	 * getHp
	 * @return
	 */
	public long getHp() {
		return this._hp;
	}

	/**
	 * setHp
	 * @param hp
	 */
	public void setHp(long hp) {
		this._hp = hp;
	}

	/**
	 * getChangeOrder
	 * @return
	 */
	public int getChangeOrder() {
		return this._changeorder;
	}

	/**
	 * setChangeOrder
	 * @param order
	 */
	public void setChangeOrder(int order) {
		this._changeorder = order;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._hp				= reader.readUInt();
		this._changeorder		= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._hp);
		writer.writeShort(this._changeorder);
	}
}