/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.bat;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * TargetInfo
 * @author FantaBlueMystery
 */
public class TargetInfo extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * flag
	 */
	protected int _flag = 0;

	/**
	 * targethandle
	 */
	protected int _targethandle = 0;

	/**
	 * target hp
	 */
	protected long _targethp = 0;

	/**
	 * target max hp
	 */
	protected long _targetmaxhp = 0;

	/**
	 * target sp
	 */
	protected long _targetsp = 0;

	/**
	 * target max sp
	 */
	protected long _targetmaxsp = 0;

	/**
	 * target lp
	 */
	protected long _targetlp = 0;

	/**
	 * target max lp
	 */
	protected long _targetmaxlp = 0;

	/**
	 * target level
	 */
	protected int _targetlevel = 0;

	/**
	 * hp change order
	 */
	protected int _hpchangeorder = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._targethandle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._targethandle = handle;
	}

	/**
	 * setFlag
	 * @param flag
	 */
	public void setFlag(int flag) {
		this._flag = flag;
	}

	/**
	 * setTargetHp
	 * @param hp
	 */
	public void setTargetHp(long hp) {
		this._targethp = hp;
	}

	/**
	 * setTargetMaxHp
	 * @param maxhp
	 */
	public void setTargetMaxHp(long maxhp) {
		this._targetmaxhp = maxhp;
	}

	/**
	 * setTargetSp
	 * @param sp
	 */
	public void setTargetSp(long sp) {
		this._targetsp = sp;
	}

	/**
	 * setTargetMaxSp
	 * @param maxsp
	 */
	public void setTargetMaxSp(long maxsp) {
		this._targetmaxsp = maxsp;
	}

	/**
	 * setTargetLp
	 * @param lp
	 */
	public void setTargetLp(long lp) {
		this._targetlp = lp;
	}

	/**
	 * setTargetMaxLp
	 * @param maxlp
	 */
	public void setTargetMaxLp(long maxlp) {
		this._targetmaxlp = maxlp;
	}

	/**
	 * setTargetLevel
	 * @param lvl
	 */
	public void setTargetLevel(int lvl) {
		this._targetlevel = lvl;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 30;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._flag			= reader.readByteInt();
		this._targethandle	= reader.readShort();
		this._targethp		= reader.readUInt();
		this._targetmaxhp	= reader.readUInt();
		this._targetsp		= reader.readUInt();
		this._targetmaxsp	= reader.readUInt();
		this._targetlp		= reader.readUInt();
		this._targetmaxlp	= reader.readUInt();
		this._targetlevel	= reader.readByteInt();
		this._hpchangeorder	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._flag);
		writer.writeShort(this._targethandle);
		writer.writeUInt(this._targethp);
		writer.writeUInt(this._targetmaxhp);
		writer.writeUInt(this._targetsp);
		writer.writeUInt(this._targetmaxsp);
		writer.writeUInt(this._targetlp);
		writer.writeUInt(this._targetmaxlp);
		writer.writeByteInt(this._targetlevel);
		writer.writeShort(this._hpchangeorder);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"TargetInfo: [" +
			" Flag: " + Integer.toString(this._flag) +
			" TargetHandle: " + Integer.toString(this._targethandle) +
			" TargetHp: " + Long.toString(this._targethp) +
			" TargetMaxHp: " + Long.toString(this._targetmaxhp) +
			" TargetSp: " + Long.toString(this._targetsp) +
			" TargetMaxSp: " + Long.toString(this._targetmaxsp) +
			" TargetLp: " + Long.toString(this._targetlp) +
			" TargetMaxLp: " + Long.toString(this._targetmaxlp) +
			" TargetLvl: " + Integer.toString(this._targetlevel) +
			" HpChangeOrder: " + Integer.toString(this._hpchangeorder) +
			"]"
			);

		return dump;
	}
}