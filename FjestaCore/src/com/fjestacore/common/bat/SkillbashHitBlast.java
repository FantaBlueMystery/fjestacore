/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.bat;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SkillbashHitBlast
 * @author FantaBlueMystery
 */
public class SkillbashHitBlast extends AbstractStruct {

	/**
	 * index
	 */
	protected int _index = 0;

	/**
	 * caster
	 */
	protected int _caster = 0;

	/**
	 * skill id
	 */
	protected int _nSkillID = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._index		= reader.readShort();
		this._caster	= reader.readShort();
		this._nSkillID	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._index);
		writer.writeShort(this._caster);
		writer.writeShort(this._nSkillID);
	}
}