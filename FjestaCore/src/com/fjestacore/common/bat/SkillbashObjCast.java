/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.bat;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * SkillbashObjCast
 * @author FantaBlueMystery
 */
public class SkillbashObjCast extends AbstractStruct {

	/**
	 * skill id
	 */
	protected int _skillId = 0;

	/**
	 * target id
	 */
	protected int _targetId = 0;

	/**
	 * getSkillId
	 * @return
	 */
	public int getSkillId() {
		return this._skillId;
	}

	/**
	 * setSkillId
	 * @param id
	 */
	public void setSkillId(int id) {
		this._skillId = id;
	}

	/**
	 * getTargetId
	 * @return
	 */
	public int getTargetId() {
		return this._targetId;
	}

	/**
	 * setTargetId
	 * @param id
	 */
	public void setTargetId(int id) {
		this._targetId = id;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._skillId = reader.readShort();
		this._targetId = reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._skillId);
		writer.writeShort(this._targetId);
	}
}