/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.bat;

import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * DotDamage
 * @author FantaBlueMystery
 */
public class DotDamage extends AbstractStruct implements IObjectHandle {

	/**
	 * object
	 */
	protected int _object = 0;

	/**
	 * rest hp
	 */
	protected long _resthp = 0;

	/**
	 * damage
	 */
	protected int _damage = 0;

	/**
	 * abstate
	 */
	protected int _abstate = 0;

	/**
	 * hp change order
	 */
	protected int _hpchangeorder = 0;

	/**
	 * is miss damage
	 */
	protected boolean _isMissDamage = false;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._object;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._object = handle;
	}

	/**
	 * getRestHp
	 * @return
	 */
	public long getRestHp() {
		return this._resthp;
	}

	/**
	 * getDamage
	 * @return
	 */
	public int getDamage() {
		return this._damage;
	}

	/**
	 * getAbstate
	 * @return
	 */
	public int getAbstate() {
		return this._abstate;
	}

	/**
	 * getHpChangeOrder
	 * @return
	 */
	public int getHpChangeOrder() {
		return this._hpchangeorder;
	}

	/**
	 * isMissDamage
	 * @return
	 */
	public boolean isMissDamage() {
		return this._isMissDamage;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 13;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._object = reader.readShort();
		this._resthp = reader.readUInt();
		this._damage = reader.readShort();
		this._abstate = reader.readShort();
		this._hpchangeorder = reader.readShort();
		this._isMissDamage = reader.readBoolean();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._object);
		writer.writeUInt(this._resthp);
		writer.writeShort(this._damage);
		writer.writeShort(this._abstate);
		writer.writeShort(this._hpchangeorder);
		writer.writeBoolean(this._isMissDamage);
	}
}