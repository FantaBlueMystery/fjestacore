/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.bat;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ReallyKill
 * @author FantaBlueMystery
 */
public class ReallyKill extends AbstractStruct {

	/**
	 * dead
	 */
	protected int _dead = 0;

	/**
	 * attacker
	 */
	protected int _attacker = 0;

	/**
	 * getDeadId
	 * @return
	 */
	public int getDeadId() {
		return this._dead;
	}

	/**
	 * getAttackerId
	 * @return
	 */
	public int getAttackerId() {
		return this._attacker;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._dead		= reader.readShort();
		this._attacker	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._dead);
		writer.writeShort(this._attacker);
	}
}