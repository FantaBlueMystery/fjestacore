/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.title;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * TitleList
 * @author FantaBlueMystery
 */
public class TitleList extends AbstractStruct {

	/**
	 * titles
	 */
	protected ArrayList<Title> _list = new ArrayList<>();

	/**
	 * add
	 * @param title
	 */
	public void add(Title title) {
		this._list.add(title);
	}

	/**
	 * getList
	 * @return
	 */
	public ArrayList getList() {
		return this._list;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (this._list.size()*2);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int count = reader.readShort();

		for( int i=0; i<count; i++ ) {
			Title ti = new Title();
			ti.read(reader);

			this._list.add(ti);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._list.size());

		for( Title ti: this._list ) {
			ti.write(writer);
		}
	}
}