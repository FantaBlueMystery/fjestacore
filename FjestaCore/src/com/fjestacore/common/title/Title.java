/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.title;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * Title
 * @author FantaBlueMystery
 */
public class Title extends AbstractStruct {

	/**
	 * type
	 */
	protected int _type = 0;

	/**
	 * bf1
	 */
	protected int _bf1 = 0;

	/**
	 * Title
	 */
	public Title() {}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._type);
		writer.writeByteInt(this._bf1);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._type = reader.readByteInt();
		this._bf1 = reader.readByteInt();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2;
	}
}