/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.abstate;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AbstateInformation
 * @author FantaBlueMystery
 */
public class AbstateInformation extends AbstractStruct {

	/**
	 * SIZE
	 */
	static public int SIZE = 12;

	/**
	 * abstateID
	 */
	protected AbstateIndex _abstateID = AbstateIndex.STA_SEVERBONE;

	/**
	 * reset keep time
	 */
	protected long _restKeeptime = 0;

	/**
	 * strength
	 */
	protected long _strength = 0;

	/**
	 * getAbstateId
	 * @return
	 */
	public AbstateIndex getAbstateId() {
		return this._abstateID;
	}

	/**
	 * getResetKeepTime
	 * @return
	 */
	public long getResetKeepTime() {
		return this._restKeeptime;
	}

	/**
	 * getStrength
	 * @return
	 */
	public long getStrength() {
		return this._strength;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return AbstateInformation.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._abstateID		= AbstateIndex.getAbstateIndex((int)reader.readUInt());
		this._restKeeptime	= reader.readUInt();
		this._strength		= reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._abstateID.getValue());
		writer.writeUInt(this._restKeeptime);
		writer.writeUInt(this._strength);
	}
}