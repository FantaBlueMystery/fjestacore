/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.abstate;

import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * AbstateChangeList
 * @author FantaBlueMystery
 */
public class AbstateChangeList extends AbstractStruct implements IObjectHandle {

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * info list
	 */
	protected ArrayList<AbstateInformation> _infoList = new ArrayList<>();

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._handle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + 1 + (this._infoList.size() + AbstateInformation.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle = reader.readShort();

		int count = reader.readByteInt();

		for( int i=0; i<count; i++ ) {
			AbstateInformation tai = new AbstateInformation();
			tai.read(reader);

			this._infoList.add(tai);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);
		writer.writeByteInt(this._infoList.size());

		for( AbstateInformation tai: this._infoList ) {
			tai.write(writer);
		}
	}
}