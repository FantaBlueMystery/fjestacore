/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.abstate;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AbstateInform
 * @author FantaBlueMystery
 */
public class AbstateInform extends AbstractStruct {

	/**
	 * abstrate
	 */
	protected AbstateIndex _abstate = AbstateIndex.STA_SEVERBONE;

	/**
	 * keeptime millisec
	 */
	protected long _keeptime_millisec = 0;

	/**
	 * getAbstate
	 * @return
	 */
	public AbstateIndex getAbstate() {
		return this._abstate;
	}

	/**
	 * setAbstate
	 * @param abstate
	 */
	public void setAbstate(AbstateIndex abstate) {
		this._abstate = abstate;
	}

	/**
	 * getKeeptimeMillisec
	 * @return
	 */
	public long getKeeptimeMillisec() {
		return this._keeptime_millisec;
	}

	/**
	 * setKeepteimeMillisec
	 * @param keeptime
	 */
	public void setKeepteimeMillisec(long keeptime) {
		this._keeptime_millisec = keeptime;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 8;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._abstate = AbstateIndex.getAbstateIndex((int) reader.readUInt());
		this._keeptime_millisec = reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._abstate.getValue());
		writer.writeUInt(this._keeptime_millisec);
	}
}