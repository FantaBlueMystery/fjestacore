/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.abstate;

import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AbnormalStateBit
 * @author FantaBlueMystery
 */
public class AbnormalStateBit extends AbstractStruct {

	/**
	 * size of object
	 */
	static public Integer SIZE = 110;

	/**
	 * state
	 */
	protected byte[] _state = new byte[AbnormalStateBit.SIZE];

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return AbnormalStateBit.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._state = reader.readBuffer(AbnormalStateBit.SIZE);
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBuffer(this._state);
	}

	/**
	 * toHexString
	 * @return
	 */
	public String toHexString() {
		return HexString.byteArrayToHexString(this._state);
	}
}