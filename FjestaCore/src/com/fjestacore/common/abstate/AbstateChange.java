/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.abstate;

import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * AbstateChange
 * @author FantaBlueMystery
 */
public class AbstateChange extends AbstractStruct implements IObjectHandle {

	/**
	 * Object handle
	 */
	protected int _handle = 0;

	/**
	 * abstate information
	 */
	protected AbstateInformation _info = new AbstateInformation();

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._handle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * getInfo
	 * @return
	 */
	public AbstateInformation getInfo() {
		return this._info;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + this._info.getSize();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle = reader.readShort();
		this._info.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);
		this._info.write(writer);
	}
}