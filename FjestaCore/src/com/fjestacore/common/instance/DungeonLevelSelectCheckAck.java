/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.instance;

import com.fjestacore.common.IdLevelType;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * DungeonLevelSelectCheckAck
 * @author FantaBlueMystery
 */
public class DungeonLevelSelectCheckAck extends AbstractStruct {

	/**
	 * char reg num
	 */
	protected long _nCharRegNum = 0;

	/**
	 * npc handle
	 */
	protected int _nNPCHandle = 0;

	/**
	 * error
	 */
	protected int _nError = 0;

	/**
	 * level type
	 */
	protected IdLevelType _eLevelType = IdLevelType.ILT_EASY;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + 2 + 2;
	}

	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nCharRegNum	= reader.readUInt();
		this._nNPCHandle	= reader.readShort();
		this._nError		= reader.readShort();
		
	}

	@Override
	public void write(WriterStream writer) throws IOException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}