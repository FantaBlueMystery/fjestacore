/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.chatblock;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ChatBlockSpamFilter
 * @author FantaBlueMystery
 */
public class ChatBlockSpamFilter extends AbstractStruct {

	/**
	 * is spam spread
	 */
	protected boolean _bIsSpamSpread = false;

	/**
	 * is spam block
	 */
	protected boolean _bIsSpamBlock = false;

	/**
	 * spam filter rate
	 */
	protected int _nSpamFilterRate = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 3;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._bIsSpamSpread		= reader.readBoolean();
		this._bIsSpamBlock		= reader.readBoolean();
		this._nSpamFilterRate	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeBoolean(this._bIsSpamSpread);
		writer.writeBoolean(this._bIsSpamBlock);
		writer.writeByteInt(this._nSpamFilterRate);
	}
}