/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.chat;

import com.fjestacore.common.item.IItem;
import com.fjestacore.common.item.Item;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ChatReq
 * @author FantaBlueMystery
 */
public class ChatReq extends AbstractStruct implements IAbstractStructDump {

	/**
	 * content
	 */
	protected String _content = "";

	/**
	 * items
	 */
	protected ArrayList<IItem> _items = new ArrayList();

	/**
	 * Chat
	 */
	public ChatReq() {}

	/**
	 * Chat
	 * @param content
	 */
	public ChatReq(String content) {
		this._content = content;
	}

	/**
	 * getContent
	 * @return
	 */
	public String getContent() {
		return this._content;
	}

	/**
	 * setContent
	 * @param content
	 */
	public void setContent(String content) {
		this._content = content;
	}

	/**
	 * getItems
	 * @return
	 */
	public ArrayList<IItem> getItems() {
		return this._items;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 2 + this._content.length();

		if( this._items.size() > 0 ) {
			size += IItem.ITEM_MAX_SIZE * this._items.size();
		}

		return size;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int itemLinkDataCount = reader.readByteInt();

		int len = reader.readByteInt();

		this._content = reader.readString(len);

		for( int i=0; i<itemLinkDataCount; i++ ) {
			Item item = new Item();
			item.read(reader);

			this._items.add(item);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._items.size());

		writer.writeByteInt(this._content.length());
		writer.writeString(this._content, this._content.length());

		for( IItem titem: _items ) {
			((Item) titem).write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"ChatReq: [" +
			" itemLinkDataCount: " + Integer.toString(this._items.size()) +
			" Content: " + this._content +
			"]"
			);

		for( IItem titem: _items ) {
			dump += "\r\n";

			if( titem instanceof IAbstractStructDump ) {
				IAbstractStructDump idump = (IAbstractStructDump) titem;

				dump += idump.dump(deep+1);
			}
			else {
				dump += MsgDump.getMsgDump(deep+1,
					"Item: [ItemId: " + Integer.toString(titem.getId()) + "]");
			}
		}

		return dump;
	}
}