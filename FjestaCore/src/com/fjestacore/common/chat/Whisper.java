/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.chat;

import com.fjestacore.common.item.Item;
import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Whisper
 * @author FantaBlueMystery
 */
public class Whisper extends AbstractStruct {

	/**
	 * receiver
	 */
	protected Name5 _receiver = new Name5();

	/**
	 * content
	 */
	protected String _content = "";

	/**
	 * items
	 */
	protected ArrayList<Item> _items = new ArrayList();

	/**
	 * Whisper
	 */
	public Whisper() {}

	/**
	 * Whisper
	 * @param receiver
	 * @param content
	 */
	public Whisper(String receiver, String content) {
		this._receiver.setContent(receiver);
		this._content = content;
	}

	/**
	 * getReceiver
	 * @return
	 */
	public Name5 getReceiver() {
		return this._receiver;
	}

	/**
	 * getCharname
	 * @return
	 */
	public String getCharname() {
		return this._receiver.getContent();
	}

	/**
	 * setCharname
	 * @param charname
	 */
	public void setCharname(String charname) {
		this._receiver.setContent(charname);
	}

	/**
	 * getContent
	 * @return
	 */
	public String getContent() {
		return this._content;
	}

	/**
	 * setContent
	 * @param content
	 */
	public void setContent(String content) {
		this._content = content;
	}

	/**
	 * getItems
	 * @return
	 */
	public ArrayList<Item> getItems() {
		return this._items;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 22 + this._content.length();

		if( this._items.size() > 0 ) {
			size += Item.ITEM_MAX_SIZE * this._items.size();
		}

		return size;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int itemLinkDataCount = reader.readByteInt();

		this._receiver.read(reader);

		int len = reader.readByteInt();

		this._content = reader.readString(len);

		for( int i=0; i<itemLinkDataCount; i++ ) {
			Item item = new Item();
			item.read(reader);

			this._items.add(item);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._items.size());

		this._receiver.write(writer);

		writer.writeByteInt(this._content.length());
		writer.writeString(this._content, this._content.length());

		for( Item titem: this._items ) {
			titem.write(writer);
		}
	}
}