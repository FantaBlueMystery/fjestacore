/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.chat;

import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ChatRestrictInfo
 * @author FantaBlueMystery
 */
public class ChatRestrictInfo extends AbstractStruct {

	/**
	 * const size
	 */
	static public int SIZE = 22;

	/**
	 * s char id
	 */
	protected Name5 _sCharId = new Name5();

	/**
	 * block is GM
	 */
	protected boolean _bIsGM = false;

	/**
	 * block is Deleted
	 */
	protected boolean _bIsDeleted = false;

	/**
	 * getsCharId
	 * @return
	 */
	public Name5 getsCharId() {
		return this._sCharId;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._sCharId.getSize() + 2;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._sCharId.read(reader);
		this._bIsGM			= reader.readBoolean();
		this._bIsDeleted	= reader.readBoolean();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._sCharId.write(writer);

		writer.writeBoolean(this._bIsGM);
		writer.writeBoolean(this._bIsDeleted);
	}
}