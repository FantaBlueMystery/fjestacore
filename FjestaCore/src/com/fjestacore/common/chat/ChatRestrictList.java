/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.chat;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ChatRestrictList
 * @author FantaBlueMystery
 */
public class ChatRestrictList extends AbstractStruct {

	/**
	 * infos
	 */
	protected ArrayList<ChatRestrictInfo> _infos = new ArrayList();

	/**
	 * getInfos
	 * @return
	 */
	public ArrayList<ChatRestrictInfo> getInfos() {
		return this._infos;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 2 + (this._infos.size() * ChatRestrictInfo.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._infos.clear();

		int nChatRestrictListCnt = reader.readShort();

		for( int i=0; i<nChatRestrictListCnt; i++ ) {
			ChatRestrictInfo tinfo = new ChatRestrictInfo();
			tinfo.read(reader);

			this._infos.add(tinfo);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._infos.size());

		for( ChatRestrictInfo tinfo: this._infos ) {
			tinfo.write(writer);
		}
	}
}