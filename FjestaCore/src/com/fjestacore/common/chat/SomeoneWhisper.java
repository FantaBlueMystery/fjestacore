/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.chat;

import com.fjestacore.common.item.Item;
import com.fjestacore.common.name.Name5;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * SomeoneWhisper
 * @author FantaBlueMystery
 */
public class SomeoneWhisper extends AbstractStruct {

	/**
	 * talker
	 */
	protected Name5 _talker = new Name5();

	/**
	 * flag
	 */
	protected int _flag = 0;

	/**
	 * content
	 */
	protected String _content = "";

	/**
	 * items
	 */
	protected ArrayList<Item> _items = new ArrayList();

	/**
	 * getTalker
	 * @return
	 */
	public Name5 getTalker() {
		return this._talker;
	}

	/**
	 * getCharname
	 * @return
	 */
	public String getCharname() {
		return this._talker.getContent();
	}

	/**
	 * setCharname
	 * @param charname
	 */
	public void setCharname(String charname) {
		this._talker.setContent(charname);
	}

	/**
	 * getContent
	 * @return
	 */
	public String getContent() {
		return this._content;
	}

	/**
	 * getItems
	 * @return
	 */
	public ArrayList<Item> getItems() {
		return this._items;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 23 + this._content.length();

		if( this._items.size() > 0 ) {
			size += Item.ITEM_MAX_SIZE * this._items.size();
		}

		return size;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int itemLinkDataCount = reader.readByteInt();

		this._talker.read(reader);

		this._flag = reader.readByteInt();

		int len = reader.readByteInt();

		this._content = reader.readString(len);

		for( int i=0; i<itemLinkDataCount; i++ ) {
			Item item = new Item();
			item.read(reader);

			this._items.add(item);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._items.size());

		this._talker.write(writer);

		writer.writeByteInt(this._flag);
		writer.writeByteInt(this._content.length());
		writer.writeString(this._content, this._content.length());

		for( Item titem: _items ) {
			titem.write(writer);
		}
	}
}