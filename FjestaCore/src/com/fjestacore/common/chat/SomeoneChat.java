/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.chat;

import com.fjestacore.common.item.IItem;
import com.fjestacore.common.item.Item;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * SomeoneChat
 * @author FantaBlueMystery
 */
public class SomeoneChat extends AbstractStruct implements IObjectHandle {

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * flag
	 */
	protected int _flag = 0;

	/**
	 * font color id
	 */
	protected int _nChatFontColorId = 0;

	/**
	 * balloon color id
	 */
	protected int _nChatBalloonColorId = 0;

	/**
	 * content
	 */
	protected String _content = "";

	/**
	 * items
	 */
	protected ArrayList<IItem> _items = new ArrayList();

	/**
	 * SomeoneChat
	 */
	public SomeoneChat() {}

	/**
	 * SomeoneChat
	 * @param handle
	 * @param flag
	 * @param content
	 */
	public SomeoneChat(int handle, int flag, String content) {
		this._handle	= handle;
		this._flag		= flag;
		this._content	= content;
	}

	/**
	 * SomeoneChat
	 * @param handle
	 * @param flag
	 * @param content
	 * @param fontColorId
	 */
	public SomeoneChat(int handle, int flag, String content, int fontColorId) {
		this._handle			= handle;
		this._flag				= flag;
		this._content			= content;
		this._nChatFontColorId	= fontColorId;
	}

	/**
	 * SomeoneChat
	 * @param handle
	 * @param flag
	 * @param achat
	 */
	public SomeoneChat(int handle, int flag, ChatReq achat) {
		this._handle	= handle;
		this._flag		= flag;
		this._content	= achat.getContent();
		this._items		= achat.getItems();
	}

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._handle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * getFlag
	 * @return
	 */
	public int getFlag() {
		return this._flag;
	}

	/**
	 * getFontColorId
	 * @return
	 */
	public int getFontColorId() {
		return this._nChatFontColorId;
	}

	/**
	 * getBallonColorId
	 * @return
	 */
	public int getBallonColorId() {
		return this._nChatBalloonColorId;
	}

	/**
	 * getContent
	 * @return
	 */
	public String getContent() {
		return this._content;
	}

	/**
	 * getItems
	 * @return
	 */
	public ArrayList<IItem> getItems() {
		return this._items;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		int size = 7 + this._content.length();

		if( this._items.size() > 0 ) {
			size += Item.ITEM_MAX_SIZE * this._items.size();
		}

		return size;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int itemLinkDataCount = reader.readByteInt();

		this._handle			= reader.readShort();

		int len = reader.readByteInt();

		this._flag				= reader.readByteInt();
		this._nChatFontColorId	= reader.readByteInt();
		this._nChatFontColorId	= reader.readByteInt();

		this._content = reader.readString(len);

		for( int i=0; i<itemLinkDataCount; i++ ) {
			Item item = new Item();
			item.read(reader);

			this._items.add(item);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._items.size());

		writer.writeShort(this._handle);
		writer.writeByteInt(this._content.length());
		writer.writeByteInt(this._flag);
		writer.writeByteInt(this._nChatFontColorId);
		writer.writeByteInt(this._nChatBalloonColorId);
		writer.writeBuffer(this._content.getBytes("ISO-8859-1"));

		for( IItem titem: _items ) {
			((Item) titem).write(writer);
		}
	}
}