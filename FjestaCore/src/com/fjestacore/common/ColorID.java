/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common;

/**
 * ColorID
 * @author FantaBlueMystery
 */
public enum ColorID {
	RED(0x0),
	BLUE(0x1),
	LIGHTSTEELBLUE(0x2),
	PALETURQUOISE(0x3),
	AQUAMARINE(0x4),
	LIME(0x5),
	WHITE(0x6),
	YELLOW(0x7),
	GOLD(0x8),
	DARKORANGE(0x9),
	ORANGE(0xA),
	SILVER(0xB),
	PINK(0xC),
	PURPLE(0xD),
	LIGHTORANGE(0xE),
	SANDYBROWN(0xF),
	PALEGREEN(0x10),
	MAGENTA(0x11),
	DEEPPINK(0x12),
	HOTPINK(0x13),
	LIGHTPINK(0x14),
	LIGHTVIOLET(0x15),
	VIOLET(0x16),
	DARKPINK(0x17),
	PALEVIOLETRED(0x18),
	LIGHTGREEN(0x19),
	SKYEBLUE(0x1A),

	CHAT_ME(0x1B),
	CHAT_OTHER(0x1C),
	CHAT_FIELDSHOUT(0x1D),
	CHAT_WHISPER(0x1E),
	CHAT_PARTY(0x1F),
	CHAT_GUILD(0x20),
	CHAT_FRIEND(0x21),
	CHAT_ALLSHOUT(0x22),
	CHAT_ANNOUNCEMENT(0x23),
	CHAT_SYSTEM(0x24),
	CHAT_GUILDACADEMY(0x25),
	CHAT_GMNORMAL(0x26),
	CHAT_GMSHOUT(0x27),
	CHAT_GMWHISPER(0x28),

	SYSTEM_ATTACK(0x29),
	SYSTEM_DAMAGE(0x2A),
	SYSTEM_SKILLATTACK(0x2B),
	SYSTEM_SKILLDAMAGE(0x2C),
	SYSTEM_BUFF(0x2D),
	SYSTEM_DEBUFF(0x2E),
	SYSTEM_SYSTEM(0x2F),
	SYSTEM_WARNING(0x30),
	SYSTEM_ETC(0x31),

	CENTER_ANNOUNCEMENT(0x32),
	CENTER_PUBLICATION(0x33),
	CENTER_QUEST(0x34),

	CHARGE_ROAR(0x35),

	CHAT_RAID(0x36),

	MAX_COLORID(0x37);

	/**
	 * value
	 */
	private int _value;

	/**
	 * ColorID
	 * @param value
	 */
	private ColorID(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}