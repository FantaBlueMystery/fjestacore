/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.mob;

/**
 * MobBriefFlag
 * @author FantaBlueMystery
 */
public enum MobBriefFlag {
	NORMAL(0x0),
	GATE(0x1);

	/**
	 * value
	 */
	private int _value;

	/**
	 * MobBriefFlag
	 * @param value
	 */
	private MobBriefFlag(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getMobBriefFlag
	 * @param value
	 * @return
	 */
	static public MobBriefFlag getMobBriefFlag(int value) {
		MobBriefFlag[] flags = MobBriefFlag.values();

		for( MobBriefFlag type: flags ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return MobBriefFlag.NORMAL;
	}
}