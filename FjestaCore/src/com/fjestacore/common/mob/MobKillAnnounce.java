/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.mob;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * MobKillAnnounce
 * @author FantaBlueMystery
 */
public class MobKillAnnounce extends AbstractStruct {

	/**
	 * text index
	 */
	protected MobKillAnnounceType _textIndex;

	/**
	 * MobKillAnnounce
	 */
	public MobKillAnnounce() {}

	/**
	 * MobKillAnnounce
	 * @param type
	 */
	public MobKillAnnounce(MobKillAnnounceType type) {
		this._textIndex = type;
	}

	/**
	 * getTextIndex
	 * @return
	 */
	public MobKillAnnounceType getTextIndex() {
		return this._textIndex;
	}

	/**
	 * setTextIndex
	 * @param type
	 */
	public void setTextIndex(MobKillAnnounceType type) {
		this._textIndex = type;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._textIndex = MobKillAnnounceType.getMobKillAnnounceType(reader.readByteInt());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._textIndex.getValue());
	}
}