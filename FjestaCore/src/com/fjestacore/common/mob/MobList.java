/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.mob;

import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * MobList
 * @author FantaBlueMystery
 */
public class MobList extends AbstractStruct implements IAbstractStructDump {

	/**
	 * mobs
	 */
	protected Map<Integer, Mob> _list = new HashMap<>();

	/**
	 * getMobs
	 * @return
	 */
	public Map<Integer, Mob> getMobs() {
		return this._list;
	}

	/**
	 * findMob
	 * @param id
	 * @return
	 */
	public Mob findMob(int id) {
		if( this._list.containsKey(id) ) {
			return this._list.get(id);
		}

		return null;
	}

	/**
	 * add
	 * @param mob
	 */
	public void add(Mob mob) {
		this._list.put(mob.getObjectHandle(), mob);
	}

	/**
	 * getListSize
	 * @return
	 */
	public int getListSize() {
		return this._list.size();
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1+(this._list.size()*171);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		int count = reader.readByteInt();

		for( int i=0; i<count; i++ ) {
			Mob tmob = new Mob();
			tmob.read(reader);

			this._list.put(tmob.getObjectHandle(), tmob);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._list.size());

		for( Map.Entry<Integer, Mob> entry : this._list.entrySet() ) {
			entry.getValue().write(writer);
		}
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"MobList: [" +
			" Count: " + Integer.toString(this._list.size()) +
			"]"
			);

		for( Map.Entry<Integer, Mob> entry : this._list.entrySet() ) {
			dump += entry.getValue().dump(deep+1);
		}

		return dump;
	}
}