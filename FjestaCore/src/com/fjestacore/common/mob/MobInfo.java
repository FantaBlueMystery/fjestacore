/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.mob;

import com.fjestacore.files.shn.CDataData;

/**
 * MobInfo
 * @author FantaBlueMystery
 */
public class MobInfo extends CDataData {

	/**
	 * getId
	 * @return
	 */
	public int getId() {
		return (int) this.getData(0);
	}

	/**
	 * getInxName
	 * @return
	 */
	public String getInxName() {
		return (String) this.getData(1);
	}

	/**
	 * getName
	 * @return
	 */
	public String getName() {
		return (String) this.getData(2);
	}

	/**
	 * getLevel
	 * @return
	 */
	public int getLevel() {
		return (int) (long) this.getData(3);
	}

	// TODO
}