/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.mob;

import com.fjestacore.files.shn.CDataData;
import java.util.HashMap;
import java.util.Map;

/**
 * MobInfoList
 * @author FantaBlueMystery
 */
public class MobInfoList {

	/**
	 * SingletonHolder
	 */
	static private final class SingletonHolder {
		private static MobInfoList INSTANCE = null;
	}

	/**
	 * setInstance
	 * @param list
	 */
	static public void setInstance(MobInfoList list) {
		MobInfoList.SingletonHolder.INSTANCE = list;
	}

	/**
	 * getInstance
	 * @return
	 */
	static public MobInfoList getInstance() {
		return MobInfoList.SingletonHolder.INSTANCE;
	}

	/**
	 * item info list
	 */
	protected HashMap<Integer, MobInfo> _list = new HashMap<>();

	/**
	 * putRows
	 * @param rows
	 */
	public void putRows(HashMap<Object, CDataData> rows) {
		for( Map.Entry<Object, CDataData> entry : rows.entrySet() ) {
			this.put((MobInfo) entry.getValue().cast(MobInfo.class));
		}
	}

	/**
	 * put
	 * @param mi
	 */
	public void put(MobInfo mi) {
		this._list.put(mi.getId(), mi);
	}

	/**
	 * get
	 * @param id
	 * @return
	 */
	public MobInfo get(int id) {
		return this._list.get(id);
	}

	/**
	 * getSize
	 * @return
	 */
	public int getSize() {
		return this._list.size();
	}
}