/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.mob;

import com.fjestacore.common.abstate.AbnormalStateBit;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * MobFlag
 * @author FantaBlueMystery
 */
public class MobFlag extends AbstractStruct {

	/**
	 * flag state
	 */
	protected MobBriefFlag _flagState = MobBriefFlag.NORMAL;

	/**
	 * abstatebit
	 */
	protected AbnormalStateBit _abstatebit = new AbnormalStateBit();

	/**
	 * gate 2 where
	 */
	protected String _gate2where = "";

	/**
	 * MobFlag
	 */
	public MobFlag() {}

	/**
	 * MobFlag
	 * @param flagstate
	 */
	public MobFlag(MobBriefFlag flagstate) {
		this._flagState = flagstate;
	}

	/**
	 * getFlagState
	 * @return
	 */
	public MobBriefFlag getFlagState() {
		return this._flagState;
	}

	/**
	 * setFlagState
	 * @param state
	 */
	public void setFlagState(MobBriefFlag state) {
		this._flagState = state;
	}

	/**
	 * getGate2Where
	 * @return
	 */
	public String getGate2Where() {
		return this._gate2where;
	}

	/**
	 * setGate2Where
	 * @param where
	 */
	public void setGate2Where(String where) {
		this._gate2where = where;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return AbnormalStateBit.SIZE+1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._flagState = MobBriefFlag.getMobBriefFlag(reader.readByteInt());

		switch( this._flagState ) {
			case NORMAL:
				this._abstatebit.read(reader);
				break;

			case GATE:
				this._gate2where = reader.readString(12);

				// read padding bytes
				reader.readBuffer(AbnormalStateBit.SIZE-12);
				break;
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._flagState.getValue());

		switch( this._flagState ) {
			case NORMAL:
				this._abstatebit.write(writer);
				break;

			case GATE:
				writer.writeString(this._gate2where, 12);
				writer.writeString("", AbnormalStateBit.SIZE-12);
				break;
		}
	}
}