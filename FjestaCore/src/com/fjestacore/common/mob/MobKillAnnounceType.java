/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.mob;

/**
 * MobKillAnnounceType
 * @author FantaBlueMystery
 */
public enum MobKillAnnounceType {
	MKL_Helga(0x0),
	MKL_Karen(0x1),
	MKL_B_CrackerHumar(0x2),
	MKL_BH_Helga(0x3),
	MKL_BH_Humar(0x4),
	MKL_Chimera(0x5),
	MKL_B_Albireo(0x6),
	MKL_BH_Albireo(0x7),
	MLK_BH_Karen(0x8),
	MKL_IS_Blakhan(0x9),
	MAX_MOBKILLANNOUNCETYPE(0xA);

	/**
	 * value
	 */
	private int _value;

	/**
	 * MobKillAnnounceType
	 * @param value
	 */
	private MobKillAnnounceType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getMobKillAnnounceType
	 * @param value
	 * @return
	 */
	static public MobKillAnnounceType getMobKillAnnounceType(int value) {
		MobKillAnnounceType[] types = MobKillAnnounceType.values();

		for( MobKillAnnounceType type: types ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return MobKillAnnounceType.MAX_MOBKILLANNOUNCETYPE;
	}
}