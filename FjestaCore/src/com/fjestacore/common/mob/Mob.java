/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.mob;

import com.fjestacore.common.animation.SAnimation;
import com.fjestacore.common.kq.KQTeamType;
import com.fjestacore.common.move.SomeoneMoveFromTo;
import com.fjestacore.common.position.PositionXYR;
import com.fjestacore.core.dump.MsgDump;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import java.io.IOException;

/**
 * Mob
 * @author FantaBlueMystery
 */
public class Mob extends AbstractStruct implements IAbstractStructDump, IObjectHandle {

	/**
	 * handle
	 */
	protected int _handle = 0;

	/**
	 * mode
	 */
	protected int _mode = 0;

	/**
	 * mob id
	 */
	protected int _mobId = 0;

	/**
	 * position
	 */
	protected PositionXYR _pos = new PositionXYR();

	/**
	 * flag
	 */
	protected MobFlag _flag = new MobFlag();

	/**
	 * sanimation
	 */
	protected SAnimation _sanimation = new SAnimation();

	/**
	 * n animation level
	 */
	protected int _nAnimationLevel = 0;

	/**
	 * n kq team type
	 */
	protected KQTeamType _nKQTeamType = KQTeamType.KQTT_MAX;

	/**
	 * b regen ani
	 */
	protected int _bRegenAni = 0;

	/**
	 * unknown byte
	 */
	protected int _unknown = 0;

	/**
	 * getObjectHandle
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._handle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._handle = handle;
	}

	/**
	 * mode
	 * @param mode
	 */
	public void setMode(int mode) {
		this._mode = mode;
	}

	/**
	 * getMode
	 * @return
	 */
	public int getMode() {
		return this._mode;
	}

	/**
	 * setMobId
	 * @param mobId
	 */
	public void setMobId(int mobId) {
		this._mobId = mobId;
	}

	/**
	 * getMobId
	 * @return
	 */
	public int getMobId() {
		return this._mobId;
	}

	/**
	 * getMobFlag
	 * @return
	 */
	public MobFlag getMobFlag() {
		return this._flag;
	}

	/**
	 * getPosition
	 * @return
	 */
	public PositionXYR getPosition() {
		return this._pos;
	}

	/**
	 * updatePosition
	 * @param move
	 */
	public void updatePosition(SomeoneMoveFromTo move) {
		this._pos.setPosition(move.getMove().getTo());
	}

	/**
	 * setAnimationLevel
	 * @param lvl
	 */
	public void setAnimationLevel(int lvl) {
		this._nAnimationLevel = lvl;
	}

	/**
	 * getKQTeamType
	 * @return
	 */
	public KQTeamType getKQTeamType() {
		return this._nKQTeamType;
	}

	/**
	 * setKQTeamType
	 * @param type
	 */
	public void setKQTeamType(KQTeamType type) {
		this._nKQTeamType = type;
	}

	/**
	 * getMobInfo
	 * @return
	 */
	public MobInfo getMobInfo() {
		if( MobInfoList.getInstance() != null ) {
			return MobInfoList.getInstance().get(this._mobId);
		}

		return null;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 161;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle	= reader.readShort();				// +2
		this._mode		= reader.readByteInt();				// +1
		this._mobId		= reader.readShort();				// +2

		this._pos.read(reader);								// +9

		this._flag.read(reader);							// +111
		this._sanimation.read(reader);						// +32

		this._nAnimationLevel	= reader.readByteInt();		// +1
		this._nKQTeamType		= KQTeamType.intToType(reader.readByteInt());		// +1

		// regenani now ushort?
		this._bRegenAni			= reader.readByteInt();		// +1
		// is sec. byte of regenani?
		this._unknown			= reader.readByteInt();		// +1
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._handle);
		writer.writeByteInt(this._mode);
		writer.writeShort(this._mobId);

		this._pos.write(writer);
		this._flag.write(writer);
		this._sanimation.write(writer);

		writer.writeByteInt(this._nAnimationLevel);
		writer.writeByteInt(this._nKQTeamType.getValue());
		writer.writeByteInt(this._bRegenAni);
		writer.writeByteInt(this._unknown);
	}

	/**
	 * dump
	 * @return
	 */
	@Override
	public String dump() {
		return this.dump(0);
	}

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	@Override
	public String dump(int deep) {
		String dump = MsgDump.getMsgDump(deep,
			"Mob: [" +
			" Handle: " + Integer.toString(this._handle) +
			" Mode: " + Integer.toString(this._mode) +
			" MobId: " + Integer.toString(this._mobId) +
			" nKQTeamType: " + this._nKQTeamType.name() +
			"]"
			);

		MobInfo mi = this.getMobInfo();

		if( mi != null ) {
			dump += MsgDump.getMsgDump(deep+1,
			"MobInfo: [" +
			" MobInxName: " + mi.getInxName() +
			" MobName: " + mi.getName() +
			"]"
			);
		}

		return dump;
	}
}