/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.minihouse;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * MinihouseBuilding
 * @author FantaBlueMystery
 */
public class MinihouseBuilding extends AbstractStruct {

	/**
	 * max visiter
	 */
	protected int _maxvisiter = 0x0F;	// 15

	/**
	 * password
	 */
	protected String _password = "";

	/**
	 * title
	 */
	protected String _title = "";

	/**
	 * b item info close?
	 */
	protected int _bItemInfoClose = 0;

	/**
	 * sNotify
	 */
	protected String _sNotify = "";

	/**
	 * MinihouseBuilding
	 */
	public MinihouseBuilding() {}

	/**
	 * MinihouseBuilding
	 * @param maxvisiter
	 * @param password
	 * @param title
	 * @param notify
	 */
	public MinihouseBuilding(int maxvisiter, String password, String title, String notify) {
		this._maxvisiter	= maxvisiter;
		this._password		= password;
		this._title			= title;
		this._sNotify		= notify;
	}

	/**
	 * getPassword
	 * @return
	 */
	public String getPassword() {
		return this._password;
	}

	/**
	 * setPassword
	 * @param password
	 */
	public void setPassword(String password) {
		this._password = password;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 133;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._maxvisiter		= reader.readByteInt();
		this._password			= reader.readString(9);
		this._title				= reader.readString(21);
		this._bItemInfoClose	= reader.readByteInt();
		this._sNotify			= reader.readString(110);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._maxvisiter);
		writer.writeString(this._password, 9);
		writer.writeString(this._title, 21);
		writer.writeByteInt(this._bItemInfoClose);
		writer.writeString(this._sNotify, 110);
	}
}