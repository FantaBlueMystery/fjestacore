/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.kq;

import com.fjestacore.common.name.Name3;
import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.TmTime;
import java.io.IOException;

/**
 * KQInformation
 * @author FantaBlueMystery
 */
public class KQInformation extends AbstractStruct {

	/**
	 * handle
	 */
	protected long _handle = 0;

	/**
	 * mapname
	 */
	protected Name3 _mapname = new Name3();

	/**
	 * position
	 */
	protected PositionXY _position = new PositionXY();

	/**
	 * date
	 */
	protected TmTime _date = new TmTime();

	/**
	 * KQInformation
	 */
	public KQInformation() {}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 28;
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._handle);

		this._mapname.write(writer);
		this._position.write(writer);

		writer.writeUInt(0);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._handle = reader.readUInt();
		this._mapname.read(reader);

		this._position.read(reader);

		reader.readUInt();
	}
}
