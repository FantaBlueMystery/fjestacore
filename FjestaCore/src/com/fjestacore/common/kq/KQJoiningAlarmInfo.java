/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.kq;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * KQJoiningInfoAlarm
 * @author FantaBlueMystery
 */
public class KQJoiningAlarmInfo extends AbstractStruct {

	/**
	 * n handle
	 */
	protected long _nHandle = 0;

	/**
	 * n ID
	 */
	protected int _nID = 0;

	/**
	 * n min level
	 */
	protected int _nMinLev = 0;

	/**
	 * n max level
	 */
	protected int _nMaxLev = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 6;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nHandle	= reader.readUInt();
		this._nID		= reader.readShort();
		this._nMinLev	= reader.readByteInt();
		this._nMaxLev	= reader.readByteInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._nHandle);
		writer.writeShort(this._nID);
		writer.writeByteInt(this._nMinLev);
		writer.writeByteInt(this._nMaxLev);
	}
}