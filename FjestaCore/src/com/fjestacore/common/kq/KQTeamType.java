/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.kq;

/**
 * KQTeamType
 * @author FantaBlueMystery
 */
public enum KQTeamType {
	KQTT_RED(0),
	KQTT_BLUE(1),
	KQTT_MAX(2);

	/**
	 * value
	 */
	private int _value;

	/**
	 * KQTeamType
	 * @param value
	 */
	private KQTeamType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * intToType
	 * @param value
	 * @return
	 */
	static public KQTeamType intToType(int value) {
		KQTeamType[] types = KQTeamType.values();

		for( KQTeamType type : types ) {
			if (type.getValue() == value) {
				return type;
			}
		}

		return KQTeamType.KQTT_MAX;
	}
}