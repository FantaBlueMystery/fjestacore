/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.kq;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.type.TmTime;
import java.io.IOException;

/**
 * KQTime
 * @author FantaBlueMystery
 */
public class KQTime extends AbstractStruct {

	/**
	 * time
	 */
	protected int _time = 0;

	/**
	 * tm time
	 */
	protected TmTime _tmTime = new TmTime();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4*10;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._time = (int) reader.readUInt();
		this._tmTime.read(reader);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._time);
		this._tmTime.write(writer);
	}
}