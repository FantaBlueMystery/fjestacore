/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.kq;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * KQJoiningAlarm
 * @author FantaBlueMystery
 */
public class KQJoiningAlarm extends AbstractStruct {

	/**
	 * KQ Info
	 */
	protected KQJoiningAlarmInfo _kQInfo = new KQJoiningAlarmInfo();

	/**
	 * Message
	 */
	protected String _msg = "";

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._kQInfo.getSize() + 1 + this._msg.length();
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._kQInfo.read(reader);

		int msgLen = reader.readByteInt();

		this._msg = reader.readString(msgLen);
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._kQInfo.write(writer);

		writer.writeByteInt(this._msg.length());
		writer.writeString(this._msg, this._msg.length());
	}
}