/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.kq;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * KQTeamTypeAck
 * @author FantaBlueMystery
 */
public class KQTeamTypeAck extends AbstractStruct {

	/**
	 * team type
	 */
	protected KQTeamType _nTeamType = KQTeamType.KQTT_MAX;

	/**
	 * KQTeamTypeAck
	 */
	public KQTeamTypeAck() {}

	/**
	 * KQTeamTypeAck
	 * @param type
	 */
	public KQTeamTypeAck(KQTeamType type) {
		this._nTeamType = type;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._nTeamType = KQTeamType.intToType(reader.readByteInt());
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._nTeamType.getValue());
	}
}