/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.mid;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * MidInstanceList
 * @author FantaBlueMystery
 */
public class MidInstanceList extends AbstractStruct {

	/**
	 * unknown
	 */
	protected int _unknown = 0;

	/**
	 * mid instances
	 */
	protected ArrayList<MidInstance> _instances = new ArrayList<>();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4 + (this._instances.size() * MidInstance.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._unknown = reader.readShort();

		int count = reader.readShort();

		for( int i=0; i<count; i++ ) {
			MidInstance midi = new MidInstance();

			midi.read(reader);

			this._instances.add(midi);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeShort(this._unknown);
		writer.writeShort(this._instances.size());

		for( MidInstance midi: this._instances ) {
			midi.write(writer);
		}
	}
}