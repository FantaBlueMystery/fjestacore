/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.common.mid;

import com.fjestacore.common.name.Name3;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * MidInstance
 * @author FantaBlueMystery
 */
public class MidInstance extends AbstractStruct {

	/**
	 * size
	 */
	static public int SIZE = 20;

	/**
	 * mapname
	 */
	protected Name3 _mapname = new Name3();

	/**
	 * unknown
	 */
	protected int _unknown = 0;

	/**
	 * min level
	 */
	protected int _minLevel = 0;

	/**
	 * max level
	 */
	protected int _maxLevel = 0;

	/**
	 * min player
	 */
	protected int _minPlayer = 0;

	/**
	 * max player
	 */
	protected int _maxPlayer = 0;

	/**
	 * unknown2
	 */
	protected int _unknown2 = 0;

	/**
	 * required item id
	 */
	protected int _requiredItemId = 0;

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return MidInstance.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._mapname.read(reader);
		this._unknown			= reader.readByteInt();
		this._minLevel			= reader.readByteInt();
		this._maxLevel			= reader.readByteInt();
		this._minPlayer			= reader.readByteInt();
		this._maxPlayer			= reader.readByteInt();
		this._unknown2			= reader.readByteInt();
		this._requiredItemId	= reader.readShort();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._mapname.write(writer);
		writer.writeByteInt(this._unknown);
		writer.writeByteInt(this._minLevel);
		writer.writeByteInt(this._maxLevel);
		writer.writeByteInt(this._minPlayer);
		writer.writeByteInt(this._maxPlayer);
		writer.writeByteInt(this._unknown2);
		writer.writeShort(this._requiredItemId);
	}
}