/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.servermenu;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ServerMenuAck
 * @author FantaBlueMystery
 */
public class ServerMenuAck extends AbstractStruct {

	/**
	 * reply
	 */
	protected int _reply = 0;

	/**
	 * getReply
	 * @return
	 */
	public int getReply() {
		return this._reply;
	}

	/**
	 * setReply
	 * @param reply
	 */
	public void setReply(int reply) {
		this._reply = reply;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 1;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._reply = reader.readByteInt();
	}

	/**
	 *
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._reply);
	}
}