/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.servermenu;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * ServerMenuEntry
 * @author FantaBlueMystery
 */
public class ServerMenuEntry extends AbstractStruct {

	/**
	 * SIZE
	 */
	static public int SIZE = 33;

	/**
	 * replay
	 */
	protected int _reply = 0;

	/**
	 * a string
	 */
	protected String _aString = "";

	/**
	 * ServerMenuEntry
	 */
	public ServerMenuEntry() {}

	/**
	 * ServerMenuEntry
	 * @param reply
	 * @param aString
	 */
	public ServerMenuEntry(int reply, String aString) {
		this._reply		= reply;
		this._aString	= aString;
	}

	/**
	 * getReply
	 * @return
	 */
	public int getReply() {
		return this._reply;
	}

	/**
	 * aString
	 * @return
	 */
	public String aString() {
		return this._aString;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return ServerMenuEntry.SIZE;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._reply		= reader.readByteInt();
		this._aString	= reader.readString(32).trim();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeByteInt(this._reply);
		writer.writeString(this._aString, 32);
	}
}