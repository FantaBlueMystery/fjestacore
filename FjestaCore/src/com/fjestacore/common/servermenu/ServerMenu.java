/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.common.servermenu;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.handle.IObjectHandle;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * ServerMenu
 * @author FantaBlueMystery
 */
public class ServerMenu extends AbstractStruct implements IObjectHandle {

	/**
	 * title
	 */
	protected String _title = "";

	/**
	 * priority
	 */
	protected int _priority = 0;

	/**
	 * npc handle
	 */
	protected int _npcHandle = 0;

	/**
	 * npc position
	 */
	protected PositionXY _npcPosition = new PositionXY();

	/**
	 * limit range
	 */
	protected int _limitRange = 0;

	/**
	 * menu
	 */
	protected ArrayList<ServerMenuEntry> _menu = new ArrayList<>();

	/**
	 * ServerMenu
	 */
	public ServerMenu() {}

	/**
	 * ServerMenu
	 * @param title
	 * @param limitRange
	 */
	public ServerMenu(String title, int limitRange) {
		this._title			= title;
		this._limitRange	= limitRange;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public int getObjectHandle() {
		return this._npcHandle;
	}

	/**
	 * setObjectHandle
	 * @param handle
	 */
	@Override
	public void setObjectHandle(int handle) {
		this._npcHandle = handle;
	}

	/**
	 * getPosition
	 * @return
	 */
	public PositionXY getPosition() {
		return this._npcPosition;
	}

	/**
	 * setPosition
	 * @param pos
	 */
	public void setPosition(PositionXY pos) {
		this._npcPosition.setPos(pos);
	}

	/**
	 * addMenuEntry
	 * @param sme
	 */
	public void addMenuEntry(ServerMenuEntry sme) {
		this._menu.add(sme);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 128+3+8+2+1+(this._menu.size()*ServerMenuEntry.SIZE);
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._title			= reader.readString(128).trim();
		this._priority		= reader.readByteInt();
		this._npcHandle		= reader.readShort();

		this._npcPosition.read(reader);

		this._limitRange	= reader.readShort();

		int count = reader.readByteInt();

		for( int i=0; i<count; i++ ) {
			ServerMenuEntry sm = new ServerMenuEntry();
			sm.read(reader);

			this._menu.add(sm);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeString(this._title, 128);
		writer.writeByteInt(this._priority);
		writer.writeShort(this._npcHandle);

		this._npcPosition.write(writer);

		writer.writeShort(this._limitRange);

		writer.writeByteInt(this._menu.size());

		for( ServerMenuEntry sm: this._menu ) {
			sm.write(writer);
		}
	}
}