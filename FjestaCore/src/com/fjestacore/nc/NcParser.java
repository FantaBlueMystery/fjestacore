/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.nc;

/**
 * NcParser
 * @author FantaBlueMystery
 */
public class NcParser {

	static public int HEAD_MOD	= 10;
	static public int NUM_MOD	= 1023;

	/**
	 * parseCmd
	 * @param cmd
	 * @return
	 */
	static public NcParts parseCmd(int cmd) {
		int header = (cmd >> NcParser.HEAD_MOD);
		int num = (cmd & NcParser.NUM_MOD);

		NcClient ncc = NcClient.valueOf(cmd);

		if( ncc != NcClient.NC_NULL ) {
			return new NcParts(NcHeader.valueOf(header), ncc, num);
		}

		NcServer ncs = NcServer.valueOf(cmd);

		if( ncs != NcServer.NC_NULL ) {
			return new NcParts(NcHeader.valueOf(header), ncs, num);
		}

		return null;
	}

	/***
	 * dumpStr
	 * @param cmd
	 * @return
	 */
	static public String dumpStr(int cmd) {
		int header = (cmd >> NcParser.HEAD_MOD);
		int num = (cmd & NcParser.NUM_MOD);

		String dump = " Header: " + NcHeader.valueOf(header) + "(" + Integer.toString(header) +	") ";
		dump += " Num: " + Integer.toString(num);

		return dump;
	}
}