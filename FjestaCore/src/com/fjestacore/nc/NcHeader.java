/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.nc;

/**
 * NcHeader
 * @author FantaBlueMystery
 */
public enum NcHeader {
	NULL(0),
	LOG(1),
	MISC(2),
	USER(3),
	CHAR(4),
	AVATAR(5),
	MAP(6),
	BRIEFINFO(7),
	ACT(8),
	BAT(9),
	OPTOOL(10),
	PATCH(11),
	ITEM(12),
	ITEMDB(13),
	PARTY(14),
	MENU(15),
	CHARSAVE(16),
	QUEST(17),
	SKILL(18),
	TRADE(19),
	SOULSTONE(20),
	FRIEND(21),
	KQ(22),
	WT(23),
	CT(24),
	ANNOUNCE(25),
	BOOTH(26),
	SCENARIO(27),
	CHAR_OPTION(28),
	GUILD(29),
	SCRIPT(30),
	PRISON(31),
	REPORT(32),
	EVENT_FRIEND(33),
	EVENT_WORLDCUP(34),
	MINIHOUSE(35),
	CHARGED(36),
	HOLY_PROMISE(37),
	GUILD_ACADEMY(38),
	// 39 ?
	PROMOTION(40),
	INSTANCE_DUNGEON(41),
	CHAT_RESTRICT(42),
	DICE_TAISAI(43),
	RAID(44),
	USER_CONNECTION(45),
	AUCTION(46),
	GAMBLE(47),
	MID(48),
	COLLECT(49),
	SYSLOG(50),
	MOVER(51),
	EVENT(52),
	PET(53)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * NcHeader
	 * @param value
	 */
	private NcHeader(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * valueOf
	 * @param number
	 * @return
	 */
	static public NcHeader valueOf(int number) {
		NcHeader[] headers = NcHeader.values();

		for( NcHeader header: headers ) {
			if( header.getValue() == number ) {
				return header;
			}
		}

		return NcHeader.NULL;
	}
}
