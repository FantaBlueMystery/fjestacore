/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.nc;

/**
 * NcParts
 * @author FantaBlueMystery
 */
public class NcParts {

	/**
	 * nc header
	 */
	protected NcHeader _header = null;

	/**
	 * nc client
	 */
	protected NcClient _client = null;

	/**
	 * nc server
	 */
	protected NcServer _server = null;

	/**
	 * nc num
	 */
	protected int _num = 0;

	/**
	 * NcParts
	 * @param header
	 * @param client
	 * @param num
	 */
	public NcParts(NcHeader header, NcClient client, int num) {
		this._header = header;
		this._client = client;
		this._num = num;
	}

	/**
	 * NcParts
	 * @param header
	 * @param server
	 * @param num
	 */
	public NcParts(NcHeader header, NcServer server, int num) {
		this._header = header;
		this._server = server;
		this._num = num;
	}

	/**
	 * getHeader
	 * @return
	 */
	public NcHeader getHeader() {
		return this._header;
	}

	/**
	 * getClient
	 * @return
	 */
	public NcClient getClient() {
		return this._client;
	}

	/**
	 * getServer
	 * @return
	 */
	public NcServer getServer() {
		return this._server;
	}

	/**
	 * getNum
	 * @return
	 */
	public int getNum() {
		return this._num;
	}

	/**
	 * isClient
	 * @return
	 */
	public boolean isClient() {
		return this._client != null;
	}

	/**
	 * isServer
	 * @return
	 */
	public boolean isServer() {
		return this._server != null;
	}
}