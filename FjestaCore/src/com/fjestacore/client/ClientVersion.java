/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.client;

import java.util.Calendar;

/**
 * ClientVersion
 * @author FantaBlueMystery
 */
public enum ClientVersion {
	VERSION_20150113("20150113"),
	VERSION_20151116("20151116"),

	VERSION_20190923("20190923"),
	VERSION_20191126("20191126"),
	VERSION_20191203("20191203"),
	VERSION_20191210("20191210"),
	VERSION_20200214("20200214"),
	VERSION_20200428("20200428"),
	;

	/**
	 * value
	 */
	private String _value;

	/**
	 * ClientVersion
	 * @param value
	 */
	private ClientVersion(String value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public String getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(String value) {
		this._value = value;
	}

	/**
	 * toTimestamp
	 * @return
	 */
	public long toTimestamp() {
		int year = Integer.valueOf(this._value.substring(0, 4));
		int month = Integer.valueOf(this._value.substring(4, 6));
		int day = Integer.valueOf(this._value.substring(6, 8));

		Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

		return cal.getTimeInMillis();
	}

	/**
	 * compareTo
	 * @param a
	 * @param b
	 * @return
	 */
	static public int compareTo(ClientVersion a, ClientVersion b) {
		if( a.toTimestamp() == b.toTimestamp() ) {
			return 0;
		}
		else if( a.toTimestamp() < b.toTimestamp() ) {
			return -1;
		}

		return 1;
	}

	/**
	 * getByString
	 * @param str
	 * @return
	 */
	static public ClientVersion getByString(String str) {
		ClientVersion[] cvs = ClientVersion.values();

		for( ClientVersion cv: cvs ) {
			String cvstr = cv.getValue();

			if( cvstr.compareTo(str) == 0 ) {
				return cv;
			}
		}

		return VERSION_20200214;
	}
}