/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.client;

/**
 * ClientType
 * @author FantaBlueMystery
 */
public enum ClientType {
	NULL(0),
	GER(1),
	US(2),
	TW(3),
	JP(4),
	CH(5)
	;

	private int _value;

	/**
	 * ClientType
	 * @param value
	 */
	private ClientType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}
