/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import com.fjestacore.nc.NcClient;
import com.fjestacore.nc.NcServer;
import com.fjestacore.core.convert.ConvertBytes;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.dump.HexDump;
import com.fjestacore.core.net.SocketType;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.core.struct.IAbstractStructDump;
import com.fjestacore.nc.NcParser;
import com.fjestacore.nc.NcParts;

/**
 * PacketDump
 * @author FantaBlueMystery
 */
public class PacketDump extends Packet {

	/**
	 * dump
	 */
	public void dump() {
		byte[] command = ConvertBytes.uShortToBytes(this._command);

		System.out.println("Dump Packet >>>>>>");
		System.out.println("Readed-Size: ");
		System.out.println(this._sizeReaded);
		System.out.println("Command: ");
		System.out.println(HexString.byteArrayToHexString(command));

		String name = "";

		if( this._type == SocketType.UNKNOWN ) {
			name = NcServer.valueOf(this._command).name();

			if( name != null ) {
				this._type = SocketType.SERVER;
			}
			else {
				name = NcClient.valueOf(this._command).name();

				if( name != null ) {
					this._type = SocketType.CLIENT;
				}
			}
		}

		if( this._type == SocketType.SERVER ) {
			name = NcServer.valueOf(this._command).name();

			if( name != null ) {
				System.out.println("Command-Name-Server:" + name);
			}
		}
		else if( this._type == SocketType.CLIENT ) {
			name = NcClient.valueOf(this._command).name();

			if( name != null ) {
				System.out.println("Command-Name-Client:" + name);
			}
		}

		if( ("".equals(name)) || (name == null) ) {
			System.out.println("Command-Name-Unknown:" + Integer.toString(this._command));
		}

		System.out.println("Buffer: ");

		if( (this._buffer != null) && (this._buffer.length > 0 ) ) {
			HexDump.dump(this._buffer);
		}
		else {
			System.out.println("Empty");
		}

		System.out.println(" ");

		if( this instanceof IPacket ) {
			AbstractStruct po = ((IPacket)this).getPacketObject();

			if( po instanceof IAbstractStructDump ) {
				IAbstractStructDump pod = (IAbstractStructDump) po;

				System.out.println(pod.dump());
			}
		}
	}

	/**
	 * dumpStr
	 * @param mainClass
	 * @return
	 */
	public String dumpStr(String mainClass) {
		byte[] command = ConvertBytes.uShortToBytes(this._command);

		String dump = mainClass + ": ";
		String hexCmd = HexString.byteArrayToHexString(command);
		char[] rhexCmd = {hexCmd.charAt(2), hexCmd.charAt(3), hexCmd.charAt(0), hexCmd.charAt(1)};

		dump += " Command: " + hexCmd + " (0x" + (new String(rhexCmd)) +  ")";

		String name = "";

		if( this._type == SocketType.UNKNOWN ) {
			name = NcServer.valueOf(this._command).name();

			if( name != null ) {
				this._type = SocketType.SERVER;
			}
			else {
				name = NcClient.valueOf(this._command).name();

				if( name != null ) {
					this._type = SocketType.CLIENT;
				}
			}
		}

		if( this._type == SocketType.SERVER ) {
			name = NcServer.valueOf(this._command).name();

			if( name != null ) {
				name = "Server: " + name;
			}
		}
		else if( this._type == SocketType.CLIENT ) {
			name = NcClient.valueOf(this._command).name();

			if( name != null ) {
				name = "Client: " + name;
			}
		}

		if( ("".equals(name)) || (name == null) ) {
			name = "Unknown: " + Integer.toString(this._command);
		}

		NcParts parts = NcParser.parseCmd(this._command);

		if( parts != null ) {
			dump += " (" + name + " " + Integer.toString(this._command) +
				"(Header: " + Integer.toString(parts.getHeader().getValue()) +
				" HName: " + parts.getHeader().name() +
				" Number: " + Integer.toString(parts.getNum()) + ")) ";
		}

		dump += " ReadSize: " + Integer.toString(this._sizeReaded);

		if( (this._buffer != null) && (this._buffer.length > 0 ) ) {
			dump += " BufferSize: " + Integer.toString(this._buffer.length);
			dump += " Buffer: ";

			dump += "\r\n";

			try {
				dump += HexDump.dumpStr(this._buffer);
			}
			catch( Exception ex ) {
				dump += ex.getMessage();
			}
		}
		else {
			dump += " Buffer: Empty";
		}

		if( this instanceof IPacket ) {
			AbstractStruct po = ((IPacket)this).getPacketObject();

			if( po instanceof IAbstractStructDump ) {
				IAbstractStructDump pod = (IAbstractStructDump) po;

				dump += "\r\n";
				dump += pod.dump();
			}
		}

		return dump;
	}
}