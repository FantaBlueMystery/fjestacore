/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.item;

import com.fjestacore.common.item.ItemCellChange;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ITEM_CELLCHANGE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ITEM_CELLCHANGE_CMD)
public class NC_ITEM_CELLCHANGE_CMD extends Packet implements IPacket {

	/**
	 * cell change
	 */
	protected ItemCellChange _cc = new ItemCellChange();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cc;
	}
}