/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.item;

import com.fjestacore.common.item.ItemChargedInvenOpenAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ITEM_CHARGEDINVENOPEN_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ITEM_CHARGEDINVENOPEN_ACK)
public class NC_ITEM_CHARGEDINVENOPEN_ACK extends Packet implements IPacket {

	/**
	 * item charged inven open
	 */
	protected ItemChargedInvenOpenAck _icio = new ItemChargedInvenOpenAck();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._icio;
	}
}