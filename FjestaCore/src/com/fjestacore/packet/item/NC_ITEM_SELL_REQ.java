/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.item;

import com.fjestacore.common.item.ItemSellReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * PROTO_NC_ITEM_SELL_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ITEM_SELL_REQ)
public class NC_ITEM_SELL_REQ extends Packet implements IPacket {

	/**
	 * item sell req
	 */
	protected ItemSellReq _is = new ItemSellReq();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._is;
	}
}