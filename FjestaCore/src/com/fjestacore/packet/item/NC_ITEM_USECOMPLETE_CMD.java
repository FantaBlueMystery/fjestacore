/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.item;

import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ITEM_USECOMPLETE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ITEM_USECOMPLETE_CMD)
public class NC_ITEM_USECOMPLETE_CMD extends Packet {}