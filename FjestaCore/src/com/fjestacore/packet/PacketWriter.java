/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import com.fjestacore.core.convert.ConvertBytes;
import com.fjestacore.core.crypt.INetCrypt;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PacketWriter
 * @author FantaBlueMystery
 */
public class PacketWriter {

	/**
	 * writePacketSize
	 * @param size
	 * @return
	 */
	static public byte[] writePacketSize(int size) {
		if( size > 255 ) {
			byte[] bsize = new byte[3];
			bsize[0] = 0x00;

			byte[] tsize = ConvertBytes.uShortToBytes(size);
			bsize[1] = tsize[0];
			bsize[2] = tsize[1];

			return bsize;
		}
		else {
			byte[] bsize = new byte[1];
			bsize[0] = (byte) (size & 0xFF);

			return bsize;
		}
	}

	/**
	 * writePacket
	 * @param packet
	 * @param crypt
	 * @return
	 */
	static public byte[] writePacket(Packet packet, INetCrypt crypt) {
		try {
			packet._onWriteBuffer();

			byte[] cmd = ConvertBytes.uShortToBytes(packet.getCommand());
			byte[] subbuffer = packet.getBuffer();

			int size = cmd.length;

			if( subbuffer != null ) {
				size += subbuffer.length;
			}

			byte[] sendbuffer = Arrays.copyOf(cmd, size);

			if( subbuffer != null ) {
				System.arraycopy(subbuffer, 0, sendbuffer, cmd.length, subbuffer.length);
			}

			if( crypt != null ) {
				sendbuffer = crypt.crypt(sendbuffer);
			}


			byte[] bsize = PacketWriter.writePacketSize(size);

			byte[] buffer = new byte[size+bsize.length];

			int bufferindex = 0;

			for( int i=0; i<bsize.length; i++ ) {
				buffer[bufferindex] = bsize[i];
				bufferindex++;
			}

			for( int i=0; i<sendbuffer.length; i++ ) {
				buffer[bufferindex] = sendbuffer[i];
				bufferindex++;
			}

			return buffer;
		}
		catch( Exception ex ) {
			Logger.getLogger(Packet.class.getName()).log(Level.SEVERE, "writePacket: " + ex.getMessage(), ex);
		}

		return null;
	}
}