/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.kq;

import com.fjestacore.common.kq.KQJoiningAlarm;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_KQ_JOINING_ALARM_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_KQ_JOINING_ALARM_CMD)
public class NC_KQ_JOINING_ALARM_CMD extends Packet implements IPacket {

	/**
	 * KQ joining alarm
	 */
	protected KQJoiningAlarm _kqja = new KQJoiningAlarm();

	/**
	 * getKQJoiningAlarm
	 * @return
	 */
	public KQJoiningAlarm getKQJoiningAlarm() {
		return this._kqja;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._kqja;
	}
}