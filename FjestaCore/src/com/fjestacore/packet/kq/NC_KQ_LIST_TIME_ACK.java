/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.kq;

import com.fjestacore.common.kq.KQTime;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_KQ_LIST_TIME_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_KQ_LIST_TIME_ACK)
public class NC_KQ_LIST_TIME_ACK extends Packet implements IPacket {

	/**
	 * server time
	 */
	protected KQTime _serverTime = new KQTime();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._serverTime;
	}
}