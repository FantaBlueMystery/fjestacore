/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.kq;

import com.fjestacore.common.kq.KQTeamTypeAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_KQ_TEAM_TYPE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_KQ_TEAM_TYPE_CMD)
public class NC_KQ_TEAM_TYPE_CMD extends Packet implements IPacket {

	/**
	 * KQ team type
	 */
	protected KQTeamTypeAck _KQtt = new KQTeamTypeAck();

	/**
	 * NC_KQ_TEAM_TYPE_CMD
	 */
	public NC_KQ_TEAM_TYPE_CMD() {
		super();
	}

	/**
	 * NC_KQ_TEAM_TYPE_CMD
	 * @param kqtt
	 */
	public NC_KQ_TEAM_TYPE_CMD(KQTeamTypeAck kqtt) {
		super();

		this._KQtt = kqtt;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._KQtt;
	}
}