/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import com.fjestacore.core.net.SocketType;
import com.fjestacore.nc.NcHeader;
import com.fjestacore.nc.NcParser;
import com.fjestacore.nc.NcParts;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PacketDictionary
 * @author FantaBlueMystery
 */
public class PacketDictionary {

	/**
	 * SingletonHolder
	 */
	static private final class SingletonHolder {
		static private final PacketDictionary INSTANCE = new PacketDictionary();
	}

	/**
	 * getInstance
	 * return instance of PacketDictionary
	 * @return
	 */
	static public PacketDictionary getInstance() {
		return PacketDictionary.SingletonHolder.INSTANCE;
	}

	/**
	 * class paths
	 */
	private final ArrayList<String> _classPaths = new ArrayList();

	/**
	 * packets
	 */
	private final Map<SocketType, HashMap<Integer, Class>> _packets = new EnumMap<>(SocketType.class);

	/**
	 * PacketDictionary
	 */
	private PacketDictionary() {
		// add default
		this._classPaths.add("com.fjestacore.packet");
	}

	/**
	 * addClassByPacketName
	 * @param head
	 * @param name
	 */
	public void addClassByPacketName(NcHeader head, String name) {
		for( String classPath: this._classPaths ) {
			String headPathname = head.name().toLowerCase();

			if( headPathname.contains("char") ) {
				headPathname += "s";
			}

			String classname = classPath + "." + headPathname + "." + name;

			try {
				Class cls = Class.forName(classname);

				this._addClassByAnnotation(cls, cls.getAnnotations());
			}
			catch( ClassNotFoundException ex ) {
				Logger.getLogger(PacketDictionary.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/**
	 * addClassByClassName
	 * @param classname
	 */
	public void addClassByClassName(String classname) {
		try {
			Class cls = Class.forName(classname);

			this._addClassByAnnotation(cls, cls.getAnnotations());
		}
		catch( ClassNotFoundException ex ) {
			Logger.getLogger(PacketDictionary.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * _addClassByAnnotation
	 * @param cls
	 * @param ans
	 */
	protected void _addClassByAnnotation(Class cls, Annotation[] ans) {
		for( Annotation an : ans ) {
			if( an instanceof PacketDefineServer ) {
				this.addClassByPDS(cls, (PacketDefineServer) an);
			}
			else if( an instanceof PacketDefineClient ) {
				this.addClassByPDC(cls, (PacketDefineClient) an);
			}
			else if( an instanceof PacketDefine ) {
				this.addClassByPD(cls, (PacketDefine) an);
			}
			else if( an instanceof PacketDefines ) {
				PacketDefines pds = (PacketDefines) an;

				for( PacketDefine tdp: pds.value() ) {
					this.addClassByPD(cls, tdp);
				}
			}
		}
	}

	/**
	 * addClassByPD
	 * add class by packet define
	 * @param byclass
	 * @param pd
	 */
	public void addClassByPD(Class byclass, PacketDefine pd) {
		this.addClass(byclass, pd.cmd(), pd.type());
	}

	/**
	 * addClassByPD
	 * add class by packet define
	 * @param byclass
	 * @param pds
	 */
	public void addClassByPDS(Class byclass, PacketDefineServer pds) {
		this.addClass(byclass, pds.nc().getValue(), SocketType.SERVER);
	}

	/**
	 * addClassByPDC
	 * @param byclass
	 * @param pdc
	 */
	public void addClassByPDC(Class byclass, PacketDefineClient pdc) {
		this.addClass(byclass, pdc.nc().getValue(), SocketType.CLIENT);
	}

	/**
	 * addClass
	 * @param aclass
	 * @param cmd
	 * @param type
	 */
	public void addClass(Class aclass, int cmd, SocketType type) {
		if( this._packets.containsKey(type) ) {
		}
		else {
			this._packets.put(type, new HashMap<>());
		}

		HashMap<Integer, Class> cmds = this._packets.get(type);

		if( cmds.containsKey(cmd) ) {
			Class doubleClass = cmds.get(cmd);

			Logger.getLogger(PacketDictionary.class.getName()).log(Level.SEVERE,
				"Packet double register! Cmd-Classname: {0} vs {1}",
				new Object[]{aclass.getName(), doubleClass.getName()});
		}

		cmds.put(cmd, aclass);
	}

	/**
	 * removeClass
	 * @param aclass
	 * @param cmd
	 * @param type
	 * @return
	 */
	public boolean removeClass(Class aclass, int cmd, SocketType type) {
		if( this._packets.containsKey(type) ) {
			HashMap<Integer, Class> cmds = this._packets.get(type);

			if( cmds.containsKey(cmd) ) {
				if( cmds.remove(cmd) != null ) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * _autoLoad
	 * @param cmd
	 * @param type
	 * @return
	 * @throws Exception
	 */
	protected Class _autoLoad(int cmd, SocketType type) throws Exception {
		NcParts parts = NcParser.parseCmd(cmd);

		if( parts != null ) {
			if( parts.isClient() ) {
				this.addClassByPacketName(parts.getHeader(), parts.getClient().name());
			}
			else if( parts.isServer() ) {
				this.addClassByPacketName(parts.getHeader(), parts.getServer().name());
			}
		}
		else {
			throw new Exception(
				"NcParser return null, Nc is not registert by cmd: " +
					Integer.toString(cmd) + " " + NcParser.dumpStr(cmd));
		}

		HashMap<Integer, Class> cmds = this._packets.get(type);

		if( cmds == null ) {
			throw new Exception(
				"Socket type is not load: " + type.name() +
				" by Header: " + parts.getHeader().name() +
				" Num: " + Integer.toString(parts.getNum()));
		}

		Class aClassCmd = cmds.get(cmd);

		return aClassCmd;
	}

	/**
	 * getClassByCmd
	 * is slow, use getClassByCmd with type
	 * @param cmd
	 * @return
	 * @throws java.lang.Exception
	 */
	public Class getClassByCmd(int cmd) throws Exception {
		for( SocketType type : this._packets.keySet() ) {
			return this.getClassByCmd(cmd, type);
		}

		return null;
	}

	/**
	 * getClassByCmd
	 * @param cmd
	 * @param type
	 * @return
	 * @throws java.lang.Exception
	 */
	public Class getClassByCmd(int cmd, SocketType type) throws Exception {
		HashMap<Integer, Class> cmds = this._packets.get(type);

		if( cmds == null ) {
			return this._autoLoad(cmd, type);
		}

		Class aClassCmd = cmds.get(cmd);

		if( aClassCmd != null ) {
			return aClassCmd;
		}

		return this._autoLoad(cmd, type);
	}
}
