/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import com.fjestacore.core.net.SocketType;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Packet
 * @author FantaBlueMystery
 */
public class Packet {

	/**
	 * type
	 */
	protected SocketType _type = SocketType.UNKNOWN;

	/**
	 * command
	 */
	protected int _command = 0;

	/**
	 * raw buffer
	 */
	protected byte[] _rawBuffer = null;

	/**
	 * buffer
	 */
	protected byte[] _buffer = null;

	/**
	 * size of packet by reading
	 */
	protected int _sizeReaded = 0;

	/**
	 * is buffer read
	 */
	protected boolean _isBufferRead = false;

	/**
	 * Packet
	 */
	public Packet() {
		Annotation[] ans = this.getClass().getAnnotations();

		// auto set command by PacketDefine
		for( Annotation an: ans ) {
			if( an instanceof PacketDefine ) {
				PacketDefine pd = (PacketDefine) an;

				this._command = pd.cmd();
				break;
			}
			else if( an instanceof PacketDefineServer ) {
				PacketDefineServer pds = (PacketDefineServer) an;

				this._command = pds.nc().getValue();
				break;
			}
			else if( an instanceof PacketDefineClient ) {
				PacketDefineClient pdc = (PacketDefineClient) an;

				this._command = pdc.nc().getValue();
				break;
			}
		}
	}

	/**
	 * setType
	 * @param type
	 */
	public void setType(SocketType type) {
		this._type = type;
	}

	/**
	 * getType
	 * @return
	 */
	public SocketType getType() {
		return this._type;
	}

	/**
	 * getType
	 * @return
	 */
	public SocketType getPacketType() {
		return this._type;
	}

	/**
	 * getCmd
	 * @return
	 */
	public int getCommand() {
		return this._command;
	}

	/**
	 * getBuffer
	 * @return
	 */
	public byte[] getBuffer() {
		return this._buffer;
	}

	/**
	 * getRawBuffer
	 * @return
	 */
	public byte[] getRawBuffer() {
		return this._rawBuffer;
	}

	/**
	 * getReader
	 * @return
	 */
	protected ReaderStream _getReader() {
		return new ReaderStream(this._buffer);
	}

	/**
	 * getReadSize
	 * @return
	 */
	public int getReadSize() {
		return this._sizeReaded;
	}

	/**
	 * isBufferRead
	 * @return
	 */
	public boolean isBufferRead() {
		return this._isBufferRead;
	}

	/**
	 * onReadBuffer
	 */
	public void onReadBuffer() {
		this._onReadBuffer();

		this._isBufferRead = true;
	}

	/**
	 * _onReadBuffer
	 * overwrite this methode
	 */
	protected void _onReadBuffer() {
		if( this instanceof IPacket ) {
			AbstractStruct po = ((IPacket)this).getPacketObject();

			try {
				po.read(this._buffer);
			}
			catch( Exception ex ) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/**
	 * _onWriteBuffer
	 * @throws java.io.IOException
	 */
	protected void _onWriteBuffer() throws IOException {
		if( this instanceof IPacket ) {
			AbstractStruct po = ((IPacket)this).getPacketObject();

			this._buffer = po.write();
		}
	}

	/**
	 * cast
	 * @return
	 */
	public Packet cast() {
		Class pclass;

		try {
			if( this._type != SocketType.UNKNOWN ) {
				pclass = PacketDictionary.getInstance().getClassByCmd(this._command, this._type);
			}
			else {
				pclass = PacketDictionary.getInstance().getClassByCmd(this._command);
			}

			if( pclass != null ) {
				return this.cast(pclass);
			}
		}
		catch( Exception ex ) {
			Logger.getLogger(Packet.class.getName()).log(Level.SEVERE, null, ex);
		}


		return null;
	}

	/**
	 *
	 * @param aclass
	 * @return
	 */
	public Packet cast(Class aclass) {
		try {
			Packet npac = (Packet) aclass.newInstance();

			npac._type			= this._type;
			npac._command		= this._command;
			npac._sizeReaded	= this._sizeReaded;
			npac._buffer		= this._buffer;
			npac._rawBuffer		= this._rawBuffer;

			return npac;
		}
		catch( InstantiationException | IllegalAccessException ex ) {
			Logger.getLogger(Packet.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}
}