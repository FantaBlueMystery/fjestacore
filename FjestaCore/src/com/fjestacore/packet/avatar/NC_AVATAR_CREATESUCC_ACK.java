/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.avatar;

import com.fjestacore.common.avatar.AvatarCreateSucc;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_AVATAR_CREATESUCC_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_AVATAR_CREATESUCC_ACK)
public class NC_AVATAR_CREATESUCC_ACK extends Packet implements IPacket {

	/**
	 * avatar create succ
	 */
	protected AvatarCreateSucc _acs = new AvatarCreateSucc();

	/**
	 * NC_AVATAR_CREATESUCC_ACK
	 */
	public NC_AVATAR_CREATESUCC_ACK() {
		super();
	}

	/**
	 * NC_AVATAR_CREATESUCC_ACK
	 * @param succ
	 */
	public NC_AVATAR_CREATESUCC_ACK(AvatarCreateSucc succ) {
		super();
		
		this._acs = succ;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._acs;
	}
}