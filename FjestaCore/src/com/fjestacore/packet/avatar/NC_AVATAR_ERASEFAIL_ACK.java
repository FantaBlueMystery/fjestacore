/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.avatar;

import com.fjestacore.common.avatar.AvatarEraseFail;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_AVATAR_ERASEFAIL_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_AVATAR_ERASEFAIL_ACK)
public class NC_AVATAR_ERASEFAIL_ACK extends Packet implements IPacket {

	/**
	 * avatar erase fail
	 */
	protected AvatarEraseFail _aef = new AvatarEraseFail();

	/**
	 * NC_AVATAR_ERASEFAIL_ACK
	 */
	public NC_AVATAR_ERASEFAIL_ACK() {
		super();
	}

	/**
	 * NC_AVATAR_ERASEFAIL_ACK
	 * @param fail
	 */
	public NC_AVATAR_ERASEFAIL_ACK(AvatarEraseFail fail) {
		super();

		this._aef = fail;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._aef;
	}
}