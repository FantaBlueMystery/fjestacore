/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.avatar;

import com.fjestacore.common.avatar.AvatarCreateFail;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_AVATAR_CREATEFAIL_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_AVATAR_CREATEFAIL_ACK)
public class NC_AVATAR_CREATEFAIL_ACK extends Packet implements IPacket {

	/**
	 * avatar create fail
	 */
	protected AvatarCreateFail _acf = new AvatarCreateFail();

	/**
	 * NC_AVATAR_CREATEFAIL_ACK
	 */
	public NC_AVATAR_CREATEFAIL_ACK() {
		super();
	}

	/**
	 * NC_AVATAR_CREATEFAIL_ACK
	 * @param acf
	 */
	public NC_AVATAR_CREATEFAIL_ACK(AvatarCreateFail acf) {
		super();
		this._acf = acf;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._acf;
	}
}