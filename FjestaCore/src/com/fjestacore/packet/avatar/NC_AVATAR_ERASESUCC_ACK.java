/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.avatar;

import com.fjestacore.common.avatar.AvatarEraseSucc;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_AVATAR_ERASESUCC_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_AVATAR_ERASESUCC_ACK)
public class NC_AVATAR_ERASESUCC_ACK extends Packet implements IPacket {

	/**
	 * avatar erase succ
	 */
	protected AvatarEraseSucc _aes = new AvatarEraseSucc();

	/**
	 * NC_AVATAR_ERASESUCC_ACK
	 */
	public NC_AVATAR_ERASESUCC_ACK() {
		super();
	}

	/**
	 * NC_AVATAR_ERASESUCC_ACK
	 * @param succ
	 */
	public NC_AVATAR_ERASESUCC_ACK(AvatarEraseSucc succ) {
		super();
		
		this._aes = succ;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._aes;
	}
}