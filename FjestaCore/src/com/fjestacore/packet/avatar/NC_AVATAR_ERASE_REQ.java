/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.avatar;

import com.fjestacore.common.avatar.AvatarErase;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_AVATAR_ERASE_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_AVATAR_ERASE_REQ)
public class NC_AVATAR_ERASE_REQ extends Packet implements IPacket {

	/**
	 * avatar erase
	 */
	protected AvatarErase _ae = new AvatarErase();

	/**
	 * getAvatarErase
	 * @return
	 */
	public AvatarErase getAvatarErase() {
		return this._ae;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ae;
	}
}