/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.avatar;

import com.fjestacore.common.avatar.AvatarCreate;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_AVATAR_CREATE_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_AVATAR_CREATE_REQ)
public class NC_AVATAR_CREATE_REQ extends Packet implements IPacket {

	/**
	 * avatar create
	 */
	protected AvatarCreate _ac = new AvatarCreate();

	/**
	 * getCreate
	 * @return
	 */
	public AvatarCreate getCreate() {
		return this._ac;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ac;
	}
}