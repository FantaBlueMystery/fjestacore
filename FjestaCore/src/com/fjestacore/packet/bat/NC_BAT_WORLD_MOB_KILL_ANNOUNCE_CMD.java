/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.bat;

import com.fjestacore.common.mob.MobKillAnnounce;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BAT_WORLD_MOB_KILL_ANNOUNCE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BAT_WORLD_MOB_KILL_ANNOUNCE_CMD)
public class NC_BAT_WORLD_MOB_KILL_ANNOUNCE_CMD extends Packet implements IPacket {

	/**
	 * mob kill announce
	 */
	protected MobKillAnnounce _mka = new MobKillAnnounce();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._mka;
	}
}