/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.bat;

import com.fjestacore.common.bat.HpChange;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BAT_HPCHANGE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BAT_HPCHANGE_CMD)
public class NC_BAT_HPCHANGE_CMD extends Packet implements IPacket {

	/**
	 * hp change
	 */
	protected HpChange _hpc = new HpChange();

	/**
	 * NC_BAT_HPCHANGE_CMD
	 */
	public NC_BAT_HPCHANGE_CMD() {
		super();
	}

	/**
	 * NC_BAT_HPCHANGE_CMD
	 * @param hpc
	 */
	public NC_BAT_HPCHANGE_CMD(HpChange hpc) {
		super();

		this._hpc = hpc;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._hpc;
	}
}