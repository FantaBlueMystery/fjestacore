/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.bat;

import com.fjestacore.common.abstate.AbstateReset;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BAT_ABSTATERESET_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BAT_ABSTATERESET_CMD)
public class NC_BAT_ABSTATERESET_CMD extends Packet implements IPacket {

	/**
	 * abstate reset
	 */
	protected AbstateReset _ar = new AbstateReset();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ar;
	}
}