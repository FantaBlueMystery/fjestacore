/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.bat;

import com.fjestacore.common.bat.Targetting;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_BAT_TARGETTING_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_BAT_TARGETTING_REQ)
public class NC_BAT_TARGETTING_REQ extends Packet implements IPacket {

	/**
	 * targetting
	 */
	protected Targetting _t = new Targetting();

	/**
	 * getTargetting
	 * @return
	 */
	public Targetting getTargetting() {
		return this._t;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._t;
	}
}