/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.bat;

import com.fjestacore.common.abstate.AbstateSet;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BAT_ABSTATESET_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BAT_ABSTATESET_CMD)
public class NC_BAT_ABSTATESET_CMD extends Packet implements IPacket {

	/**
	 * abstate set
	 */
	protected AbstateSet _as = new AbstateSet();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._as;
	}
}