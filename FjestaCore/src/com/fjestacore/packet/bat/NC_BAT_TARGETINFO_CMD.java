/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.bat;

import com.fjestacore.common.bat.TargetInfo;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BAT_TARGETINFO_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BAT_TARGETINFO_CMD)
public class NC_BAT_TARGETINFO_CMD extends Packet implements IPacket {

	/**
	 * target info
	 */
	protected TargetInfo _ti = new TargetInfo();

	/**
	 * NC_BAT_TARGETINFO_CMD
	 */
	public NC_BAT_TARGETINFO_CMD() {
		super();
	}

	/**
	 * NC_BAT_TARGETINFO_CMD
	 * @param ti
	 */
	public NC_BAT_TARGETINFO_CMD(TargetInfo ti) {
		super();

		this._ti = ti;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ti;
	}
}