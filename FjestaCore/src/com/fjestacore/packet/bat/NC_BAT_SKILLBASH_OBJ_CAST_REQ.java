/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.bat;

import com.fjestacore.common.bat.SkillbashObjCast;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_BAT_SKILLBASH_OBJ_CAST_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_BAT_SKILLBASH_OBJ_CAST_REQ)
public class NC_BAT_SKILLBASH_OBJ_CAST_REQ extends Packet implements IPacket {

	/**
	 * skillbash obj cast
	 */
	protected SkillbashObjCast _soc = new SkillbashObjCast();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._soc;
	}
}