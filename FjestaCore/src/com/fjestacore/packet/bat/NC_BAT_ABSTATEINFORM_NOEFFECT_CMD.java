/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.bat;

import com.fjestacore.common.abstate.AbstateInformNoEffect;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BAT_ABSTATEINFORM_NOEFFECT_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BAT_ABSTATEINFORM_NOEFFECT_CMD)
public class NC_BAT_ABSTATEINFORM_NOEFFECT_CMD extends Packet implements IPacket {

	/**
	 * abstate informe no effect
	 */
	protected AbstateInformNoEffect _aine = new AbstateInformNoEffect();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._aine;
	}
}