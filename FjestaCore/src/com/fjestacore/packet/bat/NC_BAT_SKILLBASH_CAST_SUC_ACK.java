/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.bat;

import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BAT_SKILLBASH_CAST_SUC_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BAT_SKILLBASH_CAST_SUC_ACK)
public class NC_BAT_SKILLBASH_CAST_SUC_ACK extends Packet {}