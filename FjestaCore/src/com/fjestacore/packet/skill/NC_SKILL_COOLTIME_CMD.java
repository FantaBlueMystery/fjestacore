/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.skill;

import com.fjestacore.common.skill.SkillCoolTime;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_SKILL_COOLTIME_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_SKILL_COOLTIME_CMD)
public class NC_SKILL_COOLTIME_CMD extends Packet implements IPacket {

	/**
	 * skill cool time
	 */
	protected SkillCoolTime _sct = new SkillCoolTime();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._sct;
	}
}