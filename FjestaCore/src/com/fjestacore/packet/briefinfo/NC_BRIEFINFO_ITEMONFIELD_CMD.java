/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.briefinfo;

import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.common.briefinfo.ItemOnField;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_ITEMONFIELD_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_ITEMONFIELD_CMD)
public class NC_BRIEFINFO_ITEMONFIELD_CMD extends Packet implements IPacket {

	/**
	 * item on field
	 */
	protected ItemOnField _iof = new ItemOnField();

	/**
	 * NC_BRIEFINFO_ITEMONFIELD_CMD
	 */
	public NC_BRIEFINFO_ITEMONFIELD_CMD() {
		super();
	}

	/**
	 * NC_BRIEFINFO_ITEMONFIELD_CMD
	 * @param iof
	 */
	public NC_BRIEFINFO_ITEMONFIELD_CMD(ItemOnField iof) {
		super();

		this._iof = iof;
	}

	/**
	 * getItemOnField
	 * @return
	 */
	public ItemOnField getItemOnField() {
		return this._iof;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._iof;
	}
}