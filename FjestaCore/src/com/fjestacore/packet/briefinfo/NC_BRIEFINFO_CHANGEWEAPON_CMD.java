/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.briefinfo.ChangeWeapon;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_CHANGEWEAPON_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_CHANGEWEAPON_CMD)
public class NC_BRIEFINFO_CHANGEWEAPON_CMD extends Packet implements IPacket {

	/**
	 * change weapon
	 */
	protected ChangeWeapon _cw = new ChangeWeapon();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cw;
	}
}
