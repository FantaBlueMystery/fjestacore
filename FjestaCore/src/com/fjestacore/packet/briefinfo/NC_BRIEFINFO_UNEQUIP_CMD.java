/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.briefinfo.Unequip;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_UNEQUIP_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_UNEQUIP_CMD)
public class NC_BRIEFINFO_UNEQUIP_CMD extends Packet implements IPacket {

	/**
	 * unequip
	 */
	protected Unequip _uq = new Unequip();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._uq;
	}
}