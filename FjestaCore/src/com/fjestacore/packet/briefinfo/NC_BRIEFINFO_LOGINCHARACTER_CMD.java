/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.briefinfo.LoginCharacter;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_LOGINCHARACTER_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_LOGINCHARACTER_CMD)
public class NC_BRIEFINFO_LOGINCHARACTER_CMD extends Packet implements IPacket {

	/**
	 * login character
	 */
	protected LoginCharacter _lc = new LoginCharacter();

	/**
	 * NC_BRIEFINFO_LOGINCHARACTER_CMD
	 */
	public NC_BRIEFINFO_LOGINCHARACTER_CMD() {
		super();
	}

	/**
	 * NC_BRIEFINFO_LOGINCHARACTER_CMD
	 * @param lc
	 */
	public NC_BRIEFINFO_LOGINCHARACTER_CMD(LoginCharacter lc) {
		super();

		this._lc = lc;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._lc;
	}
}