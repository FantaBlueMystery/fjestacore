/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.briefinfo.BriefInfoInform;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_BRIEFINFO_INFORM_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_BRIEFINFO_INFORM_CMD)
public class NC_BRIEFINFO_INFORM_CMD extends Packet implements IPacket {

	/**
	 * brief info inform
	 */
	protected BriefInfoInform _bi = new BriefInfoInform();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._bi;
	}
}
