/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.briefinfo.BriefInfoCharacter;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_CHARACTER_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_CHARACTER_CMD)
public class NC_BRIEFINFO_CHARACTER_CMD extends Packet implements IPacket {

	/**
	 * brief info character
	 */
	protected BriefInfoCharacter _bc = new BriefInfoCharacter();

	/**
	 * NC_BRIEFINFO_CHARACTER_CMD
	 */
	public NC_BRIEFINFO_CHARACTER_CMD() {
		super();
	}

	/**
	 * NC_BRIEFINFO_CHARACTER_CMD
	 * @param bc
	 */
	public NC_BRIEFINFO_CHARACTER_CMD(BriefInfoCharacter bc) {
		super();

		this._bc = bc;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._bc;
	}
}