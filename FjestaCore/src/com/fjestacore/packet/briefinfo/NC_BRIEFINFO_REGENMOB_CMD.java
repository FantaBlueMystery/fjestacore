/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.mob.Mob;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_REGENMOB_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_REGENMOB_CMD)
public class NC_BRIEFINFO_REGENMOB_CMD extends Packet implements IPacket {

	/**
	 * mob
	 */
	protected Mob _mob = new Mob();

	/**
	 * NC_BRIEFINFO_REGENMOB_CMD
	 */
	public NC_BRIEFINFO_REGENMOB_CMD() {
		super();
	}

	/**
	 * NC_BRIEFINFO_REGENMOB_CMD
	 * @param mob
	 */
	public NC_BRIEFINFO_REGENMOB_CMD(Mob mob) {
		super();

		this._mob = mob;
	}

	/**
	 * getMob
	 * @return
	 */
	public Mob getMob() {
		return this._mob;
	}

	/**
	 * setMob
	 * @param amob
	 */
	public void setMob(Mob amob) {
		this._mob = amob;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._mob;
	}
}