/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.mob.MobList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_MOB_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_MOB_CMD)
public class NC_BRIEFINFO_MOB_CMD extends Packet implements IPacket {

	/**
	 * mob list
	 */
	protected MobList _ml = new MobList();

	/**
	 * NC_BRIEFINFO_MOB_CMD
	 */
	public NC_BRIEFINFO_MOB_CMD() {
		super();
	}

	/**
	 * NC_BRIEFINFO_MOB_CMD
	 * @param ml
	 */
	public NC_BRIEFINFO_MOB_CMD(MobList ml) {
		super();

		this._ml = ml;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ml;
	}
}