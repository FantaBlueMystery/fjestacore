/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.abstate.AbstateChangeList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_ABSTATE_CHANGE_LIST_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_ABSTATE_CHANGE_LIST_CMD)
public class NC_BRIEFINFO_ABSTATE_CHANGE_LIST_CMD extends Packet implements IPacket {

	/**
	 * abstate change list
	 */
	protected AbstateChangeList _acl = new AbstateChangeList();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._acl;
	}
}