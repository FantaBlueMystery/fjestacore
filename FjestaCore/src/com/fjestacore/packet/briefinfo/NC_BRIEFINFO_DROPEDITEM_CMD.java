/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.briefinfo.DropedItem;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_DROPEDITEM_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_DROPEDITEM_CMD)
public class NC_BRIEFINFO_DROPEDITEM_CMD extends Packet implements IPacket {

	/**
	 * droped item
	 */
	protected DropedItem _di = new DropedItem();

	/**
	 * NC_BRIEFINFO_DROPEDITEM_CMD
	 */
	public NC_BRIEFINFO_DROPEDITEM_CMD() {
		super();
	}

	/**
	 * NC_BRIEFINFO_DROPEDITEM_CMD
	 * @param di
	 */
	public NC_BRIEFINFO_DROPEDITEM_CMD(DropedItem di) {
		super();
		
		this._di = di;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._di;
	}
}