/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.briefinfo;

import com.fjestacore.common.briefinfo.BriefInfoDelete;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BRIEFINFO_BRIEFINFODELETE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BRIEFINFO_BRIEFINFODELETE_CMD)
public class NC_BRIEFINFO_BRIEFINFODELETE_CMD extends Packet implements IPacket {

	/**
	 * brief info delete
	 */
	protected BriefInfoDelete _bid = new BriefInfoDelete();

	/**
	 * NC_BRIEFINFO_BRIEFINFODELETE_CMD
	 */
	public NC_BRIEFINFO_BRIEFINFODELETE_CMD() {
		super();
	}

	/**
	 * NC_BRIEFINFO_BRIEFINFODELETE_CMD
	 * @param bd
	 */
	public NC_BRIEFINFO_BRIEFINFODELETE_CMD(BriefInfoDelete bd) {
		super();

		this._bid = bd;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._bid;
	}
}