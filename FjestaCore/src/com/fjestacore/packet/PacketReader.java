/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import com.fjestacore.core.convert.ConvertBytes;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.crypt.INetCrypt;
import com.fjestacore.core.net.SocketType;
import com.fjestacore.core.stream.SocketInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PacketReader
 * @author FantaBlueMystery
 */
public class PacketReader {

	/**
	 * readPacketSize
	 * @param si
	 * @return
	 */
	static public int readPacketSize(InputStream si) {
		return PacketReader.readPacketSize(si, false);
	}

	/**
	 * readPacketSize
	 * @param si
	 * @param withSize
	 * @return
	 */
	static public int readPacketSize(InputStream si, boolean withSize) {
		byte[] sizeBuffer = new byte[1];

		int _read;

		try {
			if( (_read = si.read(sizeBuffer)) != -1 ) {
				if( sizeBuffer[0] == 0x00 ) {
					sizeBuffer = new byte[2];

					if( (_read = si.read(sizeBuffer)) == -1 ) {
						return -1;
					}
				}
			}
			else {
				return -1;
			}
		}
		catch( IOException ex ) {
			Logger.getLogger(PacketReader.class.getName()).log(Level.SEVERE, "readPacketSize: " + ex.getMessage(), ex);

			return -1;
		}

		int size = 0;

		if( sizeBuffer.length > 1 ) {
			size = ConvertBytes.bytesToUnsignedShort(sizeBuffer, false);

			if( withSize ) {
				size += 3;
			}
		}
		else {
			size = sizeBuffer[0] & 0xFF;

			if( withSize ) {
				size += 1;
			}
		}

		return size;
	}

	/**
	 * _readPacketBuffer
	 * @param si
	 * @param size
	 * @return
	 */
	static protected byte[] _readPacketBuffer(InputStream si, int size) {
		if( size <= 0 ) {
			return null;
		}

		byte[] buffer = new byte[size];

		try {
			if( si instanceof SocketInputStream ) {
				DataInputStream din = new DataInputStream(si);
				din.readFully(buffer);
			}
			else {
				int _read;

				if( (_read = si.read(buffer)) == -1 ) {
					return null;
				}
			}

			return buffer;
		}
		catch( IOException ex ) {
			Logger.getLogger(PacketReader.class.getName()).log(Level.SEVERE, "_readPacketBuffer: " + ex.getMessage(), ex);

			return null;
		}
	}

	/**
	 * readPacket
	 * @param is
	 * @return
	 */
	static public Packet readPacket(InputStream is) {
		return PacketReader.readPacket(is, null, SocketType.UNKNOWN, true);
	}

	/**
	 * readPacket
	 * @param is
	 * @param type
	 * @return
	 */
	static public Packet readPacket(InputStream is, SocketType type) {
		return PacketReader.readPacket(is, null, type, true);
	}

	/**
	 * readPacket
	 * @param is
	 * @param crypt
	 * @return
	 */
	static public Packet readPacket(InputStream is, INetCrypt crypt) {
		return PacketReader.readPacket(is, crypt, SocketType.UNKNOWN, true);
	}

	/**
	 * readPacket
	 * @param is
	 * @return
	 */
	static public Packet readPacketN(InputStream is) {
		return PacketReader.readPacket(is, null, SocketType.UNKNOWN, false);
	}

	/**
	 * readPacket
	 * @param is
	 * @param type
	 * @return
	 */
	static public Packet readPacketN(InputStream is, SocketType type) {
		return PacketReader.readPacket(is, null, type, false);
	}

	/**
	 * readPacket
	 * @param is
	 * @param crypt
	 * @return
	 */
	static public Packet readPacketN(InputStream is, INetCrypt crypt) {
		return PacketReader.readPacket(is, crypt, SocketType.UNKNOWN, false);
	}

	/**
	 * readPacket
	 * @param is
	 * @param crypt
	 * @param by
	 * @param autoBufferRead
	 * @return
	 */
	static public Packet readPacket(InputStream is, INetCrypt crypt, SocketType by, boolean autoBufferRead) {
		try {
			int size = PacketReader.readPacketSize(is);

			if( size > 0 ) {
				byte[] buffer = PacketReader._readPacketBuffer(is, size);

				int bufferSize = 0;

				if( buffer != null ) {
					bufferSize = buffer.length;
				}
				else {
					return null;
				}

				if( (bufferSize < 2) ) {
					if( (bufferSize == 1) && (buffer[0] == 0x00) ) {
						// is padding by ip header
						Logger.getLogger(PacketReader.class.getName()).log(Level.SEVERE, "is padding by ip header = empty (0x00)");
						return null;
					}
				}

				// -------------------------------------------------------------

				Packet pac = null;

				if( bufferSize < size ) {
					pac = new PacketUnReady(buffer, size);
					pac.setType(by);
				}
				else {
					pac = new Packet();
					pac.setType(by);

					pac._rawBuffer	= buffer;

					if( crypt != null ) {
						buffer = crypt.crypt(buffer);
					}

					byte[] cmd			= ConvertBytes.copyBytes(buffer, 0, 2);
					byte[] subbuffer	= ConvertBytes.copyBytes(buffer, 2, size-2);

					pac._sizeReaded	= size;
					pac._command	= ConvertBytes.bytesToUnsignedShort(cmd, false);
					pac._buffer		= subbuffer;

					// cast packet
					Packet cpac = pac.cast();

					if( cpac != null ) {
						pac = cpac;

						if( autoBufferRead ) {
							pac._onReadBuffer();
						}
					}
				}

				return pac;
			}
		}
		catch( Exception ex ) {
			Logger.getLogger(PacketReader.class.getName()).log(Level.SEVERE, "readPacket: " + ex.getMessage(), ex);
		}

		return null;
	}

	/**
	 * readUnreadyPacket
	 * @param upacket
	 * @param is
	 * @param crypt
	 * @param by
	 * @return
	 */
	static public Packet readUnreadyPacket(PacketUnReady upacket, InputStream is, INetCrypt crypt, SocketType by) {
		try {
			int size = upacket.getDifferenceSize();

			if( size > 0 ) {
				byte[] buffer = PacketReader._readPacketBuffer(is, size);

				upacket.appendBytes(buffer);

				if( upacket.isReady() ) {
					buffer = upacket.getBuffer();
				}
				else {
					// TODO Error
					Logger.getLogger(PacketReader.class.getName()).log(Level.SEVERE, "upacket.isReady != true");
				}

				// -------------------------------------------------------------

				if( crypt != null ) {
					buffer = crypt.crypt(buffer);
				}

				byte[] cmd			= ConvertBytes.copyBytes(buffer, 0, 2);
				byte[] subbuffer	= ConvertBytes.copyBytes(buffer, 2, size-2);

				Packet pac = new Packet();
				pac.setType(by);

				pac._sizeReaded	= size;
				pac._command	= ConvertBytes.bytesToUnsignedShort(cmd, false);
				pac._buffer		= subbuffer;

				// cast packet
				Packet cpac = pac.cast();

				if( cpac != null ) {
					pac = cpac;
					pac._onReadBuffer();
				}

				return pac;
			}
		}
		catch( Exception ex ) {
			Logger.getLogger(PacketReader.class.getName()).log(Level.SEVERE, "readUnreadyPacket: " + ex.getMessage(), ex);
		}

		return null;
	}

	/**
	 * readByHexString
	 * @param hexstr
	 * @param crypt
	 * @param type
	 * @return
	 */
	static public Packet[] readByHexString(String hexstr, INetCrypt crypt, SocketType type) {
		byte[] strhex = HexString.hexStringToByteArray(hexstr);
		InputStream astream = new ByteArrayInputStream(strhex);

		ArrayList<Packet> pacs = new ArrayList();

		pacs.add(PacketReader.readPacket(astream, crypt, type, true));

		try {
			int available = astream.available();

			while( available > 0 ) {
				pacs.add(PacketReader.readPacket(astream, crypt, type, true));

				available = astream.available();
			}
		}
		catch( IOException ex ) {
			Logger.getLogger(PacketReader.class.getName()).log(Level.SEVERE, null, ex);
		}

		return pacs.toArray(new Packet[]{});
	}
}