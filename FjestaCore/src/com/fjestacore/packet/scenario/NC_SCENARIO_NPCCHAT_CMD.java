/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.scenario;

import com.fjestacore.common.scenario.ScenarioNPCChat;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_SCENARIO_NPCCHAT_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_SCENARIO_NPCCHAT_CMD)
public class NC_SCENARIO_NPCCHAT_CMD extends Packet implements IPacket {

	/**
	 * scenario NPC chat
	 */
	protected ScenarioNPCChat _npcc = new ScenarioNPCChat();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._npcc;
	}
}