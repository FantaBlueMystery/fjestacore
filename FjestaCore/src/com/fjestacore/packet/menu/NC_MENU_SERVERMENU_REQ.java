/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.menu;

import com.fjestacore.common.servermenu.ServerMenu;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MENU_SERVERMENU_REQ
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MENU_SERVERMENU_REQ)
public class NC_MENU_SERVERMENU_REQ extends Packet implements IPacket {

	/**
	 * req
	 */
	protected ServerMenu _req = new ServerMenu();

	/**
	 * NC_MENU_SERVERMENU_REQ
	 */
	public NC_MENU_SERVERMENU_REQ() {
		super();
	}

	/**
	 * NC_MENU_SERVERMENU_REQ
	 * @param req
	 */
	public NC_MENU_SERVERMENU_REQ(ServerMenu req) {
		super();

		this._req = req;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._req;
	}
}