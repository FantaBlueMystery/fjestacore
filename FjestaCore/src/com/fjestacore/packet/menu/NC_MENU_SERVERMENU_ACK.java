/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.menu;

import com.fjestacore.common.servermenu.ServerMenuAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_MENU_SERVERMENU_ACK
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_MENU_SERVERMENU_ACK)
public class NC_MENU_SERVERMENU_ACK extends Packet implements IPacket {

	/**
	 * server menu ack
	 */
	protected ServerMenuAck _smc = new ServerMenuAck();

	/**
	 * getServerMenu
	 * @return
	 */
	public ServerMenuAck getServerMenu() {
		return this._smc;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._smc;
	}
}