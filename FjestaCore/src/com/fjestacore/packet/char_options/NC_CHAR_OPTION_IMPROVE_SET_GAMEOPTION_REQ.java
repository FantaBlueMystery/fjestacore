/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.char_options;

import com.fjestacore.common.gamesetting.GameSettingList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_CHAR_OPTION_IMPROVE_SET_GAMEOPTION_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_CHAR_OPTION_IMPROVE_SET_GAMEOPTION_REQ)
public class NC_CHAR_OPTION_IMPROVE_SET_GAMEOPTION_REQ extends Packet implements IPacket {

	/**
	 * game setting list
	 */
	protected GameSettingList _list = new GameSettingList();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._list;
	}
}