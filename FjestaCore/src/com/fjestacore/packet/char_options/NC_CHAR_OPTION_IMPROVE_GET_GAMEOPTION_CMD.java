/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.char_options;

import com.fjestacore.common.gamesetting.GameSettingList;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD)
public class NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD extends Packet implements IPacket {

	/**
	 * game setting list
	 */
	protected GameSettingList _list = null;

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD() {
		super();

		this._list = new GameSettingList();
	}

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD
	 * @param list
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD(GameSettingList list) {
		super();

		this._list = list;
	}

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD
	 * @param hexbuffer
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_GAMEOPTION_CMD(String hexbuffer) {
		super();

		this._list = new GameSettingList();

		this.setBufferByHexString(hexbuffer);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this.onReadBuffer();
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._list;
	}
}
