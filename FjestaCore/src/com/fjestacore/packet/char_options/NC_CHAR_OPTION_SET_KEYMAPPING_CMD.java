/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.char_options;

import com.fjestacore.common.keymap.KeyMapList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_CHAR_OPTION_SET_KEYMAPPING_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_CHAR_OPTION_SET_KEYMAPPING_CMD)
public class NC_CHAR_OPTION_SET_KEYMAPPING_CMD extends Packet implements IPacket {

	/**
	 * key map list
	 */
	protected KeyMapList _list = new KeyMapList();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._list;
	}
}