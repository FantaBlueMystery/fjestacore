/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.char_options;

import com.fjestacore.common.shortcut.ShortCutSize;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_OPTION_GET_SHORTCUTSIZE_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_OPTION_GET_SHORTCUTSIZE_ACK)
public class NC_CHAR_OPTION_GET_SHORTCUTSIZE_ACK extends Packet implements IPacket {

	/**
	 * short cut size
	 */
	protected ShortCutSize _scs = new ShortCutSize();

	/**
	 * NC_CHAR_OPTION_GET_SHORTCUTSIZE_ACK
	 */
	public NC_CHAR_OPTION_GET_SHORTCUTSIZE_ACK() {
		super();
	}

	/**
	 * getShortCutSize
	 * @return
	 */
	public ShortCutSize getShortCutSize() {
		return this._scs;
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._scs;
	}
}