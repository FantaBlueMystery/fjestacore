/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.char_options;

import com.fjestacore.common.shortcut.ShortCutList;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD)
public class NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD extends Packet implements IPacket {

	/**
	 * List
	 */
	protected ShortCutList _list = null;

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD() {
		super();

		this._list = new ShortCutList();
	}

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD
	 * @param list
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD(ShortCutList list) {
		super();

		this._list = list;
	}

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD
	 * @param hexstring
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_SHORTCUTDATA_CMD(String hexstring) {
		super();

		this._list = new ShortCutList();
		
		this.setBufferByHexString(hexstring);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getList
	 * @return
	 */
	public ShortCutList getList() {
		return this._list;
	}

	/**
	 * setList
	 * @param list
	 */
	public void setList(ShortCutList list) {
		this._list = list;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._list;
	}
}