/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.char_options;

import com.fjestacore.common.keymap.KeyMapList;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD)
public class NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD extends Packet implements IPacket {

	/**
	 * key map list
	 */
	protected KeyMapList _list = null;

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD() {
		super();

		this._list = new KeyMapList();
	}

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD
	 * @param list
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD(KeyMapList list) {
		super();

		this._list = list;
	}

	/**
	 * NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD
	 * @param hexstring
	 */
	public NC_CHAR_OPTION_IMPROVE_GET_KEYMAP_CMD(String hexstring) {
		super();

		this._list = new KeyMapList();

		this.setBufferByHexString(hexstring);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._list;
	}
}