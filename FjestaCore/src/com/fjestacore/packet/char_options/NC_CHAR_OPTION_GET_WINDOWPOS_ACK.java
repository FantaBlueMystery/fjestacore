/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.char_options;

import com.fjestacore.common.winpos.WinPos;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_OPTION_GET_WINDOWPOS_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_OPTION_GET_WINDOWPOS_ACK)
public class NC_CHAR_OPTION_GET_WINDOWPOS_ACK extends Packet implements IPacket {

	/**
	 * win pos
	 */
	protected WinPos _wp = new WinPos();

	/**
	 * NC_CHAR_OPTION_GET_WINDOWPOS_ACK
	 */
	public NC_CHAR_OPTION_GET_WINDOWPOS_ACK() {
		super();
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._wp;
	}
}