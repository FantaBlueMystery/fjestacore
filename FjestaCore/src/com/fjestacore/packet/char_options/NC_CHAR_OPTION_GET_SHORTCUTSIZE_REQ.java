/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.char_options;

import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ)
public class NC_CHAR_OPTION_GET_SHORTCUTSIZE_REQ extends Packet {}