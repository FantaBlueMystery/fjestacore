/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.friend;

import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_FRIEND_POINT_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_FRIEND_POINT_REQ)
public class NC_FRIEND_POINT_REQ extends Packet {}