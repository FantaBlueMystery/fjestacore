/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.friend;

import com.fjestacore.common.friend.FriendSetConfirmAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_FRIEND_SET_CONFIRM_ACK
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_FRIEND_SET_CONFIRM_ACK)
public class NC_FRIEND_SET_CONFIRM_ACK extends Packet implements IPacket {

	/**
	 * friend set confirm ack
	 */
	protected FriendSetConfirmAck _fsca = new FriendSetConfirmAck();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._fsca;
	}
}