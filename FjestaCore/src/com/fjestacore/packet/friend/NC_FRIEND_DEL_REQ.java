/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.friend;

import com.fjestacore.common.friend.FriendDelReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_FRIEND_DEL_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_FRIEND_DEL_REQ)
public class NC_FRIEND_DEL_REQ extends Packet implements IPacket {

	/**
	 * friend del req
	 */
	protected FriendDelReq _fdr = new FriendDelReq();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._fdr;
	}
}