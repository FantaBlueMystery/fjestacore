/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.friend;

import com.fjestacore.common.friend.FriendLogin;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_FRIEND_LOGIN_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_FRIEND_LOGIN_CMD)
public class NC_FRIEND_LOGIN_CMD extends Packet implements IPacket {

	/**
	 * fl
	 */
	protected FriendLogin _fl = new FriendLogin();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._fl;
	}
}
