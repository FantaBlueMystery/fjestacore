/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.friend;

import com.fjestacore.common.friend.FriendPoint;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_FRIEND_POINT_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_FRIEND_POINT_ACK)
public class NC_FRIEND_POINT_ACK extends Packet implements IPacket {

	/**
	 * friend point
	 */
	protected FriendPoint _fp = new FriendPoint();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._fp;
	}
}
