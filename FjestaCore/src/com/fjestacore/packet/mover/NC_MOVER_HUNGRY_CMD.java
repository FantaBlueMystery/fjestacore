/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.mover;

import com.fjestacore.common.mover.MoverHunger;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MOVER_HUNGRY_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MOVER_HUNGRY_CMD)
public class NC_MOVER_HUNGRY_CMD extends Packet implements IPacket {

	/**
	 * mover hunger
	 */
	protected MoverHunger _mh = new MoverHunger();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._mh;
	}
}
