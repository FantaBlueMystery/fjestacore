/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.mover;

import com.fjestacore.common.mover.MoverSomeoneRideOn;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MOVER_SOMEONE_RIDE_ON_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MOVER_SOMEONE_RIDE_ON_CMD)
public class NC_MOVER_SOMEONE_RIDE_ON_CMD extends Packet implements IPacket {

	/**
	 * mover someone ride on
	 */
	protected MoverSomeoneRideOn _msro = new MoverSomeoneRideOn();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._msro;
	}
}