/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.minihouse;

import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_MINIHOUSE_LOGINCOMPLETE_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_MINIHOUSE_LOGINCOMPLETE_REQ)
public class NC_MINIHOUSE_LOGINCOMPLETE_REQ extends Packet  {}