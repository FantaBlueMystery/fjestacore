/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.minihouse;

import com.fjestacore.common.minihouse.MinihouseBuilding;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_MINIHOUSE_BUILDING_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_MINIHOUSE_BUILDING_REQ)
public class NC_MINIHOUSE_BUILDING_REQ extends Packet implements IPacket {

	/**
	 * minihouse building
	 */
	protected MinihouseBuilding _mb = new MinihouseBuilding();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._mb;
	}
}