/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import com.fjestacore.core.struct.AbstractStruct;

/**
 * IPacket
 * @author FantaBlueMystery
 */
public interface IPacket {

	/**
	 * getPacketObject
	 * @return
	 */
	public AbstractStruct getPacketObject();
}