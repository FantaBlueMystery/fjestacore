/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.party;

import com.fjestacore.common.party.PartyLogin;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_PARTY_LOGIN_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_PARTY_LOGIN_CMD)
public class NC_PARTY_LOGIN_CMD extends Packet implements IPacket {

	/**
	 * party login
	 */
	protected PartyLogin _pl = new PartyLogin();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._pl;
	}
}