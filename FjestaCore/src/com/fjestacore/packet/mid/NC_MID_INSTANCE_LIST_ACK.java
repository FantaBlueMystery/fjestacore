/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fjestacore.packet.mid;

import com.fjestacore.common.mid.MidInstanceList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MID_INSTANCE_LIST_ACK
 * a part of NC_MATCH_INSTANCE_DUNGEON (MID)
 * @author swe
 */
@PacketDefineServer(nc = NcServer.NC_MID_INSTANCE_LIST_ACK)
public class NC_MID_INSTANCE_LIST_ACK extends Packet implements IPacket {

	/**
	 * Mid instance list
	 */
	protected MidInstanceList _mil = new MidInstanceList();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._mil;
	}
}