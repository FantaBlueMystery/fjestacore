/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PacketUnReady
 * @author FantaBlueMystery
 */
public class PacketUnReady extends Packet {

	/**
	 * offset current readed bytes
	 */
	protected int _offset = 0;

	/**
	 * fullsize
	 */
	protected int _fullsize = 0;

	/**
	 * PacketUnReady
	 * @param buffer
	 * @param fullsize
	 */
	public PacketUnReady(byte[] buffer, int fullsize) {
		this._buffer = new byte[fullsize];
		this._offset = buffer.length;
		this._fullsize = fullsize;

		try {
			this._writeBuffer(buffer, 0);
		}
		catch( IOException ex ) {
			Logger.getLogger(PacketUnReady.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * _writePacketBuffer
	 * @param buffer
	 * @param offset
	 * @throws IOException
	 */
	protected void _writeBuffer(byte[] buffer, int offset) throws IOException {
		int afteroffset = (offset + buffer.length);

		if( afteroffset > this._buffer.length ) {
			throw new IOException("out of buffer size!");
		}

		ByteArrayOutputStream ou = new ByteArrayOutputStream(this._buffer.length);

		// ---------------------------------------------------------------------

		int pos = 0;

		if( offset > 0 ) {
			int prevlen = offset;
			byte[] prevbyte = new byte[prevlen];

			System.arraycopy(this._buffer, 0, prevbyte, 0, offset);

			ou.write(prevbyte, 0, prevlen);

			pos += prevlen;
		}

		// ---------------------------------------------------------------------

		ou.write(buffer, 0, buffer.length);

		pos += buffer.length;

		// ---------------------------------------------------------------------

		if( afteroffset < this._buffer.length ) {
			int afterlen	= this._buffer.length - afteroffset;
			byte[] afterbyte = new byte[afterlen];
			int index = 0;

			for( int i=pos; i<this._buffer.length; i++ ) {
				afterbyte[index] = this._buffer[i];
				index++;
			}

			ou.write(afterbyte, 0, afterlen);
		}

		// ---------------------------------------------------------------------

		this._buffer = ou.toByteArray();
	}

	/**
	 * isReady
	 * @return
	 */
	public boolean isReady() {
		return this._offset == this._fullsize;
	}

	/**
	 * getDifferenceSize
	 * @return
	 */
	public int getDifferenceSize() {
		return this._fullsize - this._offset;
	}

	/**
	 * appendBytes
	 * @param buffer
	 */
	public void appendBytes(byte[] buffer) {
		if( buffer.length <= this.getDifferenceSize() ) {
			try {
				this._writeBuffer(buffer, this._offset);
				this._offset += buffer.length;
			}
			catch (IOException ex) {
				Logger.getLogger(PacketUnReady.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		else {
			// TODO error
		}
	}
}