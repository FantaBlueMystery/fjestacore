/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.world.WorldLogin;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_LOGINWORLD_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_LOGINWORLD_REQ)
public class NC_USER_LOGINWORLD_REQ extends Packet implements IPacket {

	/**
	 * WorldLogin
	 */
	protected WorldLogin _login;

	/**
	 * NC_USER_LOGINWORLD_REQ
	 */
	public NC_USER_LOGINWORLD_REQ() {
		super();

		this._login = new WorldLogin();
	}

	/**
	 * NC_USER_LOGINWORLD_REQ
	 * @param login
	 */
	public NC_USER_LOGINWORLD_REQ(WorldLogin login) {
		super();

		this._login = login;
	}

	/**
	 * getLogin
	 * @return
	 */
	public WorldLogin getLogin() {
		return this._login;
	}

	/**
	 * setLogin
	 * @param login
	 */
	public void setLogin(WorldLogin login) {
		this._login = login;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._login;
	}
}