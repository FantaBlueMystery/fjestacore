/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_WORLD_STATUS_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_WORLD_STATUS_REQ)
public class NC_USER_WORLD_STATUS_REQ extends Packet {}