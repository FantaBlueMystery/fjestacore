/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.login.ILogin;
import com.fjestacore.common.login.LoginGer;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_GER_LOGIN_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_GER_LOGIN_REQ)
public class NC_USER_GER_LOGIN_REQ extends Packet implements IPacket {

	/**
	 * Login
	 */
	protected LoginGer _login = new LoginGer();

	/**
	 * NC_USER_GER_LOGIN_REQ
	 */
	public NC_USER_GER_LOGIN_REQ() {
		super();
	}

	/**
	 * NC_USER_GER_LOGIN_REQ
	 * @param login
	 */
	public NC_USER_GER_LOGIN_REQ(LoginGer login) {
		super();

		this._login = login;
	}

	/**
	 * getLogin
	 * @return
	 */
	public ILogin getLogin() {
		return this._login;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._login;
	}
}