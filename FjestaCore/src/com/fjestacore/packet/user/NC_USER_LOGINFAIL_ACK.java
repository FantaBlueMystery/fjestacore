/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.login.LoginFail;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;
import com.fjestacore.server.ServerError;

/**
 * NC_USER_LOGINFAIL_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_USER_LOGINFAIL_ACK)
public class NC_USER_LOGINFAIL_ACK extends Packet implements IPacket {

	/**
	 * login fail
	 */
	protected LoginFail _lf = new LoginFail();

	/**
	 * NC_USER_LOGINFAIL_ACK
	 */
	public NC_USER_LOGINFAIL_ACK() {
		super();
	}

	/**
	 * NC_USER_LOGINFAIL_ACK
	 * @param lf
	 */
	public NC_USER_LOGINFAIL_ACK(LoginFail lf) {
		super();

		this._lf = lf;
	}

	/**
	 * NC_USER_LOGINFAIL_ACK
	 * LoginError
	 * @param errorCode
	 */
	public NC_USER_LOGINFAIL_ACK(ServerError errorCode) {
		super();

		this._lf = new LoginFail(errorCode);
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._lf;
	}
}