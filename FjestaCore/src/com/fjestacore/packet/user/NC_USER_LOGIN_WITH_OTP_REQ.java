/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.user.UserLoginWithOtp;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_LOGIN_WITH_OTP_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_LOGIN_WITH_OTP_REQ)
public class NC_USER_LOGIN_WITH_OTP_REQ extends Packet implements IPacket {

	/**
	 * user login with otp
	 */
	protected UserLoginWithOtp _ulwo = new UserLoginWithOtp();

	/**
	 * getUserLoginWithOtp
	 * @return
	 */
	public UserLoginWithOtp getUserLoginWithOtp() {
		return this._ulwo;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ulwo;
	}
}