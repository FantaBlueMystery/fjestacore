/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.world.IWorldList;
import com.fjestacore.common.world.WorldList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_USER_LOGIN_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_USER_LOGIN_ACK)
public class NC_USER_LOGIN_ACK extends Packet implements IPacket {

	/**
	 * worlds
	 */
	protected IWorldList _worlds;

	/**
	 * NC_USER_LOGIN_ACK
	 */
	public NC_USER_LOGIN_ACK() {
		super();

		this._worlds = new WorldList();
	}

	/**
	 * NC_USER_LOGIN_ACK
	 * @param worlds
	 */
	public NC_USER_LOGIN_ACK(IWorldList worlds) {
		super();

		this._worlds = worlds;
	}

	/**
	 * setWorldList
	 * @param wl
	 */
	public void setWorldList(IWorldList wl) {
		this._worlds = wl;
	}

	/**
	 * getWorldList
	 * @return
	 */
	public IWorldList getWorldList() {
		return this._worlds;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return (WorldList)this._worlds;
	}
}