/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.user.UserWillWorldSelect;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_USER_WILL_WORLD_SELECT_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_USER_WILL_WORLD_SELECT_ACK)
public class NC_USER_WILL_WORLD_SELECT_ACK extends Packet implements IPacket {

	/**
	 * will world select
	 */
	protected UserWillWorldSelect _wsack = new UserWillWorldSelect();

	/**
	 * NC_USER_WILL_WORLD_SELECT_ACK
	 */
	public NC_USER_WILL_WORLD_SELECT_ACK() {
		super();
	}

	/**
	 * NC_USER_WILL_WORLD_SELECT_ACK
	 * @param wsack
	 */
	public NC_USER_WILL_WORLD_SELECT_ACK(UserWillWorldSelect wsack) {
		super();
		
		this._wsack = wsack;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._wsack;
	}
}