/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.user.XtrapReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_XTRAP_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_XTRAP_REQ)
public class NC_USER_XTRAP_REQ extends Packet implements IPacket {

	/**
	 * xr
	 */
	protected XtrapReq _xr = new XtrapReq();

	/**
	 * getXtrapReq
	 * @return
	 */
	public XtrapReq getXtrapReq() {
		return this._xr;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._xr;
	}
}