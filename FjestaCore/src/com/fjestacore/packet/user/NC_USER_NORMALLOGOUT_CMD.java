/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.logout.NormalLogout;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_NORMALLOGOUT_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_NORMALLOGOUT_CMD)
public class NC_USER_NORMALLOGOUT_CMD extends Packet implements IPacket {

	/**
	 * normal logout
	 */
	protected NormalLogout _nl = new NormalLogout();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._nl;
	}
}