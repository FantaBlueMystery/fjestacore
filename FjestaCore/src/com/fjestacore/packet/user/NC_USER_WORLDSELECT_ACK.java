/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.world.WorldConnect;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_USER_WORLDSELECT_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_USER_WORLDSELECT_ACK)
public class NC_USER_WORLDSELECT_ACK extends Packet implements IPacket {

	/**
	 * connect
	 */
	protected WorldConnect _connect;

	/**
	 * NC_USER_WORLDSELECT_ACK
	 */
	public NC_USER_WORLDSELECT_ACK() {
		super();

		this._connect = new WorldConnect();
	}

	/**
	 * NC_USER_WORLDSELECT_ACK
	 * @param connect
	 */
	public NC_USER_WORLDSELECT_ACK(WorldConnect connect) {
		super();

		this._connect = connect;
	}

	/**
	 * getConnect
	 * @return
	 */
	public WorldConnect getConnect() {
		return this._connect;
	}

	/**
	 * setConnect
	 * @param connect
	 */
	public void setConnect(WorldConnect connect) {
		this._connect = connect;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._connect;
	}
}