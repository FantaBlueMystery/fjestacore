/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_USER_WORLD_STATUS_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_USER_WORLD_STATUS_ACK)
public class NC_USER_WORLD_STATUS_ACK extends NC_USER_LOGIN_ACK {}