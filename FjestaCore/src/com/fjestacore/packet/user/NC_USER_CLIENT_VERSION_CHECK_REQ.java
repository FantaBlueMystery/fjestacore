/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.user.UserClientVersion;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_CLIENT_VERSION_CHECK_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_CLIENT_VERSION_CHECK_REQ)
public class NC_USER_CLIENT_VERSION_CHECK_REQ extends Packet implements IPacket {

	/**
	 * user client version
	 */
	protected UserClientVersion _ucv = new UserClientVersion();

	/**
	 * getUserClientVersion
	 * @return
	 */
	public UserClientVersion getUserClientVersion() {
		return this._ucv;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ucv;
	}
}