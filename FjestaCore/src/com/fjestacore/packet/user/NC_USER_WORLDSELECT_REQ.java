/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.world.WorldSelect;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_USER_WORLDSELECT_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_USER_WORLDSELECT_REQ)
public class NC_USER_WORLDSELECT_REQ extends Packet implements IPacket {

	/**
	 * select
	 */
	protected WorldSelect _select;

	/**
	 * NC_USER_WORLDSELECT_REQ
	 */
	public NC_USER_WORLDSELECT_REQ() {
		super();

		this._select = new WorldSelect();
	}

	/**
	 * NC_USER_WORLDSELECT_REQ
	 * @param select
	 */
	public NC_USER_WORLDSELECT_REQ(WorldSelect select) {
		super();

		this._select = select;
	}

	/**
	 * getSelect
	 * @return
	 */
	public WorldSelect getSelect() {
		return this._select;
	}

	/**
	 * setSelect
	 * @param select
	 */
	public void setSelect(WorldSelect select) {
		this._select = select;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._select;
	}
}