/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.user.XtrapAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_USER_XTRAP_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_USER_XTRAP_ACK)
public class NC_USER_XTRAP_ACK extends Packet implements IPacket {

	/**
	 * xtrap ack
	 */
	protected XtrapAck _xa = new XtrapAck();

	/**
	 * getXtrapAck
	 * @return
	 */
	public XtrapAck getXtrapAck() {
		return this._xa;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._xa;
	}
}