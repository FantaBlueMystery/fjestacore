/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.user;

import com.fjestacore.common.login.UserWorldLogin;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_USER_LOGINWORLD_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_USER_LOGINWORLD_ACK)
public class NC_USER_LOGINWORLD_ACK extends Packet implements IPacket {

	/**
	 * login
	 */
	protected UserWorldLogin _login;

	/**
	 * NC_USER_LOGINWORLD_ACK
	 * CharacterList
	 */
	public NC_USER_LOGINWORLD_ACK() {
		super();

		this._login = new UserWorldLogin();
	}

	/**
	 * NC_USER_LOGINWORLD_ACK
	 * @param login
	 */
	public NC_USER_LOGINWORLD_ACK(UserWorldLogin login) {
		super();

		this._login = login;
	}

	/**
	 * getLogin
	 * @return
	 */
	public UserWorldLogin getLogin() {
		return this._login;
	}

	/**
	 * setLogin
	 * @param login
	 */
	public void setLogin(UserWorldLogin login) {
		this._login = login;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._login;
	}
}