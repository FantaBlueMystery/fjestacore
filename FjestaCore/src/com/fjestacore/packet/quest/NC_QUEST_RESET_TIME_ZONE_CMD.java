/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.quest;

import com.fjestacore.common.quest.QuestRestTimeZone;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_QUEST_RESET_TIME_ZONE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_QUEST_RESET_TIME_ZONE_CMD)
public class NC_QUEST_RESET_TIME_ZONE_CMD extends Packet implements IPacket {

	/**
	 * quest reset time zone
	 */
	protected QuestRestTimeZone _qrtz = new QuestRestTimeZone();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._qrtz;
	}
}
