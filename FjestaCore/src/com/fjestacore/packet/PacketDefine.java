/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet;

import com.fjestacore.core.net.SocketType;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * PacketDefine
 * @author FantaBlueMystery
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PacketDefine {

	/**
	 * type client/server
	 * @return
	 */
	SocketType type();

	/**
	 * command of packet
	 * @return
	 */
	int cmd();
}