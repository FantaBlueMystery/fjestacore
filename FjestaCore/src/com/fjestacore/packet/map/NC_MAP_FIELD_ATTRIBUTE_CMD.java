/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.map;

import com.fjestacore.common.map.FieldAttribute;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MAP_FIELD_ATTRIBUTE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MAP_FIELD_ATTRIBUTE_CMD)
public class NC_MAP_FIELD_ATTRIBUTE_CMD extends Packet implements IPacket {

	/**
	 * field attribute
	 */
	protected FieldAttribute _attr = new FieldAttribute();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._attr;
	}
}