/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.map;

import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_MAP_LOGINCOMPLETE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_MAP_LOGINCOMPLETE_CMD)
public class NC_MAP_LOGINCOMPLETE_CMD extends Packet {}