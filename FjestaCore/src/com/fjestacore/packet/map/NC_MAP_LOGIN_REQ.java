/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.map;

import com.fjestacore.common.login.MapLogin;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_MAP_LOGIN_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_MAP_LOGIN_REQ)
public class NC_MAP_LOGIN_REQ extends Packet implements IPacket {

	/**
	 * map login
	 */
	protected MapLogin _ml = new MapLogin();

	/**
	 * getMapLogin
	 * @return
	 */
	public MapLogin getMapLogin() {
		return this._ml;
	}

	/**
	 * setMapLogin
	 * @param ml
	 */
	public void setMapLogin(MapLogin ml) {
		this._ml = ml;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ml;
	}
}
