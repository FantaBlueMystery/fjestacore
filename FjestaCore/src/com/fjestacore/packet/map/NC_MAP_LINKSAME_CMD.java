/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.map;

import com.fjestacore.common.map.MapLinkSame;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;

/**
 * NC_MAP_LINKSAME_CMD
 * @author FantaBlueMystery
 */
public class NC_MAP_LINKSAME_CMD extends Packet implements IPacket {

	/**
	 * map link same
	 */
	protected MapLinkSame _mls = new MapLinkSame();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._mls;
	}
}