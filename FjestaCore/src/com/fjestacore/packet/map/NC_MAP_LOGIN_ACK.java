/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.map;

import com.fjestacore.common.character.CharMapLogin;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MAP_LOGIN_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MAP_LOGIN_ACK)
public class NC_MAP_LOGIN_ACK extends Packet implements IPacket {

	/**
	 * char map login
	 */
	protected CharMapLogin _cml = new CharMapLogin();

	/**
	 * NC_MAP_LOGIN_ACK
	 */
	public NC_MAP_LOGIN_ACK() {
		super();
	}

	/**
	 * NC_MAP_LOGIN_ACK
	 * @param ml
	 */
	public NC_MAP_LOGIN_ACK(CharMapLogin ml) {
		super();

		this._cml = ml;
	}

	/**
	 * getCharMapLogin
	 * @return
	 */
	public CharMapLogin getCharMapLogin() {
		return this._cml;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cml;
	}
}