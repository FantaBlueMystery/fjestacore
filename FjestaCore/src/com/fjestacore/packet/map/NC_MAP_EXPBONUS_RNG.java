/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.map;

import com.fjestacore.common.map.MapExpBonus;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MAP_EXPBONUS_RNG
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MAP_EXPBONUS_RNG)
public class NC_MAP_EXPBONUS_RNG extends Packet implements IPacket {

	/**
	 * map exp bonus
	 */
	protected MapExpBonus _meb = new MapExpBonus();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._meb;
	}
}