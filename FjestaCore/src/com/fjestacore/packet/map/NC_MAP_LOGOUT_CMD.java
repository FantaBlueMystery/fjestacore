/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.map;

import com.fjestacore.common.login.MapLogout;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MAP_LOGOUT_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MAP_LOGOUT_CMD)
public class NC_MAP_LOGOUT_CMD extends Packet implements IPacket {

	/**
	 * map lotgout
	 */
	protected MapLogout _ml = new MapLogout();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ml;
	}
}