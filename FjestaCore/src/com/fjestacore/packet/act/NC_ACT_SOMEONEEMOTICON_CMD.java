/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.common.act.SomeoneEmoticon;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_SOMEONEEMOTICON_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_SOMEONEEMOTICON_CMD)
public class NC_ACT_SOMEONEEMOTICON_CMD extends Packet implements IPacket {

	/**
	 * someone emoticon
	 */
	protected SomeoneEmoticon _se = new SomeoneEmoticon();

    /**
     * NC_ACT_SOMEONEEMOTICON_CMD
     */
    public NC_ACT_SOMEONEEMOTICON_CMD() {
        super();
    }

    /**
     * NC_ACT_SOMEONEEMOTICON_CMD
     * @param someoneEmoticon
     */
    public NC_ACT_SOMEONEEMOTICON_CMD(SomeoneEmoticon someoneEmoticon) {
        super();

        this._se = someoneEmoticon;
    }


	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._se;
	}
}