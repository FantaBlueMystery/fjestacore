/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.common.chat.SomeoneChat;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_SOMEONECHAT_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_SOMEONECHAT_CMD)
public class NC_ACT_SOMEONECHAT_CMD extends Packet implements IPacket {

	/**
	 * someone chat
	 */
	protected SomeoneChat _sc = new SomeoneChat();

	/**
	 * NC_ACT_SOMEONECHAT_CMD
	 */
	public NC_ACT_SOMEONECHAT_CMD() {
		super();
	}

	/**
	 * NC_ACT_SOMEONECHAT_CMD
	 * @param chat
	 */
	public NC_ACT_SOMEONECHAT_CMD(SomeoneChat chat) {
		super();

		this._sc = chat;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._sc;
	}
}