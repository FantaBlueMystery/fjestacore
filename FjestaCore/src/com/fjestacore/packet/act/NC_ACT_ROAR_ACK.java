/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.act.RoarAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_ROAR_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_ROAR_ACK)
public class NC_ACT_ROAR_ACK extends Packet implements IPacket {

	/**
	 * roar ack
	 */
	protected RoarAck _ra = new RoarAck();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ra;
	}
}