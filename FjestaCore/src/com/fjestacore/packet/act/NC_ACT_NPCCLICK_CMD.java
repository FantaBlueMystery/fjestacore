/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.common.act.NpcClick;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_NPCCLICK_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_NPCCLICK_CMD)
public class NC_ACT_NPCCLICK_CMD extends Packet implements IPacket {

	/**
	 * npc click
	 */
	protected NpcClick _npcClick = new NpcClick();

	/**
	 * getNpcClick
	 * @return
	 */
	public NpcClick getNpcClick() {
		return this._npcClick;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._npcClick;
	}
}