/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.common.act.NpcAction;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_NPC_ACTION_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_NPC_ACTION_CMD)
public class NC_ACT_NPC_ACTION_CMD extends Packet implements IPacket {

	/**
	 * npc action
	 */
	protected NpcAction _na = new NpcAction();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._na;
	}
}