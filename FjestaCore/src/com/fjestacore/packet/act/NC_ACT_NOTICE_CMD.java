/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.common.act.Notice;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_NOTICE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_NOTICE_CMD)
public class NC_ACT_NOTICE_CMD extends Packet implements IPacket {

	/**
	 * notice
	 */
	protected Notice _notice = new Notice();

	/**
	 * NC_ACT_NOTICE_CMD
	 */
	public NC_ACT_NOTICE_CMD() {
		super();
	}

	/**
	 * NC_ACT_NOTICE_CMD
	 * @param notice
	 */
	public NC_ACT_NOTICE_CMD(Notice notice) {
		super();

		this._notice = notice;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._notice;
	}
}