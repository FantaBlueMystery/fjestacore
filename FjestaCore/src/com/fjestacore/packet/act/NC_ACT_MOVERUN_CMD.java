/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_MOVERUN_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_MOVERUN_CMD)
public class NC_ACT_MOVERUN_CMD extends NC_ACT_MOVEWALK_CMD {}