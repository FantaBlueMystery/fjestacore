/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.act.RoarReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_ROAR_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_ROAR_REQ)
public class NC_ACT_ROAR_REQ extends Packet implements IPacket {

	/**
	 * roar req
	 */
	protected RoarReq _rr = new RoarReq();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._rr;
	}
}