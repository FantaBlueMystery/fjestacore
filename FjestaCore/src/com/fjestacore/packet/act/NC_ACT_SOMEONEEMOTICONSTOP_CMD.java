/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.act.SomeoneEmoticonStop;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_SOMEONEEMOTICONSTOP_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_SOMEONEEMOTICONSTOP_CMD)
public class NC_ACT_SOMEONEEMOTICONSTOP_CMD extends Packet implements IPacket {

	/**
	 * someone emoticon stop
	 */
	protected SomeoneEmoticonStop _ses = new SomeoneEmoticonStop();

	/**
	 * NC_ACT_SOMEONEEMOTICONSTOP_CMD
	 */
	public NC_ACT_SOMEONEEMOTICONSTOP_CMD() {
		super();
	}

	/**
	 * NC_ACT_SOMEONEEMOTICONSTOP_CMD
	 * @param es
	 */
	public NC_ACT_SOMEONEEMOTICONSTOP_CMD(SomeoneEmoticonStop es) {
		super();

		this._ses = es;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ses;
	}
}