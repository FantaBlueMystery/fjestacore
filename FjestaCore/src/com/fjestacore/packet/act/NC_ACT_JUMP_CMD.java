/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_JUMP_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_JUMP_CMD)
public class NC_ACT_JUMP_CMD extends Packet {}