/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.common.position.PositionXY;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_STOP_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_STOP_REQ)
public class NC_ACT_STOP_REQ extends Packet implements IPacket {

	/**
	 * position
	 */
	protected PositionXY _pos = new PositionXY();

	/**
	 * getPosition
	 * @return
	 */
	public PositionXY getPosition() {
		return this._pos;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._pos;
	}
}