/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.act.ChangeMode;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_CHANGEMODE_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_CHANGEMODE_REQ)
public class NC_ACT_CHANGEMODE_REQ extends Packet implements IPacket {

	/**
	 * change mode
	 */
	protected ChangeMode _cm = new ChangeMode();

	/**
	 * getChangeMode
	 * @return
	 */
	public ChangeMode getChangeMode() {
		return this._cm;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cm;
	}
}