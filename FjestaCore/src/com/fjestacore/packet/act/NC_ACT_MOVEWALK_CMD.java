/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.move.MoveFromTo;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_MOVEWALK_CMD
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_MOVEWALK_CMD)
public class NC_ACT_MOVEWALK_CMD extends Packet implements IPacket {

	/**
	 * move from to
	 */
	protected MoveFromTo _mft = new MoveFromTo();

	/**
	 * NC_ACT_MOVEWALK_CMD
	 */
	public NC_ACT_MOVEWALK_CMD() {
		super();
	}

	/**
	 * NC_ACT_MOVEWALK_CMD
	 * @param move
	 */
	public NC_ACT_MOVEWALK_CMD(MoveFromTo move) {
		super();

		this._mft = move;
	}

	/**
	 * getMove
	 * @return
	 */
	public MoveFromTo getMove() {
		return this._mft;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._mft;
	}
}