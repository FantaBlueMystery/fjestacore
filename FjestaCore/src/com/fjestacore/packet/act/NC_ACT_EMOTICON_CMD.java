/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.act.Emoticon;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_EMOTICON_CMD
 * @author FantaBlueMystery
 * @author EndOfFile
 */
@PacketDefineClient(nc = NcClient.NC_ACT_EMOTICON_CMD)
public class NC_ACT_EMOTICON_CMD extends Packet implements IPacket {

    /**
     * emoticon
     */
    protected Emoticon _em = new Emoticon();

        /**
     * NC_ACT_EMOTICON_CMD
     */
    public NC_ACT_EMOTICON_CMD() {
        super();
    }

    /**
     * NC_ACT_EMOTICON_CMD
     * @param emoticon
     */
    public NC_ACT_EMOTICON_CMD(Emoticon emoticon) {
        super();

        this._em = emoticon;
    }

    /**
     * getMove
     * @return
     */
    public Emoticon getEmoticon() {
        return this._em;
    }
    
    /**
     * getPacketObject
     * @return
     */
    @Override
    public AbstractStruct getPacketObject() {
        return this._em;
    }
}