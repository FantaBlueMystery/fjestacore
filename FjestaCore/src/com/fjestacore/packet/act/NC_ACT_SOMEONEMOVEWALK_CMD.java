/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.move.SomeoneMoveFromTo;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_SOMEONEMOVEWALK_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_SOMEONEMOVEWALK_CMD)
public class NC_ACT_SOMEONEMOVEWALK_CMD extends Packet implements IPacket {

	/**
	 *  someone move
	 */
	protected SomeoneMoveFromTo _smove = new SomeoneMoveFromTo();

	/**
	 * NC_ACT_SOMEONEMOVEWALK_CMD
	 */
	public NC_ACT_SOMEONEMOVEWALK_CMD() {
		super();
	}

	/**
	 * NC_ACT_SOMEONEMOVEWALK_CMD
	 * @param move
	 */
	public NC_ACT_SOMEONEMOVEWALK_CMD(SomeoneMoveFromTo move) {
		super();

		this._smove = move;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._smove;
	}
}