/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.act;

import com.fjestacore.common.chat.ChatReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_ACT_CHAT_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_ACT_CHAT_REQ)
public class NC_ACT_CHAT_REQ extends Packet implements IPacket {

	/**
	 * chat req
	 */
	protected ChatReq _cr = new ChatReq();

	/**
	 * getChat
	 * @return
	 */
	public ChatReq getChat() {
		return this._cr;
	}

	/**
	 * setChat
	 * @param tchat
	 */
	public void setChat(ChatReq tchat) {
		this._cr = tchat;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cr;
	}
}