/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.act;

import com.fjestacore.common.act.SomeoneJump;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ACT_SOMEEONEJUMP_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ACT_SOMEEONEJUMP_CMD)
public class NC_ACT_SOMEEONEJUMP_CMD extends Packet implements IPacket {

	/**
	 * someone jump
	 */
	protected SomeoneJump _sj = new SomeoneJump();

	/**
	 * NC_ACT_SOMEEONEJUMP_CMD
	 */
	public NC_ACT_SOMEEONEJUMP_CMD() {
		super();
	}

	/**
	 * NC_ACT_SOMEEONEJUMP_CMD
	 * @param jump
	 */
	public NC_ACT_SOMEEONEJUMP_CMD(SomeoneJump jump) {
		super();

		this._sj = jump;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._sj;
	}
}