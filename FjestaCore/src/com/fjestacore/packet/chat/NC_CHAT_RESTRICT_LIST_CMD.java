/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.chat;

import com.fjestacore.common.chat.ChatRestrictList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAT_RESTRICT_LIST_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAT_RESTRICT_LIST_CMD)
public class NC_CHAT_RESTRICT_LIST_CMD extends Packet implements IPacket {

	/**
	 * chat restict list
	 */
	protected ChatRestrictList _crl = new ChatRestrictList();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._crl;
	}
}