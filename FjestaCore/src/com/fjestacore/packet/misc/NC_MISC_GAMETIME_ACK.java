/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.misc;

import com.fjestacore.common.misc.GameTime;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MISC_GAMETIME_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MISC_GAMETIME_ACK)
public class NC_MISC_GAMETIME_ACK extends Packet implements IPacket {

	/**
	 * game time ack
	 */
	protected GameTime _gt = new GameTime();

	/**
	 * getGameTime
	 * @return
	 */
	public GameTime getGameTime() {
		return this._gt;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._gt;
	}
}