/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.misc;

import com.fjestacore.common.chatblock.ChatBlockSpamFilter;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MISC_GET_CHAT_BLOCK_SPAM_FILTER_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MISC_GET_CHAT_BLOCK_SPAM_FILTER_CMD)
public class NC_MISC_GET_CHAT_BLOCK_SPAM_FILTER_CMD extends Packet implements IPacket {

	/**
	 * cbsf
	 */
	protected ChatBlockSpamFilter _cbsf = new ChatBlockSpamFilter();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cbsf;
	}
}