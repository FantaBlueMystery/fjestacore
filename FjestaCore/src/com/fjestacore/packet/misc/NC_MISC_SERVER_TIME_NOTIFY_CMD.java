/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.misc;

import com.fjestacore.common.misc.ServerTimeNotify;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;

/**
 * NC_MISC_SERVER_TIME_NOTIFY_CMD
 * @author FantaBlueMystery
 */
public class NC_MISC_SERVER_TIME_NOTIFY_CMD extends Packet implements IPacket {

	/**
	 * server time notify
	 */
	protected ServerTimeNotify _stn = new ServerTimeNotify();

	/**
	 * getServerTimeNotify
	 * @return
	 */
	public ServerTimeNotify getServerTimeNotify() {
		return this._stn;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._stn;
	}
}