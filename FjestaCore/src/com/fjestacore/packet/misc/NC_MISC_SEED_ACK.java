/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.misc;

import com.fjestacore.common.misc.SeedAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_MISC_SEED_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_MISC_SEED_ACK)
public class NC_MISC_SEED_ACK extends Packet implements IPacket {

	/**
	 * seed ack
	 */
	protected SeedAck _sa = new SeedAck();

	/**
	 * getSeed
	 * @return
	 */
	public SeedAck getSeed() {
		return this._sa;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._sa;
	}
}