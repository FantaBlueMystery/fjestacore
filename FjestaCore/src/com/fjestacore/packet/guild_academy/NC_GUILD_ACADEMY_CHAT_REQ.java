/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.guild_academy;

import com.fjestacore.common.chat.ChatReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_GUILD_ACADEMY_CHAT_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_GUILD_ACADEMY_CHAT_REQ)
public class NC_GUILD_ACADEMY_CHAT_REQ extends Packet implements IPacket {

	/**
	 * chat
	 */
	protected ChatReq _chat = new ChatReq();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._chat;
	}
}