/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.guild_academy;

import com.fjestacore.common.guildacademy.GuildAcademyChat;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_GUILD_ACADEMY_CHAT_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_GUILD_ACADEMY_CHAT_CMD)
public class NC_GUILD_ACADEMY_CHAT_CMD extends Packet implements IPacket {

	/**
	 * guild academy chat
	 */
	protected GuildAcademyChat _gac = new GuildAcademyChat();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._gac;
	}
}