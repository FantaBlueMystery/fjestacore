/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.soulstone;

import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_SOULSTONE_HP_USESUC_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_SOULSTONE_HP_USESUC_ACK)
public class NC_SOULSTONE_HP_USESUC_ACK extends Packet {}