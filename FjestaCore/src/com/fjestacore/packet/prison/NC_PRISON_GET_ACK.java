/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.prison;

import com.fjestacore.common.prison.Prison;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_PRISON_GET_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_PRISON_GET_ACK)
public class NC_PRISON_GET_ACK extends Packet implements IPacket {

	/**
	 * prison
	 */
	protected Prison _prison = null;

	/**
	 * NC_PRISON_GET_ACK
	 */
	public NC_PRISON_GET_ACK() {
		super();

		this._prison = new Prison();
	}

	/**
	 * NC_PRISON_GET_ACK
	 * @param p
	 */
	public NC_PRISON_GET_ACK(Prison p) {
		super();

		this._prison = p;
	}

	/**
	 * getPrison
	 * @return
	 */
	public Prison getPrison() {
		return this._prison;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._prison;
	}
}