/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.guild;

import com.fjestacore.common.guild.GuildTournamentListAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_GUILD_TOURNAMENT_LIST_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_GUILD_TOURNAMENT_LIST_ACK)
public class NC_GUILD_TOURNAMENT_LIST_ACK extends Packet implements IPacket {

	/**
	 * gtl
	 */
	protected GuildTournamentListAck _gtl = new GuildTournamentListAck();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._gtl;
	}
}