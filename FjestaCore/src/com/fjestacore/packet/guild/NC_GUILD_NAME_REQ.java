/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.guild;

import com.fjestacore.common.guild.GuildNameReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_GUILD_NAME_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_GUILD_NAME_REQ)
public class NC_GUILD_NAME_REQ extends Packet implements IPacket {

	/**
	 * guild name req
	 */
	protected GuildNameReq _gr = new GuildNameReq();

	/**
	 * getGuildNameReq
	 * @return
	 */
	public GuildNameReq getGuildNameReq() {
		return this._gr;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._gr;
	}
}