/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.guild;

import com.fjestacore.common.guild.GuildNameAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_GUILD_NAME_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_GUILD_NAME_ACK)
public class NC_GUILD_NAME_ACK extends Packet implements IPacket {

	/**
	 * guild name ack
	 */
	protected GuildNameAck _gna = new GuildNameAck();

	/**
	 * NC_GUILD_NAME_ACK
	 */
	public NC_GUILD_NAME_ACK() {
		super();
	}

	/**
	 * NC_GUILD_NAME_ACK
	 * @param gn
	 */
	public NC_GUILD_NAME_ACK(GuildNameAck gn) {
		super();

		this._gna = gn;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._gna;
	}
}