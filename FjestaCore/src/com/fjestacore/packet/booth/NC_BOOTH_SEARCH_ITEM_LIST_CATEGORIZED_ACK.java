/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.booth;

import com.fjestacore.common.booth.BoothSearchItemList;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_BOOTH_SEARCH_ITEM_LIST_CATEGORIZED_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_BOOTH_SEARCH_ITEM_LIST_CATEGORIZED_ACK)
public class NC_BOOTH_SEARCH_ITEM_LIST_CATEGORIZED_ACK extends Packet implements IPacket {

	/**
	 * booth search item list
	 */
	protected BoothSearchItemList _bsil = new BoothSearchItemList();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._bsil;
	}
}