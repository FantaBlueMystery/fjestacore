/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.booth;

import com.fjestacore.common.booth.BoothSearchItemListCategorized;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_BOOTH_SEARCH_ITEM_LIST_CATEGORIZED_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_BOOTH_OPEN_REQ)
public class NC_BOOTH_SEARCH_ITEM_LIST_CATEGORIZED_REQ extends Packet implements IPacket {

	/**
	 * booth search item list categorized
	 */
	protected BoothSearchItemListCategorized _bsilc = new BoothSearchItemListCategorized();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._bsilc;
	}
}