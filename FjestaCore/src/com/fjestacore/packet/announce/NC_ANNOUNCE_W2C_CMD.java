/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.announce;

import com.fjestacore.common.announce.AnnounceMessage;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_ANNOUNCE_W2C_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_ANNOUNCE_W2C_CMD)
public class NC_ANNOUNCE_W2C_CMD extends Packet implements IPacket {

	/**
	 * accounce message
	 */
	protected AnnounceMessage _am = new AnnounceMessage();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._am;
	}
}