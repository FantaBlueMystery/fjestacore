/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.skill.CharClientSkills;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_SKILL_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_SKILL_CMD)
public class NC_CHAR_CLIENT_SKILL_CMD extends Packet implements IPacket {

	/**
	 * skill
	 */
	protected CharClientSkills _skill;

	/**
	 * NC_CHAR_CLIENT_SKILL_CMD
	 */
	public NC_CHAR_CLIENT_SKILL_CMD() {
		super();

		this._skill = new CharClientSkills();
	}

	/**
	 * NC_CHAR_CLIENT_SKILL_CMD
	 * @param ccskills
	 */
	public NC_CHAR_CLIENT_SKILL_CMD(CharClientSkills ccskills) {
		super();

		this._skill = ccskills;
	}

	/**
	 * NC_CHAR_CLIENT_SKILL_CMD
	 * @param hexstring
	 */
	public NC_CHAR_CLIENT_SKILL_CMD(String hexstring) {
		super();

		this._skill = new CharClientSkills();

		this.setBufferByHexString(hexstring);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getCharClientSkills
	 * @return
	 */
	public CharClientSkills getCharClientSkills() {
		return this._skill;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._skill;
	}
}
