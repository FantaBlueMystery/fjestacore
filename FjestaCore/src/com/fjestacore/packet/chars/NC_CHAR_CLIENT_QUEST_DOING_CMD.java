/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.quest.QuestDoing;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_QUEST_DOING_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_QUEST_DOING_CMD)
public class NC_CHAR_CLIENT_QUEST_DOING_CMD extends Packet implements IPacket {

	/**
	 * quest doing
	 */
	protected QuestDoing _qd;

	/**
	 * NC_CHAR_CLIENT_QUEST_DOING_CMD
	 */
	public NC_CHAR_CLIENT_QUEST_DOING_CMD() {
		super();

		this._qd = new QuestDoing();
	}

	/**
	 * NC_CHAR_CLIENT_QUEST_DOING_CMD
	 * @param qd
	 */
	public NC_CHAR_CLIENT_QUEST_DOING_CMD(QuestDoing qd) {
		super();

		this._qd = qd;
	}

	/**
	 * NC_CHAR_CLIENT_QUEST_DOING_CMD
	 * @param hexstring
	 */
	public NC_CHAR_CLIENT_QUEST_DOING_CMD(String hexstring) {
		super();

		this._qd = new QuestDoing();

		this.setBufferByHexString(hexstring);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getQuestDoing
	 * @return
	 */
	public QuestDoing getQuestDoing() {
		return this._qd;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._qd;
	}
}
