/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharLoginReq;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcClient;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineClient;

/**
 * NC_CHAR_LOGIN_REQ
 * @author FantaBlueMystery
 */
@PacketDefineClient(nc = NcClient.NC_CHAR_LOGIN_REQ)
public class NC_CHAR_LOGIN_REQ extends Packet implements IPacket {

	/**
	 * char login
	 */
	protected CharLoginReq _cl = new CharLoginReq();

	/**
	 * NC_CHAR_LOGIN_REQ
	 * CharSelect
	 */
	public NC_CHAR_LOGIN_REQ() {
		super();
	}

	/**
	 * NC_CHAR_LOGIN_REQ
	 * @param cl
	 */
	public NC_CHAR_LOGIN_REQ(CharLoginReq cl) {
		super();

		this._cl = cl;
	}

	/**
	 * getCharLogin
	 * @return
	 */
	public CharLoginReq getCharLogin() {
		return this._cl;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cl;
	}
}