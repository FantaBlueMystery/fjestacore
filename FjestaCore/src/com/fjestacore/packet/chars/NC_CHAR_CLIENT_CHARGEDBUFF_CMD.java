/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharChargeBuff;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_CHARGEDBUFF_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_CHARGEDBUFF_CMD)
public class NC_CHAR_CLIENT_CHARGEDBUFF_CMD extends Packet implements IPacket {

	/**
	 * char charge buff
	 */
	protected CharChargeBuff _ccb = new CharChargeBuff();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._ccb;
	}
}
