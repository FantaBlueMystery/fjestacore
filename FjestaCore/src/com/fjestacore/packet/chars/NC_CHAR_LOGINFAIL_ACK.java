/**
 * FjestaServer - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharLoginFail;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_LOGINFAIL_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_LOGINFAIL_ACK)
public class NC_CHAR_LOGINFAIL_ACK extends Packet implements IPacket {

	/**
	 * char login fail
	 */
	protected CharLoginFail _clf = new CharLoginFail();

	/**
	 * NC_CHAR_LOGINFAIL_ACK
	 */
	public NC_CHAR_LOGINFAIL_ACK() {
		super();
	}

	/**
	 * NC_CHAR_LOGINFAIL_ACK
	 * @param lf
	 */
	public NC_CHAR_LOGINFAIL_ACK(CharLoginFail lf) {
		super();

		this._clf = lf;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._clf;
	}
}