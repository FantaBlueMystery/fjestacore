/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharBase;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_BASE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_BASE_CMD)
public class NC_CHAR_CLIENT_BASE_CMD extends Packet implements IPacket {

	/**
	 * character base
	 */
	protected CharBase _cb = new CharBase();

	/**
	 * NC_CHAR_CLIENT_BASE_CMD
	 */
	public NC_CHAR_CLIENT_BASE_CMD() {
		super();
	}

	/**
	 * NC_CHAR_CLIENT_BASE_CMD
	 * @param cb
	 */
	public NC_CHAR_CLIENT_BASE_CMD(CharBase cb) {
		super();

		this._cb = cb;
	}

	/**
	 * NC_CHAR_CLIENT_BASE_CMD
	 * @param hexbuffer
	 */
	public NC_CHAR_CLIENT_BASE_CMD(String hexbuffer) {
		super();

		this.setBufferByHexString(hexbuffer);
	}

	/**
	 * getCharacter
	 * @return
	 */
	public CharBase getCharBase() {
		return this._cb;
	}

	/**
	 * setCharacter
	 * @param cb
	 */
	public void setCharBase(CharBase cb) {
		this._cb = cb;
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cb;
	}
}