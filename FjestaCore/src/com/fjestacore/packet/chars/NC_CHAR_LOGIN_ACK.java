/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharLoginAck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_LOGIN_ACK
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_LOGIN_ACK)
public class NC_CHAR_LOGIN_ACK extends Packet implements IPacket {

	/**
	 * login
	 */
	protected CharLoginAck _login;

	/**
	 * NC_CHAR_LOGIN_ACK
	 */
	public NC_CHAR_LOGIN_ACK() {
		super();

		this._login = new CharLoginAck();
	}

	/**
	 * NC_CHAR_LOGIN_ACK
	 * @param login
	 */
	public NC_CHAR_LOGIN_ACK(CharLoginAck login) {
		super();

		this._login = login;
	}

	/**
	 * getLogin
	 * @return
	 */
	public CharLoginAck getLogin() {
		return this._login;
	}

	/**
	 * setLogin
	 * @param login
	 */
	public void setLogin(CharLoginAck login) {
		this._login = login;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._login;
	}
}
