/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.inventory.Inventory;
import com.fjestacore.common.inventory.InventoryType;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_ITEM_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_ITEM_CMD)
public class NC_CHAR_CLIENT_ITEM_CMD extends Packet implements IPacket {

	/**
	 * inv
	 */
	protected Inventory _inv = null;

	/**
	 * NC_CHAR_CLIENT_ITEM_CMD
	 */
	public NC_CHAR_CLIENT_ITEM_CMD() {
		super();

		this._inv = new Inventory();
	}

	/**
	 * NC_CHAR_CLIENT_ITEM_CMD
	 * CharacterItemList
	 * @param type
	 */
	public NC_CHAR_CLIENT_ITEM_CMD(InventoryType type) {
		super();

		this._inv = new Inventory(type);
	}

	/**
	 * NC_CHAR_CLIENT_ITEM_CMD
	 * @param inv
	 */
	public NC_CHAR_CLIENT_ITEM_CMD(Inventory inv) {
		super();

		this._inv = inv;
	}

	/**
	 * NC_CHAR_CLIENT_ITEM_CMD
	 * @param hexbuffer
	 */
	public NC_CHAR_CLIENT_ITEM_CMD(String hexbuffer) {
		super();

		this.setBufferByHexString(hexbuffer);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getInventory
	 * @return
	 */
	public Inventory getInventory() {
		return this._inv;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._inv;
	}
}