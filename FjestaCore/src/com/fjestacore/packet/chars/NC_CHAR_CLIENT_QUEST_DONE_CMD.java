/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.quest.QuestDone;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_QUEST_DONE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_QUEST_DONE_CMD)
public class NC_CHAR_CLIENT_QUEST_DONE_CMD extends Packet implements IPacket {

	/**
	 * quest done
	 */
	protected QuestDone _qd;

	/**
	 * NC_CHAR_CLIENT_QUEST_DONE_CMD
	 */
	public NC_CHAR_CLIENT_QUEST_DONE_CMD() {
		super();

		this._qd = new QuestDone();
	}

	/**
	 * NC_CHAR_CLIENT_QUEST_DONE_CMD
	 * @param qd
	 */
	public NC_CHAR_CLIENT_QUEST_DONE_CMD(QuestDone qd) {
		super();

		this._qd = qd;
	}

	/**
	 * NC_CHAR_CLIENT_QUEST_DONE_CMD
	 * @param hexstring
	 */
	public NC_CHAR_CLIENT_QUEST_DONE_CMD(String hexstring) {
		super();

		this._qd = new QuestDone();

		this.setBufferByHexString(hexstring);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._qd;
	}
}
