/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharAniFileCheck;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_ANI_FILE_CHECK_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_ANI_FILE_CHECK_CMD)
public class NC_CHAR_ANI_FILE_CHECK_CMD extends Packet implements IPacket {

	/**
	 * char ani file check
	 */
	protected CharAniFileCheck _cafc = new CharAniFileCheck();

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cafc;
	}
}