/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharacterLook;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_SHAPE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_SHAPE_CMD)
public class NC_CHAR_CLIENT_SHAPE_CMD extends Packet implements IPacket {

	/**
	 * look object
	 */
	protected CharacterLook _look;

	/**
	 * NC_CHAR_CLIENT_SHAPE_CMD
	 */
	public NC_CHAR_CLIENT_SHAPE_CMD() {
		super();

		this._look = new CharacterLook();
	}

	/**
	 * NC_CHAR_CLIENT_SHAPE_CMD
	 * @param look
	 */
	public NC_CHAR_CLIENT_SHAPE_CMD(CharacterLook look) {
		super();

		this._look = look;
	}

	/**
	 * getLookObject
	 * @return
	 */
	public CharacterLook getLookObject() {
		return this._look;
	}

	/**
	 * setLookObject
	 * @param look
	 */
	public void setLookObject(CharacterLook look) {
		this._look = look;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._look;
	}
}