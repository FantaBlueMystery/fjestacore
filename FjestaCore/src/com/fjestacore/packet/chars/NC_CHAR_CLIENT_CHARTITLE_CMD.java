/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharTitle;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_CHARTITLE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_CHARTITLE_CMD)
public class NC_CHAR_CLIENT_CHARTITLE_CMD extends Packet implements IPacket {

	/**
	 * char title
	 */
	protected CharTitle _charTitle = new CharTitle();

	/**
	 * NC_CHAR_CLIENT_CHARTITLE_CMD
	 */
	public NC_CHAR_CLIENT_CHARTITLE_CMD() {
		super();
	}

	/**
	 * NC_CHAR_CLIENT_CHARTITLE_CMD
	 * @param charTitle
	 */
	public NC_CHAR_CLIENT_CHARTITLE_CMD(CharTitle charTitle) {
		super();

		this._charTitle = charTitle;
	}

	/**
	 * setCharTitle
	 * @param charTitle
	 */
	public void setCharTitle(CharTitle charTitle) {
		this._charTitle = charTitle;
	}

	/**
	 * getCharTitle
	 * @return
	 */
	public CharTitle getCharTitle() {
		return this._charTitle;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._charTitle;
	}
}