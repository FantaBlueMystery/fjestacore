/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.quest.QuestRead;
import com.fjestacore.core.convert.HexString;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_QUEST_READ_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_QUEST_READ_CMD)
public class NC_CHAR_CLIENT_QUEST_READ_CMD extends Packet implements IPacket {

	/**
	 * quest read
	 */
	protected QuestRead _qr;

	/**
	 * NC_CHAR_CLIENT_QUEST_READ_CMD
	 */
	public NC_CHAR_CLIENT_QUEST_READ_CMD() {
		super();

		this._qr = new QuestRead();
	}

	/**
	 * NC_CHAR_CLIENT_QUEST_READ_CMD
	 * @param qr
	 */
	public NC_CHAR_CLIENT_QUEST_READ_CMD(QuestRead qr) {
		super();

		this._qr = qr;
	}

	/**
	 * NC_CHAR_CLIENT_QUEST_READ_CMD
	 * @param hexstring
	 */
	public NC_CHAR_CLIENT_QUEST_READ_CMD(String hexstring) {
		super();

		this._qr = new QuestRead();

		this.setBufferByHexString(hexstring);
	}

	/**
	 * setBufferByHexString
	 * @param buffer
	 */
	public void setBufferByHexString(String buffer) {
		this._buffer = HexString.hexStringToByteArray(buffer);
		this._onReadBuffer();
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._qr;
	}
}
