/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.packet.chars;

import com.fjestacore.common.skill.PassivSkills;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_CLIENT_PASSIVE_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_CLIENT_PASSIVE_CMD)
public class NC_CHAR_CLIENT_PASSIVE_CMD extends Packet implements IPacket {

	/**
	 * skills
	 */
	protected PassivSkills _skills;

	/**
	 * CharacterPassiveSkillList
	 */
	public NC_CHAR_CLIENT_PASSIVE_CMD() {
		super();

		this._skills = new PassivSkills();
	}

	/**
	 * getPassivSkills
	 * @return
	 */
	public PassivSkills getPassivSkills() {
		return this._skills;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._skills;
	}
}