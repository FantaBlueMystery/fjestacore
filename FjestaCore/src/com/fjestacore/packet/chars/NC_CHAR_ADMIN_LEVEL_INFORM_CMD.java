/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.packet.chars;

import com.fjestacore.common.character.CharAdminLevel;
import com.fjestacore.core.struct.AbstractStruct;
import com.fjestacore.nc.NcServer;
import com.fjestacore.packet.IPacket;
import com.fjestacore.packet.Packet;
import com.fjestacore.packet.PacketDefineServer;

/**
 * NC_CHAR_ADMIN_LEVEL_INFORM_CMD
 * @author FantaBlueMystery
 */
@PacketDefineServer(nc = NcServer.NC_CHAR_ADMIN_LEVEL_INFORM_CMD)
public class NC_CHAR_ADMIN_LEVEL_INFORM_CMD extends Packet implements IPacket {

	/**
	 * char admin level
	 */
	protected CharAdminLevel _cal = new CharAdminLevel();

	/**
	 * NC_CHAR_ADMIN_LEVEL_INFORM_CMD
	 */
	public NC_CHAR_ADMIN_LEVEL_INFORM_CMD() {
		super();
	}

	/**
	 * NC_CHAR_ADMIN_LEVEL_INFORM_CMD
	 * @param cal
	 */
	public NC_CHAR_ADMIN_LEVEL_INFORM_CMD(CharAdminLevel cal) {
		super();

		this._cal = cal;
	}

	/**
	 * getPacketObject
	 * @return
	 */
	@Override
	public AbstractStruct getPacketObject() {
		return this._cal;
	}
}