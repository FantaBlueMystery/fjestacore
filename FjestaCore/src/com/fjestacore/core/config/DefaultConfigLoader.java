/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DefaultConfigLoader
 * @author FantaBlueMystery
 */
public class DefaultConfigLoader extends ConfigLoader {

	/**
	 * DefaultConfigLoader
	 * @param pGlobalFile
	 * @throws java.io.FileNotFoundException
	 * @throws java.io.IOException
	 */
	public DefaultConfigLoader(String pGlobalFile) throws FileNotFoundException, IOException {
		this._global = (GlobalConfig) this.loadPropertiesFile(pGlobalFile, this._createProperties());
		this._global.setProperty(GlobalConfig.DATA_PATH, (new File(pGlobalFile)).getParent());
	}

	/**
	 * _createProperties
	 * @return
	 */
	protected Properties _createProperties() {
		return new GlobalConfig();
	}

	/**
	 * _loadPropertiesFile
	 * @param sfile
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Properties loadPropertiesFile(String sfile) throws FileNotFoundException, IOException {
		return this.loadPropertiesFile(sfile, new Properties());
	}

	/**
	 * _loadPropertiesFile
	 * @param sfile
	 * @param prop
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Properties loadPropertiesFile(String sfile, Properties prop) throws FileNotFoundException, IOException {
		return this.loadProperties(new File(sfile), prop);
	}

	/**
	 * loadProperties
	 * @param name
	 * @return
	 */
	@Override
	public Properties loadProperties(String name) {
		if( name.contains(ConfigLoader.CONFIG_GLOBAL) ) {
			return this._global;
		}

		try {
			return this.loadPropertiesFile(name);
		}
		catch( Exception ex ) {
			Logger.getLogger(DefaultConfigLoader.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

	/**
	 * loadProperties
	 * @param name
	 * @param prop
	 * @return
	 */
	@Override
	public Properties loadProperties(String name, Properties prop) {
		try {
			return this.loadPropertiesFile(name, prop);
		}
		catch( Exception ex ) {
			Logger.getLogger(DefaultConfigLoader.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

	/**
	 * loadProperties
	 * @param afile
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Override
	public Properties loadProperties(File afile) throws FileNotFoundException, IOException {
		return this.loadProperties(afile, new Properties());
	}

	/**
	 * loadProperties
	 * @param afile
	 * @param prop
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Override
	public Properties loadProperties(File afile, Properties prop) throws FileNotFoundException, IOException {
		if( !afile.exists() ) {
			throw new FileNotFoundException(afile.toString());
		}

		InputStream input = new FileInputStream(afile);

		prop.load(input);

		return prop;
	}
}