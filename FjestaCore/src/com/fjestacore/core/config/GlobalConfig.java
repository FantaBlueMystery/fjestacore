/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.config;

import java.util.Properties;

/**
 * GlobalConfig
 * global config properties
 * @author FantaBlueMystery
 */
public class GlobalConfig extends Properties {

	/**
	 * KEY data path to files and configs
	 */
	static public String DATA_PATH = "data_path";

	/**
	 * KEY debug enable/disable
	 */
	static public String DEBUG = "debug";

	/**
	 * KEY dump bytes to logger
	 */
	static public String DUMP = "dump";

	/**
	 * KEY client type which version is used internally
	 */
	static public String CLIENT_TYPE = "clientType";

	/**
	 * KEY client version shn which version is used internally
	 */
	static public String CLIENT_VERSION_SHN = "clientVersionShn";

	/**
	 * KEY client version crypt which version is used internally
	 */
	static public String CLIENT_VERSION_CRYPT = "clientVersionCrypt";

	/**
	 * getDataPath
	 * @return
	 */
	public String getDataPath() {
		return this.getProperty(GlobalConfig.DATA_PATH, "");
	}

	/**
	 * isDebug
	 * @return
	 */
	public boolean isDebug() {
		String strDebug = this.getProperty(GlobalConfig.DEBUG, "0");

		return (strDebug.compareTo("1") == 0);
	}

	/**
	 * isDump
	 * @return
	 */
	public boolean isDump() {
		String strDump = this.getProperty(GlobalConfig.DUMP, "0");

		return (strDump.compareTo("1") == 0);
	}

	/**
	 * getClientType
	 * @return
	 */
	public String getClientType() {
		return this.getProperty(GlobalConfig.CLIENT_TYPE, "GER");
	}

	/**
	 * getClientVersionShn
	 * @return
	 */
	public String getClientVersionShn() {
		return this.getProperty(GlobalConfig.CLIENT_VERSION_SHN, "20191210");
	}

	/**
	 * getClientVersionCrypt
	 * @return
	 */
	public String getClientVersionCrypt() {
		return this.getProperty(GlobalConfig.CLIENT_VERSION_CRYPT, "20191126");
	}

	/**
	 * _getPathByLoaction
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	protected String _getPathByLoaction(String key, String defaultValue) {
		String tmpPath = this.getProperty(key, defaultValue);

		if( tmpPath.startsWith("./") ) {
			tmpPath = tmpPath.replaceFirst("./", this.getDataPath() + "/");
		}

		return tmpPath;
	}
}