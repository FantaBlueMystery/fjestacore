/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * AConfigLoader
 * @author FantaBlueMystery
 */
public abstract class ConfigLoader {

	/**
	 * CONST
	 */
	static public String CONFIG_GLOBAL = "global";

	/**
	 * instance
	 */
	static private ConfigLoader _instance = null;

	/**
	 * getInstance
	 * @return
	 * @throws Exception
	 */
	static public ConfigLoader getInstance() throws Exception {
		if( ConfigLoader._instance == null ) {
			throw new Exception("Instance of configloader not found!");
		}

		return ConfigLoader._instance;
	}

	/**
	 * setInstance
	 * @param instance
	 */
	static public void setInstance(ConfigLoader instance) {
		ConfigLoader._instance = instance;
	}

	/**
	 * global properties
	 */
	protected GlobalConfig _global = new GlobalConfig();

	/**
	 * loadProperties
	 * return properties by name
	 * @param name
	 * @return
	 * @throws java.io.FileNotFoundException
	 * @throws java.io.IOException
	 */
	abstract public Properties loadProperties(String name) throws FileNotFoundException, IOException;

	/**
	 * loadProperties
	 * @param name
	 * @param prop
	 * @return
	 * @throws java.io.FileNotFoundException
	 * @throws java.io.IOException
	 */
	abstract public Properties loadProperties(String name, Properties prop) throws FileNotFoundException, IOException;

	/**
	 * loadProperties
	 * @param afile
	 * @return
	 * @throws java.io.FileNotFoundException
	 * @throws java.io.IOException
	 */
	abstract public Properties loadProperties(File afile) throws FileNotFoundException, IOException;

	/**
	 * loadProperties
	 * @param afile
	 * @param prop
	 * @return
	 * @throws java.io.FileNotFoundException
	 * @throws java.io.IOException
	 */
	abstract public Properties loadProperties(File afile, Properties prop) throws FileNotFoundException, IOException;

	/**
	 * loadProperties
	 * return global properties
	 * @return
	 */
	public Properties loadProperties() {
		return this._global;
	}
}