/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.convert;

import java.util.Arrays;

/**
 * ConvertBytes
 * @author FantaBlueMystery
 */
public class ConvertBytes {

	/**
	 * shortToBytes
	 * @param value
	 * @return
	 */
	static public byte[] shortToBytes(int value) {
		byte[] returnByteArray = new byte[2];

		returnByteArray[0] = (byte) (value & 0xff);
		returnByteArray[1] = (byte) ((value >>> 8) & 0xff);

		return returnByteArray;
	}

	/**
	 * shortToBytes
	 * @param value
	 * @return
	 */
	static public byte[] uShortToBytes(int value) {
		byte[] returnByteArray = new byte[2];

		returnByteArray[0] = (byte) (value & 0xff);
		returnByteArray[1] = (byte) ((value >>> 8) & 0xff);

		return returnByteArray;
	}

	/**
	 * copyBytes
	 * @param bytes
	 * @param offset
	 * @param len
	 * @return
	 */
	static public byte[] copyBytes(byte[] bytes, int offset, int len) {
		int tpos = offset+len;

		if( tpos > bytes.length ) {
			throw new IllegalArgumentException("offset + len > bytes.length");
		}

		byte[] tbytes = new byte[len];

		int toffset = 0;

		for( int i=offset; i<tpos; i++ ) {
			tbytes[toffset] = bytes[i];
			toffset++;
		}

		return tbytes;
	}

	/**
	 * bytesToUnsignedShort
	 * @param tbytes
	 * @param bigEndian
	 * @return
	 */
	static public int bytesToUnsignedShort(byte[] tbytes, boolean bigEndian) {
		return ConvertBytes.bytesToUnsignedShort(tbytes[0], tbytes[1], bigEndian);
	}

	/**
	 * bytesToUnsignedShort
	 * @param byte1
	 * @param byte2
	 * @param bigEndian
	 * @return
	 */
	static public int bytesToUnsignedShort(byte byte1, byte byte2, boolean bigEndian) {
		if( bigEndian ) {
			return ( ( (byte1 & 0xFF) << 8) | (byte2 & 0xFF) );
		}

		return ( ( (byte2 & 0xFF) << 8) | (byte1 & 0xFF) );
	}

	/**
	 * intToBytes
	 * @param data
	 * @return
	 */
	static public byte[] intToBytes(final int data) {
		return new byte[] {
			(byte)((data >> 24) & 0xff),
			(byte)((data >> 16) & 0xff),
			(byte)((data >> 8) & 0xff),
			(byte)((data >> 0) & 0xff)
			};
	}

	/**
	 * uIntToBytes
	 * @param value
	 * @param bigEndian
	 * @return
	 */
	static public byte[] uIntToBytes(long value, boolean bigEndian) {
		byte[] byteArray = new byte[4];
        int shift;

		for( int i=0; i<byteArray.length; i++ ) {
            if( bigEndian ) {
				shift = (byteArray.length - 1 - i) * 8; // 24, 16, 8, 0
			}
			else {
                shift = i * 8; // 0,8,16,24
			}

            byteArray[i] = (byte) (value >>> shift);
        }

        return byteArray;
	}

	/**
	 * uLongToBytes
	 * @param value
	 * @param bigEndian
	 * @return
	 */
	static public byte[] uLongToBytes(long value, boolean bigEndian) {
		byte[] byteArray = new byte[8];
        int shift;

		for( int i=0; i<byteArray.length; i++ ) {
            if( bigEndian ) {
				shift = (byteArray.length - 1 - i) * 8; // 24, 16, 8, 0
			}
			else {
                shift = i * 8; // 0,8,16,24
			}

            byteArray[i] = (byte) (value >>> shift);
        }

        return byteArray;
	}

	/**
	 * charArrayToByteArray
	 * @param c_array
	 * @return
	 */
	static public byte[] charArrayToByteArray(char[] c_array) {
        byte[] b_array = new byte[c_array.length];

        for( int i= 0; i < c_array.length; i++ ) {
            b_array[i] = (byte)(0xFF & (int)c_array[i]);
        }

        return b_array;
	}

	/**
	 * byteArrayToCharArray
	 * @param b_array
	 * @return
	 */
	static public char[] byteArrayToCharArray(byte[] b_array) {
		char[] c_array = new char[b_array.length];

		for( int i= 0; i < b_array.length; i++ ) {
            c_array[i] = (char)((int)b_array[i] & 0xFF);
        }

        return c_array;
	}

	/**
	 * convertNegativShort
	 * @param ashort
	 * @return
	 */
	static public int convertNegativShort(int ashort) {
		if( ashort < 0 ) {
			return ((32767 - Math.abs(ashort)) + 32767 + 2);
		}

		return ashort;
	}

	/**
	 * reverse
	 * @param data
	 * @return
	 */
	static public byte[] reverse(byte[] data) {
		byte[] array = Arrays.copyOf(data, data.length);

		if( array == null ) {
			return new byte[]{};
		}

		int i = 0;
		int j = array.length - 1;
		byte tmp;

		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}

		return array;
	}

	/**
	 * floatToByteArray
	 * @param value
	 * @return
	 */
	static public byte[] floatToByteArray(float value) {
		int intBits =  Float.floatToIntBits(value);

		return new byte[] {
			(byte) (intBits >> 24), (byte) (intBits >> 16), (byte) (intBits >> 8), (byte) (intBits) };
	}

	/**
	 * byteArrayToFloat
	 * @param bytes
	 * @return
	 */
	static public float byteArrayToFloat(byte[] bytes) {
		int intBits = bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);

		return Float.intBitsToFloat(intBits);
	}
}