/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.convert;

/**
 * HexString
 * @author FantaBlueMystery
 */
public class HexString {

	/**
	 * fromHexString
	 * @param hex
	 * @return
	 */
	public static String fromHexString(String hex) {
		StringBuilder b=new StringBuilder(hex.length()/2);

		for( int i=0; i<hex.length(); i+=2 ) {
			b.append((char)Integer.parseInt(hex.substring(i, i+2) , 16));
		}

		return b.toString();
	}

	/**
	 * byteArrayToHexString
	 * @param bArr
	 * @return
	 */
	static public String byteArrayToHexString(byte[] bArr) {
      StringBuilder sb = new StringBuilder();

		for (int i = 0; i < bArr.length; i++) {
			int unsigned = bArr[i] & 0xff;

			String t = Integer.toHexString((unsigned));

			if( t.length() == 1 ) {
				t = "0" + t;
			}

            sb.append(t);
		}

		return sb.toString();
	}

	/**
	 * hexStringToByteArray
	 * @param s
	 * @return
	 */
	static public byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];

		for( int i=0; i<len; i += 2 ) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
				+ Character.digit(s.charAt(i+1), 16));
		}

		return data;
	}

	/**
	 * toHexString
	 * @param text
	 * @return
	 */
	static public String toHexString(String text) {
		StringBuilder builder = new StringBuilder(text.length()*2);

		char c;

		for( int i=0; i<text.length(); i++ ) {
			builder.append( ((c=text.charAt(i))<(char)0x10 ? "0" : "") + Integer.toHexString(c));
		}

		return builder.toString();
	}

	/**
	 * javaIntToHex
	 * @param value
	 * @return
	 */
	static public String javaIntToHex(int value) {
		Integer tvalue = (value & 0xff);
		String t = Integer.toHexString(tvalue).toUpperCase();

		if( t.length() == 1 ) {
			t = "0" + t;
		}

		return t;
	}
}