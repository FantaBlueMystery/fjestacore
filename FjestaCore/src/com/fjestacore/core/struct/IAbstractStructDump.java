/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.struct;

/**
 * IAbstractStructDump
 * @author FantaBlueMystery
 */
public interface IAbstractStructDump {

	/**
	 * dump
	 * @return
	 */
	public String dump();

	/**
	 * dump
	 * @param deep
	 * @return
	 */
	public String dump(int deep);
}