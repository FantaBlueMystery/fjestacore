/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.struct;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;

/**
 * AbstractStruct
 * @author FantaBlueMystery
 */
public abstract class AbstractStruct {

	/**
	 * getSize
	 * @return
	 */
	public abstract int getSize();

	/**
	 * read
	 * @param buffer
	 * @throws IOException
	 */
	public void read(byte[] buffer) throws IOException {
		this.read(buffer, 0);
	}

	/**
	 * read
	 * @param buffer
	 * @param offset
	 * @throws IOException
	 */
	public void read(byte[] buffer, int offset) throws IOException {
		ReaderStream reader = new ReaderStream(buffer);

		reader.setOffset(offset);

		this.read(reader);

		if( !reader.isEOS() ) {
			throw new IOException(
				"Struct not ready read, position end by: " +
				Long.toString(reader.getOffset())
				);
		}
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	public abstract void read(ReaderStream reader) throws IOException;

	/**
	 * write
	 * @return
	 * @throws java.io.IOException
	 */
	public byte[] write() throws IOException {
		WriterStream writer = new WriterStream(this.getSize());

		this.write(writer);

		return writer.getBuffer();
	}

	/**
	 * write
	 * @param writer
	 * @throws java.io.IOException
	 */
	public abstract void write(WriterStream writer) throws IOException;
}