/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.net;

/**
 * SocketType
 * @author FantaBlueMystery
 */
public enum SocketType {
	UNKNOWN(0),
	SERVER(1),
	CLIENT(2);

	private int _value;

	/**
	 * SocketType
	 * @param value
	 */
	private SocketType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}