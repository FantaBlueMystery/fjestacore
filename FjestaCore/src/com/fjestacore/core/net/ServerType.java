/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.net;

/**
 * ServerType
 * @author FantaBlueMystery
 */
public enum ServerType {
	NONE(0),
	LOGIN(1),
	WORLD(2),
	ZONE(3);

	private int _value;

	/**
	 * ServerType
	 * @param value
	 */
	private ServerType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getServerType
	 * @param value
	 * @return
	 */
	static public ServerType getServerType(int value) {
		ServerType[] types = ServerType.values();

		for( ServerType type: types ) {
			if( type.getValue() == value ) {
				return type;
			}
		}

		return ServerType.NONE;
	}
}