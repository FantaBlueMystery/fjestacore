/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.utils;

import com.fjestacore.core.convert.HexString;
import java.security.MessageDigest;

/**
 * Md5Hash
 * @author FantaBlueMystery
 */
public class Md5Hash {

	/**
	 * getHash
	 * @param str
	 * @return
	 */
	static public String getHash(String str) {
		return Md5Hash.getHash(str.getBytes());
	}

	/**
	 * getHash
	 * @param bytes
	 * @return
	 */
	static public String getHash(byte[] bytes) {
		String hash = "";

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] bhash = md.digest(bytes);

			hash = HexString.byteArrayToHexString(bhash);
		}
		catch( Exception ex ) {

		}

		return hash;
	}
}