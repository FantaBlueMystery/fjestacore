/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.utils;

/**
 * Str
 * @author FantaBlueMystery
 */
public class Str {

	/**
	 * repeate
	 * @param i
	 * @param s
	 * @return
	 */
	public static String repeate(int i, String s) {
		StringBuilder sb = new StringBuilder();

		for( int j=0; j<i; j++ ) {
			sb.append(s);
		}

		return sb.toString();
	}
}