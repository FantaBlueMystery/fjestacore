/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.type;

import java.math.BigInteger;

/**
 * ULong
 * @author FantaBlueMystery
 */
public class ULong extends Number implements Comparable<ULong> {

	/**
     * Generated UID
     */
    static private final long serialVersionUID = -6821055240959745390L;

	/**
     * A constant holding the minimum value an <code>unsigned long</code> can
     * have, 0.
     */
    static public final BigInteger MIN_VALUE = BigInteger.ZERO;

    /**
     * A constant holding the maximum value an <code>unsigned long</code> can
     * have, 2<sup>64</sup>-1.
     */
    static public final BigInteger MAX_VALUE = new BigInteger("18446744073709551615");

	/**
     * A constant holding the maximum value + 1 an <code>signed long</code> can
     * have, 2<sup>63</sup>.
     */
    static public final BigInteger MAX_VALUE_LONG = new BigInteger("9223372036854775808");

	/**
     * A constant holding the minimum value an <code>unsigned long</code> can
     * have as ULong, 0.
     */
    public static final ULong MIN = ULong.valueOf(MIN_VALUE.longValue());

	/**
     * A constant holding the maximum value + 1 an <code>signed long</code> can
     * have as ULong, 2<sup>63</sup>.
     */
    public static final ULong MAX = ULong.valueOf(MAX_VALUE);

	/**
	 * valueOf
     * create an <code>unsigned long</code>
     *
	 * @param value
	 * @return
     * @throws NumberFormatException If <code>value</code> does not contain a
     *             parsable <code>unsigned long</code>.
     */
    static public ULong valueOf(String value) throws NumberFormatException {
        return new ULong(value);
    }

    /**
	 * valueOf
     * create an <code>unsigned long</code> by masking it with
     * <code>0xFFFFFFFFFFFFFFFF</code> i.e. <code>(long) -1</code> becomes
     * <code>(uint) 18446744073709551615</code>
	 * @param value
	 * @return
     */
    static public ULong valueOf(long value) {
        return new ULong(value);
    }

    /**
	 * valueOf
     * create an <code>unsigned long</code>
	 * @param value
	 * @return
     * @throws NumberFormatException If <code>value</code> is not in the range
     *             of an <code>unsigned long</code>
     */
    static public ULong valueOf(BigInteger value) throws NumberFormatException {
        return new ULong(value);
    }

	/**
	 * compare
	 * @param x
	 * @param y
	 * @return
	 */
	static public int compare(long x, long y) {
        x += Long.MIN_VALUE;
        y += Long.MIN_VALUE;

        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

	/**
     * the value modelling the content of this <code>unsigned long</code>
     */
    private final long _value;

	/**
	 * ULong
     * create an <code>unsigned long</code>
     * @throws NumberFormatException If <code>value</code> is not in the range
     *             of an <code>unsigned long</code>
     */
    private ULong(BigInteger value) throws NumberFormatException {
        if( (value.compareTo(MIN_VALUE) < 0) || (value.compareTo(MAX_VALUE) > 0) ) {
            throw new NumberFormatException();
		}
		else {
            this._value = value.longValue();
		}
    }

    /**
	 * ULong
     * create an <code>unsigned long</code> by masking it with
     * <code>0xFFFFFFFFFFFFFFFF</code> i.e. <code>(long) -1</code> becomes
     * <code>(uint) 18446744073709551615</code>
     */
    private ULong(long value) {
        this._value = value;
    }

    /**
	 * ULong
     * create an <code>unsigned long</code>
     * @throws NumberFormatException If <code>value</code> does not contain a
     *             parsable <code>unsigned long</code>.
     */
    private ULong(String value) throws NumberFormatException {
        if( value == null ) {
            throw new NumberFormatException("null");
		}

        int length = value.length();

        if( length == 0 ) {
            throw new NumberFormatException("Empty input string");
		}

        if( value.charAt(0) == '-' ) {
            throw new NumberFormatException(
                String.format("Illegal leading minus sign on unsigned string %s", value));
		}

        if( length <= 18 ) {
            this._value = Long.parseLong(value, 10);
            return;
        }

        final long first = Long.parseLong(value.substring(0, length - 1), 10);
        final int second = Character.digit(value.charAt(length - 1), 10);

        if( second < 0 ) {
            throw new NumberFormatException("Bad digit at end of " + value);
		}

        long result = first * 10 + second;

        if( ULong.compare(result, first) < 0 ) {
            throw new NumberFormatException(
                String.format("String value %s exceeds range of unsigned long", value));
		}

        this._value = result;
    }

	/**
	 * intValue
	 * @return
	 */
	@Override
	public int intValue() {
		return (int) this._value;
	}

	/**
	 * longValue
	 * @return
	 */
	@Override
	public long longValue() {
		return this._value;
	}

	/**
	 * floatValue
	 * @return
	 */
	@Override
	public float floatValue() {
		if( this._value < 0 ) {
            return ((float) (this._value & Long.MAX_VALUE)) + Long.MAX_VALUE;
		}
		else {
            return this._value;
		}
	}

	/**
	 * doubleValue
	 * @return
	 */
	@Override
	public double doubleValue() {
		if( this._value < 0 ) {
            return ((double) (this._value & Long.MAX_VALUE)) + Long.MAX_VALUE;
		}
		else {
            return this._value;
		}
	}

	/**
	 * hashCode
	 * @return
	 */
	@Override
    public int hashCode() {
        return Long.valueOf(this._value).hashCode();
    }

	/**
	 * equals
	 * @param obj
	 * @return
	 */
    @Override
    public boolean equals(Object obj) {
        if( obj instanceof ULong ) {
            return this._value == ((ULong) obj)._value;
		}

        return false;
    }

	/**
	 * toString
	 * @return
	 */
    @Override
    public String toString() {
        if( this._value >= 0 ) {
            return Long.toString(this._value);
		}
		else {
            return BigInteger.valueOf(this._value & Long.MAX_VALUE).add(MAX_VALUE_LONG).toString();
		}
    }

	/**
	 * compareTo
	 * @param o
	 * @return
	 */
	@Override
	public int compareTo(ULong o) {
		return compare(this._value, o._value);
	}

	/**
	 * add
	 * @param val
	 * @return
	 * @throws NumberFormatException
	 */
	public ULong add(ULong val) throws NumberFormatException {
        if( this._value < 0 && val._value < 0 ) {
            throw new NumberFormatException();
		}

        final long result = this._value + val._value;

        if( (this._value < 0 || val._value < 0) && result >= 0 ) {
            throw new NumberFormatException();
		}

        return ULong.valueOf(result);
    }

	/**
	 * add
	 * @param val
	 * @return
	 * @throws NumberFormatException
	 */
    public ULong add(int val) throws NumberFormatException {
        return this.add((long) val);
    }

	/**
	 * add
	 * @param val
	 * @return
	 * @throws NumberFormatException
	 */
    public ULong add(long val) throws NumberFormatException {
        if( val < 0 ) {
            return subtract(Math.abs(val));
		}

        final long result = this._value + val;

        if( this._value < 0 && result >= 0 ) {
            throw new NumberFormatException();
		}

        return ULong.valueOf(result);
    }

	/**
	 * subtract
	 * @param val
	 * @return
	 */
    public ULong subtract(final ULong val) {
        if( this.compareTo(val) < 0 ) {
            throw new NumberFormatException();
		}

        final long result = this._value - val._value;

        if( this._value < 0 && result >= 0 ) {
            throw new NumberFormatException();
		}

        return ULong.valueOf(result);
    }

	/**
	 * subtract
	 * @param val
	 * @return
	 */
    public ULong subtract(final int val) {
        return this.subtract((long) val);
    }

	/**
	 * subtract
	 * @param val
	 * @return
	 */
    public ULong subtract(final long val) {
        if( val < 0 ) {
            return this.add(-val);
		}

        if( ULong.compare(this._value, val) < 0 ) {
            throw new NumberFormatException();
		}

        final long result = this._value - val;

        if( this._value < 0 && result >= 0 ) {
            throw new NumberFormatException();
		}

        return ULong.valueOf(result);
    }

	/**
	 * toBigInteger
     * get this number as a {@link BigInteger}. This is a convenience method for
     * calling <code>new BigInteger(toString())</code>
	 * @return
     */
    public BigInteger toBigInteger() {
        return new BigInteger(this.toString());
    }
}