/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.type;

import java.math.BigInteger;

/**
 * Unsigned
 * @author FantaBlueMystery
 */
public class Unsigned {

	/**
	 * ulong
     * create an <code>unsigned long</code>
	 * @param value
	 * @return
     * @throws NumberFormatException If <code>value</code> does not contain a
     *             parsable <code>unsigned long</code>.
     * @see ULong#valueOf(String)
     */
    public static ULong ulong(String value) throws NumberFormatException {
        return value == null ? null : ULong.valueOf(value);
    }

    /**
	 * ulong
     * create an <code>unsigned long</code> by masking it with
     * <code>0xFFFFFFFFFFFFFFFF</code> i.e. <code>(long) -1</code> becomes
     * <code>(uint) 18446744073709551615</code>
     *
	 * @param value
	 * @return
     * @see ULong#valueOf(long)
     */
    public static ULong ulong(long value) {
        return ULong.valueOf(value);
    }

    /**
	 * ulong
     * create an <code>unsigned long</code>
	 * @param value
	 * @return
     * @throws NumberFormatException If <code>value</code> is not in the range
     *             of an <code>unsigned long</code>
     * @see ULong#valueOf(BigInteger)
     */
    public static ULong ulong(BigInteger value) throws NumberFormatException {
        return ULong.valueOf(value);
    }
}