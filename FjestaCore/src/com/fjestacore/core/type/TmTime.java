/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.type;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * TmTime
 * @author FantaBlueMystery
 */
public class TmTime extends AbstractStruct {

	/**
	 * sec
	 * seconds,  range 0 to 59
	 */
	protected int _sec = 0;

	/**
	 * min
	 * minutes, range 0 to 59
	 */
	protected int _min = 0;

	/**
	 * hour
	 * hours, range 0 to 23
	 */
	protected int _hour = 0;

	/**
	 * day in mon
	 * day of the month, range 1 to 31
	 */
	protected int _mday = 0;

	/**
	 * mon
	 * month, range 0 to 11
	 */
	protected int _mon = 0;

	/**
	 * year
	 * The number of years since 1900
	 */
	protected int _year = 0;

	/**
	 * wday
	 * day of the week, range 0 to 6
	 */
	protected int _wday = 0;

	/**
	 * yday
	 * day in the year, range 0 to 365
	 */
	protected int _yday = 0;

	/**
	 * isDst
	 * daylight saving time
	 */
	protected int _isDst = 0;

	/**
	 * TmTime
	 */
	public TmTime() {
		LocalDateTime now = LocalDateTime.now();

		this._sec	= now.getSecond();
		this._min	= now.getMinute();
		this._hour	= now.getHour();
		this._mday	= now.getDayOfMonth();
		this._mon	= now.getMonthValue();
		this._year	= now.getYear();
		this._yday	= now.getDayOfYear();
		this._wday	= now.getDayOfWeek().getValue();
	}

	/**
	 * setSec
	 * @param sec
	 */
	public void setSec(int sec) {
		this._sec = sec;
	}

	/**
	 * getSec
	 * @return
	 */
	public int getSec() {
		return this._sec;
	}

	/**
	 * setMin
	 * @param min
	 */
	public void setMin(int min) {
		this._min = min;
	}

	/**
	 * getMin
	 * @return
	 */
	public int getMin() {
		return this._min;
	}

	/**
	 * hour
	 * @param hour
	 */
	public void setHour(int hour) {
		this._hour = hour;
	}

	/**
	 * getHour
	 * @return
	 */
	public int getHour() {
		return this._hour;
	}

	/**
	 * setMDay
	 * @param day
	 */
	public void setMDay(int day) {
		this._mday = day;
	}

	/**
	 * getMDay
	 * @return
	 */
	public int getMDay() {
		return this._mday;
	}

	/**
	 * setMon
	 * @param mon
	 */
	public void setMon(int mon) {
		this._mon = mon;
	}

	/**
	 * getMon
	 * @return
	 */
	public int getMon() {
		return this._mon;
	}

	/**
	 * setYear
	 * @param year
	 */
	public void setYear(int year) {
		this._year = year;
	}

	/**
	 * getYear
	 * @return
	 */
	public int getYear() {
		return this._year;
	}

	/**
	 * getCalendar
	 * @return
	 */
	public Calendar getCalendar() {
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(this._year, this._mon, this._mday, this._hour, this._min, this._sec);

		return cal;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._sec	= (int) reader.readUInt();
		this._min	= (int) reader.readUInt();
		this._hour	= (int) reader.readUInt();
		this._mday	= (int) reader.readUInt();
		this._mon	= (int) reader.readUInt();
		this._year	= (int) reader.readUInt();
		this._wday	= (int) reader.readUInt();
		this._yday	= (int) reader.readUInt();
		this._isDst	= (int) reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		writer.writeUInt(this._sec);
		writer.writeUInt(this._min);
		writer.writeUInt(this._hour);
		writer.writeUInt(this._mday);
		writer.writeUInt(this._mon);
		writer.writeUInt(this._year);
		writer.writeUInt(this._wday);
		writer.writeUInt(this._yday);
		writer.writeUInt(this._isDst);
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return 4*9;
	}
}