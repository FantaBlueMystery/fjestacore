/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */

package com.fjestacore.core.crypt;

import com.fjestacore.client.ClientType;
import com.fjestacore.client.ClientVersion;

/**
 * CryptNotFoundException
 * @author FantaBlueMystery
 */
public class CryptNotFoundException extends Exception {

	/**
	 * client version
	 */
	protected ClientVersion _cv = null;

	/**
	 * client type
	 */
	protected ClientType _ct = null;

	/**
	 * CryptNotFoundException
	 */
	public CryptNotFoundException() {
		super();
	}

	/**
	 * CryptNotFoundException
	 * @param msg
	 */
	public CryptNotFoundException(String msg) {
		super(msg);
	}

	/**
	 * CryptNotFoundException
	 * @param cv
	 * @param ct
	 */
	public CryptNotFoundException(ClientVersion cv, ClientType ct) {
		super("Crypt not found ClientVersion: " + cv.getValue() + " ClientType: " + ct.name());
		
		this._cv = cv;
		this._ct = ct;
	}
}