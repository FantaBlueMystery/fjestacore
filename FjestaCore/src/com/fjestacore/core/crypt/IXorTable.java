/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

/**
 * IXorTable
 * @author FantaBlueMystery
 */
public interface IXorTable {

	/**
	 * isTableInit
	 * @return
	 */
	public boolean isTableInit();

	/**
	 * initTable
	 * @param table
	 * @param vector
	 */
	public void initTable(byte[] table, int vector);
}