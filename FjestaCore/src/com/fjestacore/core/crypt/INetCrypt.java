/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

/**
 * INetCrypt
 * @author FantaBlueMystery
 */
public interface INetCrypt {

	/**
	 * setOffset
	 * @param offset
	 */
	public void setOffset(int offset);

	/**
	 * isOffsetSet
	 * @return
	 */
	public boolean isOffsetSet();

	/**
	 * crypt
	 * @param buffer
	 * @return
	 */
	public byte[] crypt(byte[] buffer);
}