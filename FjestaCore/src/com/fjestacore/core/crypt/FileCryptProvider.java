/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

import com.fjestacore.client.ClientType;
import com.fjestacore.client.ClientVersion;
import com.fjestacore.core.config.ConfigLoader;
import com.fjestacore.core.config.GlobalConfig;
import com.fjestacore.core.crypt.file.FileCryptDefault;
import java.lang.annotation.Annotation;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FileCryptProvider
 * @author FantaBlueMystery
 */
public class FileCryptProvider {

	/**
	 * crypt register
	 */
	static protected Class[] _cryptRegister = {
		FileCryptDefault.class
		};

	/**
	 * getCrypt
	 * @param cv
	 * @param ct
	 * @return
	 */
	static public IFileCrypt getCrypt(ClientVersion cv, ClientType ct) {
		for( Class aclass: FileCryptProvider._cryptRegister ) {
			Annotation[] ans = aclass.getAnnotations();

			for( Annotation an : ans ) {
				if( an instanceof CryptDefine ) {
					CryptDefine cd = (CryptDefine) an;

					if( cd.byClientType() == ct ) {
						if( (ClientVersion.compareTo(cv, cd.fromClientVersion()) >= 0) &&
							(ClientVersion.compareTo(cd.toClientVersion(), cv) >= 0) )
						{
							try {
								return (IFileCrypt) aclass.newInstance();
							}
							catch( InstantiationException | IllegalAccessException ex ) {
								Logger.getLogger(FileCryptProvider.class.getName()).log(Level.SEVERE, null, ex);
							}
						}
					}
				}
			}
		}

		return null;
	}

	/**
	 * getCrypt
	 * @return
	 * @throws com.fjestacore.core.crypt.CryptNotFoundException
	 * @throws java.lang.Exception
	 */
	static public IFileCrypt getCrypt() throws CryptNotFoundException, Exception {
		GlobalConfig prop = (GlobalConfig) ConfigLoader.getInstance().loadProperties();

		ClientVersion cv = ClientVersion.getByString(prop.getClientVersionCrypt());
		ClientType ct = ClientType.valueOf(prop.getClientType());

		IFileCrypt fc = FileCryptProvider.getCrypt(cv, ct);

		if( fc == null ) {
			throw new CryptNotFoundException(cv, ct);
		}

		return fc;
	}
}