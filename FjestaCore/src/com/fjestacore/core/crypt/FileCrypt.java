/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

import com.fjestacore.core.crypt.config.CryptConfigLoader;
import java.lang.annotation.Annotation;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FileCrypt
 * @author FantaBlueMystery
 */
public abstract class FileCrypt implements IFileCrypt, IXorTable {

	/**
	 * FileCrypt
	 */
	public FileCrypt() {
		Annotation[] ans = this.getClass().getAnnotations();

		for( Annotation an : ans ) {
			if( an instanceof CryptDefine ) {
				CryptDefine cd = (CryptDefine) an;

				try {
					if( this instanceof IXorTable ) {
						IXorTable xtself = (IXorTable) this;

						if( !xtself.isTableInit() ) {
							CryptConfigLoader config = new CryptConfigLoader(
								cd.byClientType(), cd.fromClientVersion());

							byte[] table = config.getConfig().getFileTable();

							xtself.initTable(table, 0);
						}
					}
				}
				catch( Exception ex ) {
					Logger.getLogger(NetCrypt.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	/**
	 * getStaticTable
	 * @return
	 */
	abstract public byte[] getStaticTable();
}