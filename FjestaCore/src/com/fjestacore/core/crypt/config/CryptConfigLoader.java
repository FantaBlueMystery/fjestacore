/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt.config;

import com.fjestacore.client.ClientType;
import com.fjestacore.client.ClientVersion;
import com.fjestacore.core.config.ConfigLoader;
import com.fjestacore.core.config.GlobalConfig;

/**
 * CryptConfigLoader
 * @author FantaBlueMystery
 */
public final class CryptConfigLoader {

	static public String PROPERTY_FILE	= "crypt.properties";

	static public String P_KEY_NET_VECTOR	= "net_vector";
	static public String P_KEY_NET_TABLE	= "net_table";
	static public String P_KEY_FILE_TABLE	= "file_table";

	/**
	 * properties
	 */
	protected CryptConfig _prop = null;

	/**
	 * CryptConfigLoader
	 * @throws Exception
	 */
	public CryptConfigLoader() throws Exception {
		GlobalConfig prop = (GlobalConfig) ConfigLoader.getInstance().loadProperties();

		this._loadPropertiesFile(
			prop.getDataPath(),
			prop.getClientType(),
			prop.getClientVersionCrypt()
			);
	}

	/**
	 * CryptConfigLoader
	 * @param type
	 * @param version
	 * @throws Exception
	 */
	public CryptConfigLoader(ClientType type, ClientVersion version) throws Exception {
		GlobalConfig prop = (GlobalConfig) ConfigLoader.getInstance().loadProperties();

		this._loadPropertiesFile(
			prop.getDataPath(),
			type.name(),
			version.getValue()
			);
	}

	/**
	 * _loadPropertiesFile
	 * @param dataPath
	 * @param type
	 * @param version
	 * @throws Exception
	 */
	protected void _loadPropertiesFile(String dataPath, String type, String version) throws Exception {
		String cryptfiel =
			dataPath + "/" +
			type + "/" +
			version + "/" +
			CryptConfigLoader.PROPERTY_FILE;

		this._prop = (CryptConfig) ConfigLoader.getInstance().loadProperties(cryptfiel, new CryptConfig());
	}

	/**
	 * getConfig
	 * @return
	 */
	public CryptConfig getConfig() {
		return this._prop;
	}
}