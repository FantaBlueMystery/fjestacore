/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt.config;

import com.fjestacore.core.convert.HexString;
import java.util.Properties;

/**
 * CryptConfig
 * @author FantaBlueMystery
 */
public class CryptConfig extends Properties {

	/**
	 * KEY network crypt vector
	 */
	static public String NET_VECTOR = "net_vector";

	/**
	 * KEY network crypt table (hexstring)
	 */
	static public String NET_TABLE = "net_table";

	/**
	 * KEY file crypt table (hexstring)
	 */
	static public String FILE_TABLE = "file_table";

	/**
	 * getNetVector
	 * @return
	 */
	public int getNetVector() {
		String netVector = this.getProperty(CryptConfig.NET_VECTOR, "0");

		return Integer.valueOf(netVector);
	}

	/**
	 * getNetTable
	 * @return
	 */
	public byte[] getNetTable() {
		String netTable = this.getProperty(CryptConfig.NET_TABLE, "");

		return HexString.hexStringToByteArray(netTable);
	}

	/**
	 * getFileTable
	 * @return
	 */
	public byte[] getFileTable() {
		String fileTable = this.getProperty(CryptConfig.FILE_TABLE, "");

		return HexString.hexStringToByteArray(fileTable);
	}
}