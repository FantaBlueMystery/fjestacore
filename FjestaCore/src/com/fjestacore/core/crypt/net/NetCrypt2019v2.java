/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt.net;

import com.fjestacore.core.crypt.NetCrypt;
import com.fjestacore.core.crypt.CryptDefine;
import com.fjestacore.core.crypt.IXorTable;

/**
 * NetCrypt2019v2
 * @author FantaBlueMystery
 */
@CryptDefine()
public class NetCrypt2019v2 extends NetCrypt implements IXorTable {

	/**
	 * table
	 */
	static protected int[] _table = null;

	/**
	 * isTableInit
	 * @return
	 */
	@Override
	public boolean isTableInit() {
		return NetCrypt2019v2._table != null;
	}

	/**
	 * initTable
	 * @param table
	 * @param vector
	 */
	@Override
	public void initTable(byte[] table, int vector) {
		int indx = 0;

		NetCrypt2019v2._table = new int[table.length];

		do
		{
			NetCrypt2019v2._table[indx] = table[indx];
			++indx;
		}
		while ( indx < table.length );
	}

	/**
	 * getStaticTable
	 * @return
	 */
	@Override
	public int[] getStaticTable() {
		return NetCrypt2019v2._table;
	}

	/**
	 * crypt
	 * @param buffer
	 * @return
	 */
	@Override
	public byte[] crypt(byte[] buffer) {
		int[] tab = this.getStaticTable();
		int tsize = tab.length;

		byte[] rbuffer = new byte[buffer.length];

		int len		= buffer.length;
		int offset	= 0;

		do {
			int xorByte = tab[this._offset];

			byte tbyte = (byte)(0xff & ((int)buffer[offset] & 0xff) ^ (xorByte & 0xff));

			this._offset++;

			rbuffer[offset] = tbyte;

			if( this._offset >= tsize ) {
				this._offset = 0;
			}

			offset++;
		}
		while( offset < len );

		return rbuffer;
	}
}