/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

/**
 * IFileCrypt
 * @author FantaBlueMystery
 */
public interface IFileCrypt {

	/**
	 * crypt
	 * @param data
	 * @param index
	 * @param length
	 */
	public void crypt(byte[] data, int index, int length);
}