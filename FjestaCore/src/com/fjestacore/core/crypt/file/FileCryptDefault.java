/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt.file;

import com.fjestacore.core.crypt.CryptDefine;
import com.fjestacore.core.crypt.FileCrypt;

/**
 * FileCryptDefault
 * @author FantaBlueMystery
 */
@CryptDefine()
public class FileCryptDefault extends FileCrypt {

	/**
	 * xor table
	 */
	static protected byte _xorTable[] = null;

	/**
	 * isTableInit
	 * @return
	 */
	@Override
	public boolean isTableInit() {
		return FileCryptDefault._xorTable != null;
	}

	/**
	 * initTable
	 * @param table
	 * @param vector
	 */
	@Override
	public void initTable(byte[] table, int vector) {
		FileCryptDefault._xorTable = table;
	}

	/**
	 * getStaticTable
	 * @return
	 */
	@Override
	public byte[] getStaticTable() {
		return FileCryptDefault._xorTable;
	}

	/**
	 * crypt
	 * @param data
	 * @param index
	 * @param length
	 */
	@Override
	public void crypt(byte[] data, int index, int length) {
		if( ((index < 0) || (length < 1)) || ((index + length) > data.length) ) {
			throw new IndexOutOfBoundsException();
		}

		byte[] xorTable = FileCryptDefault._xorTable;
		byte num = (byte)length;

		for( int i = length - 1; i >= 0; i--) {
			data[i] = (byte)(data[i] ^ num);

			byte num3 = (byte)i;

			num3 = (byte)(num3 & xorTable[0]);
			num3 = (byte)(num3 + xorTable[1]);
			num3 = (byte)(num3 ^ ((byte)(((byte)i) * xorTable[2])));
			num3 = (byte)(num3 ^ num);
			num3 = (byte)(num3 ^ xorTable[3]);

			num = num3;
		}
	}
}