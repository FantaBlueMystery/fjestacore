/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

import com.fjestacore.client.ClientType;
import com.fjestacore.client.ClientVersion;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * CryptDefine
 * @author FantaBlueMystery
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CryptDefine {

	/**
	 * type of client (GER, US ..)
	 * @return
	 */
	ClientType byClientType() default ClientType.GER;

	/**
	 * form client version
	 * @return
	 */
	ClientVersion fromClientVersion() default ClientVersion.VERSION_20191126;

	/**
	 * to client version
	 * @return
	 */
	ClientVersion toClientVersion() default ClientVersion.VERSION_20200428;
}