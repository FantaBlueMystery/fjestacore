/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

import com.fjestacore.core.crypt.config.CryptConfigLoader;
import java.lang.annotation.Annotation;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * NetCrypt
 * @author FantaBlueMystery
 */
public abstract class NetCrypt implements INetCrypt {

	/**
	 * offset
	 */
	protected int _offset = 0;

	/**
	 * is offset set
	 */
	protected boolean _isOffsetSet = false;

	/**
	 * ANetCrypt
	 */
	public NetCrypt() {
		Annotation[] ans = this.getClass().getAnnotations();

		for( Annotation an : ans ) {
			if( an instanceof CryptDefine ) {
				CryptDefine cd = (CryptDefine) an;

				try {
					if( this instanceof IXorTable ) {
						IXorTable xtself = (IXorTable) this;

						if( !xtself.isTableInit() ) {
							CryptConfigLoader config = new CryptConfigLoader(
								cd.byClientType(), cd.fromClientVersion());

							int vector = config.getConfig().getNetVector();
							byte[] table = config.getConfig().getNetTable();

							xtself.initTable(table, vector);
						}
					}
				}
				catch( Exception ex ) {
					Logger.getLogger(NetCrypt.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	/**
	 * getStaticTable
	 * @return
	 */
	abstract public int[] getStaticTable();

	/**
	 * setOffset
	 * @param offset
	 */
	@Override
	public void setOffset(int offset) {
		int tsize = this.getStaticTable().length;

		while( offset >= tsize ) {
            if(offset >= tsize) {
                offset = offset - tsize;
            }
        }

		this._offset = offset;
		this._isOffsetSet = true;
	}

	/**
	 * isOffsetSet
	 * @return
	 */
	@Override
	public boolean isOffsetSet() {
		return this._isOffsetSet;
	}

	/**
	 * getOffset
	 * @return
	 */
	public int getOffset() {
		return this._offset;
	}

	/**
	 * getTableByte
	 * @param offset
	 * @return
	 */
	public int getTableByte(int offset) {
		int[] tab = this.getStaticTable();
		int tsize = tab.length;

		while( offset >= tsize ) {
            if(offset >= tsize) {
                offset = offset - tsize;
            }
        }

		return tab[offset];
	}
}