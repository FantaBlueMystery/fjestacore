/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.crypt;

import com.fjestacore.client.ClientType;
import com.fjestacore.client.ClientVersion;
import com.fjestacore.core.config.ConfigLoader;
import com.fjestacore.core.config.GlobalConfig;
import com.fjestacore.core.crypt.net.NetCrypt2019v2;
import java.lang.annotation.Annotation;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * NetCryptProvider
 * @author FantaBlueMystery
 */
public class NetCryptProvider {

	/**
	 * crypt register
	 */
	static protected Class[] _cryptRegister = {
		NetCrypt2019v2.class
		};

	/**
	 * getCrypt
	 * @param cv
	 * @param ct
	 * @return
	 */
	static public INetCrypt getCrypt(ClientVersion cv, ClientType ct) {
		for( Class aclass: NetCryptProvider._cryptRegister ) {
			Annotation[] ans = aclass.getAnnotations();

			for( Annotation an : ans ) {
				if( an instanceof CryptDefine ) {
					CryptDefine cd = (CryptDefine) an;

					if( cd.byClientType() == ct ) {
						if( (ClientVersion.compareTo(cv, cd.fromClientVersion()) >= 0) &&
							(ClientVersion.compareTo(cd.toClientVersion(), cv) >= 0) )
						{
							try {
								return (INetCrypt) aclass.newInstance();
							}
							catch( InstantiationException | IllegalAccessException ex ) {
								Logger.getLogger(NetCryptProvider.class.getName()).log(Level.SEVERE, null, ex);
							}
						}
					}
				}
			}
		}

		return null;
	}

	/**
	 * getCrypt
	 * @return
	 * @throws java.lang.Exception
	 */
	static public INetCrypt getCrypt() throws Exception {
		GlobalConfig prop = (GlobalConfig) ConfigLoader.getInstance().loadProperties();

		ClientVersion cv = ClientVersion.getByString(prop.getClientVersionCrypt());
		ClientType ct = ClientType.valueOf(prop.getClientType());

		return NetCryptProvider.getCrypt(cv, ct);
	}
}