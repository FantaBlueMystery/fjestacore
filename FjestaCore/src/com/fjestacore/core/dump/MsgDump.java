/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.dump;

import com.fjestacore.core.utils.Str;

/**
 * MsgDump
 * @author FantaBlueMystery
 */
public class MsgDump {

	/**
	 * getMsgDump
	 * @param deep
	 * @param msg
	 * @return
	 */
	static public String getMsgDump(int deep, String msg) {
		String dump = "";
		String padding = "";

		if( deep > 0 ) {
			padding = Str.repeate(deep, " ");
		}

		dump += padding + "|-> ";
		dump += msg;
		dump += "\r\n";

		return dump;
	}
}