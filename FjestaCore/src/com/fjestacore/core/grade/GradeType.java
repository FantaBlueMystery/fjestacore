/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.grade;

/**
 * GradeType
 * @author FantaBlueMystery
 */
public enum GradeType {
	GT_NORMAL(0x0),
	GT_NAMED(0x1),
	GT_RARE(0x2),
	GT_UNIQUE(0x3),
	GT_CHARGE(0x4),
	GT_SET(0x5),
	GT_LEGENDARY(0x6),
	GT_MYTHIC(0x7),
	MAX_GRADETYPE(0x8);

	/**
	 * value
	 */
	private int _value;

	/**
	 * GradeType
	 * @param value
	 */
	private GradeType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * getGradeType
	 * @param value
	 * @return
	 */
	static public GradeType getGradeType(int value) {
		GradeType[] types = GradeType.values();

		for( GradeType atype: types ) {
			if( atype.getValue() == value ) {
				return atype;
			}
		}

		return GradeType.GT_NORMAL;
	}
}