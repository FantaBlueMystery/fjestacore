/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.list;

/**
 * MKey2
 * @author FantaBlueMystery
 * @param <K1>
 * @param <K2>
 */
public class MKey2<K1, K2> {

	protected K1 _key1;
	protected K2 _key2;

	/**
	 * MKey2
	 * @param key1
	 * @param key2
	 */
	public MKey2(K1 key1, K2 key2) {
		this._key1 = key1;
		this._key2 = key2;
	}

	/**
	 * getKey
	 * @return
	 */
	public K1 getKey() {
		return this._key1;
	}

	/**
	 * getKey2
	 * @return
	 */
	public K2 getKey2() {
		return this._key2;
	}

	/**
	 * equals
	 * @param o
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		if( this == o ) {
			return true;
		}

		if( (o == null) || (getClass() != o.getClass()) ) {
			return false;
		}

		MKey2 key = (MKey2) o;

		if( this._key1 != null ? !this._key1.equals(key._key1) : key._key1 != null ) {
			return false;
		}

		if( this._key2 != null ? !this._key2.equals(key._key2) : key._key2 != null ) {
			return false;
		}

		return true;
	}

	/**
	 * hashCode
	 * @return
	 */
	@Override
	public int hashCode() {
		int result = this._key1 != null ? this._key1.hashCode() : 0;
		result = 31 * result + (this._key2 != null ? this._key2.hashCode() : 0);

		return result;
	}

	/**
	 * toString
	 * @return
	 */
	@Override
	public String toString() {
		return "[" + this._key1 + ", " + this._key2 + "]";
	}
}