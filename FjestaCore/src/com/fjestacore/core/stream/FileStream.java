/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.stream;

import com.fjestacore.core.convert.HexString;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * FileStream
 * @author FantaBlueMystery
 */
public class FileStream {

	/**
	 * readHexString
	 * @param afile
	 * @return
	 * @throws java.lang.Exception
	 */
	static public String readHexStringByFile(File afile) throws Exception {
		if( !afile.exists() ) {
			throw new Exception("File not found: " + afile.toString());
		}

		FileInputStream input_stream = new FileInputStream(afile);

		String tdata = "";
		byte tchar = -1;

		do {
			if( tchar != -1 ) {
				tdata += (char)tchar;
			}

			tchar = (byte) input_stream.read();

		}
		while( tchar !=-1 );

		String[] tokens = tdata.split("\n");
		String cdata = "";

		for( String token : tokens ) {
			if( !token.startsWith("#") ) {
				cdata += token.replaceAll("\\s", "");
			}
		}

		return cdata;
	}

	/**
	 * readBytesByFile
	 * @param afile
	 * @param convertHex
	 * @return
	 * @throws Exception
	 */
	static public byte[] readBytesByFile(File afile, boolean convertHex) throws Exception {
		if( !afile.exists() ) {
			throw new Exception("File not found: " + afile.toString());
		}

		FileInputStream input_stream = new FileInputStream(afile);

		String tdata = "";
		byte tchar = -1;

		do {
			if( tchar != -1 ) {
				tdata += (char)tchar;
			}

			tchar = (byte) input_stream.read();

		}
		while( tchar !=-1 );

		String[] tokens = tdata.split("\n");
		String cdata = "";

		for( String token : tokens ) {
			if( !token.startsWith("#") ) {
				cdata += token.replaceAll("\\s", "");
			}
		}

		if( convertHex ) {
			return HexString.hexStringToByteArray(cdata);
		}

		return cdata.getBytes();
	}

	/**
	 * toFileInHexString
	 * @param bytes
	 * @param filename
	 * @throws java.io.FileNotFoundException
	 */
	static public void toFileInHexString(byte[] bytes, String filename) throws FileNotFoundException {
		PrintWriter out = new PrintWriter(filename);
		out.println(HexString.byteArrayToHexString(bytes));
		out.close();
	}
}