/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.stream;

import com.fjestacore.core.convert.ConvertBytes;
import com.fjestacore.core.dump.HexDump;
import com.fjestacore.core.type.ULong;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ReaderStream
 * @author FantaBlueMystery
 */
public class ReaderStream {

	/**
	 * encoding
	 */
	static public String ENCODING = "ISO-8859-1";

	/**
	 * inputstream
	 */
	protected ByteArrayInputStream _in = null;

	/**
	 * size
	 */
	protected int _size = 0;

	/**
	 * offset
	 */
	protected long _offset = 0;

	/**
	 * ReaderStream
	 * @param in
	 * @throws java.io.IOException
	 */
	public ReaderStream(InputStream in) throws IOException {
		if( in instanceof FileInputStream ) {
			this._size = in.available();

			byte[] _data = new byte[this._size];

			in.read(_data);

			this._in = new ByteArrayInputStream(_data);
		}
		else if( in instanceof ByteArrayInputStream ) {
			this._size	= in.available();
			this._in	= (ByteArrayInputStream) in;
		}
	}

	/**
	 * ReaderStream
	 * @param buffer
	 */
	public ReaderStream(byte[] buffer) {
		this._in = new ByteArrayInputStream(buffer);
		this._size	= buffer.length;
	}

	/**
	 * setOffset
	 * @param offset
	 */
	public void setOffset(long offset) {
		this._offset = offset;
	}

	/**
	 * getOffset
	 * @return
	 */
	public long getOffset() {
		return this._offset;
	}

	/**
	 * getSize
	 * @return
	 */
	public int getSize() {
		return this._size;
	}

	/**
	 * readBuffer
	 * @return
	 * @throws java.io.IOException
	 */
	public byte[] readBuffer() throws IOException {
		int len = (int) (this._size - this._offset);

		return this.readBuffer(this._offset, len);
	}

	/**
	 * readBuffer
	 * @param len
	 * @return
	 * @throws java.io.IOException
	 */
	public byte[] readBuffer(int len) throws IOException {
		return this.readBuffer(this._offset, len);
	}

	/**
	 * readBuffer
	 * @param offset
	 * @param len
	 * @return
	 * @throws java.io.IOException
	 */
	public byte[] readBuffer(long offset, int len) throws IOException {
		byte[] buffer = new byte[len];

		this._in.reset();
		this._in.skip(offset);
		this._in.read(buffer, 0, len);

		this._offset = this._offset + len;

		return buffer;
	}

	/**
	 * readRemaining
	 * @return
	 * @throws IOException
	 */
	public byte[] readRemaining() throws IOException {
		return this.readRemaining(this._offset);
	}

	/**
	 * readRemaining
	 * @param offset
	 * @return
	 * @throws IOException
	 */
	public byte[] readRemaining(long offset) throws IOException {
		long size = this._size - offset;

		if( size > 0 ) {
			return this.readBuffer(offset, (int) size);
		}

		return new byte[]{};
	}

	/**
	 * readChars
	 * @param len
	 * @return
	 * @throws java.io.IOException
	 */
	public char[] readChars(int len) throws IOException {
		return this.readChars(this._offset, len);
	}

	/**
	 * readChars
	 * @param offset
	 * @param len
	 * @return
	 * @throws java.io.IOException
	 */
	public char[] readChars(long offset, int len) throws IOException {
		byte[] _tbyte = this.readBuffer(offset, len);
		char[] _tchar = new char[_tbyte.length];

		for( int i=0; i<_tbyte.length; i++ ) {
			_tchar[i] = (char) _tbyte[i];
		}

		return _tchar;
	}

	/**
	 * readString
	 * @param len
	 * @return
	 * @throws java.io.IOException
	 */
	public String readString(int len) throws IOException {
		return this.readString(this._offset, len, ReaderStream.ENCODING);
	}

	/**
	 * readString
	 * @param offset
	 * @param len
	 * @return
	 * @throws java.io.IOException
	 */
	public String readString(long offset, int len) throws IOException {
		return new String(this.readBuffer(offset, len)).trim();
	}

	/**
	 * readString
	 * @param len
	 * @param charsetName
	 * @return
	 * @throws java.io.IOException
	 */
	public String readString(int len, String charsetName) throws IOException {
		return this.readString(this._offset, len, charsetName);
	}

	/**
	 * readString
	 * @param offset
	 * @param len
	 * @param charsetName
	 * @return
	 * @throws java.io.IOException
	 */
	public String readString(long offset, int len, String charsetName) throws IOException {
		return new String(this.readBuffer(offset, len), charsetName).trim();
	}


	/**
	 * readBoolean
	 * @return
	 * @throws java.io.IOException
	 */
	public boolean readBoolean() throws IOException {
		return this.readBoolean(this._offset);
	}

	/**
	 * readBoolean
	 * @param offset
	 * @return
	 * @throws java.io.IOException
	 */
	public boolean readBoolean(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 1);

		return (abyte[0] == 0x01);
	}

	/**
	 * readShort
	 * @return
	 * @throws java.io.IOException
	 */
	public int readShort() throws IOException {
		return this.readShort(this._offset);
	}

	/**
	 * readShort
	 * @param offset
	 * @return
	 * @throws java.io.IOException
	 */
	public int readShort(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 2);

		return ConvertBytes.bytesToUnsignedShort(abyte, false);
	}

	/**
	 * readByteInt
	 * @return
	 * @throws java.io.IOException
	 */
	public int readByteInt() throws IOException {
		return this.readByteInt(this._offset);
	}

	/**
	 * readByteInt
	 * @param offset
	 * @return
	 * @throws java.io.IOException
	 */
	public int readByteInt(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 1);

		return Byte.toUnsignedInt(abyte[0]);
	}

	/**
	 * readByte
	 * @return
	 * @throws IOException
	 */
	public byte readByte() throws IOException {
		return this.readByte(this._offset);
	}

	/**
	 * readByte
	 * @param offset
	 * @return
	 * @throws IOException
	 */
	public byte readByte(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 1);

		return abyte[0];
	}

	/**
	 * readUInt
	 * @return
	 * @throws java.io.IOException
	 */
	public long readUInt() throws IOException {
		return this.readUInt(this._offset);
	}

	/**
	 * readUInt
	 * @param offset
	 * @return
	 * @throws java.io.IOException
	 */
	public long readUInt(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 4);

		long value =
			((abyte[0] & 0xFF) <<  0) |
			((abyte[1] & 0xFF) <<  8) |
			((abyte[2] & 0xFF) << 16) |
			((abyte[3] & 0xFF) << 24);

		return value;
	}

	/**
	 * readInt
	 * @return
	 * @throws java.io.IOException
	 */
	public int readInt() throws IOException {
		return this.readInt(this._offset);
	}

	/**
	 * readInt
	 * @param offset
	 * @return
	 * @throws IOException
	 */
	public int readInt(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 4);

		return
			(abyte[0] << 24 |
			(abyte[1] & 0xFF) << 16 |
			(abyte[2] & 0xFF) << 8 |
			(abyte[3] & 0xFF));
	}

	/**
	 * readULong
	 * @return
	 * @throws IOException
	 */
	public ULong readULong() throws IOException {
		return this.readULong(this._offset);
	}

	/**
	 * readULong
	 * @param offset
	 * @return
	 * @throws IOException
	 */
	public ULong readULong(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 8);

		return ULong.valueOf(new BigInteger(1, ConvertBytes.reverse(abyte)));
	}

	/**
	 * readFloat
	 * @return
	 * @throws java.io.IOException
	 */
	public float readFloat() throws IOException {
		return this.readFloat(this._offset);
	}

	/**
	 * readFloat
	 * @param offset
	 * @return
	 * @throws java.io.IOException
	 */
	public float readFloat(long offset) throws IOException {
		byte[] abyte = this.readBuffer(offset, 4);

		return ByteBuffer.wrap(abyte).order(ByteOrder.LITTLE_ENDIAN).getFloat();
	}

	/**
	 * isEOS
	 * is end of stream
	 * @return
	 */
	public boolean isEOS() {
		if( this._offset >= this._size ) {
			return true;
		}

		return false;
	}

	/**
	 * close
	 * @throws IOException
	 */
	public void close() throws IOException {
		this._in.close();
	}

	/**
	 * dump
	 * @return
	 */
	public String dump() {
		long tmpskip = this._in.skip(0);
		String dump = "";

		try {
			dump = HexDump.dumpStr(this.readBuffer(this._size));
		}
		catch( UnsupportedEncodingException ex ) {
			Logger.getLogger(ReaderStream.class.getName()).log(Level.SEVERE, null, ex);
		}
		catch( IOException ex ) {
			Logger.getLogger(ReaderStream.class.getName()).log(Level.SEVERE, null, ex);
		}

		this._in.skip(tmpskip);

		return dump;
	}
}