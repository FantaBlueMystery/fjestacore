/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.stream;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * SocketInputStream
 * @author FantaBlueMystery
 */
public class SocketInputStream extends InputStream {

	/**
	 * input stream
	 */
	protected InputStream _is = null;

	/**
	 * SocketInputStream
	 * @param socket
	 * @throws java.io.IOException
	 */
	public SocketInputStream(Socket socket) throws IOException {
		this._is = socket.getInputStream();
	}

	/**
	 * SocketInputStream
	 * @param is
	 */
	public SocketInputStream(InputStream is) {
		this._is = is;
	}

	/**
	 * getInputStream
	 * @return
	 */
	public InputStream getInputStream() {
		return this._is;
	}

	/**
	 * read
	 * @return
	 * @throws IOException
	 */
	@Override
	public int read() throws IOException {
		return this._is.read();
	}

	/**
	 * skip
	 * @param n
	 * @return
	 * @throws IOException
	 */
	@Override
	public long skip(long n) throws IOException {
		return this._is.skip(n);
	}

	/**
	 * available
	 * @return
	 * @throws IOException
	 */
	@Override
	public int available() throws IOException {
		return this._is.available();
	}

	/**
	 * close
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException {
		this._is.close();
	}

	/**
	 * mark
	 * @param readlimit
	 */
	@Override
	public synchronized void mark(int readlimit) {
		this._is.mark(readlimit);
	}

	/**
	 * reset
	 * @throws IOException
	 */
	@Override
	public synchronized void reset() throws IOException {
		this._is.reset();
	}

	/**
	 * markSupported
	 * @return
	 */
	@Override
	public boolean markSupported() {
		return this._is.markSupported();
	}
}