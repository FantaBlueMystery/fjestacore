/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.stream;

import com.fjestacore.core.convert.ConvertBytes;
import com.fjestacore.core.type.ULong;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * WriterStream
 * @author FantaBlueMystery
 */
public class WriterStream {

	/**
	 * offset
	 */
	protected int _offset = 0;

	/**
	 * buffer
	 */
	protected byte[] _buffer = null;

	/**
	 * WriterStream
	 * @param size
	 */
	public WriterStream(int size) {
		this._buffer = new byte[size];
	}

	/**
	 * WriterStream
	 * @param size
	 */
	public WriterStream(long size) {
		this._buffer = new byte[(int)size];
	}

	/**
	 * WriterStream
	 * @param buffer
	 */
	public WriterStream(byte[] buffer) {
		this._buffer = buffer;
		this._offset = this._buffer.length;
	}

	/**
	 * WriterStream
	 * @param reader
	 * @throws IOException
	 */
	public WriterStream(ReaderStream reader) throws IOException {
		reader.setOffset(0);
		this._buffer = reader.readBuffer();
		this._offset = this._buffer.length;
	}

	/**
	 * WriterStream
	 * @param reader
	 * @param offset
	 * @throws IOException
	 */
	public WriterStream(ReaderStream reader, long offset) throws IOException {
		reader.setOffset(offset);
		this._buffer = reader.readBuffer();
		this._offset = this._buffer.length;
	}

	/**
	 * getOffset
	 * @return
	 */
	public int getOffset() {
		return this._offset;
	}

	/**
	 * writeBuffer
	 * @param buffer
	 * @throws IOException
	 */
	public void writeBuffer(byte[] buffer) throws IOException {
		this.writeBuffer(buffer, this._offset);
	}

	/**
	 * writeBuffer
	 * @param buffer
	 * @param offset
	 * @throws IOException
	 */
	public void writeBuffer(byte[] buffer, int offset) throws IOException {
		int afteroffset = (offset + buffer.length);

		if( afteroffset > this._buffer.length ) {
			throw new IOException("out of buffer size!");
		}

		ByteArrayOutputStream ou = new ByteArrayOutputStream(this._buffer.length);

		// ---------------------------------------------------------------------

		int pos = 0;

		if( offset > 0 ) {
			int prevlen = offset;
			byte[] prevbyte = new byte[prevlen];

			System.arraycopy(this._buffer, 0, prevbyte, 0, offset);

			ou.write(prevbyte, 0, prevlen);

			pos += prevlen;
		}

		// ---------------------------------------------------------------------

		ou.write(buffer, 0, buffer.length);

		pos += buffer.length;

		// ---------------------------------------------------------------------

		if( afteroffset < this._buffer.length ) {
			int afterlen	= this._buffer.length - afteroffset;
			byte[] afterbyte = new byte[afterlen];
			int index = 0;

			for( int i=pos; i<this._buffer.length; i++ ) {
				afterbyte[index] = this._buffer[i];
				index++;
			}

			ou.write(afterbyte, 0, afterlen);
		}

		// ---------------------------------------------------------------------

		this._buffer = ou.toByteArray();
		this._offset = this._offset + buffer.length;
	}

	/**
	 * writeByte
	 * @param abyte
	 * @throws IOException
	 */
	public void writeByte(byte abyte) throws IOException {
		this.writeByte(abyte, this._offset);
	}

	/**
	 * writeByte
	 * @param abyte
	 * @param offset
	 * @throws IOException
	 */
	public void writeByte(byte abyte, int offset) throws IOException {
		byte[] _bytes = new byte[1];

		_bytes[0] = abyte;

		this.writeBuffer(_bytes, offset);
	}

	/**
	 * writeChars
	 * @param buffer
	 * @throws IOException
	 */
	public void writeChars(char[] buffer) throws IOException {
		this.writeChars(buffer, this._offset);
	}

	/**
	 * writeChars
	 * @param buffer
	 * @param offset
	 * @throws IOException
	 */
	public void writeChars(char[] buffer, int offset) throws IOException {
		byte[] _bytes = new byte[buffer.length];

		for( int i=0; i<buffer.length; i++ ) {
			_bytes[i] = (byte) buffer[i];
		}

		this.writeBuffer(_bytes, offset);
	}

	/**
	 * writeString
	 * @param str
	 * @throws IOException
	 */
	public void writeString(String str) throws IOException {
		this.writeString(str, str.length());
	}

	/**
	 * _writeString
	 * @param str
	 * @param maxlen
	 * @throws java.io.IOException
	 */
	public void writeString(String str, int maxlen) throws IOException {
		this.writeString(str, this._offset, maxlen);
	}

	/**
	 * _writeString
	 * @param str
	 * @param offset
	 * @param maxlen
	 * @throws java.io.IOException
	 */
	public void writeString(String str, int offset, int maxlen) throws IOException {
		this.writeString(str, offset, maxlen, true);
	}

	/**
	 * _writeString
	 * @param str
	 * @param maxlen
	 * @param fillchar
	 * @throws java.io.IOException
	 */
	public void writeString(String str, int maxlen, boolean fillchar) throws IOException {
		this.writeString(str, this._offset, maxlen, fillchar);
	}

	/**
	 * _writeString
	 * @param str
	 * @param offset
	 * @param maxlen
	 * @param fillchar
	 * @throws java.io.IOException
	 */
	public void writeString(String str, int offset, int maxlen, boolean fillchar) throws IOException {
		str = str.trim();

		if( str.length() > maxlen ) {
			str = str.substring(0, maxlen);
		}

		byte[] tstr = str.getBytes();
		byte[] buffer = new byte[maxlen];

		for( int i=0; i<maxlen; i++ ) {
			if( i < tstr.length ) {
				buffer[i] = tstr[i];
			}
			else {
				if( fillchar ) {
					buffer[i] = 0x00;
				}
			}
		}

		this.writeBuffer(buffer, offset);
	}

	/**
	 * writeBoolean
	 * @param value
	 * @throws IOException
	 */
	public void writeBoolean(boolean value) throws IOException {
		this.writeBoolean(value, this._offset);
	}

	/**
	 * writeBoolean
	 * @param value
	 * @param offset
	 * @throws IOException
	 */
	public void writeBoolean(boolean value, int offset) throws IOException {
		byte[] abyte = new byte[] {0x00};

		if( value ) {
			abyte[0] = 0x01;
		}

		this.writeBuffer(abyte, offset);
	}

	/**
	 * writeByteInt
	 * @param value
	 * @throws IOException
	 */
	public void writeByteInt(int value) throws IOException {
		this.writeByteInt(value, this._offset);
	}

	/**
	 * writeByteInt
	 * @param value
	 * @param offset
	 * @throws IOException
	 */
	public void writeByteInt(int value, int offset) throws IOException {
		Integer tvalue = value;
		byte[] abyte = new byte[1];
		abyte[0] = tvalue.byteValue();

		this.writeBuffer(abyte, offset);
	}

	/**
	 * writeShort
	 * @param value
	 * @throws IOException
	 */
	public void writeShort(int value) throws IOException {
		this.writeShort(value, this._offset);
	}

	/**
	 * writeShort
	 * @param value
	 * @param offset
	 * @throws IOException
	 */
	public void writeShort(int value, int offset) throws IOException {
		this.writeBuffer(ConvertBytes.shortToBytes(value), offset);
	}

	/**
	 * writeUInt
	 * @param value
	 * @throws IOException
	 */
	public void writeUInt(long value) throws IOException {
		this.writeUInt(value, this._offset);
	}

	/**
	 * writeUInt
	 * @param value
	 * @param offset
	 * @throws IOException
	 */
	public void writeUInt(long value, int offset) throws IOException {
		this.writeBuffer(ConvertBytes.uIntToBytes(value, false), offset);
	}

	/**
	 * writeULong
	 * @param value
	 * @throws IOException
	 */
	public void writeULong(ULong value) throws IOException {
		this.writeULong(value, this._offset);
	}

	/**
	 * writeULong
	 * @param value
	 * @param offset
	 * @throws IOException
	 */
	public void writeULong(ULong value, int offset) throws IOException {
		BigInteger tmp = value.toBigInteger();
		byte[] bvalues = ConvertBytes.reverse(tmp.toByteArray());

		this.writeBuffer(Arrays.copyOf(bvalues, 8), offset);
	}

	/**
	 * writeFloat
	 * @param value
	 * @throws IOException
	 */
	public void writeFloat(float value) throws IOException {
		this.writeFloat(value, this._offset);
	}

	/**
	 * writeFloat
	 * @param value
	 * @param offset
	 * @throws IOException
	 */
	public void writeFloat(float value, int offset) throws IOException {
		byte[] bfloat = ConvertBytes.floatToByteArray(value);

		this.writeBuffer(Arrays.copyOf(bfloat, 4), offset);
	}

	/**
	 * writeInt
	 * @param value
	 * @throws IOException
	 */
	public void writeInt(int value) throws IOException {
		this.writeInt(value, this._offset);
	}

	/**
	 * writeInt
	 * @param value
	 * @param offset
	 * @throws IOException
	 */
	public void writeInt(int value, int offset) throws IOException {
		byte[] bint = ConvertBytes.intToBytes(value);

		this.writeBuffer(Arrays.copyOf(bint, 4), offset);
	}

	/**
	 * appendBuffer
	 * @param buffer
	 */
	public void appendBuffer(byte[] buffer) {
		byte[] newbuffer = new byte[this._buffer.length + buffer.length];

		System.arraycopy(this._buffer, 0, newbuffer, 0, this._buffer.length);
		System.arraycopy(buffer, 0, newbuffer, this._buffer.length, buffer.length);

		this._buffer = newbuffer;
		this._offset = this._buffer.length;
	}

	/**
	 * getStream
	 * @return
	 */
	public ByteArrayOutputStream getStream() {
		ByteArrayOutputStream out = new ByteArrayOutputStream(this._buffer.length);
		out.write(this._buffer, 0, this._buffer.length);

		return out;
	}

	/**
	 * getBuffer
	 * @return
	 */
	public byte[] getBuffer() {
		return this._buffer;
	}
}