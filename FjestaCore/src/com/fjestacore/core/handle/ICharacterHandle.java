/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.handle;

/**
 * ICharacterHandle
 * @author FantaBlueMystery
 */
public interface ICharacterHandle extends IFjestaHandle {

	/**
	 * getCharacterHandle
	 * @return
	 */
	public long getCharacterHandle();

	/**
	 * setCharacterHandle
	 * @param handle
	 */
	public void setCharacterHandle(long handle);
}
