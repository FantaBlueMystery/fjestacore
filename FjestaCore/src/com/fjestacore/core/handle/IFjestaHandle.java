/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.handle;

/**
 * IFjestaHandle
 * Fjesta-Handle is an abstract reference to a resource in Fjesta
 * @author FantaBlueMystery
 */
public interface IFjestaHandle {}