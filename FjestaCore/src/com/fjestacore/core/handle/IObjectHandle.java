/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.handle;

/**
 * IObjectHandle
 * @author FantaBlueMystery
 */
public interface IObjectHandle extends IFjestaHandle {

	/**
	 * getObjectHandle
	 * get handle id of object
	 * @return
	 */
	public int getObjectHandle();

	/**
	 * setObjectHandle
	 * set handle id of object
	 * @param handle
	 */
	public void setObjectHandle(int handle);
}