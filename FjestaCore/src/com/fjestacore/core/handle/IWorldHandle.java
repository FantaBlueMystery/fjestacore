/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.core.handle;

/**
 * IWorldHandle
 * this is a world handle, first used in NC_USER_LOGINWORLD_ACK
 * the world handle is reference to client of world server
 * see by object UserWorldLogin
 * @author FantaBlueMystery
 */
public interface IWorldHandle extends IFjestaHandle {

	/**
	 * getWorldHandle
	 * return handle id of world
	 * @return
	 */
	public int getWorldHandle();

	/**
	 * setWorldHandle
	 * set handle id of world
	 * @param handle
	 */
	public void setWorldHandle(int handle);
}