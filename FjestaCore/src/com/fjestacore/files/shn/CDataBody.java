/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.shn;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * CDataBody
 * @author FantaBlueMystery
 */
public class CDataBody {

	/**
	 * rows
	 */
	protected HashMap<Object, CDataData> _rows = new HashMap<>();

	/**
	 * constructor
	 */
	public CDataBody() {}

	/**
	 * getRows
	 * @return
	 */
	public HashMap<Object, CDataData> getRows() {
		return this._rows;
	}

	/**
	 * getRowByIndex
	 * @param index
	 * @return
	 */
	public CDataData getRowByIndex(Object index) {
		return this._rows.get(index);
	}

	/**
	 * getRowSize
	 * @return
	 */
	public int getRowSize() {
		return this._rows.size();
	}

	/**
	 * read
	 * @param reader
	 * @param head
	 * @throws IOException
	 */
	public void read(ReaderStream reader, CDataHead head) throws IOException {
		long numOfRecord	= head.getNumOfRecord();
		int fieldSize		= (int) head.getFieldSize();

		for( int i=0; i<numOfRecord; i++ ) {
			int rowLength = reader.readShort();

			ReaderStream rowStream = new ReaderStream(reader.readBuffer(rowLength-2));

			CDataData data = new CDataData();
			data.read(rowStream, head.getFields(), fieldSize);

			if( !rowStream.isEOS() ) {
				throw new IOException("destroyed row stream");
			}

			this._rows.put(data.getData(0), data);
		}
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	public void write(WriterStream writer, CDataHead head) throws IOException {

	}
}