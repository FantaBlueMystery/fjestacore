/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.shn;

import com.fjestacore.common.name.Name48;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CDataField
 * @author FantaBlueMystery
 */
public class CDataField extends AbstractStruct {

	/**
	 * name
	 */
	protected Name48 _name = new Name48();

	/**
	 * type
	 */
	protected CDataTypeList _type = CDataTypeList.TYPE_LIST_UNKNOWNED;

	/**
	 * size
	 */
	protected long _size = 0;

	/**
	 * getName
	 * @return
	 */
	public String getName() {
		return this._name.getContent();
	}

	/**
	 * getType
	 * @return
	 */
	public CDataTypeList getType() {
		return this._type;
	}

	/**
	 * getFieldSize
	 * @return
	 */
	public long getFieldSize() {
		return this._size;
	}

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		return this._name.getSize() + 8;
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._name.read(reader);
		this._type = CDataTypeList.valueOf((int)reader.readUInt());
		this._size = reader.readUInt();
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	@Override
	public void write(WriterStream writer) throws IOException {
		this._name.write(writer);
		writer.writeUInt(this._type.getValue());
		writer.writeUInt(this._size);
	}
}