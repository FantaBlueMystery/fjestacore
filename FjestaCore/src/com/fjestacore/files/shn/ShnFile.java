/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.shn;

import com.fjestacore.core.stream.ReaderStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * ShnFile
 * @author FantaBlueMystery
 */
public class ShnFile {

	/**
	 * head
	 */
	protected CDataHead _head = null;

	/**
	 * body
	 */
	protected CDataBody _body = null;

	/**
	 * ShnFile
	 * @param file
	 */
	public ShnFile(String file) throws IOException {
		this._read(new ReaderStream(new FileInputStream(new File(file))));
	}

	/**
	 * ShnFile
	 * @param file
	 * @throws IOException
	 */
	public ShnFile(File file) throws IOException {
		this._read(new ReaderStream(new FileInputStream(file)));
	}

	/**
	 * ShnFile
	 * @param stream
	 * @throws IOException
	 */
	public ShnFile(ReaderStream stream) throws IOException {
		this._read(stream);
	}

	/**
	 * ShnFile
	 */
	public ShnFile() {
		this._head	= new CDataHead();
		this._body	= new CDataBody();
	}

	/**
	 * _read
	 * @param stream
	 * @throws IOException
	 */
	protected final void _read(ReaderStream stream) throws IOException {
		this._head = new CDataHead();
		stream = this._head.read(stream);

		this._body = new CDataBody();
		this._body.read(stream, this._head);
	}

	/**
	 * getHead
	 * @return
	 */
	public CDataHead getHead() {
		return this._head;
	}

	/**
	 * getBody
	 * @return
	 */
	public CDataBody getBody() {
		return this._body;
	}
}