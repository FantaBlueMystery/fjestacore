/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.shn;

/**
 * CDataTypeList
 * @author FantaBlueMystery
 */
public enum CDataTypeList {
	TYPE_LIST_END(0x0),					// 0
	TYPE_LIST_BYTE(0x1),				// 1
	TYPE_LIST_WORD(0x2),				// 2
	TYPE_LIST_DWORD(0x3),				// 3
	TYPE_LIST_QWORD(0x4),				// 4
	TYPE_LIST_FLOAT(0x5),				// 5
	TYPE_LIST_FILENAME(0x6),			// 6
	TYPE_LIST_FILEAUTO(0x7),			// 7
	TYPE_LIST_REMARK(0x8),				// 8
	TYPE_LIST_STR(0x9),					// 9
	TYPE_LIST_STRAUTO(0xA),				// 10
	TYPE_LIST_INX(0xB),					// 11
	TYPE_LIST_INXBYTE(0xC),				// 12
	TYPE_LIST_INXWORD(0xD),				// 13
	TYPE_LIST_INXDWORD(0xE),			// 14
	TYPE_LIST_INXQWORD(0xF),			// 15
	TYPE_LIST_BYTE_BIT(0x10),			// 16
	TYPE_LIST_WORD_BIT(0x11),			// 17
	TYPE_LIST_DWORD_BIT(0x12),			// 18
	TYPE_LIST_QWORD_BIT(0x13),			// 19
	TYPE_LIST_BYTE_ARRAY(0x14),			// 20
	TYPE_LIST_WORD_ARRAY(0x15),			// 21
	TYPE_LIST_DWORD_ARRAY(0x16),		// 22
	TYPE_LIST_QWORD_ARRAY(0x17),		// 23
	TYPE_LIST_STR_ARRAY(0x18),			// 24
	TYPE_LIST_STRAUTO_ARRAY(0x19),		// 25
	TYPE_LIST_VARSTR(0x1A),				// 26
	TYPE_LIST_INXSTR(0x1B),				// 27
	TYPE_LIST_UNKNOWNED(0x1C),			// 28
	TYPE_LIST_TWO_INX(0x1D)				// 29
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CDataReaderTypeList
	 * @param value
	 */
	private CDataTypeList(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * valueOf
	 * @param value
	 * @return
	 */
	static public CDataTypeList valueOf(int value) {
		CDataTypeList[] values = CDataTypeList.values();

		for( CDataTypeList tvalue: values ) {
			if( tvalue.getValue() == value ) {
				return tvalue;
			}
		}

		return CDataTypeList.TYPE_LIST_UNKNOWNED;
	}
}