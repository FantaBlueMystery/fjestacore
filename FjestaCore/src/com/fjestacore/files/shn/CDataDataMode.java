/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.shn;

/**
 * CDataDataMode
 * @author FantaBlueMystery
 */
public enum CDataDataMode {
	DATA_MODE_NORMAL(0),
	DATA_MODE_ENCRYPTION(1)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CDataReaderDataMode
	 * @param value
	 */
	private CDataDataMode(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}

	/**
	 * valueOf
	 * @param value
	 * @return
	 */
	static public CDataDataMode valueOf(int value) {
		CDataDataMode[] values = CDataDataMode.values();

		for( CDataDataMode tvalue: values ) {
			if( tvalue.getValue() == value ) {
				return tvalue;
			}
		}

		return CDataDataMode.DATA_MODE_NORMAL;
	}
}