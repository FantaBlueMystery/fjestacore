/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.shn;

import com.fjestacore.common.name.Name5;
import com.fjestacore.core.crypt.FileCryptProvider;
import com.fjestacore.core.crypt.IFileCrypt;
import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.utils.Md5Hash;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CDataHead
 * @author FantaBlueMystery
 */
public class CDataHead {

	/**
	 * n version key
	 */
	protected long _nVersionKey = 0;

	/**
	 * s version
	 */
	protected Name5 _sVersion = new Name5();

	/**
	 * reserved
	 */
	protected long _nReserved = 0;

	/**
	 * n data mode
	 */
	protected CDataDataMode _nDataMode = CDataDataMode.DATA_MODE_NORMAL;

	/**
	 * file size
	 */
	protected long _nFileSize = 0;

	/**
	 * data size
	 */
	protected long _nDataSize = 0;

	/**
	 * num of record
	 */
	protected long _nNumOfRecord = 0;

	/**
	 * field size
	 */
	protected long _nFieldSize = 0;

	/**
	 * num of field
	 */
	protected long _nNumOfField = 0;

	/**
	 * fields
	 */
	protected ArrayList<CDataField> _fields = new ArrayList();

	/**
	 * check sum
	 * is temp and not read or write, it generate by memory
	 */
	protected String _checkSum = "";

	/**
	 * getSize
	 * @return
	 */
	public int getSize() {
		return 32 + this._sVersion.getSize();
	}

	/**
	 * getFields
	 * @return
	 */
	public CDataField[] getFields() {
		return this._fields.toArray(new CDataField[]{});
	}

	/**
	 * getNumOfRecord
	 * @return
	 */
	public long getNumOfRecord() {
		return this._nNumOfRecord;
	}

	/**
	 * setNumOfRecord
	 * @param nNumOfRecord
	 */
	public void setNumOfRecord(long nNumOfRecord) {
		this._nNumOfRecord = nNumOfRecord;
	}

	/**
	 * getFieldSize
	 * @return
	 */
	public long getFieldSize() {
		return this._nFieldSize;
	}

	/**
	 * _setHashByByteArray
	 * @param bdata
	 */
	protected void _setHashByByteArray(byte[] bdata) {
		WriterStream stream = new WriterStream(this._nFileSize);

		try {
			stream.writeUInt(this._nVersionKey);
			this._sVersion.write(stream);
			stream.writeUInt(this._nReserved);
			stream.writeUInt(this._nDataMode.getValue());
			stream.writeUInt(this._nFileSize);
			stream.writeBuffer(bdata);

			this._checkSum = Md5Hash.getHash(stream.getBuffer());
		}
		catch( IOException ex ) {
			Logger.getLogger(CDataHead.class.getName()).log(Level.SEVERE, null, ex);
			this._checkSum = "";
		}
	}

	/**
	 * read
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	public ReaderStream read(ReaderStream reader) throws IOException {
		this._nVersionKey = reader.readUInt();
		this._sVersion.read(reader);
		this._nReserved	= reader.readUInt();
		this._nDataMode = CDataDataMode.valueOf((int)reader.readUInt());
		this._nFileSize = reader.readUInt();

		byte[] bdata = reader.readBuffer();

		reader.close();

		if( this._nDataMode == CDataDataMode.DATA_MODE_ENCRYPTION ) {
			try {
				IFileCrypt crypt = FileCryptProvider.getCrypt();

				crypt.crypt(bdata, 0, bdata.length);
			}
			catch( Exception ex ) {
				Logger.getLogger(CDataHead.class.getName()).log(Level.SEVERE, null, ex);
				return null;
			}
		}

		// generate hash
		this._setHashByByteArray(bdata);

		// read fields
		reader = new ReaderStream(bdata);

		this._nDataSize		= reader.readUInt();
		this._nNumOfRecord	= reader.readUInt();
		this._nFieldSize	= reader.readUInt();
		this._nNumOfField	= reader.readUInt();

		for( int i=0; i<this._nNumOfField; i++ ) {
			CDataField field = new CDataField();
			field.read(reader);

			this._fields.add(field);
		}

		return reader;
	}

	/**
	 * write
	 * @param writer
	 * @throws IOException
	 */
	public void write(WriterStream writer) throws IOException {

	}

	/**
	 * getRowSize
	 * @return
	 */
	public long getRowSize() {
		long size = 0;

		for( CDataField field: this._fields ) {
			size += field.getFieldSize();
		}

		return size;
	}
}