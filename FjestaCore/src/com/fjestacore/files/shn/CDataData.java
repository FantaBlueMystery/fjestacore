/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.shn;

import com.fjestacore.core.stream.ReaderStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * CDataData
 * @author FantaBlueMystery
 */
public class CDataData {

	/**
	 * data
	 */
	protected ArrayList<Object> _data = new ArrayList();

	/**
	 * getData
	 * @param index
	 * @return
	 */
	public Object getData(int index) {
		return this._data.get(index);
	}

	/**
	 * read
	 * @param reader
	 * @param fields
	 * @param defaultFieldSize
	 * @throws java.io.IOException
	 */
	public void read(ReaderStream reader, CDataField[] fields, int defaultFieldSize) throws IOException {
		for( CDataField field: fields ) {
			int fieldsize	= (int) field.getFieldSize();
			Object data		= null;

			switch( field.getType() ) {
				case TYPE_LIST_BYTE:
				case TYPE_LIST_INXBYTE:
				case TYPE_LIST_BYTE_BIT:
					data = reader.readByte();
					break;

				case TYPE_LIST_WORD:
					data = reader.readShort();
					break;

				case TYPE_LIST_DWORD:
				case TYPE_LIST_INX:
				case TYPE_LIST_DWORD_BIT:
				case TYPE_LIST_INXSTR:
					data = reader.readUInt();
					break;

				case TYPE_LIST_FLOAT:
					data = reader.readFloat();
					break;

				case TYPE_LIST_STR:
				case TYPE_LIST_STRAUTO:
				case TYPE_LIST_STR_ARRAY:
					data = reader.readString(fieldsize);
					break;

				case TYPE_LIST_INXWORD:
				case TYPE_LIST_WORD_ARRAY:
					data = reader.readShort();
					break;

				case TYPE_LIST_BYTE_ARRAY:
					data = reader.readByte();
					break;

				case TYPE_LIST_DWORD_ARRAY:
					data = reader.readInt();
					break;

				case TYPE_LIST_VARSTR:
					data = reader.readString(fieldsize - defaultFieldSize + 1);
					break;

				case TYPE_LIST_TWO_INX:
					data = reader.readULong();
					break;

				default:
					data = reader.readBuffer(fieldsize);
			}

			this._data.add(data);
		}
	}

	/**
	 * cast
	 * @param aclass
	 * @return
	 */
	public CDataData cast(Class aclass) {
		try {
			Object aobj = aclass.newInstance();

			if( aobj instanceof CDataData ) {
				CDataData data = (CDataData)aobj;
				data._data = this._data;

				return data;
			}
		}
		catch( InstantiationException | IllegalAccessException ex ) {
			return null;
		}

		return null;
	}
}