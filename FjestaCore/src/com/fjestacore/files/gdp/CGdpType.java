/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.gdp;

/**
 * CGdpType
 * @author FantaBlueMystery
 */
public enum CGdpType {
	GDP_TYPE_MAIN(0x1A504447),
	GDP_TYPE_LINK(0x1),
	GDP_TYPE_FILE(0x2),
	GDP_TYPE_DELETED(0x3),
	GDP_TYPE_EMPTY(0x4),
	GDP_TYPE_END(0x5)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CGdpType
	 * @param value
	 */
	private CGdpType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}