/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.gdp;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;
import java.util.ArrayList;

/**
 * CGdpChunkList
 * @author FantaBlueMystery
 */
public class CGdpChunkList extends AbstractStruct {

	/**
	 * chuck fpos
	 */
	protected long _chunkFPos = 0;

	/**
	 * chunk fpos high
	 */
	protected long _chunkFPosHigh = 0;

	/**
	 * chunks
	 */
	protected ArrayList<CGdpChunk> _chunks = new ArrayList();

	/**
	 * getSize
	 * @return
	 */
	@Override
	public int getSize() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	/**
	 * read
	 * @param reader
	 * @throws IOException
	 */
	@Override
	public void read(ReaderStream reader) throws IOException {
		this._chunkFPos = reader.readUInt();
	}

	@Override
	public void write(WriterStream writer) throws IOException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}


}
