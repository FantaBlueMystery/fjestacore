/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.gdp;

/**
 * CGdpTypeDir
 * @author FantaBlueMystery
 */
public enum CGdpTypeDir {
	TYPE_MAIN(0),
	TYPE_LINK(1),
	TYPE_DIR(2)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CGdpTypeDir
	 * @param value
	 */
	private CGdpTypeDir(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}
