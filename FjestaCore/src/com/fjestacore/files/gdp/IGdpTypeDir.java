/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.gdp;

/**
 * IGdpTypeDir
 * @author FantaBlueMystery
 */
public interface IGdpTypeDir {

	/**
	 * getTypeDir
	 * @return
	 */
	public CGdpTypeDir getTypeDir();
}