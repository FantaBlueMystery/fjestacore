/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.gdp;

/**
 * CGdpStoreType
 * @author FantaBlueMystery
 */
public enum CGdpStoreType {
	GDP_STORE_COPY(0x0),
	GDP_STORE_DIB(0x1),
	GDP_STORE_ZIP(0x2)
	;

	/**
	 * value
	 */
	private int _value;

	/**
	 * CGdpStoreType
	 * @param value
	 */
	private CGdpStoreType(int value) {
		this._value = value;
	}

	/**
	 * getValue
	 * @return
	 */
	public int getValue() {
		return this._value;
	}

	/**
	 * setValue
	 * @param value
	 */
	protected void setValue(int value) {
		this._value = value;
	}
}