/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.gdp;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CGdpDir
 * @author FantaBlueMystery
 */
public class CGdpDir extends AbstractStruct implements IGdpTypeDir {

	/**
	 * name
	 */
	protected String _name = "";

	/**
	 * store type
	 */
	protected CGdpStoreType _storeType = CGdpStoreType.GDP_STORE_COPY;

	

	/**
	 * getTypeDir
	 * @return
	 */
	@Override
	public CGdpTypeDir getTypeDir() {
		return CGdpTypeDir.TYPE_DIR;
	}

	@Override
	public int getSize() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void read(ReaderStream reader) throws IOException {
		this._name = reader.readString(260);

	}

	@Override
	public void write(WriterStream writer) throws IOException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
