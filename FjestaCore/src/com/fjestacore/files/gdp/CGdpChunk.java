/**
 * FjestaCore - API
 *
 * @author FantaBlueMystery
 * @copyright 2018 by FantaBlueMystery
 * @license http://opensource.org/licenses/lgpl-license.php LGPL - GNU Lesser General Public License
 */
package com.fjestacore.files.gdp;

import com.fjestacore.core.stream.ReaderStream;
import com.fjestacore.core.stream.WriterStream;
import com.fjestacore.core.struct.AbstractStruct;
import java.io.IOException;

/**
 * CGdpChunk
 * @author FantaBlueMystery
 */
public class CGdpChunk extends AbstractStruct {

	/**
	 * type
	 */
	protected CGdpType _type = CGdpType.GDP_TYPE_EMPTY;

	/**
	 * encrypt
	 */
	protected long _encrypt = 0;

	/**
	 * typedir
	 */
	protected IGdpTypeDir _typedir = null;

	@Override
	public int getSize() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void read(ReaderStream reader) throws IOException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void write(WriterStream writer) throws IOException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}